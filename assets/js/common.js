jQuery(document).ready(function() {
	// POP OVER
	jQuery(".confirm").popConfirm();
	// Multi select options
	$(function() {
		$('.multiselect-ui').multiselect({
			enableFiltering: true,
            includeSelectAllOption: true,
            maxHeight: 300,
            dropUp: false,
			nonSelectedText:'Select Users to Invite '
		});
	});
	
	setTimeout(function(){
		$("#message-alert").fadeOut( "slow");
	}, 5000);
	
});

/* Change Backgroupd on  Success or Error*/
function changeBackground(trid,type){ // type = 1 success 2 error
	if(type == 1){
		var color = "#81d3b6";
	}else{
		var color = "#e58282";
	}
	var $el = $("#"+trid),
	x = 3000,
	originalColor = $el.css("background");
	$el.css("background", color);
	setTimeout(function(){
		$el.css("background", originalColor);
	}, x);
}

/* Delete Allowed email ids for sign-up */
function delete_allowed_email(tid,allowed_id){
	$.ajax( {
        type: 'post',
		url: 'Ajax/delete_allowed_email',
        data: { allowed_id: allowed_id},
        success: function ( result )
        {
			var data = jQuery.parseJSON(result);
			if ( data.status == 200 ){
				jQuery("#"+tid).animate({ backgroundColor: "#f49090" }, "fast").animate({ opacity: "hide" },  2000);
				changeBackground(tid,1);
			} else {
				alert(data.msg);
				changeBackground(tid,2);
			}
		},
        error: function ( result )
        {
            alert('Internal server error!. Please try again.');
        }
    });
}

/* Delete User */
function deleteUser(tid,user_id){
	$.ajax( {
        type: 'post',
		url: 'Ajax/deleteUser',
        data: { user_id: user_id},
        success: function ( result )
        {
			var data = jQuery.parseJSON(result);
			if ( data.status == 200 ){
				jQuery("#"+tid).animate({ backgroundColor: "#f49090" }, "fast").animate({ opacity: "hide" },  2000);
				changeBackground(tid,1);
			} else {
				alert(data.msg);
				changeBackground(tid,2);
			}
		},
        error: function ( result )
        {
            alert('Internal server error!. Please try again.');
        }
    });
}


/* Block User */
function blockUser(tid,user_id){
	$.ajax( {
        type: 'post',
		url: 'Ajax/block_user',
        data: { user_id: user_id},
        success: function ( result )
        {
			var data = jQuery.parseJSON(result);
			if ( data.status == 200 || data.status == 201 ){
				if(data.status == 200){
					$("#block"+tid).attr('data-original-title', '<b>Are you sure to unblock!</b>');
					$("#block"+tid).html('Unblock');
				}else{
					$("#block"+tid).attr('data-original-title', '<b>Are you sure to block!</b>');
					$("#block"+tid).html('Block');
				}
				changeBackground(tid,1);
			} else {
				alert(data.msg);
				changeBackground(tid,2);
			};
		},
        error: function ( result )
        {
            alert('Internal server error!. Please try again.');
        }
    });
}
/* Delete Team */
function delete_team(table_id,team_id){
	$.ajax( {
        type: 'post',
		url: 'Ajax/delete_team',
        data: { team_id: team_id},
        success: function ( result )
        {
			var data = jQuery.parseJSON(result);
			if ( data.status == 200 ){
				jQuery("#"+table_id).animate({ backgroundColor: "#f49090" }, "fast").animate({ opacity: "hide" },  2000);
			} else {
				alert(data.msg);
				var $el = $("#"+table_id),
				x = 3000,
				originalColor = $el.css("background");
				$el.css("background", "#d89868");
				setTimeout(function(){
				  $el.css("background", originalColor);
				}, x);
			};
		},
        error: function ( result )
        {
          alert('Internal server error!. Please try again.');
        }
    });
}
/* Remove Team Member */
function remove_teamMember(table_id,member_id,team_id){
	$.ajax( {
        type: 'post',
		url: '../Ajax/remove_teamMember',
        data: { member_id: member_id, team_id: team_id},
        success: function ( result )
        {
			var data = jQuery.parseJSON(result);
			if ( data.status == 200 ){
				jQuery("#"+table_id).animate({ backgroundColor: "#f49090" }, "fast").animate({ opacity: "hide" },  2000);
			} else {
				alert(data.msg);
				var $el = $("#"+table_id),
				x = 3000,
				originalColor = $el.css("background");
				$el.css("background", "#d89868");
				setTimeout(function(){
				  $el.css("background", originalColor);
				}, x);
			};
		},
        error: function (result)
        {
			alert('Internal server error!. Please try again.');
        }
    });
}
/* Remove Team Invites */
function delete_invites(table_id,invite_id,team_id){
	$.ajax( {
        type: 'post',
		url: '../Ajax/delete_invites',
        data: { invite_id: invite_id, team_id: team_id},
        success: function ( result )
        {
			var data = jQuery.parseJSON(result);
			if ( data.status == 200 ){
				jQuery("#"+table_id).animate({ backgroundColor: "#f49090" }, "fast").animate({ opacity: "hide" },  2000);
			} else {
				alert(data.msg);
				var $el = $("#"+table_id),
				x = 3000,
				originalColor = $el.css("background");
				$el.css("background", "#d89868");
				setTimeout(function(){
				  $el.css("background", originalColor);
				}, x);
			};
		},
        error: function (result)
        {
			alert('Internal server error!. Please try again.');
        }
    });
}
/* Remove Assigned Tasks */
function delete_assignedTask(table_id,todoid,team_id){
	$.ajax( {
        type: 'post',
		url: 'Ajax/delete_assignedTask',
        data: { todoid: todoid, team_id: team_id},
        success: function ( result )
        {
			var data = jQuery.parseJSON(result);
			if ( data.status == 200 ){
				jQuery("#"+table_id).animate({ backgroundColor: "#f49090" }, "fast").animate({ opacity: "hide" },  2000);
			} else {
				alert(data.msg);
				var $el = $("#"+table_id),
				x = 3000,
				originalColor = $el.css("background");
				$el.css("background", "#d89868");
				setTimeout(function(){
				  $el.css("background", originalColor);
				}, x);
			};
		},
        error: function (result)
        {
			alert('Internal server error!. Please try again.');
        }
    });
}
/* Delete News Channel */
function delete_newsChannel(table_id,channel_id){
	$.ajax( {
        type: 'post',
		url: 'Ajax/delete_newsChannel',
        data: { channel_id: channel_id},
        success: function ( result )
        {
			var data = jQuery.parseJSON(result);
			if ( data.status == 200 ){
				jQuery("#"+table_id).animate({ backgroundColor: "#f49090" }, "fast").animate({ opacity: "hide" },  2000);
			} else {
				alert(data.msg);
				var $el = $("#"+table_id),
				x = 3000,
				originalColor = $el.css("background");
				$el.css("background", "#d89868");
				setTimeout(function(){
				  $el.css("background", originalColor);
				}, x);
			};
		},
        error: function (result)
        {
			alert('Internal server error!. Please try again.');
        }
    });
}
/* Delete News Channel Follower */
function deleteChannelFollower(table_id,subscribe_id,channel_id){
	$.ajax( {
        type: 'post',
		url: '../Ajax/deleteChannelFollower',
        data: { subscribe_id: subscribe_id, channel_id: channel_id},
        success: function ( result )
        {
			var data = jQuery.parseJSON(result);
			if ( data.status == 200 ){
				jQuery("#"+table_id).animate({ backgroundColor: "#f49090" }, "fast").animate({ opacity: "hide" },  2000);
			} else {
				alert(data.msg);
				var $el = $("#"+table_id),
				x = 3000,
				originalColor = $el.css("background");
				$el.css("background", "#d89868");
				setTimeout(function(){
				  $el.css("background", originalColor);
				}, x);
			};
		},
        error: function (result)
        {
			alert('Internal server error!. Please try again.');
        }
    });
}
/* Delete News Channel News Brief */
function deleteChannelNewsBrief(table_id,newsbrief_id){
	$.ajax( {
        type: 'post',
		url: '../Ajax/deleteChannelNewsBrief',
        data: { newsbrief_id: newsbrief_id },
        success: function ( result )
        {
			var data = jQuery.parseJSON(result);
			if ( data.status == 200 ){
				jQuery("#"+table_id).animate({ backgroundColor: "#f49090" }, "fast").animate({ opacity: "hide" },  2000);
			} else {
				alert(data.msg);
				var $el = $("#"+table_id),
				x = 3000,
				originalColor = $el.css("background");
				$el.css("background", "#d89868");
				setTimeout(function(){
				  $el.css("background", originalColor);
				}, x);
			};
		},
        error: function (result)
        {
			alert('Internal server error!. Please try again.');
        }
    });
}
/* Delete ANY News Channel News Brief */
function deleteAnyChannelNewsBrief(table_id,newsbrief_id){
	$.ajax( {
        type: 'post',
		url: 'Ajax/deleteAnyChannelNewsBrief',
        data: { newsbrief_id: newsbrief_id },
        success: function ( result )
        {
			var data = jQuery.parseJSON(result);
			if ( data.status == 200 ){
				jQuery("#"+table_id).animate({ backgroundColor: "#f49090" }, "fast").animate({ opacity: "hide" },  2000);
			} else {
				alert(data.msg);
				var $el = $("#"+table_id),
				x = 3000,
				originalColor = $el.css("background");
				$el.css("background", "#d89868");
				setTimeout(function(){
				  $el.css("background", originalColor);
				}, x);
			};
		},
        error: function (result)
        {
			alert('Internal server error!. Please try again.');
        }
    });
}

/* Block Unblock Business Template */
function blockUnblockTemplate(tid,template_id){
	$.ajax( {
        type: 'post',
		url: 'Ajax/blockUnblockTemplate',
        data: { template_id: template_id},
        success: function ( result )
        {
			var data = jQuery.parseJSON(result);
			if ( data.status == 200 || data.status == 201 ){
				if(data.status == 200){
					$("#block"+tid).attr('data-original-title', '<b>Are you sure to unblock!</b>');
					$("#block"+tid).html('Unblock');
				}else{
					$("#block"+tid).attr('data-original-title', '<b>Are you sure to block!</b>');
					$("#block"+tid).html('Block');
				}
				var $el = $("#"+tid),
				x = 3000,
				originalColor = $el.css("background");
				$el.css("background", "#81d3b6");
				setTimeout(function(){
				  $el.css("background", originalColor);
				}, x);
			} else {
				alert(data.msg);
				var $el = $("#"+tid),
				x = 5000,
				originalColor = $el.css("background");
				$el.css("background", "#81d3b6");
				setTimeout(function(){
				  $el.css("background", originalColor);
				}, x);
				
			};
		},
        error: function ( result )
        {
            alert('Internal server error!. Please try again.');
        }
    });
}