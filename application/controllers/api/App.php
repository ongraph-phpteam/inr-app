<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * App Name       -    	INR-Circle
 * author         -     Hem Thakur
 * created        -     10/09/2017
**/		

class App extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->secret = $this->config->item('encryption_key');
		$this->Aes_encryption = new Aes_encryption();
	}
	/*
	*
	* Check Unique  User Per Device 
	*
	*/
	## unique_device_id =  Apple Id or Andorid Id
	public function check_uniqueDeviceUser(){
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'check_uniqueDeviceUser'));
		} else {
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if($check_auth_client == true){
				$input = $this->Aes_encryption->decryptPostCall($this->secret,$_POST);
				$response = $this->AppBackend->ht_check_uniqueDeviceUser($input);
				json_output($response['status'],$response);
			}
		}
	}
	/*
	*
	* Check Mobile Number 
	*
	*/
	public function check_mobileNumber(){
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'check_mobileNumber'));
		} else {
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if($check_auth_client == true){
				$input = $this->Aes_encryption->decryptPostCall($this->secret,$_POST);
				$response = $this->AppBackend->ht_check_mobileNumber($input);
				json_output($response['status'],$response);
			}
		}
	}
	/*
	*
	* Check sign up allowed for this email or account already exist 
	*
	*/
	public function check_signupAllowedOrAccountExist(){
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'check_signupAllowedOrAccountExist'));
		} else {
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if($check_auth_client == true){
				$input = $this->Aes_encryption->decryptPostCall($this->secret,$_POST);
				$check 	= check_required(array('email'=>@$input['email']));
				if ($check) {
					$respStatus = 400;
					$resp = array('status' => 400,'message' =>  $check.' required','method'=>'check_signupAllowedOrAccountExist');
				} else {
					$respStatus = 200;
					$resp = $this->AppBackend->ht_check_signupAllowedOrAccountExist($input);
				}
				json_output($respStatus,$resp);
			}
		}
	}	
	/*
	*
	* Check Account exist with email 
	*
	*/
	public function check_emailAccountExist(){
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'check_mobileNumber'));
		} else {
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if($check_auth_client == true){
				$input = $this->Aes_encryption->decryptPostCall($this->secret,$_POST);
				$response = $this->AppBackend->ht_check_emailAccountExist($input);
				json_output($response['status'],$response);
			}
		}
	}
	/*
	*
	* Check Password Strength 
	*
	*/
	public function check_passwordStrength(){
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'check_passwordStrength'));
		} else {
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if($check_auth_client == true){
				$input = $this->Aes_encryption->decryptPostCall($this->secret,$_POST);
				$response = $this->AppBackend->ht_check_passwordStrength($input);
				json_output($response['status'],$response);
			}
		}
	}
	/*
	*
	* Check Multiple account Configured with UDID 
	*
	*/
	public function check_mulitAccountsWithUdid(){
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'check_mulitAccountsWithUdid'));
		} else {
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if($check_auth_client == true){
				$input = $this->Aes_encryption->decryptPostCall($this->secret,$_POST);
				$response = $this->AppBackend->ht_check_mulitAccountsWithUdid($input);
				json_output($response['status'],$response);
			}
		}
	}
	/*
	*
	* Country Code 
	*
	*/
	public function get_countryCode(){
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET'){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'get_countryCode'));
		} else {
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			
			if($check_auth_client == true){
				$response = $this->AppBackend->ht_get_countryCode();
				json_output($response['status'],$response);
			}
		}
	}
	/*
	*
	* TimeZone Name 
	*
	*/
	public function get_timeZoneName(){
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET'){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'get_timeZoneName'));
		} else {
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			
			if($check_auth_client == true){
				$response = $this->AppBackend->ht_get_timeZoneName();
				json_output($response['status'],$response);
			}
		}
	}
	/*
	*
	* Remove Account from device 
	*
	*/
	public function removeAccountFromDevice(){
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'removeAccountFromDevice'));
		} else {
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if($check_auth_client == true){
				$input = $this->Aes_encryption->decryptPostCall($this->secret,$_POST);
				$response = $this->AppBackend->ht_removeAccountFromDevice($input);
				json_output($response['status'],$response);
			}
		}
	}
	/*
	*
	* Update Account DeviceId 
	*
	*/
	public function updateAccountDeviceId(){
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'updateAccountDeviceId'));
		} else {
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if($check_auth_client == true){
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input = $this->Aes_encryption->decryptPostCall($this->secret,$_POST);
					$input['user_id'] = $auth['id'];
					$response = $this->AppBackend->ht_updateAccountDeviceId($input);
					json_output($response['status'],$response);
				}
			}
		}
	}
	
	/*
	*
	* User Sign Up
	*
	*/
	public function user_signup()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'user_signup'));
		} else {
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if($check_auth_client == true){
				$input = $this->Aes_encryption->decryptPostCall($this->secret,$_POST);
				$check 	= check_required(array('user_name'=>@$input['user_name'],'email'=>@$input['email'],'device_type'=>@$input['device_type'],'device_token'=>@$input['device_token']));
				
				if ($check) {
					$respStatus = 400;
					$resp = array('status' => 400,'message' =>  $check.' required','method'=>'user_signup');
				} else {
					$respStatus = 200;
					$input['country_code'] = isset($input['country_code']) ? $input['country_code'] : '';
					$resp = $this->AppBackend->ht_user_signup($input);
				}
				json_output($respStatus,$resp);
		    }
		}
	}
	/*
	*
	* User Sign In
	*
	*/
	public function user_login()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'user_login'));
		} else {
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			
			if($check_auth_client == true){
				$input = $this->Aes_encryption->decryptPostCall($this->secret,$_POST);
				$check ="";
				if (!isset($input['udid'])){
					$check 	= check_required(array('username'=>@$input['username'],'password'=>@$input['password'],'device_type'=>@$input['device_type'],'device_token'=>@$input['device_token']));
					$input['udid'] = "";
					$input['touch_login'] = "";
				}
				if ($check){
					$response = array('status' => 400,'message' =>  $check.' can\'t empty','method'=>'user_login');
				}else{
					$response = $this->AppBackend->ht_user_login($input);
				}
				json_output($response['status'],$response);
			}
		}
	}
	/*
	*
	* Get Security Question
	*
	*/
	public function get_SecurityQuestions()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET'){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'get_SecurityQuestions'));
		} else {
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if($check_auth_client == true){
				$response = $this->AppBackend->ht_get_SecurityQuestions();
				json_output($response['status'],$response);
			}
		}
	}
	/*
	*
	* Verify email id after signup
	*
	*/
	public function verifyEmailId()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'verifyEmailId'));
		} else {
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if($check_auth_client == true){
				$input = $this->Aes_encryption->decryptPostCall($this->secret,$_POST);
				$check 	= check_required(array('verification_code'=>@$input['verification_code'],'email'=>@$input['email']));
				if($check){
					$response = array('status' => 400,'message' =>  $check.' can\'t empty','method'=>'verifyEmailId');
				}else{
					$response = $this->AppBackend->ht_verifyEmailId($input);
				}
				json_output($response['status'],$response);
				
			}
		}
	}
	/*
	*
	* Verify Code for email and device change
	*
	*/
	public function verifyCode()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'verifyCode'));
		} else {
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if($check_auth_client == true){
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input = $this->Aes_encryption->decryptPostCall($this->secret,$_POST);
					$check 	= check_required(array('section'=>@$input['section']));
					if($check){
						$response = array('status' => 400,'message' =>  $check.' can\'t empty','method'=>'verifyCode');
					}else{
						$input['step'] = @$input['step']; ## step only for device change verifiction; step = 1 before mobile number verification after that step = 2 for verifiy account
						if (($input['step'] == 1 || $input['section']==2) && $input['verification_code']==""){
							$response = array('status' => 400,'message' =>'verification_code can\'t empty','method'=>'verifyCode');
						}else{
							$input['user_id'] = $auth['id'];
							$response = $this->AppBackend->ht_verifyCode($input);
						}
					}
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Get My Security Question Answers
	*
	*/
	public function get_mySecurityQuestionAnswers()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET'){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'get_mySecurityQuestionAnswers'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if($check_auth_client == true){
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$response = $this->AppBackend->ht_get_mySecurityQuestionAnswers(array('user_id'=>$auth['id']));
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Set My Security Answers
	*
	*/
	public function set_mySecurityAnswers()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'set_mySecurityAnswers'));
		} else {
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if($check_auth_client == true){
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input = ($this->Aes_encryption->decryptPostCall($this->secret,$_POST));
					$input['data'] = json_decode($input['data'],TRUE);
					$input['user_id'] = $auth['id'];
					$response = $this->AppBackend->ht_set_mySecurityAnswers($input);
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Change Login Type
	*
	*/
	public function change_loginType()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'change_loginType'));
		} else {
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if($check_auth_client == true){
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input = $this->Aes_encryption->decryptPostCall($this->secret,$_POST);
					$input['user_id'] = $auth['id'];
					
					$response = $this->AppBackend->ht_change_loginType($input);
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Forgot password
	*
	*/
	public function forgot_password()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'forgot_password'));
		} else {
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			
			if($check_auth_client == true){
				$input 	= $this->Aes_encryption->decryptPostCall($this->secret,$_POST);
				//$check 	= check_required(array('email'=>@$input['email']));
				$check 	= check_required(array('email'=>@$input['email'],'answer'=>@$input['answer'],'question_id'=>@$input['question_id']));
				if ($check){
					$response['status'] = 400;
					$response = array('status' => 400,'message' =>  $check.' can\'t empty','method'=>'forgot_password');
				}else{
					$response = $this->AppBackend->ht_forgot_password($input);
				}
				json_output($response['status'],$response);
			}
		}
	}
	/*
	*
	* User Log Out
	*
	*/
	public function logout()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'logout'));
		} else {
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if($check_auth_client == true){
		        $response = $this->AppBackend->ht_logout();
				json_output($response['status'],$response);
			}
		}
	}
	##---> FROFILE SECTION -->
	/*
	*
	* View my Business Card
	*
	*/
	public function view_profile()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET'){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'view_profile'));
		} else {
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if($check_auth_client == true){
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
		        	$response = $this->AppBackend->ht_view_profile(array('user_id'=>$auth['id']));
					json_output($response['status'],$response);
				}
			}	
		}
	}
	/*
	*
	* Get Designations Dropdown
	*
	*/
	public function get_designationsList()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET'){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'get_designationsList'));
		} else {
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if($check_auth_client == true){
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
		        	$response = $this->AppBackend->ht_get_designationsList(array('user_id'=>$auth['id']));
					json_output($response['status'],$response);
				}
			}	
		}
	}
	/*
	*
	* Update Profile
	*
	*/
	public function update_profile()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method !="POST"){
			json_output(400,array('status'=>400,'message'=>'Bad request','method'=>'update_profile'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        
				if($auth['status'] == 200){
					$input = $this->Aes_encryption->decryptPostCall($this->secret,$_POST);
					$check = check_required(array("user_name"=>@$input['user_name']));
					
					if ($check){
						$response = array('status' => 400,'message' => $check.' can\'t empty','method'=>'update_profile');
					}else{
						$input['user_id'] = $auth['id'];
						$response = $this->AppBackend->ht_update_profile($input);
					}
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Update Rolodex Location setting
	*
	*/
	public function update_RolodexCollectionInfoSetting()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method !="POST"){
			json_output(400,array('status'=>400,'message'=>'Bad request','method'=>'update_RolodexCollectionInfoSetting'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input = $this->Aes_encryption->decryptPostCall($this->secret,$_POST);
					$input['user_id'] = $auth['id'];
					$response = $this->AppBackend->ht_update_RolodexCollectionInfoSetting($input);
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Change password
	*
	*/
	public function change_password()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method !="POST"){
			json_output(400,array('status'=>400,'message'=>'Bad request','method'=>'change_password'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        
				if($auth['status'] == 200){
					$input = $this->Aes_encryption->decryptPostCall($this->secret,$_POST);
					$check = check_required(array("oldpassword"=>@$input['oldpassword'],"newpassword"=>@$input['newpassword']));
					if ($check){
						$response = array('status' => 400,'message' => $check.' can\'t empty','method'=>'change_password');
					}else{
						$input['user_id'] = $auth['id'];
						$response = $this->AppBackend->ht_change_password($input);
					}
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Send Change Email verification Code
	*
	*/
	public function send_EmailChangeVerificationCode()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method !="POST"){
			json_output(400,array('status'=>400,'message'=>'Bad request','method'=>'send_EmailChangeVerificationCode'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input = $this->Aes_encryption->decryptPostCall($this->secret,$_POST);
					$input['user_id'] = $auth['id'];
					$response = $this->AppBackend->ht_send_EmailChangeVerificationCode($input);
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Change Email
	*
	*/
	public function change_Email()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method !="POST"){
			json_output(400,array('status'=>400,'message'=>'Bad request','method'=>'change_Email'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input = $this->Aes_encryption->decryptPostCall($this->secret,$_POST);
					$check = check_required(array("email"=>@$input['email']));
					if ($check){
						$response = array('status' => 400,'message' => $check.' can\'t empty','method'=>'change_Email');
					}else{
						$input['user_id'] = $auth['id'];
						$response = $this->AppBackend->ht_change_Email($input);
					}
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Change Mobile number
	*
	*/
	public function change_mobileNumber()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method !="POST"){
			json_output(400,array('status'=>400,'message'=>'Bad request','method'=>'change_mobileNumber'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input = $this->Aes_encryption->decryptPostCall($this->secret,$_POST);
					$check = check_required(array("new_number"=>@$input['new_number'],'new_country_code'=>@$input['new_country_code']));
					if ($check){
						$response = array('status' => 400,'message' => $check.' can\'t empty','method'=>'change_mobileNumber');
					}else{
						$input['user_id'] = $auth['id'];
						$response = $this->AppBackend->ht_change_mobileNumber($input);
					}
					json_output($response['status'],$response);
				}
			}
		}
	}
	##<--- END PROFILE SECTION || 
	
	##---> CARD SECTION -->
	/*
	*
	* Create my Card
	*
	*/
	
	public function create_businessCard()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method !="POST"){
			json_output(400,array('status'=>400,'message'=>'Bad request','method'=>'create_businessCard'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			
			if ($check_auth_client === true) {
				
				$auth = $this->AppBackend->ht_auth();
		        
				if($auth['status'] == 200){
					$input = $this->Aes_encryption->decryptPostCall($this->secret,$_POST);
					$check = check_required(array("name"=>@$input['name'],"company_name"=>@$input['company_name'],"email_address"=>@$input['email_address']));
					if ($check){
						$response = array('status' => 400,'message' =>  $check.' can\'t empty','method'=>'create_businessCard');
					}else{
						$input['user_id'] = $auth['id'];
						$response = $this->AppBackend->ht_create_businessCard($input);
					}
					json_output($response['status'],$response);
				}
			}
		}
	}
	
	/*
	*
	* View my Business Card
	*
	*/
	public function view_businessCard()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		$segment_four = $this->Aes_encryption->aesDecryption($this->secret,urldecode($this->uri->segment(4)));
		if($method != 'GET' || $segment_four == '' || is_numeric($segment_four) == FALSE){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'view_businessCard'));
		} else {
			$card_id = $segment_four;
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if($check_auth_client == true){
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
		        	$response = $this->AppBackend->ht_view_businessCard(array('user_id'=>$auth['id'],'card_id'=>$card_id));
					json_output($response['status'],$response);
				}
			}	
		}
	}
	/*
	*
	* Edit Business Card
	*
	*/
	public function edit_businessCard()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method !="POST"){
			json_output(400,array('status'=>400,'message'=>'Bad request','method'=>'edit_businessCard'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		    
				if($auth['status'] == 200){
					$input = $this->Aes_encryption->decryptPostCall($this->secret,$_POST);
					$check = check_required(array('card_id'=>@$input['card_id'],"name"=>@$input['name'],"company_name"=>@$input['company_name'],"email_address"=>@$input['email_address']));
					if ($check){
						$response = array('status' => 400,'message' =>  $check.' can\'t empty','method'=>'edit_businessCard');
					}else{
						$input['user_id'] = $auth['id'];
						$response = $this->AppBackend->ht_edit_businessCard($input);
					}
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Delet Business Card Picture
	*
	*/
	public function delete_businessCardPicture()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method !="POST"){
			json_output(400,array('status'=>400,'message'=>'Bad request','method'=>'delete_businessCardPicture'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		    
				if($auth['status'] == 200){
					$input = $this->Aes_encryption->decryptPostCall($this->secret,$_POST);
					$check = check_required(array('card_id'=>@$input['card_id']));
					if ($check){
						$response = array('status' => 400,'message' =>  $check.' can\'t empty','method'=>'delete_businessCardPicture');
					}else{
						$input['user_id'] = $auth['id'];
						$response = $this->AppBackend->ht_delete_businessCardPicture($input);
					}
					json_output($response['status'],$response);
				}
			}
		}
	}
	
	/*
	*
	* Get My All Business Card
	*
	*/
	public function get_myBusinessCards()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET'){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'get_myBusinessCards'));
		} else {
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if($check_auth_client == true){
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$segment_four = $this->Aes_encryption->aesDecryption($this->secret,urldecode($this->uri->segment(4)));
					$page	= $segment_four!="" ? $segment_four : 1; 
					$input = array("user_id"=>$auth['id'],'page'=>$page);
		        	$response = $this->AppBackend->ht_get_myBusinessCards($input);
					json_output($response['status'],$response);
				}
			}	
		}
	}
	/*
	*
	* Add business card to rolodex
	*
	*/
	public function add_cardToRolodex()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method !="POST"){
			json_output(400,array('status'=>400,'message'=>'Bad request','method'=>'add_cardToRolodex'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        
				if($auth['status'] == 200){
					$input = $this->Aes_encryption->decryptPostCall($this->secret,$_POST);
					if ($input['card_id']==""){
						$response = array('status' => 400,'message' => 'card_id can\'t empty','method'=>'add_cardToRolodex');
					}else{
						$input['user_id'] = $auth['id'];
						$response = $this->AppBackend->ht_add_cardToRolodex($input);
					}
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Forward Business card to INR-Circle Users and Contacts
	*
	*/
	public function forward_businesscard()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method !="POST"){
			json_output(400,array('status'=>400,'message'=>'Bad request','method'=>'forward_businesscard'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        
				if($auth['status'] == 200){
					$input = $this->Aes_encryption->decryptPostCall($this->secret,$_POST);
					if ($input['card_id']==""){
						$response = array('status' => 400,'message' => 'Select card to forward','method'=>'forward_businesscard');
					}else{
						$input['user_id'] = $auth['id'];
						$response = $this->AppBackend->ht_forward_businesscard($input);
					}
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Remove business card from rolodex
	*
	*/
	public function removeRolodex_BusinessCard()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		$segment_four = $this->Aes_encryption->aesDecryption($this->secret,urldecode($this->uri->segment(4)));
		if($method != 'DELETE' || $segment_four == '' || is_numeric($segment_four) == FALSE){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'removeRolodex_BusinessCard'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        
				if($auth['status'] == 200){
					$input = array('rolodex_id'=>$segment_four,'user_id'=>$auth['id']);
					$response = $this->AppBackend->ht_removeRolodex_BusinessCard($input);
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Add Note on Business card -Rolodex
	*
	*/
	
	public function addNote_BusinessCard()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method !="POST"){
			json_output(400,array('status'=>400,'message'=>'Bad request','method'=>'addNote_BusinessCard'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        
				if($auth['status'] == 200){
					$input = $this->Aes_encryption->decryptPostCall($this->secret,$_POST);
					$check = check_required(array('rolodex_id'=>@$input['rolodex_id'],'note'=>@$input['note']));
					
					if ($check){
						$response = array('status' => 400,'message' => $check.' can\'t empty','method'=>'addNote_BusinessCard');
					}else{
						$input['user_id'] = $auth['id'];
						$response = $this->AppBackend->ht_addNote_BusinessCard($input);
					}
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Update Note on Business card -Rolodex
	*
	*/
	
	public function updateNote_BusinessCard()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method !="POST"){
			json_output(400,array('status'=>400,'message'=>'Bad request','method'=>'updateNote_BusinessCard'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        
				if($auth['status'] == 200){
					$input = $this->Aes_encryption->decryptPostCall($this->secret,$_POST);
					$check = check_required(array('noteid'=>@$input['noteid'],'rolodex_id'=>@$input['rolodex_id'],'note'=>@$input['note']));
					if ($check){
						$response = array('status' => 400,'message' => $check.' can\'t empty','method'=>'updateNote_BusinessCard');
					}else{
						$input['user_id'] = $auth['id'];
						$response = $this->AppBackend->ht_updateNote_BusinessCard($input);
					}
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Remove Note on Business card -Rolodex
	 
	*/
	public function removeNote_BusinessCard()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		$segment_four = $this->Aes_encryption->aesDecryption($this->secret,urldecode($this->uri->segment(4)));
		$segment_five = $this->Aes_encryption->aesDecryption($this->secret,urldecode($this->uri->segment(5)));
		if($method != 'DELETE' || $segment_four == '' || is_numeric($segment_four) == FALSE || $segment_five == '' || is_numeric($segment_five) == FALSE){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'removeNote_BusinessCard'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        
				if($auth['status'] == 200){
					$input = array('noteid'=>$segment_four,'rolodex_id'=>$segment_five);
					$check = check_required(array('noteid'=>@$input['noteid'],'rolodex_id'=>@$input['rolodex_id']));
					if ($check){
						$response = array('status' => 400,'message' => $check.' can\'t empty','method'=>'removeNote_BusinessCard');
					}else{
						$input['user_id'] = $auth['id'];
						$response = $this->AppBackend->ht_removeNote_BusinessCard($input);
					}
					json_output($response['status'],$response);
				}
			}
		}
	}
	
	/*
	*
	* Get My Rolodex
	*
	*/
	
	public function get_myRolodex()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET'){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'get_myRolodex'));
		} else {
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if($check_auth_client == true){
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$segment_four = $this->Aes_encryption->aesDecryption($this->secret,urldecode($this->uri->segment(4)));
					$page	= $segment_four!="" ? $segment_four : 1; 
					$input = array("user_id"=>$auth['id'],'page'=>$page);
		        	$response = $this->AppBackend->ht_get_myRolodex($input);
					json_output($response['status'],$response);
				}
			}	
		}
	}
	/*
	*
	* Search My Rolodex Card
	*
	*/
	
	public function search_myRolodexCard()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'search_myRolodexCard'));
		} else {
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if($check_auth_client == true){
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input = $this->Aes_encryption->decryptPostCall($this->secret,$_POST);
					$input['user_id'] = $auth['id'];
		        	$response = $this->AppBackend->ht_search_myRolodexCard($input);
					json_output($response['status'],$response);
				}
			}	
		}
	}
	/*
	*
	* Add Pictures of Business card -Rolodex
	*
	*/
	public function addPictures_BusinessCard()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method !="POST"){
			json_output(400,array('status'=>400,'message'=>'Bad request','method'=>'addPictures_BusinessCard'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        
				if($auth['status'] == 200){
					$input = $this->Aes_encryption->decryptPostCall($this->secret,$_POST);
					$check = check_required(array('rolodex_id'=>@$input['rolodex_id'],'picture'=>@$_FILES['picture']['name'][0]));
					
					if ($check){
						$response = array('status' => 400,'message' => $check.' can\'t empty','method'=>'addPictures_BusinessCard');
					}else{
						$input['user_id'] = $auth['id'];
						$response = $this->AppBackend->ht_addPictures_BusinessCard($input);
					}
					json_output($response['status'],$response);
				}
			}
		}
	}
	
	/*
	*
	* Remove Pro Picture Or Company Logo of Business card 
	*
	*/
	public function removeProPicOrLogoBusinessCard()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		$segment_four = $this->Aes_encryption->aesDecryption($this->secret,urldecode($this->uri->segment(4)));
		$segment_five = $this->Aes_encryption->aesDecryption($this->secret,urldecode($this->uri->segment(5)));
		if($method != 'DELETE' || $segment_four == '' || is_numeric($segment_four) == FALSE || $segment_five == '' || is_numeric($segment_five) == FALSE){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'removeProPicOrLogoBusinessCard'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        
				if($auth['status'] == 200){
					$input = array('card_id'=>$segment_four,'section'=>$segment_five);
					$input['user_id'] = $auth['id'];
					$response = $this->AppBackend->ht_removeProPicOrLogoBusinessCard($input);
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Remove Picture of Business card -Rolodex Grid View 
	*
	*/
	public function removePictures_BusinessCard()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		$segment_four = $this->Aes_encryption->aesDecryption($this->secret,urldecode($this->uri->segment(4)));
		$segment_five = $this->Aes_encryption->aesDecryption($this->secret,urldecode($this->uri->segment(5)));
		if($method != 'DELETE' || $segment_four == '' || is_numeric($segment_four) == FALSE || $segment_five == '' || is_numeric($segment_five) == FALSE){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'removePictures_BusinessCard'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        
				if($auth['status'] == 200){
					$input = array('picture_id'=>$segment_four,'rolodex_id'=>$segment_five);
					$input['user_id'] = $auth['id'];
					$response = $this->AppBackend->ht_removePictures_BusinessCard($input);
				}
			}
		}
	}
	## END CARD SECTION <--
	
	
	##--> BRIF SECTION -->
	
	/*
	*
	* Create a New Team
	*
	*/
	public function create_newTeam()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method !="POST"){
			json_output(400,array('status'=>400,'message'=>'Bad request','method'=>'create_newTeam'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if ($check_auth_client === true) {
				
				$auth = $this->AppBackend->ht_auth();
		        
				if($auth['status'] == 200){
					$input = $this->Aes_encryption->decryptPostCall($this->secret,$_POST);
					$check = check_required(array("team_name"=>@$input['team_name']));
					if ($check){
						$response = array('status' => 400,'message' =>  $check.' can\'t empty','method'=>'create_newTeam');
					}else{
						$input['user_id'] = $auth['id'];
						$input['invite_userids'] = @$input['invite_userids'];
						
						$response = $this->AppBackend->ht_create_newTeam($input);
					}
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Get My Teams
	*
	*/
	public function get_myTeams()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET'){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'get_myTeams'));
		} else {
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if($check_auth_client == true){
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$segment_four = $this->Aes_encryption->aesDecryption($this->secret,urldecode($this->uri->segment(4)));
					$page	= $segment_four!="" ? $segment_four : 1; 
					$input = array("user_id"=>$auth['id'],'page'=>$page);
		        	$response = $this->AppBackend->ht_get_myTeams($input);
					json_output($response['status'],$response);
				}
			}	
		}
	}
	/*
	*
	* Get Teams Invites to me
	*
	*/
	public function get_MyInvites()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET'){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'get_MyInvites'));
		} else {
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if($check_auth_client == true){
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$response = $this->AppBackend->ht_get_MyInvites($auth['id']);
					json_output($response['status'],$response);
				}
			}	
		}
	}
	/*
	*
	* Search a Users for Team Member
	*
	*/
	public function search_usersForTeam()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method !="GET"){
			json_output(400,array('status'=>400,'message'=>'Bad request','method'=>'search_usersForTeam'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if ($check_auth_client === true) {
				
				$auth = $this->AppBackend->ht_auth();
		        
				if($auth['status'] == 200){
					$segment_four = $this->Aes_encryption->aesDecryption($this->secret,urldecode($this->uri->segment(4)));
					$check = check_required(array("keyword"=>@$segment_four));
					if ($check){
						$response = array('status' => 400,'message' =>  $check.' can\'t empty','method'=>'search_usersForTeam');
					}else{
						$segment_five = $this->Aes_encryption->aesDecryption($this->secret,urldecode($this->uri->segment(5)));
						$segment_six = $this->Aes_encryption->aesDecryption($this->secret,urldecode($this->uri->segment(6)));
						$page		= $segment_five!="" ? $segment_five : 1;
						$team_id	= $segment_six!="" ? $segment_six : 0;
						$input 		= array("user_id"=>$auth['id'],'search'=>$segment_four,'page'=>$page,'team_id'=>$team_id);
						$response 	= $this->AppBackend->ht_search_usersForTeam($input);
					}
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Send Team Join invites
	*
	*/
	public function send_teamJoinInvites()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method !="POST"){
			json_output(400,array('status'=>400,'message'=>'Bad request','method'=>'search_usersForTeam'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if ($check_auth_client === true) {
				
				$auth = $this->AppBackend->ht_auth();
		        
				if($auth['status'] == 200){
					$input = $this->Aes_encryption->decryptPostCall($this->secret,$_POST);
					$check = check_required(array("team_id"=>@$input['team_id'],'invite_userid'=>@$input['invite_userid']));
					if ($check){
						$response = array('status' => 400,'message' =>  $check.' can\'t empty','method'=>'send_teamJoinInvites');
					}else{
						$input['user_id'] = $auth['id'];
						$response = $this->AppBackend->ht_send_teamJoinInvites($input);
					}
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Accept or Reject team invites 
	*
	*/
	public function acceptOrReject_teamJoinInvite()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method !="POST"){
			json_output(400,array('status'=>400,'message'=>'Bad request','method'=>'acceptOrReject_teamJoinInvite'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input = $this->Aes_encryption->decryptPostCall($this->secret,$_POST);
					$check = check_required(array("invite_id"=>@$input['invite_id'],'action'=>@$input['action']));
					if ($check){
						$response = array('status' => 400,'message' =>  $check.' can\'t empty','method'=>'acceptOrReject_teamJoinInvite');
					}else{
						$input['user_id'] = $auth['id'];
						$response = $this->AppBackend->ht_acceptOrReject_teamJoinInvite($input);
					}
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Get Team Members 
	*
	*/
	public function getTeamMembers()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		$segment_four = $this->Aes_encryption->aesDecryption($this->secret,urldecode($this->uri->segment(4)));
		$segment_five = $this->Aes_encryption->aesDecryption($this->secret,urldecode($this->uri->segment(5)));
		$segment_six  = $this->Aes_encryption->aesDecryption($this->secret,urldecode($this->uri->segment(6)));
		if($method != 'GET' || $segment_four == '' || is_numeric($segment_four) == FALSE || $segment_five == '' || is_numeric($segment_five) == FALSE){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'getTeamMembers'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input = array('team_id'=>$segment_four,'page'=>$segment_five);
					$check = check_required(array('team_id'=>@$input['team_id']));
					if ($check){
						$response = array('status' => 400,'message' => $check.' can\'t empty','method'=>'getTeamMembers');
					}else{
						$input['user_id'] = $auth['id'];
						$input['search'] = $segment_six; 
						$response = $this->AppBackend->ht_getTeamMembers($input);
					}
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Get Team Invites and Declined Requests  
	*
	*/
	public function getTeamInvitesAndDeclinedRequests()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		$segment_four = $this->Aes_encryption->aesDecryption($this->secret,urldecode($this->uri->segment(4)));
		$segment_five = $this->Aes_encryption->aesDecryption($this->secret,urldecode($this->uri->segment(5)));
		$segment_six  = $this->Aes_encryption->aesDecryption($this->secret,urldecode($this->uri->segment(6)));
		if($method != 'GET' || $segment_four == '' || is_numeric($segment_four) == FALSE || $segment_five == '' || is_numeric($segment_five) == FALSE){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'getTeamInvitesAndDeclinedRequests'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
		
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
				if($auth['status'] == 200){
					$input = array('team_id'=>$segment_four,'page'=>$segment_five);
					$check = check_required(array('team_id'=>@$input['team_id']));
					if ($check){
						$response = array('status' => 400,'message' => $check.' can\'t empty','method'=>'getTeamInvitesAndDeclinedRequests');
					}else{
						$input['user_id'] = $auth['id'];
						$input['search'] = $segment_six; 
						$response = $this->AppBackend->ht_getTeamInvitesAndDeclinedRequests($input);
					}
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Delete Invites or Declined invites 
	*
	*/
	public function deleteSentInvitesOrDeclinedInvites()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method !="POST"){
			json_output(400,array('status'=>400,'message'=>'Bad request','method'=>'deleteSentInvitesOrDeclinedInvites'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input = $this->Aes_encryption->decryptPostCall($this->secret,$_POST);
					$check = check_required(array("invite_id"=>@$input['invite_id'],'action'=>@$input['action']));
					if ($check){
						$response = array('status' => 400,'message' =>  $check.' can\'t empty','method'=>'send_teamJoinInvites');
					}else{
						$input['user_id'] = $auth['id'];
						$response = $this->AppBackend->ht_acceptOrReject_teamJoinInvite($input);
					}
					json_output($response['status'],$response);
				}
			}
		}
	}
	
	
	
	
	/*
	*
	* Remove Team Member
	*
	*/
	public function remove_TeamMember()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		$segment_four = $this->Aes_encryption->aesDecryption($this->secret,urldecode($this->uri->segment(4)));
		$segment_five = $this->Aes_encryption->aesDecryption($this->secret,urldecode($this->uri->segment(5)));
		if($method != 'DELETE' || $segment_four == '' || is_numeric($segment_four) == FALSE || $segment_five == '' || is_numeric($segment_five) == FALSE){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'remove_TeamMember'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input['member_id'] = $segment_four;
					$input['team_id']	= $segment_five;
					$input['user_id'] 	= $auth['id'];
					$response = $this->AppBackend->ht_remove_TeamMember($input);
					json_output($response['status'],$response);
				}
			}
		}
	}
	
	/*
	*
	* Set validity of briefings
	*
	*/
	public function set_briefingValidity()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'set_briefingValidity'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input = $this->Aes_encryption->decryptPostCall($this->secret,$_POST);
					$check = check_required(array('team_id'=>@$input['team_id'],'briefing_validity'=>$input['briefing_validity']));
					if ($check){
						$response = array('status' => 400,'message' => $check.' can\'t empty','method'=>'set_briefingValidity');
					}else{
						$input['user_id'] = $auth['id'];
						$response = $this->AppBackend->ht_set_briefingValidity($input);
					}
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Appoint a new Team Leader
	*
	*/
	public function appoint_newTeamLeader()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'appoint_newTeamLeader'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input = $this->Aes_encryption->decryptPostCall($this->secret,$_POST);
					$check = check_required(array('team_id'=>@$input['team_id'],'appoint_userid'=>$input['appoint_userid']));
					if ($check){
						$response = array('status' => 400,'message' => $check.' can\'t empty','method'=>'appoint_newTeamLeader');
					}else{
						$input['user_id'] = $auth['id'];
						$response = $this->AppBackend->ht_appoint_newTeamLeader($input);
					}
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Appoint secretary
	*
	*/
	public function appoint_reject_secretary()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'appoint_reject_secretary'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input = $this->Aes_encryption->decryptPostCall($this->secret,$_POST);
					$check = check_required(array('team_id'=>@$input['team_id'],'appoint_userid'=>$input['appoint_userid']));
					if ($check){
						$response = array('status' => 400,'message' => $check.' can\'t empty','method'=>'appoint_reject_secretary');
					}else{
						$input['user_id'] = $auth['id'];
						$response = $this->AppBackend->ht_appoint_reject_secretary($input);
					}
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Assign New task 
	*
	*/
	public function assign_NewTask()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'assign_NewTask'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input = $this->Aes_encryption->decryptPostCall($this->secret,$_POST);
					$check = check_required(array('team_id'=>@$input['team_id'],'accountability'=>@$input['accountability'],'action_name'=>@$input['action_name'],'action_description'=>@$input['action_description'],'deadline'=>@$input['deadline']));
					if ($check){
						$response = array('status' => 400,'message' => $check.' can\'t empty','method'=>'assign_NewTask');
					}else{
						$input['assigned_by'] = $auth['id'];
						$response = $this->AppBackend->ht_assign_NewTask($input);
					}
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Edit Assigned task 
	*
	*/
	public function edit_assignedTask()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'edit_assignedTask'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input = $this->Aes_encryption->decryptPostCall($this->secret,$_POST);
					$check = check_required(array('todoid'=>@$input['todoid'],'team_id'=>@$input['team_id'],'accountability'=>@$input['accountability'],'action_name'=>@$input['action_name'],'action_description'=>@$input['action_description'],'deadline'=>@$input['deadline']));
					if ($check){
						$response = array('status' => 400,'message' => $check.' can\'t empty','method'=>'edit_assignedTask');
					}else{
						$input['assigned_by'] = $auth['id'];
						$input['accountable_changed'] = isset($input['accountable_changed']) ? $input['accountable_changed'] : 0;
						$response = $this->AppBackend->ht_edit_assignedTask($input);
					}
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Delete Assigned task 
	*
	*/
	public function delete_assignedTask()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		$segment_four = $this->Aes_encryption->aesDecryption($this->secret,urldecode($this->uri->segment(4)));
		if($method != 'DELETE' || $segment_four == '' || is_numeric($segment_four) == FALSE){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'delete_assignedTask'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input['user_id'] = $auth['id'];
					$input['todoid'] = $segment_four;
					$response = $this->AppBackend->ht_delete_assignedTask($input);
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Get My Todo list
	*
	*/
	public function get_myTodolist()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET'){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'get_myTodolist'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input['user_id'] = $auth['id'];
					$response = $this->AppBackend->ht_get_myTodolist($input);
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Get Refresh My Todo list
	*
	*/
	public function refresh_myTodolist()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		$segment_four = $this->Aes_encryption->aesDecryption($this->secret,urldecode($this->uri->segment(4)));
		if($method != 'GET' || $segment_four == '' || is_numeric($segment_four) == FALSE ){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'refresh_myTodolist'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input['user_id'] 	= $auth['id'];
					$input['todoid']	= $segment_four;
					$response 			= $this->AppBackend->ht_refresh_myTodolist($input);
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Get Action List
	*
	*/
	public function get_ActionList()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		$segment_four = $this->Aes_encryption->aesDecryption($this->secret,urldecode($this->uri->segment(4)));
		$segment_five = $this->Aes_encryption->aesDecryption($this->secret,urldecode($this->uri->segment(5)));
		if($method != 'GET' || $segment_four == '' || is_numeric($segment_four) == FALSE || $segment_five == '' || is_numeric($segment_five) == FALSE ){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'get_ActionList'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input['user_id']	= $auth['id'];
					$input['team_id']	= $segment_four;
					$input['page']		= $segment_five;
					$response = $this->AppBackend->ht_get_ActionList($input);
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Refresh Action List
	*
	*/
	public function refresh_ActionList()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		$segment_four = $this->Aes_encryption->aesDecryption($this->secret,urldecode($this->uri->segment(4)));
		$segment_five = $this->Aes_encryption->aesDecryption($this->secret,urldecode($this->uri->segment(5)));
		if($method != 'GET' || $segment_four == '' || is_numeric($segment_four) == FALSE || $segment_five == '' || is_numeric($segment_five) == FALSE ){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'refresh_myTodolist'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input['user_id']	= $auth['id'];
					$input['team_id']	= $segment_four;
					$input['todoid']		= $segment_five;
					$response = $this->AppBackend->ht_refresh_ActionList($input);
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Mark Action Accomplish
	*
	*/
	public function mark_ActionAccomplished()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'  ){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'mark_ActionAccomplished'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input = $this->Aes_encryption->decryptPostCall($this->secret,$_POST);
					$check = check_required(array('todoid'=>@$input['todoid']));
					if ($check){
						$response = array('status' => 400,'message' => $check.' can\'t empty','method'=>'assign_NewTask');
					}else{
						$input['user_id'] = $auth['id'];
						$response = $this->AppBackend->ht_mark_ActionAccomplished($input);
					}
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Get Ttalk Index 
	*
	*/
	public function get_TtalkInbox()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET'){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'get_TtalkInbox'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input['user_id'] = $auth['id'];
					$response = $this->AppBackend->ht_get_TtalkInbox($input);
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Send Message
	*
	*/
	public function send_newMessage()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'  ){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'send_newMessage'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input = $this->Aes_encryption->decryptPostCall($this->secret,$_POST);
					$check = check_required(array('team_id'=>@$input['team_id'],'privacy'=>@$input['privacy'],'message_type'=>@$input['message_type']));
					if ($check){
						$response = array('status' => 400,'message' => $check.' can\'t empty','method'=>'send_newMessage');
					}else{
						$input['sender_id'] = $auth['id'];
						$response = $this->AppBackend->ht_send_newMessage($input);
					}
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Send one to one  message to members
	*
	*/
	public function send_newMessageOnetoOne()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'  ){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'send_newMessageOnetoOne'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input = $this->Aes_encryption->decryptPostCall($this->secret,$_POST);
					$check = check_required(array('receiver_id'=>@$input['receiver_id'],'message_type'=>@$input['message_type']));
					if ($check){
						$response = array('status' => 400,'message' => $check.' can\'t empty','method'=>'send_newMessageOnetoOne');
					}else{
						$input['sender_id'] = $auth['id'];
						$response = $this->AppBackend->ht_send_newMessageOnetoOne($input);
					}
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Forward message to single or bulk
	*
	*/
	public function forward_Messages()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'  ){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'forward_Messages'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input = $this->Aes_encryption->decryptPostCall($this->secret,$_POST);
					$check = check_required(array('receiver_id'=>@$input['receiver_id'],'ttalkid'=>@$input['ttalkid'],'forward_type'=>@$input['forward_type']));
					if ($check){
						$response = array('status' => 400,'message' => $check.' can\'t empty','method'=>'forward_Messages');
					}else{
						$input['sender_id'] = $auth['id'];
						$response = $this->AppBackend->ht_forward_Messages($input);
					}
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Get Ttalk messages
	*
	*/
	public function get_tTalkMessage()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		$segment_four = $this->Aes_encryption->aesDecryption($this->secret,urldecode($this->uri->segment(4)));
		$segment_five = $this->Aes_encryption->aesDecryption($this->secret,urldecode($this->uri->segment(5)));
		$segment_six = $this->Aes_encryption->aesDecryption($this->secret,urldecode($this->uri->segment(6)));
		if($method != 'GET' || $segment_four == '' || is_numeric($segment_four) == FALSE || $segment_five == '' || is_numeric($segment_five) == FALSE){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'get_tTalkMessage'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input['sender_id'] = $auth['id'];
					$input['team_id'] = $segment_four;
					$input['page'] = $segment_five;
					$input['timezone'] = $segment_six!='' ? $segment_six : "utc";
					$response = $this->AppBackend->ht_get_tTalkMessage($input);
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Get Chat one to one
	*
	*/
	public function get_oneToOneChat()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		$segment_four = $this->Aes_encryption->aesDecryption($this->secret,urldecode($this->uri->segment(4)));
		$segment_five = $this->Aes_encryption->aesDecryption($this->secret,urldecode($this->uri->segment(5)));
		$segment_six = $this->Aes_encryption->aesDecryption($this->secret,urldecode($this->uri->segment(6)));
		
		if($method != 'GET' || $segment_four == '' || is_numeric($segment_four) == FALSE || $segment_five == '' || is_numeric($segment_five) == FALSE){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'get_oneToOneChat'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input['user_id'] = $auth['id'];
					$input['receiver_id'] = $segment_four;
					$input['page'] = $segment_five;
					$input['timezone'] = $segment_six!='' ? $segment_six : "utc";
					$response = $this->AppBackend->ht_get_oneToOneChat($input);
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Refesh Chat
	*
	*/
	public function refreshChat()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		$segment_four = $this->Aes_encryption->aesDecryption($this->secret,urldecode($this->uri->segment(4)));
		$segment_five = $this->Aes_encryption->aesDecryption($this->secret,urldecode($this->uri->segment(5)));
		$segment_six  = $this->Aes_encryption->aesDecryption($this->secret,urldecode($this->uri->segment(6)));
		$segment_seven  = $this->Aes_encryption->aesDecryption($this->secret,urldecode($this->uri->segment(7)));
		if($method != 'GET' || $segment_four == '' || is_numeric($segment_four) == FALSE || $segment_five == '' || is_numeric($segment_five) == FALSE || $segment_six == '' || is_numeric($segment_six) == FALSE){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'refreshChat'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input['user_id'] = $auth['id'];
					$input['thread_id'] = $segment_four;
					$input['ttalkid'] = $segment_five;
					$input['section'] = $segment_six;
					$input['timezone'] = $segment_seven!='' ? $segment_seven : "utc";
					$response = $this->AppBackend->ht_refreshChat($input);
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Delete Message
	*
	*/
	public function delete_Message()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		$segment_four = $this->Aes_encryption->aesDecryption($this->secret,urldecode($this->uri->segment(4)));
		if($method != 'DELETE' || $segment_four == ''){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'delete_Message'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input['user_id'] = $auth['id'];
					$input['ttalkid'] = $segment_four;
					$response = $this->AppBackend->ht_delete_Message($input);
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Print Protocol
	*
	*/
	public function print_Protocol()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		$segment_four = $this->Aes_encryption->aesDecryption($this->secret,urldecode($this->uri->segment(4)));
		$segment_five = $this->Aes_encryption->aesDecryption($this->secret,urldecode($this->uri->segment(5)));
		$segment_six  = $this->Aes_encryption->aesDecryption($this->secret,urldecode($this->uri->segment(6)));
		$segment_seven  = $this->Aes_encryption->aesDecryption($this->secret,urldecode($this->uri->segment(7)));
		$segment_eight  = $this->Aes_encryption->aesDecryption($this->secret,urldecode($this->uri->segment(8)));
		$segment_nine  = $this->Aes_encryption->aesDecryption($this->secret,urldecode($this->uri->segment(9)));
		if($method != 'GET' || $segment_four == '' || is_numeric($segment_four) == FALSE || $segment_five == '' || is_numeric($segment_five) == FALSE){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'print_Protocol'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input['user_id'] = $auth['id'];
					$input['thread_id'] = $segment_four;
					$input['section'] = $segment_five;
					$input['receiver_id'] = $segment_six!='' ? $segment_six : '0';
					$input['timezone'] = $segment_seven!='' ? $segment_seven : "utc";
					$input['start'] = $segment_eight!='' ? $segment_eight : "";
					$input['end'] = $segment_nine!='' ? $segment_nine : "";
					$response = $this->AppBackend->ht_print_Protocol($input);
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Print Protocol Ttalk
	*
	*/
	##$input['section'] = 1 for Team Talk, 2 One to one 
	##$input['thread_id'] 
	##$input['receiver_id'] > section = 2 else 0
	##$input['timezone'] > if not defined : UTC
	##$input['start'] : From Date
	##$input['end']  : End Date
	public function print_ProtocolTtalk()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		
		if($method != 'POST'){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'print_ProtocolTtalk'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input = $this->Aes_encryption->decryptPostCall($this->secret,$_POST);
					
					$check = check_required(array('section'=>@$input['section'],'thread_id'=>@$input['thread_id']));
					if ($check){
						$response = array('status' => 400,'message' => $check.' can\'t empty','method'=>'print_ProtocolTtalk');
					}else{
						$input['user_id'] = $auth['id'];
						isset($input['timezone']) ? $input['timezone'] = $input['timezone'] : $input['timezone'] ="UTC";
						isset($input['receiver_id']) ? $input['receiver_id'] = $input['receiver_id'] : $input['receiver_id'] ="0";
						$response = $this->AppBackend->ht_print_ProtocolTtalk($input);
					}
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Print Protocol Todo
	*
	*/
	##$input['team_id'] > team id for get all action list of team else 0
	##$input['accountability'] > userid if perticular user else 0
	##$input['timezone'] > if not defined : UTC
	##$input['start'] : From Date
	##$input['end']  : End Date
	public function print_ProtocolTodo()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		
		if($method != 'POST'){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'print_ProtocolTodo'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input = $this->Aes_encryption->decryptPostCall($this->secret,$_POST);
					$input['user_id'] = $auth['id'];
					isset($input['team_id']) ? $input['team_id'] = $input['team_id'] : $input['team_id'] ="0";
					isset($input['timezone']) ? $input['timezone'] = $input['timezone'] : $input['timezone'] ="UTC";
					isset($input['accountability']) ? $input['accountability'] = $input['accountability'] : $input['accountability'] ="0";
					$response = $this->AppBackend->ht_print_ProtocolTodo($input);
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Leave Team
	*
	*/
	public function leaveTeam()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'  ){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'send_newMessage'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input = $this->Aes_encryption->decryptPostCall($this->secret,$_POST);
					$check = check_required(array('team_id'=>@$input['team_id']));
					if ($check){
						$response = array('status' => 400,'message' => $check.' can\'t empty','method'=>'send_newMessage');
					}else{
						$input['user_id'] = $auth['id'];
						$response = $this->AppBackend->ht_leaveTeam($input);
					}
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Get Reassign Task Action to admin
	*
	*/
	public function get_reassignTaskList()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET'  ){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'get_reassignTaskList'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input['user_id'] = $auth['id'];
					$response = $this->AppBackend->ht_get_reassignTaskList($input);
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Reassign task to team members
	*
	*/
	public function reassign_task()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'reassign_task'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input = $this->Aes_encryption->decryptPostCall($this->secret,$_POST);
					$check = check_required(array('reassignaction_id'=>@$input['reassignaction_id'],'todoid'=>@$input['todoid'],'accountability'=>@$input['accountability'],'deadline'=>@$input['deadline']));
					if ($check){
						$response = array('status' => 400,'message' => $check.' can\'t empty','method'=>'reassign_task');
					}else{
						$input['assigned_by'] = $auth['id'];
						$response = $this->AppBackend->ht_reassign_task($input);
					}
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Dissolve	Team
	*
	*/
	public function dissolveTeam()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'  ){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'dissolveTeam'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input = $this->Aes_encryption->decryptPostCall($this->secret,$_POST);
					$check = check_required(array('team_id'=>@$input['team_id']));
					if ($check){
						$response = array('status' => 400,'message' => $check.' can\'t empty','method'=>'dissolveTeam');
					}else{
						$input['user_id'] = $auth['id'];
						$response = $this->AppBackend->ht_dissolveTeam($input);
					}
					json_output($response['status'],$response);
				}
			}
		}
	}
	
	
	
	
	
	### <----- End of Team - Ttalk - Todo Section   <<<---
	##### ----> News Channel Section ----->
	
	/*
	*
	* Create News Channel
	*
	*/
	public function create_NewsChannel()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'  ){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'create_NewsChannel'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input = $this->Aes_encryption->decryptPostCall($this->secret,$_POST);
					$check = check_required(array('channel_name'=>@$input['channel_name']));
					if ($check){
						$response = array('status' => 400,'message' => $check.' can\'t empty','method'=>'create_NewsChannel');
					}else{
						$input['channel_adminid'] = $auth['id'];
						$response = $this->AppBackend->ht_create_NewsChannel($input);
					}
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Update News Channel info
	*
	*/
	public function update_NewsChannelInfo()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'  ){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'update_NewsChannelInfo'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input = $this->Aes_encryption->decryptPostCall($this->secret,$_POST);
					$check = check_required(array('channel_id'=>@$input['channel_id'],'channel_name'=>@$input['channel_name']));
					if ($check){
						$response = array('status' => 400,'message' => $check.' can\'t empty','method'=>'create_NewsChannel');
					}else{
						$input['channel_adminid'] = $auth['id'];
						$response = $this->AppBackend->ht_update_NewsChannelInfo($input);
					}
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Delete News Channel
	*
	*/
	public function delete_NewsChannel()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		$segment_four = $this->Aes_encryption->aesDecryption($this->secret,urldecode($this->uri->segment(4)));
		if($method != 'DELETE' || $segment_four == '' || is_numeric($segment_four) == FALSE){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'delete_NewsChannel'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input['channel_adminid'] = $auth['id'];
					$input['channel_id'] = $segment_four;
					$response = $this->AppBackend->ht_delete_NewsChannel($input);
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Get My News channels
	*
	*/
	public function get_myNewsChannel()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET'){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'get_myNewsChannel'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input['channel_adminid'] = $auth['id'];
					$response = $this->AppBackend->ht_get_myNewsChannel($input);
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Search News channels
	*
	*/
	public function search_newsChannel()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		$segment_four = $this->Aes_encryption->aesDecryption($this->secret,urldecode($this->uri->segment(4)));
		$segment_five = $this->Aes_encryption->aesDecryption($this->secret,urldecode($this->uri->segment(5)));
		if($method != 'GET' || $segment_four == '' || is_numeric($segment_four) == FALSE ){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'search_newsChannel'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input['user_id'] = $auth['id'];
					$input['page'] = $segment_four;
					$input['search'] = $segment_five!='' ? $segment_five : '';
					$response = $this->AppBackend->ht_search_newsChannel($input);
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Subscribe - unsubscribe News channel
	*
	*/
	public function subs_unsbuscribeNewsChannel()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'subs_unsbuscribeNewsChannel'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input = $this->Aes_encryption->decryptPostCall($this->secret,$_POST);
					$check = check_required(array('channel_id'=>@$input['channel_id']));
					if ($check){
						$response = array('status' => 400,'message' => $check.' can\'t empty','method'=>'subs_unsbuscribeNewsChannel');
					}else{
						$input['user_id'] = $auth['id'];
						$response = $this->AppBackend->ht_subs_unsbuscribeNewsChannel($input);
						}
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	*  Publish News Brief
	*
	*/
	public function publish_NewsBrief()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'  ){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'publish_NewsBrief'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input = $this->Aes_encryption->decryptPostCall($this->secret,$_POST);
					$check = check_required(array('channel_id'=>@$input['channel_id']));
					if ($check){
						$response = array('status' => 400,'message' => $check.' can\'t empty','method'=>'publish_NewsBrief');
					}else{
						$input['user_id'] = $auth['id'];
						$response = $this->AppBackend->ht_publish_NewsBrief($input);
					}
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	*  Edit News Brief
	*
	*/
	public function edit_NewsBrief()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'  ){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'edit_NewsBrief'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input = $this->Aes_encryption->decryptPostCall($this->secret,$_POST);
					$check = check_required(array('newsbrief_id'=>@$input['newsbrief_id']));
					if ($check){
						$response = array('status' => 400,'message' => $check.' can\'t empty','method'=>'edit_NewsBrief');
					}else{
						$input['user_id'] = $auth['id'];
						$response = $this->AppBackend->ht_edit_NewsBrief($input);
					}
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Get latest news briefs
	*
	*/
	public function get_latest_newsBriefs()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		$segment_four = $this->Aes_encryption->aesDecryption($this->secret,urldecode($this->uri->segment(4)));
		$segment_five = $this->Aes_encryption->aesDecryption($this->secret,urldecode($this->uri->segment(5)));
		if($method != 'GET' || $segment_four == '' || is_numeric($segment_four) == FALSE ){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'get_latest_newsBriefs'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input['user_id'] = $auth['id'];
					$input['page'] = $segment_four;
					$input['timezone'] = $segment_five!='' ? $segment_five : "utc";
					$response = $this->AppBackend->ht_get_latest_newsBriefs($input);
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Refresh latest news briefs
	*
	*/
	public function refresh_newsBriefs()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		$segment_four = $this->Aes_encryption->aesDecryption($this->secret,urldecode($this->uri->segment(4)));
		$segment_five = $this->Aes_encryption->aesDecryption($this->secret,urldecode($this->uri->segment(5)));
		if($method != 'GET' || $segment_four == '' || is_numeric($segment_four) == FALSE ){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'refresh_newsBriefs'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input['user_id'] = $auth['id'];
					$input['newsbrief_id'] = $segment_four;
					$input['timezone'] = $segment_five!='' ? $segment_five : "utc";
					$response = $this->AppBackend->ht_refresh_newsBriefs($input);
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Delete News Brief
	*
	*/
	public function delete_newsBrief()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		$segment_four = $this->Aes_encryption->aesDecryption($this->secret,urldecode($this->uri->segment(4)));
		$segment_five = $this->Aes_encryption->aesDecryption($this->secret,urldecode($this->uri->segment(5)));
		if($method != 'DELETE' || $segment_four == '' || is_numeric($segment_four) == FALSE){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'delete_newsBrief'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input['user_id'] = $auth['id'];
					$input['newsbrief_id'] = $segment_four;
					$input['hide'] = $segment_five!='' ? '1' : '0';
					$response = $this->AppBackend->ht_delete_newsBrief($input);
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* New Notification alerts
	*
	*/
	public function get_notificationsAlerts()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET'){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'get_notificationsAlerts'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input['user_id'] = $auth['id'];
					$response = $this->AppBackend->ht_get_notificationsAlerts($input);
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Get company information Notification
	*
	*/
	public function get_company_information()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		$segment_four = $this->Aes_encryption->aesDecryption($this->secret,urldecode($this->uri->segment(4)));
		if($method != 'GET' || $segment_four == '' || is_numeric($segment_four) == FALSE){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'get_company_information'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input['user_id'] = $auth['id'];
					$input['company_id'] = $segment_four;
					$response = $this->AppBackend->ht_get_company_information($input);
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Send Company Account Request
	*
	*/
	public function send_company_account_request()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'send_company_account_request'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input = $this->Aes_encryption->decryptPostCall($this->secret,$_POST);
					$check = check_required(array('empoloyees_count'=>@$input['empoloyees_count'],'card_id'=>@$input['card_id']));
					if ($check){
						$response = array('status' => 400,'message' => $check.' can\'t empty','method'=>'edit_NewsBrief');
					}else{
						$input['user_id'] = $auth['id'];
						$response = $this->AppBackend->ht_send_company_account_request($input);
					}
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Export data to Google csv and Outlook Excel Format
	*
	*/
	public function exportContactsTo_GoogleOrOutlook()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		$segment_four = $this->Aes_encryption->aesDecryption($this->secret,urldecode($this->uri->segment(4)));
		$segment_five = $this->Aes_encryption->aesDecryption($this->secret,urldecode($this->uri->segment(5)));
		if($method != 'GET'){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'send_company_account_request'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input['export_to'] = $segment_four;
					$input['rolodex_id'] = $segment_five;
					$input['user_id'] = $auth['id'];
					$response = $this->AppBackend->ht_exportContactsTo_GoogleOrOutlook($input);
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Delete News Brief
	*
	*/
	public function delete_InvitesOrRejection()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		$segment_four = $this->Aes_encryption->aesDecryption($this->secret,urldecode($this->uri->segment(4)));
		if($method != 'DELETE' || $segment_four == '' || is_numeric($segment_four) == FALSE){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'delete_InvitesOrRejection'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input['user_id'] = $auth['id'];
					$input['invite_id'] = $segment_four;
					$response = $this->AppBackend->ht_delete_InvitesOrRejection($input);
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Get INR User Info By Mobile Number
	*
	*/
	public function get_UserInfoByMobileNumber()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		$segment_four = $this->Aes_encryption->aesDecryption($this->secret,urldecode($this->uri->segment(4)));
		if($method != 'GET' || $segment_four == '' || is_numeric($segment_four) == FALSE){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'get_UserInfoByMobileNumber'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input['user_id'] = $auth['id'];
					$input['mobile_number'] = $segment_four;
					$response = $this->AppBackend->ht_get_UserInfoByMobileNumber($input);
					json_output($response['status'],$response);
				}
			}
		}
	}
	/*
	*
	* Get INR User Info By Mobile Number
	*
	*/
	public function get_businessCardTemplate()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET'){
			json_output(400,array('status' => 400,'message' => 'Bad request.','method'=>'get_businessCardTemplate'));
		}else{
			$check_auth_client = $this->AppBackend->ht_check_auth_client();
			if ($check_auth_client === true) {
				$auth = $this->AppBackend->ht_auth();
		        if($auth['status'] == 200){
					$input['user_id'] = $auth['id'];
					$response = $this->AppBackend->ht_get_businessCardTemplate($input);
					json_output($response['status'],$response);
				}
			}
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	### Test Apis
	function sendTestEmail(){
		
		$from 		= $_POST['from_email'];
		$to 		= $_POST['to_email'];
		$subject	= $_POST['subject'];
		$message 	= $_POST['message'];
		
		$this->load->library('email');
		$this->email->from($from,'No Reply');
		$this->email->to($to);
		$this->email->subject($subject);
		$this->email->set_mailtype("html");
		$email_message .= "<div style='width:800px;background:#fff;color:#000;font-family:Source Sans Pro,sans-serif;padding:10px;' >";	
		$email_message .= "<div>".$message."</div>";
		$email_message .= "</div>" ;
		$this->email->message($email_message);
		if ($this->email->send()){
			$this->email->clear(TRUE);
			$response =  array('status' => 200,'message' => 'Mail send','method'=>'sendTestEmail');
		}else{
			$response =  array('status' => 400,'message' => 'There are some problem to sending email! Please try again.','method'=>'sendTestEmail');
		}
		json_output($response['status'],$response);
	}
	
	function get_countries(){
		$data = $this->db->select('countries_name, country_code, countries_iso_code as flag')->from('countries')->order_by('countries_name','ASC')->get()->result_array();
		for($i=0;$i<count($data);$i++){
			$data[$i]['flag'] = base_url().'media/flags/'.strtolower($data[$i]['flag']).'.png';
		}
		$response =  array('status' => 200,'message' => 'Mail send','method'=>'get_countries','data'=>$data);
		json_output($response['status'],$response);
	}
	
}