<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Comapany_accountrequests extends SUPER_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('Super_comapany_accountrequests');
	}

	public function index() {
		$data['data'] = $this->Super_comapany_accountrequests->select_comapany_accountrequests();
		$this->load_view('comapany_accountrequests', $data);
	}
	
	public function view_request(){
		$get  = $this->uri->segment_array();
		$data = $this->Super_comapany_accountrequests->view_request(end($get));
		$data['error_msg'] = '';
		if ($data['status']==400){
			$this->session->set_flashdata('error_msg', $data['msg']);
			redirect(base_url().'admin/accountrequest');
		}
		//print_r("<pre>"); print_r($data); exit;
		$this->load_view('viewrequest', $data);
	}
	
	#Create Company Account of Request
	
	public function process_request(){
		$get  = $this->uri->segment_array();
		$data = $this->Super_comapany_accountrequests->view_request(end($get));
		$data['error_msg'] = '';
		if ($this->input->post( 'submit' )){
			$this->form_validation->set_rules('company_name', 'Company Name', 'trim|required');
			$this->form_validation->set_rules('domain', 'Company Domain', 'trim|required');
			$this->form_validation->set_rules('contact_person', 'Contact Person', 'trim|required');
			$this->form_validation->set_rules('contact_email', 'Contact Email', 'trim|required');
			$this->form_validation->set_rules('contact_phone', 'Contact Number', 'trim|required');
			$this->form_validation->set_rules('company_address', 'Company Address', 'trim|required');
			$this->form_validation->set_rules('city', 'City Name', 'trim|required');
			$this->form_validation->set_rules('zipcode', 'Zipcode', 'trim|required');

			$input = $this->input->post();
			$error = "";
			if ($this->form_validation->run() == TRUE) {
				$this->load->model('Super_companies');
				
				$result = $this->Super_companies->newcompany($input);
				
				if($result['status']== 200){
					$this->session->set_flashdata('success_msg', $result['msg']);
					redirect(base_url().'admin/accountrequest');
				}else{
					$data['error_msg']  = $result['msg'];
				}
			}else{
				$data['error_msg'] =  validation_errors();
			}
		}
		if ($data['status']==400){
			$this->session->set_flashdata('error_msg', $data['msg']);
			redirect(base_url().'admin/accountrequest');
		}
		
		$this->load_view('process_request', $data);
	}
	
	
}