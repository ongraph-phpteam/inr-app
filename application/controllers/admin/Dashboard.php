<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends SUPER_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('Super');
	}

	public function index() {
		$data['data'] = $this->Super->get_analytics();
		$graph['data'] = $this->Super->users_chart();
		//print "<pre>";print_r($graph); exit;
		$this->load_view('dashboard', $data);
		$this->load->view('admin/chart', $graph);
	}
		
}