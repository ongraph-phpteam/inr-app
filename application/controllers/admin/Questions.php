<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Questions extends SUPER_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('Super_questions');
	}

	public function index() {
		$data['data'] = $this->Super_questions->select_questions();
		$this->load_view('security_questions', $data);
	}
	
	public function new_question() {
		
		$error['error_msg'] = '';
		if ($this->input->post( 'submit' )){
			
			$this->form_validation->set_rules('question', 'Security Question', 'trim|required');
			$data = $this->input->post();
			if ($this->form_validation->run() == TRUE) {
				$result = $this->Super_questions->new_question($data);
				if($result['status']== 200){
					$this->session->set_flashdata('success_msg', $result['msg']);
					redirect(base_url().'admin/questions');
					
				}else{
					$error['error_msg'] = $result['msg'];
					
				}
			}else{
				$error['error_msg'] =  validation_errors();
			}
		}	
		$this->load_view('new_question', $error);
		
	}
	
	public function edit_question(){
		$get  = $this->uri->segment_array();
		$data = $this->Super_questions->view_question(end($get));
		$data['error_msg'] = '';
		if ($this->input->post( 'submit' )){
			$this->form_validation->set_rules('question', 'Security Question', 'trim|required');
			$input = $this->input->post();
			$error = "";
			if ($this->form_validation->run() == TRUE) {
				$result = $this->Super_questions->edit_question($input);
				if($result['status']== 200){
					$this->session->set_flashdata('success_msg', $result['msg']);
					redirect(base_url().'admin/questions');
				}else{
					$data['error_msg'] = $result['msg'];
				}
			}else{
				$data['error_msg'] =  validation_errors();
			}
		}
		
		if ($data['status']==400){
			$this->session->set_flashdata('error_msg', $data['msg']);
			redirect(base_url().'admin/questions');
		}
		$this->load_view('edit_question', $data);
	}
}
