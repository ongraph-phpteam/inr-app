<?php
defined('BASEPATH') OR exit('No direct script access allowed');

## Company Section

class Not_allocated extends SUPER_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('Super_not_allocated');
		$this->load->model('Common');
	}

	public function index() {
		$this->load->helper('date');
		$data['data'] = $this->Super_not_allocated->select_allusers();
		$this->load_view('not_allocated', $data);
	}
	
	
	
}