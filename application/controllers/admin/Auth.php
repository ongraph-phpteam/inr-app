<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('Super_auth');
	}
	
	public function index() {
		$session = $this->session->userdata('superstatus');
		if ($session != 'SUPER') {
			$this->load->view('admin/login');
		} else {
			redirect(base_url() . 'admin/dashboard');
		}
	}
	public function login() {
		$this->form_validation->set_rules('username', 'Username', 'required|min_length[4]|max_length[15]');
		$this->form_validation->set_rules('password', 'Password', 'required');
		if ($this->form_validation->run() == TRUE) {
			$username = trim($_POST['username']);
			$password = trim($_POST['password']);

			$data = $this->Super_auth->login($username, $password);
			
			if ($data == false) {
				$this->session->set_flashdata('error_msg', 'Username / Password not matched.');
				redirect(base_url().'admin');
			} else {
				$session = [
					'superdata' => $data,
					'superstatus' => "SUPER"
				];
				$this->session->set_userdata($session);
				redirect(base_url() . 'admin/dashboard');
			}
		} else {
			$this->session->set_flashdata('error_msg', validation_errors());
			redirect(base_url() .'admin');
		}
	}

	public function logout() {
		$this->session->unset_userdata('superdata');
        $this->session->unset_userdata('superstatus');
        //$this->session->sess_destroy();
		redirect(base_url() .'admin');
	}
}

/* End of file Login.php */
/* Location: ./application/controllers/Login.php */