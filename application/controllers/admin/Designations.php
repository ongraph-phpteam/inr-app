<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Designations extends SUPER_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('Super_designation');
	}

	public function index() {
		$data['data'] = $this->Super_designation->select_designations();
		$this->load_view('designations', $data);
	}
	
	public function newdesignation() {
		
		$error['error_msg'] = '';
		if ($this->input->post( 'submit' )){
			
			$this->form_validation->set_rules('designation', 'Designation Name', 'trim|required');
			$data = $this->input->post();
			if ($this->form_validation->run() == TRUE) {
				$result = $this->Super_designation->newdesignation($data);
				if($result['status']== 200){
					$this->session->set_flashdata('success_msg', $result['msg']);
					redirect(base_url().'admin/designations');
					
				}else{
					$error['error_msg'] = $result['msg'];
					
				}
			}else{
				$error['error_msg'] =  validation_errors();
			}
		}	
		$this->load_view('newdesignation', $error);
		
	}
	
	public function editdesignation(){
		$get  = $this->uri->segment_array();
		$data = $this->Super_designation->view_designation(end($get));
		$data['error_msg'] = '';
		if ($this->input->post( 'submit' )){
			$this->form_validation->set_rules('designation', 'Designation Name', 'trim|required');
			$input = $this->input->post();
			$error = "";
			if ($this->form_validation->run() == TRUE) {
				$result = $this->Super_designation->editdesignation($input);
				if($result['status']== 200){
					$this->session->set_flashdata('success_msg', $result['msg']);
					redirect(base_url().'admin/designations');
				}else{
					$data['error_msg'] = $result['msg'];
				}
			}else{
				$data['error_msg'] =  validation_errors();
			}
		}
		
		if ($data['status']==400){
			$this->session->set_flashdata('error_msg', $data['msg']);
			redirect(base_url().'admin/designations');
		}
		$this->load_view('editdesignation', $data);
	}
	
	public function delete_designation(){
		$request 		= $this->uri->segment_array();
		$designation_id	= end($request);
		$this->Super_designation->delete_designation($designation_id);
		redirect(base_url().'admin/designations');
	}
	
	
	
	
}