<?php
defined('BASEPATH') OR exit('No direct script access allowed');

## Company Section

class Users extends ADMIN_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('Company_users');
		$this->load->model('Common');
	}

	public function index() {
		$this->load->helper('date');
		$data['data'] = $this->Company_users->select_allusers();
		$this->load_view('users', $data);
	}
	
	public function newuser() {
		$data['error_msg'] = '';
		$data['company_id'] = $this->session->userdata()['userdata']['company_id'];
		$data['designations'] = $this->Common->get_designationsList($data['company_id']);
		$data['country_code'] = $this->Common->get_countryCode();
		if ($this->input->post( 'submit' )){
			$this->form_validation->set_rules('user_name', 'User Name', 'trim|required');
			$this->form_validation->set_rules('email', 'Email', 'trim|required');
			$this->form_validation->set_rules('country_code', 'Country code', 'trim|required');
			$this->form_validation->set_rules('mobile_number', 'Mobile No', 'trim|required');
			$this->form_validation->set_rules('designation', 'Area of responsibility', 'trim|required');
			$input = $this->input->post();
			if ($this->form_validation->run() == TRUE) {
				$result = $this->Company_users->newuser($input);
				if($result['status']== 200){
					$this->session->set_flashdata('success_msg', $result['msg']);
					redirect(base_url().'company/users');
				}else{
					$data['error_msg'] = $result['msg'];
				}
			}else{
				$data['error_msg'] =  validation_errors();
			}
		}	
		$this->load_view('newuser', $data);
	}
	
	public function edit_userprofile(){
		$get  = $this->uri->segment_array();
		$user_id = $this->Aes_encryption->aesDecryption($this->secret,urldecode(end($get)));
		$data = $this->Company_users->view_userprofile($user_id);
		$data['company_id'] = $this->session->userdata()['userdata']['company_id'];
		$data['designations'] = $this->Common->get_designationsList($data['company_id']);
		$data['country_code'] = $this->Common->get_countryCode();
		$data['error_msg'] = '';
		if ($this->input->post( 'submit' )){
			$this->form_validation->set_rules('user_name', 'User Name', 'trim|required');
			$this->form_validation->set_rules('email', 'Email', 'trim|required');
			//$this->form_validation->set_rules('country_code', 'Country code', 'trim|required');
			$this->form_validation->set_rules('mobile_number', 'Mobile No', 'trim|required');
			$this->form_validation->set_rules('designation', 'Designation', 'trim|required');
			$input = $this->input->post();
			$error = "";
			if ($this->form_validation->run() == TRUE) {
				$result = $this->Company_users->edit_userprofile($input);
				if($result['status']== 200){
					$this->session->set_flashdata('success_msg', $result['msg']);
					redirect(base_url().'company/users');
				}else{
					$data['error_msg'] = $result['msg'];
				}
			}else{
				$data['error_msg'] =  validation_errors();
			}
		}
		if ($data['status']==400){
			$this->session->set_flashdata('error_msg', $data['msg']);
			redirect(base_url().'company/users');
		}
		$this->load_view('editprofile', $data);
	}
	
	
}