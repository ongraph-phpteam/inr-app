<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Todo extends ADMIN_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('Company_todo');
		$this->load->model('Common');
	}

	public function index() {
		$this->load->helper('date');
		$data['data'] = $this->Company_todo->get_todolist();
		//print "<pre>"; print_r($data); exit;
		$this->load_view('todolist', $data);
	}
	
	
	
	
}