<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends ADMIN_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('Company');
		$this->secret = $this->config->item('encryption_key');
		$this->Aes_encryption = new Aes_encryption();
	}

	public function index() {
		$company_id = $this->session->userdata()['userdata']['company_id'];
		$data['data'] = $this->Company->get_analytics($company_id);
		$data['news'] = $this->Company->latest_newsbriefs($company_id);
		$data['todo'] = $this->Company->get_latest_todolist($company_id);
		//print"<pre>"; print_r($data['todo']); exit;
		$graph['data'] = $this->Company->users_chart($company_id);
		$this->load_view('dashboard', $data);
		$this->load->view('admin/chart', $graph);
	}
}
