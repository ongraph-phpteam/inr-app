<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('Company_auth');
		$this->secret = $this->config->item('encryption_key');
		$this->Aes_encryption = new Aes_encryption();
	}
	
	public function index() {
		$session = $this->session->userdata('status');
		if ($session != 'ADMIN') {
			$this->load->view('company/login');
		} else {
			redirect(base_url() . 'company/dashboard');
		}
	}
	public function login() {
		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');
		if ($this->form_validation->run() == TRUE) {
			$username = trim($_POST['username']);
			$password = trim($_POST['password']);

			$data = $this->Company_auth->login($username, $password);
			if ($data == 401) {
				$this->session->set_flashdata('error_msg', 'Admin account not exist with email address you have entered. Please try again');
				redirect(base_url().'company');
			}elseif($data == 400){
				$this->session->set_flashdata('error_msg', 'Wrong password entered.');
				redirect(base_url().'company');
			} else {
				$session = [
					'userdata' => $data,
					'status' => "ADMIN"
				];
				$this->session->set_userdata($session);
				redirect(base_url() . 'company/dashboard');
			}
		} else {
			$this->session->set_flashdata('error_msg', validation_errors());
			redirect(base_url() .'company');
		}
	}
	public function password_recover() {
		$this->form_validation->set_rules('email', 'Email', 'required');
		if ($this->form_validation->run() == TRUE) {
			$email = trim($_POST['email']);
			$data = $this->Company_auth->password_recover($email);
			if ($data == 401) {
				$this->session->set_flashdata('error_msg', 'Admin account not exist with email address you have entered. Please try again');
				redirect(base_url().'company');
			}elseif($data == 400){
				$this->session->set_flashdata('error_msg', 'Wrong password entered.');
				redirect(base_url().'company');
			} else {
				$session = [
					'userdata' => $data,
					'status' => "ADMIN"
				];
				$this->session->set_userdata($session);
				redirect(base_url() . 'company/dashboard');
			}
		} else {
			$this->session->set_flashdata('error_msg', validation_errors());
			redirect(base_url() .'company');
		}
	}
	# Super Admin Masquerade as company Admin
	public function masquerade(){
		$get  = $this->uri->segment_array();
		$company_id = $this->Aes_encryption->aesDecryption($this->secret,urldecode(end($get)));
		$data = $this->Company_auth->masquerade($company_id);
		if ($data == false) {
			$this->session->set_flashdata('error_msg', 'Something went wrong! Try again.');
			redirect(base_url().'company');
		} else {
			$session = [
				'userdata' => $data,
				'status' => "ADMIN"
			];
			$this->session->set_userdata($session);
			redirect(base_url() . 'company/dashboard');
		}
	}
	public function logout() {
		$this->session->unset_userdata('userdata');
        $this->session->unset_userdata('status');
		redirect(base_url() .'company');
	}
}

/* End of file Login.php */
/* Location: ./application/controllers/Login.php */