<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Teams extends ADMIN_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('Company_teams');
		$this->load->model('Common');
	}

	public function index() {
		$this->load->helper('date');
		$data['data'] = $this->Company_teams->select_allteams();
		$this->load_view('teams', $data);
	}
	public function new_team() {
		$company_id = $this->session->userdata()['userdata']['company_id'];
		$user_id	= $this->session->userdata()['userdata']['user_id'];
		$data['users'] = $this->Common->get_CompanyMembers($company_id,array($user_id));
		$data['error_msg'] = '';
		if ($this->input->post( 'submit' )){
			$this->form_validation->set_rules('team_name', 'Team Name', 'trim|required');
			$input_data = $this->input->post();
			$input_data['user_id'] = $user_id;
			$input_data['company_id']= $company_id;
			if ($this->form_validation->run() == TRUE) {
				$result = $this->Company_teams->new_team($input_data);
				if($result['status']== 200){
					$this->session->set_flashdata('success_msg', $result['msg']);
					redirect(base_url().'company/teams');
					
				}else{
					$data['error_msg'] = $result['msg'];
					
				}
			}else{
				$data['error_msg'] =  validation_errors();
			}
		}	
		$this->load_view('new_team', $data);
		
	}
	
	public function update_team(){
		$get  = $this->uri->segment_array();
		$team_id = $this->Aes_encryption->aesDecryption($this->secret,urldecode(end($get)));
		$data = $this->Company_teams->view_team($team_id);
		$data['error_msg'] = '';
		if ($this->input->post( 'submit' )){
			$this->form_validation->set_rules('team_name', 'Team Name', 'trim|required');
			$input_data = $this->input->post();
			$error = "";
			if ($this->form_validation->run() == TRUE) {
				$input_data['team_id'] = $team_id;
				$input_data['user_id'] = $this->session->userdata()['userdata']['user_id'];
				$result = $this->Company_teams->update_team($input_data);
				if($result['status']== 200){
					$this->session->set_flashdata('success_msg', $result['msg']);
					redirect(base_url().'company/teams');
				}else{
					$data['error_msg'] = $result['msg'];
				}
			}else{
				$data['error_msg'] =  validation_errors();
			}
		}
		
		if ($data['status']==400){
			$this->session->set_flashdata('error_msg', $data['msg']);
			redirect(base_url().'company/teams');
		}
		$this->load_view('update_team', $data);
	}
	
	## View Team Member
	public function team_members(){
		$get  = $this->uri->segment_array();
		$team_id = $this->Aes_encryption->aesDecryption($this->secret,urldecode(end($get)));
		$data = $this->Company_teams->team_members($team_id);
		$data['id'] = end($get);
		$data['team_name'] = getTeamdata($team_id,'team_name');
		if ($data['status']==400){
			$this->session->set_flashdata('error_msg', $data['msg']);
			redirect(base_url().'company/teams');
		}
		$this->load_view('team_members', $data);
	}
	## View Team Invites
	public function team_invites(){
		$get  = $this->uri->segment_array();
		$team_id = $this->Aes_encryption->aesDecryption($this->secret,urldecode(end($get)));
		$data = $this->Company_teams->team_invites($team_id);
		$data['team_name'] = getTeamdata($team_id,'team_name');
		$data['id'] = end($get);
		if ($data['status']==400){
			$this->session->set_flashdata('error_msg', $data['msg']);
			redirect(base_url().'company/teams');
		}
		$this->load_view('team_invites', $data);
	}	
	## View Team Invites Declines list
	public function team_invites_decline_list(){
		$get  = $this->uri->segment_array();
		$team_id = $this->Aes_encryption->aesDecryption($this->secret,urldecode(end($get)));
		$data = $this->Company_teams->team_invites_decline_list($team_id);
		$data['team_name'] = getTeamdata($team_id,'team_name');
		$data['id'] = end($get);
		if ($data['status']==400){
			$this->session->set_flashdata('error_msg', $data['msg']);
			redirect(base_url().'company/teams');
		}
		$this->load_view('team_invites_decline_list', $data);
	}
	## Invite Members
	public function invite_members(){
		$get  = $this->uri->segment_array();
		$company_id = $this->session->userdata()['userdata']['company_id'];
		$team_id 	= $this->Aes_encryption->aesDecryption($this->secret,urldecode(end($get)));
		$alreadymembers = array_column($this->Company_teams->team_members($team_id)['data'],'user_id');
		$alreadyinvites = array_column($this->Company_teams->team_invites($team_id)['data'],'user_id');
		$data['users'] = $this->Common->get_CompanyMembers($company_id,array_merge($alreadymembers,$alreadyinvites));
		$data['error_msg'] = '';
		if ($this->input->post('submit' )){
			$input_data = $this->input->post();
			$error = "";
			if ($input_data['invite_userids'][0] !=0) {
				$input_data['team_id'] = $team_id;
				$input_data['user_id'] = $this->session->userdata()['userdata']['user_id'];
				$result = $this->Company_teams->invite_members($input_data);
				if($result['status']== 200){
					$this->session->set_flashdata('success_msg', $result['msg']);
					redirect(base_url().'company/teams');
				}else{
					$data['error_msg'] = $result['msg'];
				}
			}else{
				$data['error_msg'] =  "There are no user to invite.";
			}
		}
		$this->load_view('invite_members', $data);
	}
	
	
	
	
}