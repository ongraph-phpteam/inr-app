<?php
defined('BASEPATH') OR exit('No direct script access allowed');

## Company Section

class Signup_allow extends ADMIN_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('Company_signup_allow');
		$this->load->model('Common');
		$this->load->library('Csvimport');
	}

	public function index() {
		$this->load->helper('date');
		$data['data'] = $this->Company_signup_allow->select_signupAllowed();
		$this->load_view('signups_allow', $data);
	}
	
	public function add_emailids() {
		$data['error_msg'] = '';
		if ($this->input->post( 'submit' )){
			$input = $this->input->post();
			$result = $this->Company_signup_allow->add_emailids($input);
			if($result['status']== 200){
				$this->session->set_flashdata('success_msg', $result['msg']);
				redirect(base_url().'company/signup_allow');
			}else{
				$data['error_msg'] = $result['msg'];
			}
		}	
		$this->load_view('add_emailids', $data);
	}
	
}