<?php
defined('BASEPATH') OR exit('No direct script access allowed');

## Company Section

class Ajax extends ADMIN_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('Company_ajax');
		$this->load->model('Common');
	}

	public function delete_allowed_email(){
		$delete = $this->Company_ajax->delete_allowed_email($_POST['allowed_id']);
		echo json_encode($delete);
	}
	public function deleteUser(){
		$block = $this->Company_ajax->deleteUser($_POST['user_id']);
		echo json_encode($block);
	}
	
	public function block_user(){
		$block = $this->Company_ajax->block_user($_POST['user_id']);
		echo json_encode($block);
	}
	
	public function delete_team(){
		$delete = $this->Company_ajax->delete_team($_POST['team_id']);
		echo json_encode($delete);
	}
	
	public function remove_teamMember(){
		
		$delete = $this->Company_ajax->remove_teamMember($_POST);
		echo json_encode($delete);
	}
	
	public function delete_invites(){
		$delete = $this->Company_ajax->delete_invites($_POST);
		echo json_encode($delete);
	}
	
	public function delete_assignedTask(){
		$delete = $this->Company_ajax->delete_assignedTask($_POST);
		echo json_encode($delete);
	}
	
	public function delete_newsChannel(){
		$delete = $this->Company_ajax->delete_newsChannel($_POST);
		echo json_encode($delete);
	}
	
	public function deleteChannelFollower(){
		$delete = $this->Company_ajax->deleteChannelFollower($_POST);
		echo json_encode($delete);
	}
	
	public function deleteChannelNewsBrief(){
		$delete = $this->Company_ajax->deleteChannelNewsBrief($_POST);
		echo json_encode($delete);
	}
	
	public function deleteAnyChannelNewsBrief(){
		$delete = $this->Company_ajax->deleteChannelNewsBrief($_POST);
		echo json_encode($delete);
	}
	public function blockUnblockTemplate(){
		$block = $this->Company_ajax->blockUnblockTemplate($_POST['template_id']);
		echo json_encode($block);
	}
	
	
	
	
}