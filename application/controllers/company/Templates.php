<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Templates extends ADMIN_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('Company_templates');
		$this->load->model('Common');
	}

	public function index() {
		$this->load->helper('date');
		$data['data'] = $this->Company_templates->select_allChannels();
		$this->load_view('templates', $data);
	}
	public function new_template() {
		$company_id = $this->session->userdata()['userdata']['company_id'];
		$user_id	= $this->session->userdata()['userdata']['user_id'];
		$data['error_msg'] = '';
		if ($this->input->post( 'submit' )){
			$input_data = $this->input->post();
			$input_data['added_by']	 = $user_id;
			$input_data['company_id']= $company_id;
			$result = $this->Company_templates->new_template($input_data);
			if($result['status']== 200){
				$this->session->set_flashdata('success_msg', $result['msg']);
				redirect(base_url().'company/templates');
			}else{
				$data['error_msg'] = $result['msg'];
			}
		}	
		$this->load_view('new_template', $data);
	}
	
	public function update_template(){
		$get  = $this->uri->segment_array();
		$template_id = $this->Aes_encryption->aesDecryption($this->secret,urldecode(end($get)));
		$data = $this->Company_templates->view_template($template_id);
		$data['error_msg'] = '';
		if ($this->input->post( 'submit' )){
			$input_data = $this->input->post();
			$error = "";
			$input_data['company_id']= $this->session->userdata()['userdata']['company_id'];
			$input_data['template_id'] = $template_id;
			$input_data['added_by'] = $this->session->userdata()['userdata']['user_id'];
			$result = $this->Company_templates->update_template($input_data);
			if($result['status']== 200){
				$this->session->set_flashdata('success_msg', $result['msg']);
				redirect(base_url().'company/templates');
			}else{
				$data['error_msg'] = $result['msg'];
			}
		}
		
		if ($data['status']==400){
			$this->session->set_flashdata('error_msg', $data['msg']);
			redirect(base_url().'company/templates');
		}
		$this->load_view('update_template', $data);
	}

}