<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News_channels extends ADMIN_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('Company_news_channels');
		$this->load->model('Common');
	}

	public function index() {
		$this->load->helper('date');
		$data['data'] = $this->Company_news_channels->select_allChannels();
		$this->load_view('news_channels', $data);
	}
	public function new_channel() {
		$company_id = $this->session->userdata()['userdata']['company_id'];
		$user_id	= $this->session->userdata()['userdata']['user_id'];
		$data['error_msg'] = '';
		if ($this->input->post( 'submit' )){
			$this->form_validation->set_rules('channel_name', 'Channel Name', 'trim|required');
			$input_data = $this->input->post();
			$input_data['channel_adminid'] = $user_id;
			$input_data['company_id']= $company_id;
			if ($this->form_validation->run() == TRUE) {
				$result = $this->Company_news_channels->new_channel($input_data);
				if($result['status']== 200){
					$this->session->set_flashdata('success_msg', $result['msg']);
					redirect(base_url().'company/news_channels');
				}else{
					$data['error_msg'] = $result['msg'];
				}
			}else{
				$data['error_msg'] =  validation_errors();
			}
		}	
		$this->load_view('new_channel', $data);
	}
	
	public function update_channel(){
		$get  = $this->uri->segment_array();
		$channel_id = $this->Aes_encryption->aesDecryption($this->secret,urldecode(end($get)));
		$data = $this->Company_news_channels->view_channel($channel_id);
		$data['error_msg'] = '';
		if ($this->input->post( 'submit' )){
			$this->form_validation->set_rules('channel_name', 'News Channel Name', 'trim|required');
			$input_data = $this->input->post();
			$error = "";
			if ($this->form_validation->run() == TRUE) {
				$input_data['channel_id'] = $channel_id;
				$input_data['user_id'] = $this->session->userdata()['userdata']['user_id'];
				$result = $this->Company_news_channels->update_channel($input_data);
				if($result['status']== 200){
					$this->session->set_flashdata('success_msg', $result['msg']);
					redirect(base_url().'company/news_channels');
				}else{
					$data['error_msg'] = $result['msg'];
				}
			}else{
				$data['error_msg'] =  validation_errors();
			}
		}
		
		if ($data['status']==400){
			$this->session->set_flashdata('error_msg', $data['msg']);
			redirect(base_url().'company/news_channels');
		}
		$this->load_view('update_channel', $data);
	}
	
	## View Channel Followers
	public function channel_followers(){
		$get  = $this->uri->segment_array();
		$channel_id = $this->Aes_encryption->aesDecryption($this->secret,urldecode(end($get)));
		$data = $this->Company_news_channels->channel_followers($channel_id);
		$data['id'] = end($get);
		$data['channel_name'] = getNewsChannelData($channel_id,'channel_name');
		$data['channel_adminid'] = getNewsChannelData($channel_id,'channel_adminid');
		if ($data['status']==400){
			$this->session->set_flashdata('error_msg', $data['msg']);
			redirect(base_url().'company/news_channels');
		}
		$this->load_view('channel_followers', $data);
	}
	## View Active News Brief
	public function channel_newsbriefs(){
		
		$get  = $this->uri->segment_array();
		$channel_id = $this->Aes_encryption->aesDecryption($this->secret,urldecode(end($get)));
		$data = $this->Company_news_channels->channel_newsbriefs($channel_id);
		$data['id'] = end($get);
		$data['channel_name'] = getNewsChannelData($channel_id,'channel_name');
		if ($data['status']==400){
			$this->session->set_flashdata('error_msg', $data['msg']);
			redirect(base_url().'company/news_channels');
		}
		$this->load_view('channel_newsbriefs', $data);
		
		
		
	}	
	
}