<div id="content">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span6">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
                        <h5>Update Company-info</h5>
                    </div>
                    <div class="widget-content nopadding">
						<?php
							echo show_err_msg($error_msg);
						?>
                        <form action="" method="post" class="form-horizontal"  enctype="multipart/form-data" >
							<input type="hidden" name="company_id" value="<?php echo $data['company_id']; ?>" />
                            <div class="control-group">
                                <label class="control-label">Company Name :</label>
                                <div class="controls">
                                    <input type="text" name="company_name" value="<?php echo $data['company_name']; ?>" id="required" class="span11" placeholder="Company name"  />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Company Domain :</label>
                                <div class="controls">
                                    <input type="text" name="domain" value="<?php echo $data['domain']; ?>" class="span11" placeholder="Company Domain eg. gmail.com"  />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Contact Person</label>
                                <div class="controls">
                                    <input type="text" name="contact_person" value="<?php echo $data['contact_person']; ?>" class="span11" placeholder="Contact Person" required />
                                </div>
                            </div>
							<div class="control-group">
                                <label class="control-label">Contact Email</label>
                                <div class="controls">
                                    <input type="email" name="contact_email" value="<?php echo $data['contact_email']; ?>" class="span11" placeholder="Contact Email" required />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Contact Number</label>
								<div class="controls">
									<select class="span11" name="country_code" required >
										<option value="">Select Country Code</option>
                            <?php	foreach($country_code as $code){  
										if($code['country_code'] == $data['country_code']){
											$selc = "selected";
										}else{
											$selc = "";
										}
							?>		
										<option value="<?= $code['country_code']; ?>" <?= $selc; ?>><?= $code['countries_name']; ?> (<?= $code['country_code']; ?>)</option>
							<?php	} ?>	
									</select>
                                    <input type="number" name="contact_phone" value="<?= $data['contact_phone']?>" class="span11" placeholder="Contact Number" required />
								</div>
							</div>
                            <div class="control-group">
                                <label class="control-label">Company Address :</label>
                                <div class="controls">
                                    <input type="text" name="company_address" value="<?php echo $data['company_address']; ?>" class="span11" placeholder="Company Address" required />
                                </div>
                            </div>
							<div class="control-group">
                                <label class="control-label">City:</label>
                                <div class="controls">
                                    <input type="text" name="city" value="<?php echo $data['city']; ?>" class="span11" placeholder="City Name" required />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">State:</label>
                                <div class="controls">
                                    <input type="text" name="state" value="<?php echo $data['state']; ?>" class="span11" placeholder="State Name" required />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Zipcode:</label>
                                <div class="controls">
                                    <input type="text" name="zipcode" value="<?php echo $data['zipcode']; ?>" class="span11" placeholder="Zipcode" required />
                                </div>
                            </div>
							<div class="control-group">
                                <label class="control-label">Country:</label>
                                <div class="controls">
                                    <input type="text" name="country" value="<?php echo $data['country']; ?>" class="span11" placeholder="Country Name" required />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Company Logo:</label>
                                <div class="controls">
                                    <input type="file" name="company_logo" class="span11" />
                                    <span class="help-block">(Only .jpeg, .jpg, .png format supported)</span> </div>
                            </div>
                            <div class="form-actions">
                                <input type="submit" name="submit" value="Update Informaation" class="btn btn-success">
								<input type="reset" onclick="window.history.back();" value="Cancel" class="btn btn-warning">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
		</div>
        <hr>
    </div>
</div>
</div>