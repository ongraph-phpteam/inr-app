<div id="content">
    <div class="container-fluid">
		<div class="widget-box widget-plain">
			<div class="center">
				<ul class="stat-boxes">
					<li>
						<div class="left peity_bar_neutral">
							<span>
								<span style="display: none;">2,4,9,7,12,10,12</span>
								<canvas width="50" height="30"></canvas>
							</span><?= $data['total_company']; ?>
						</div>
						<a href="companies">
							<div class="right"> <strong><?= $data['total_company']; ?></strong> Companies </div>
						</a>
					</li>
					<li>
						<div class="left peity_line_neutral"><span><span style="display: none;">10,15,8,14,13,10,10,15</span>
							<canvas width="50" height="24"></canvas>
							</span><?= $data['total_users']; ?></div>
						<div class="right"> <strong><?= $data['total_users']; ?></strong> Total Users </div>
					</li>
					<li>
						<div class="left peity_line_good"><span><span style="display: none;">12,6,9,23,14,10,17</span>
							<canvas width="50" height="24"></canvas>
							</span><?= $data['active_users']; ?></div>
						<div class="right"> <strong><?= $data['active_users']; ?></strong> Active Users </div>
					</li>
					<li>
						
						<div class="left peity_line_good"><span><span style="display: none;">3,5,6,16,8,10,6</span>
							<canvas width="50" height="24"></canvas>
							</span><?= $data['new_requests']; ?></div>
						<a href="accountrequest">
						<div class="right"> <strong><?= $data['new_requests']; ?></strong> New requests</div>
						</a>
					</li>
					
					<li>
						<div class="left peity_bar_good"><span>12,6,9,23,14,10,13</span><?= $data['total_newchannel']; ?></div>
						<div class="right"> <strong><?= $data['total_newchannel']; ?></strong>News Brief Channels</div>
					</li>
					<li>
						<div class="left peity_line_good"><span>12,6,9,23,14,10,5</span><?= $data['total_ttalks']; ?></div>
						<div class="right"><strong><?= $data['total_ttalks']; ?></strong> Total Teams</div>
					</li>
				</ul>
			</div>
		</div>
		
		<div class="row-fluid">
            <div class="widget-box">
                <div class="widget-title"><span class="icon"><i class="icon-tasks"></i></span>
                    <h5>Users Analytics</h5>
                    <div class="buttons"><a href="dashboard" class="btn btn-mini btn-success"><i class="icon-refresh"></i> Refresh data</a></div>
                </div>
                <div class="widget-content">
                    <div class="row-fluid">
                        <div class="span12">
                            <div id="new_comp_reg" style="min-width: 193px; height: 400px; margin: 0 auto"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr>
    </div>
</div>
</div>
</div>