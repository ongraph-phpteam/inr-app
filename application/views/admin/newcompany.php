<div id="content">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span6">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
                        <h5>Add Company-info</h5>
                    </div>
                    <div class="widget-content nopadding">
						<?php
							echo show_err_msg($error_msg);
						?>
						
                        <form action="" method="post" class="form-horizontal"  enctype="multipart/form-data" >
                            <div class="control-group">
                                <label class="control-label">Company Name :</label>
                                <div class="controls">
                                    <input type="text" name="company_name" value="<?php echo set_value('company_name'); ?>" id="required" class="span11" placeholder="Company name"  required />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Company Domain :</label>
                                <div class="controls">
                                    <input type="text" name="domain" value="<?php echo set_value('domain'); ?>" class="span11" placeholder="Company Domain eg. gmail.com"  required/>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Contact Person</label>
                                <div class="controls">
                                    <input type="text" name="contact_person" value="<?php echo set_value('contact_person'); ?>" class="span11" placeholder="Contact Person" required />
                                </div>
                            </div>
							<div class="control-group">
                                <label class="control-label">Contact Email</label>
                                <div class="controls">
                                    <input type="email" name="contact_email" value="<?php echo set_value('contact_email'); ?>" class="span11" placeholder="Contact Email" required />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Contact Number</label>
                                <div class="controls">
									<select class="span11" name="(<?= $code['country_code']; ?>)" required >
										<option value="">Select Country Code</option>
                            <?php	foreach($country_code as $code){  ?>		
										<option value="<?= $code['country_code']; ?>"><?= $code['countries_name']; ?> (<?= $code['country_code']; ?>)</option>
							<?php	} ?>	
									</select>
									<input type="number" name="contact_phone" value="<?php echo set_value('contact_phone'); ?>" class="span11" placeholder="Contact Number" required />
									
								</div>
								
								
								
                            </div>
                            <div class="control-group">
                                <label class="control-label">Company Address :</label>
                                <div class="controls">
                                    <input type="text" name="company_address" value="<?php echo set_value('company_address'); ?>" class="span11" placeholder="Company Address" required />
                                </div>
                            </div>
							<div class="control-group">
                                <label class="control-label">City:</label>
                                <div class="controls">
                                    <input type="text" name="city" value="<?php echo set_value('city'); ?>" class="span11" placeholder="City Name" required />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">State:</label>
                                <div class="controls">
                                    <input type="text" name="state" value="<?php echo set_value('state'); ?>" class="span11" placeholder="State Name" required />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Zipcode:</label>
                                <div class="controls">
                                    <input type="text" name="zipcode" value="<?php echo set_value('zipcode'); ?>" class="span11" placeholder="Zipcode" required />
                                </div>
                            </div>
							<div class="control-group">
                                <label class="control-label">Country:</label>
                                <div class="controls">
                                    <input type="text" name="country" value="<?php echo set_value('country'); ?>" class="span11" placeholder="Country Name" required />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Company Logo:</label>
                                <div class="controls">
                                    <input type="file" name="company_logo" class="span11" required />
                                    <span class="help-block">(Only .jpeg, .jpg, .png format supported)</span> </div>
                            </div>
                            <div class="form-actions">
                                <input type="submit" name="submit" value="Submit Data" class="btn btn-success">
								<input type="reset" name="reset" value="Reset" class="btn btn-warning">
								<input type="reset" onclick="window.history.back();" value="Cancel" class="btn btn-danger">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
		</div>
        <hr>
    </div>
</div>
</div>