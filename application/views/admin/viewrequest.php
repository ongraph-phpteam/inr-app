<div id="content">
    <div class="container-fluid">
        <div class="data-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title">
                        <span class="icon"><i class="icon-th"></i></span>
                        <h5>New Company Account Requests:</h5>
                    </div>
                    <div class="widget-content nopadding">
						<?php
							echo show_err_msg($this->session->flashdata('error_msg'));
							echo show_succ_msg($this->session->flashdata('success_msg'));
						?>
                        <table class="table table-bordered ">
							<tbody>
								<tr>
									<td><b>Name</b></td>
									<td><?= $data['title']; ?> <?= $data['name']; ?></td>
								</tr>
								<tr>
									<td><b>Position</b></td>
									<td><?= $data['position']; ?></td>
								</tr>
								<tr>
									<td><b>Mobile</b></td>
									<td><?= $data['mobile_number']; ?></td>
								</tr>
								<tr>
									<td><b>Email</b></td>
									<td><?= $data['email_address']; ?></td>
								</tr>
								<tr>
									<td><b>Company</b></td>
									<td><?= $data['company_name']; ?></td>
								</tr>
								<tr>
									<td><b>Total Employees</b></td>
									<td><?= $data['empoloyees_count']; ?></td>
								</tr>
								<tr>
									<td><b>Address</b></td>
									<td><?= preg_replace('/(, )\\1+/', '$1',($data['building_name']." ". $data['house_number'].", ".$data['building_name'].", ".$data['building_name'].", ".$data['street_address'].", ".$data['state'].", ".$data['city'].", ".$data['country'].' '.$data['postal'])); ?></td>
								</tr>
								<tr>
									<td><b>Office Number</b></td>
									<td><?= $data['office_number']; ?></td>
								</tr>
								<tr>
									<td><b>Office FAX</b></td>
									<td><?= $data['fax_number']; ?></td>
								</tr>
								<tr>
									<td><b>Website</b></td>
									<td><?= $data['website']; ?></td>
								</tr>
								<tr>
									<td><b>Request Date</b></td>
									<td><?= unix_to_human((human_to_unix($data['request_date']))); ?></td>

								</tr>
								<tr>
									<td colspan="2">
									<div class="article-post pull-left">
										<div class="fr"><a onclick="window.history.back();" class="btn btn-info btn-mini">Back</a>&emsp;<a href="<?= base_url(); ?>admin/process_request/<?= $data['request_id']; ?>" class="btn btn-primary btn-mini">Create Company Account</a>&emsp;<a href="#" class="btn btn-danger btn-mini confirm" onclick="" title="<b>Are you sure to delete!</b>">Delete request</a></div>
									</div>
									</td>
								</tr>
							</tbody>
						</table>
						
                    </div>
                </div>
			</div>
        </div>
    </div>
</div>