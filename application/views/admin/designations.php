<div id="content">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title">
                        <span class="icon"><i class="icon-th"></i></span>
                       <div class="span4"><h5><a href="newdesignation">+ Add New</a></h5></div><div class="span4"><h6>Manage Designation</h6></div>
						
                    </div>
                    <div class="widget-content nopadding">
						<?php
							echo show_err_msg($this->session->flashdata('error_msg'));
							echo show_succ_msg($this->session->flashdata('success_msg'));
						?>
                        <table class="table table-bordered data-table">
                            <thead>
                                <tr>
                                    <th width="10%">#</th>
                                    <th width="75%">Designation</th>
                                    <th width="15%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
						<?php $i = 1; 
							foreach($data as $row){ ?>
                                <tr class="gradeX">
                                    <td><?= $i; ?></td>
                                    <td><?= $row['designation']; ?></td>
                                    <td class="center">
                                        <div class="article-post">
                                            <div class="fr"><a href="editdesignation/<?= $row['designation_id']; ?>" class="btn btn-primary btn-mini">Edit</a> <a href="delete_designation/<?= $row['designation_id']; ?>" class="btn btn-danger btn-mini confirm" title="<b>Are you sure to delete!</b>">Delete</a>
										</div>
                                        </div>
                                    </td>
                                </tr>
						<?php	
								$i++;
							} ?>
							</tbody>
                        </table>
                    </div>
                </div>
			</div>
        </div>
    </div>
</div>