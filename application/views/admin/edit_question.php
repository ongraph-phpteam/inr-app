<div id="content">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span6">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
                        <h5>Update Question</h5>
                    </div>
                    <div class="widget-content nopadding">
						<?php
							echo show_err_msg($error_msg);
						?>
                        <form action="" method="post" class="form-horizontal" >
							<input type="hidden" name="question_id" value="<?php echo $data['question_id']; ?>" />
                            <div class="control-group">
                                <label class="control-label">Question:</label>
                                <div class="controls">
                                    <textarea name="question" id="required" class="span11" placeholder="Security Question" ><?php echo $data['question']; ?></textarea>
                                </div>
                            </div>
                            <div class="form-actions">
                                <input type="submit" name="submit" value="Update Question" class="btn btn-success">
								<input type="reset" onclick="window.history.back();" value="Cancel" class="btn btn-warning">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
		</div>
        <hr>
    </div>
</div>
</div>