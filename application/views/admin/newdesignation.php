<div id="content">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span6">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
                        <h5>Add Designation</h5>
                    </div>
                    <div class="widget-content nopadding">
						<?php
							echo show_err_msg($error_msg);
						?>
						
                        <form action="" method="post" class="form-horizontal" >
                            <div class="control-group">
                                <label class="control-label">Designation:</label>
                                <div class="controls">
                                    <input type="text" name="designation" value="<?php echo set_value('designation'); ?>" id="required" class="span11" placeholder="Designation name"  />
                                </div>
                            </div>
                            <div class="form-actions">
                                <input type="submit" name="submit" value="Submit Data" class="btn btn-success">
								<input type="reset" name="reset" value="Reset" class="btn btn-warning">
								<input type="reset" onclick="window.history.back();" value="Cancel" class="btn btn-danger">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
		</div>
        <hr>
    </div>
</div>
</div>