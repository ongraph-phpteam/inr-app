<div id="content">
	
    <div class="container-fluid">
		
        <div class="row-fluid">
            <div class="span12">
				<div class="widget-box">
				    <div class="widget-title">
                        <span class="icon"><i class="icon-th"></i></span>
                        <div class="span4"><h5><a href="new_question">+ New Question</a></h5></div><div class="span4"><h6>Manage Security Questions</h6></div>
                    </div>
                    <div class="widget-content nopadding">
						<?php
							echo show_err_msg($this->session->flashdata('error_msg'));
							echo show_succ_msg($this->session->flashdata('success_msg'));
					
						?>
                        <table class="table table-bordered data-table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Question</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
						<?php 	$x = 0;
								foreach($data as $row){ ?>
                                <tr class="gradeX">
                                    <td><?= ($x+1); ?></td>
                                    <td><?= $row['question']; ?> 
									<td class="center">
                                        <div class="article-post">
                                            <div class="fr">
											<a href="edit_question/<?= $row['question_id']; ?>" class="btn btn-primary btn-mini">Edit</a></div>
                                        </div>
                                    </td>
                                </tr>
						<?php $x++;
								} ?>
							</tbody>
                        </table>
                    </div>
                </div>
			</div>
        </div>
    </div>
</div>
