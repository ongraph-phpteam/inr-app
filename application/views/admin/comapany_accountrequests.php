<div id="content">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title">
                        <span class="icon"><i class="icon-th"></i></span>
                        <h5>New Company Account Requests:</h5>
                    </div>
                    <div class="widget-content nopadding">
						<?php
							echo show_err_msg($this->session->flashdata('error_msg'));
							echo show_succ_msg($this->session->flashdata('success_msg'));
						?>
                        <table class="table table-bordered data-table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Mobile</th>
                                    <th>Email</th>
                                    <th>Company</th>
                                    <th>Address</th>
                                    <th>Employees</th>
                                    <th>Requested</th>
                                    <th width="15%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
						<?php $i = 1; 
							foreach($data as $row){ ?>
                                <tr class="gradeX">
                                    <td><?= $i; ?></td>
                                    <td><?= $row['name']; ?><br><?= $row['title']; ?></td>
                                    <td><?= $row['mobile_number']; ?></td>
                                    <td><?= $row['email_address']; ?></td>
                                    <td><?= $row['company_name']; ?></td>
                                    <td><?= preg_replace('/(, )\\1+/', '$1',($row['building_name']." ". $row['house_number'].", ".$row['building_name'].", ".$row['building_name'].", ".$row['street_address'].", ".$row['state'].", ".$row['city'].", ".$row['country'].' '.$row['postal'])); ?></td>
                                    <td><?= $row['empoloyees_count']; ?></td>
                                    <td><?= unix_to_human((human_to_unix($row['request_date']))); ?></td>
                                    <td class="center">
                                        <div class="article-post">
                                            <div class="fr"><a href="viewrequest/<?= $row['request_id']; ?>" class="btn btn-info btn-mini">View</a> <a href="<?= base_url(); ?>admin/process_request/<?= $row['request_id']; ?>" class="btn btn-primary btn-mini">Create</a> <a href="#" class="btn btn-danger btn-mini confirm" onclick="" title="<b>Are you sure to delete!</b>">Delete</a></div>
                                        </div>
                                    </td>
                                </tr>
						<?php	
								$i++;
							} ?>
							</tbody>
                        </table>
                    </div>
                </div>
			</div>
        </div>
    </div>
</div>