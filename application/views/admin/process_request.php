<div id="content">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span6">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
                        <h5>Create Requested Company Account </h5>
                    </div>
                    <div class="widget-content nopadding">
						<?php
							echo show_err_msg($error_msg);
						?>
                        <form action="" method="post" class="form-horizontal"  enctype="multipart/form-data" >
							<div class="control-group">
                                <label class="control-label">Company Name :</label>
                                <div class="controls">
                                    <input type="text" name="company_name" value="<?php echo $_POST ? set_value('company_name') : $data['company_name']; ?>" id="required" class="span11" placeholder="Company name" required />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Company Domain :</label>
                                <div class="controls">
                                    <input type="text" name="domain" value="<?php echo set_value('domain'); ?>" class="span11" placeholder="Company Domain eg. gmail.com" required />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Contact Person</label>
                                <div class="controls">
                                    <input type="text" name="contact_person" value="<?php echo $_POST ? set_value('contact_person') : $data['name']; ?>" class="span11" placeholder="Contact Person" required />
                                </div>
                            </div>
							<div class="control-group">
                                <label class="control-label">Contact Email</label>
                                <div class="controls">
                                    <input type="email" name="contact_email" value="<?php  echo $_POST ? set_value('contact_email') : $data['email_address']; ?>" class="span11" placeholder="Contact Email" required />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Contact Number</label>
                                <div class="controls">
                                    <input type="text" name="contact_phone" value="<?php echo $_POST ? set_value('contact_phone') : $data['mobile_number']; ?>" class="span11" placeholder="Contact Number" required />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Company Address :</label>
                                <div class="controls">
                                    <input type="text" name="company_address" value="<?php echo $_POST ? set_value('company_address') : $data['house_number']." ".$data['street_address']; ?>" class="span11" placeholder="Company Address" required />
                                </div>
                            </div>
							<div class="control-group">
                                <label class="control-label">City:</label>
                                <div class="controls">
                                    <input type="text" name="city" value="<?php echo $_POST ? set_value('city') : $data['city']; ?>" class="span11" placeholder="City Name" required />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">State:</label>
                                <div class="controls">
                                    <input type="text" name="state" value="<?php echo $_POST ? set_value('state') : $data['state']; ?>" class="span11" placeholder="State Name" required />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Zipcode:</label>
                                <div class="controls">
                                    <input type="text" name="zipcode" value="<?php echo $_POST ? set_value('zipcode') : $data['postal']; ?>" class="span11" placeholder="Zipcode" required />
                                </div>
                            </div>
							<div class="control-group">
                                <label class="control-label">Country:</label>
                                <div class="controls">
                                    <input type="text" name="country" value="<?php echo $_POST ? set_value('country') : $data['country']; ?>" class="span11" placeholder="Country Name" required />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Company Logo:</label>
                                <div class="controls">
                                    <input type="file" name="company_logo" class="span11" />
                                    <span class="help-block">(Only .jpeg, .jpg, .png format supported)</span>
								</div>
								
								<label class="control-label">Uploaded Logo:</label>
                                <div class="controls">
                                    <span class="help-block"><img src="<?= $data['company_logo']; ?>" width="100px"></span>
								</div>
								
                            </div>
                            <div class="form-actions">
                                <input type="submit" name="submit" value="Create Company Account" class="btn btn-success">
								<input type="reset" onclick="window.history.back();" value="Cancel" class="btn btn-warning">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
		</div>
        <hr>
    </div>
</div>
</div>