<!--Header-part-->
<div id="header">
    <h1><a href="dashboard.html">INR Super Admin</a></h1>
</div>
<!--close-Header-part-->
<!--top-Header-menu-->
<div id="user-nav" class="navbar navbar-inverse">
    <ul class="nav">
        <li class=""><a title="" href="#"><i class="icon icon-user"></i> <span class="text">Profile</span></a></li>
        <li class=""><a title="" href="Auth/logout"><i class="icon icon-share-alt"></i> <span class="text">Logout</span></a></li>
    </ul>
</div>
<!--close-top-Header-menu-->

<div id="sidebar">
    <ul>
        <li class="active"><a href="<?= base_url(); ?>admin/dashboard"><i class="icon icon-home"></i> <span>Dashboard</span></a> </li>
        
		<li> <a href="<?= base_url(); ?>admin/companies"><i class="icon icon-signal"></i> <span>Manage Companies</span></a> </li>
		
		<li><a href="<?= base_url(); ?>admin/designations"><i class="icon icon-fullscreen"></i> <span>Manage Designations</span></a></li>
        
		<li> <a href="<?= base_url(); ?>admin/accountrequest"><i class="icon icon-inbox"></i> <span>Company Account Requests</span></a> </li>
		
       <li> <a href="<?= base_url(); ?>admin/questions"><i class="icon icon-inbox"></i> <span>Security Questions</span></a> </li>
      
       <li> <a href="<?= base_url(); ?>admin/not_allocated"><i class="icon icon-user"></i> <span>Not Allocated</span></a> </li>
       

    </ul>
</div>