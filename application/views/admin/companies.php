<div id="content">
	
    <div class="container-fluid">
		
        <div class="row-fluid">
            <div class="span12">
				<div class="widget-box">
				    <div class="widget-title">
                        <span class="icon"><i class="icon-th"></i></span>
                        <div class="span4"><h5><a href="newcompany">+ New Company</a></h5></div><div class="span4"><h6>Manage Companies</h6></div>
                    </div>
                    <div class="widget-content nopadding">
						<?php
							echo show_err_msg($this->session->flashdata('error_msg'));
							echo show_succ_msg($this->session->flashdata('success_msg'));
					
						?>
                        <table class="table table-bordered data-table">
                            <thead>
                                <tr>
                                    <th>Company Name</th>
                                    <th>Domain</th>
                                    <th>Contact Person</th>
                                    <th>Address</th>
                                    <th>Total Users</th>
                                    <th>Status</th>
                                    <th>Masquerade</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
						<?php foreach($data as $row){ ?>
                                <tr class="gradeX">
                                    <td>
										<div class="span5">
											<img src="<?= base_url().$row['company_logo']; ?>"  style="background-color:#c6c6c6; height:60px; width:120px" alt="No Logo">
										</div>
										<div class="span7">
											<p class="top-padding-name"><?= $row['company_name']; ?></p>
										</div>
									</td>
                                    <td><?= $row['domain']; ?></td>
                                    <td><?= $row['contact_person']; ?><br/><?= $row['contact_email']; ?><br /><?= sprintf("%+d",$row['country_code'])." ".$row['contact_phone']; ?> 
                                    </td>
                                    <td><?= $row['company_address']; ?>, <?= $row['city']; ?>, <?= $row['state']; ?>, <?= $row['zipcode']; ?>, <?= $row['country']; ?> </td>
                                    <td class="center"><?= $row['totalusers']; ?></td>
                                    <td class="center"><?= $row['is_active']==1 ? "<font color='green'><b>Active</b></font>" : "<font color='red'><b>In-active</b></font>"; ?></td>
                                    <td class="center"><div class="article-post">
										<div class="fr"><a  href="<?= base_url()?>company/masquerade/<?= urlencode(helper_aesEncryption($this->secret,$row['company_id'])); ?>" class="btn btn-info btn-mini" title="Login as company Admin!" target="_blank" >Login as Admin</a></div>
                                    </div></td>
									
                                    <td class="center">
                                        <div class="article-post">
                                            <div class="fr">
											<a href="updateinfo/<?= $row['company_id']; ?>" class="btn btn-primary btn-mini">Edit</a> <a href="#" class="btn btn-warning btn-mini confirm" onclick=")" title="<b>Are you sure to block!</b>">Block</a> <!--a href="#" class="btn btn-danger btn-mini confirm" onclick=")" title="<b>Are you sure to delete!</b>">Delete</a--></div>
                                        </div>
                                    </td>
                                </tr>
						<?php	} ?>
							</tbody>
                        </table>
                    </div>
                </div>
			</div>
        </div>
    </div>
</div>
