<div id="content">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title">
                        <span class="icon"><i class="icon-th"></i></span>
                       <div class="span5"><h6>Not Allocated Users</h6></div>
                    </div>
                    <div class="widget-content nopadding">
						<?php
							echo show_err_msg($this->session->flashdata('error_msg'));
							echo show_succ_msg($this->session->flashdata('success_msg'));
						?>
                        <table class="table table-bordered data-table">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Mobile</th>
                                    <th>Position</th>
                                    <th>Address</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
					<?php 	$id = 1;
							foreach($data as $row){ ?>
                                <tr id='<?= $id; ?>' class="gradeX">
                                    <td>
										<div class="span2">
											<?= $row['profile_pic'] ? '<img src="'.base_url().$row['profile_pic'].'" height="100px" width="100px" onerror="this.src=\''.base_url().'assets/img/dummy.svg\'" style="background-color:#c6c6c6;">' : '<img src="'.base_url().'assets/img/dummy.svg" height="100px" width="100px" style="background-color:#c6c6c6;">' ?>
										</div>
										<div class="span10">
											<p class="top-padding-name">
												<?= $row['name']." ".$row['last_name']; ?>
											</p>
										</div>
									</td>
                                    <td><?= $row['email_address']; ?></td>
                                    <td><?= ltrim(sprintf("%+d",$row['mobile_country_code']).'-'.$row['mobile_number'],'-'); ?> 
                                    </td>
                                    <td><?= $row['position']; ?></td>
                                    <td><?= $row['house_number'].", ".$row['street_address'].", ".$row['city'].", ".$row['state'].", ".$row['country']."-".$row['postal']; ?></td>
                                    <td class="center">
                                        <div class="article-post">
                                            <div class="fr"> <a class="btn btn-danger btn-mini confirm" id="" onclick="()" title="<b>Are you sure to delete!</b>" >Delete</a></div>
                                        </div>
                                    </td>
                                </tr>
					<?php	$id++;
							} ?>
							</tbody>
                        </table>
                    </div>
                </div>
			</div>
        </div>
    </div>
</div>