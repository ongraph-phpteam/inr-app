<div class="row-fluid">
  <div id="footer" class="span12"> <?= date('Y'); ?> &copy; INR <a href="http://inr-circle.io">inr-circle.io</a> </div>
</div>
<script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script> 
<script src="<?php echo base_url(); ?>assets/js/excanvas.min.js"></script> 
<script src="<?php echo base_url(); ?>assets/js/jquery.ui.custom.js"></script> 
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script> 
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.js"></script> 
<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script> 
<script src="<?php echo base_url(); ?>assets/js/jquery.flot.min.js"></script> 
<script src="<?php echo base_url(); ?>assets/js/jquery.flot.resize.min.js"></script> 
<script src="<?php echo base_url(); ?>assets/js/jquery.peity.min.js"></script> 
<script src="<?php echo base_url(); ?>assets/js/fullcalendar.min.js"></script> 
<script src="<?php echo base_url(); ?>assets/js/jquery.uniform.js"></script> 
<script src="<?php echo base_url(); ?>assets/js/select2.min.js"></script> 
<script src="<?php echo base_url(); ?>assets/js/jquery.validate.js"></script> 
<script src="<?php echo base_url(); ?>assets/js/jquery.dataTables.min.js"></script> 
<script src="<?php echo base_url(); ?>assets/js/maruti.js"></script> 
<script src="<?php echo base_url(); ?>assets/js/dashboard.js"></script> 
<script src="<?php echo base_url(); ?>assets/js/chat.js"></script> 
<script src="<?php echo base_url(); ?>assets/js/tables.js"></script> 
<script src="<?php echo base_url(); ?>assets/js/jquery.popconfirm.js"></script> 
<script src="<?php echo base_url(); ?>assets/js/multiselect.js"></script> 
<script src="<?php echo base_url(); ?>assets/js/common.js"></script>  


<script type="text/javascript">
	// This function is called from the pop-up menus to transfer to
	// a different page. Ignore if the value returned is a null string:
	function goPage (newURL) {
		// if url is empty, skip the menu dividers and reset the menu selection to default
		if (newURL != "") {
	  
			// if url is "-", it is this page -- reset the menu:
			if (newURL == "-" ) {
				resetMenu();            
			} 
			// else, send page to designated URL            
			else {  
				document.location.href = newURL;
			}
		}
	}

	// resets the menu selection upon entry to this page:
	function resetMenu() {
	   document.gomenu.selector.selectedIndex =1;
	}
</script>
