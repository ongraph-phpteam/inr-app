<div id="content">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span6">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
                        <h5>Add New User</h5>
                    </div>
                    <div class="widget-content nopadding">
						<?php
							echo show_err_msg($error_msg);
						?>
						
                        <form action="" method="post" class="form-horizontal"  enctype="multipart/form-data" >
							<input type="hidden" name="company_id" value="<?= $company_id?>">
                            <div class="control-group">
                                <label class="control-label">Full Name :</label>
                                <div class="controls">
                                    <input type="text" name="user_name" value="<?php echo set_value('user_name'); ?>" id="required" class="span11" placeholder="User full name" required  />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Email :</label>
                                <div class="controls">
                                    <input type="email" name="email" value="<?php echo set_value('email'); ?>" class="span11" placeholder="User email" required />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Mobile Number</label>
                                <div class="controls">
									<select class="span11" name="country_code" required >
										<option value="">Select Country Code</option>
                            <?php	foreach($country_code as $code){  ?>		
										<option value="<?= $code['country_code']; ?>"><?= $code['countries_name']; ?> (<?= $code['country_code']; ?>)</option>
							<?php	} ?>	
									</select>
									<input type="number" name="mobile_number" value="<?php echo set_value('mobile_number'); ?>" class="span11" placeholder="Mobile Number" required />
									
								</div>
							</div>
					
                            <div class="control-group">
								<label class="control-label">Designation</label>
								<div class="controls">
									<select class="span11" name="designation" required >
										<option value="">Select an option</option>
							<?php	foreach($designations as $row){  ?>		
										<option value="<?= $row['designation_id']; ?>"><?= $row['designation']; ?></option>
							<?php	} ?>	
									</select>
								</div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Profile Photo:</label>
                                <div class="controls">
                                    <input type="file" name="profile_picture" class="span11" />
                                    <span class="help-block">(Only .jpeg, .jpg, .png format supported)</span> </div>
                            </div>
                            <div class="form-actions">
                                <input type="submit" name="submit" value="Submit Data" class="btn btn-success">
								<input type="reset" name="reset" value="Reset" class="btn btn-warning">
								<input type="reset" onclick="window.history.back();" value="Cancel" class="btn btn-danger">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
		</div>
        <hr>
    </div>
</div>
</div>