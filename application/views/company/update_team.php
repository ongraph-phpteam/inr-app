<div id="content">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span6">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
                        <h5>New Team</h5>
                    </div>
                    <div class="widget-content nopadding">
						<?php
							echo show_err_msg($error_msg);
						?>
						
                        <form action="" method="post" class="form-horizontal" enctype="multipart/form-data" >
                            <div class="control-group">
                                <label class="control-label">Team Name:</label>
                                <div class="controls">
                                    <textarea type="text" name="team_name" id="team_name" class="span11" placeholder="Team name" required ><?= $edit['team_name'] ? $edit['team_name'] : set_value('team_name'); ?></textarea>
                                </div>
                            </div>
							<div class="control-group">
								<label class="control-label">Briefing Validity</label>
								<div class="controls">
									<label>
									<input type="radio" name="briefing_validity"  value="7" <?php if ($edit['briefing_validity']==7) echo 'checked'; ?> required />
									7 Days</label>
									<label>
									<input type="radio" name="briefing_validity" value="5" <?php if ($edit['briefing_validity']==5) echo 'checked'; ?>/>
									5 Days</label>
									<label>
									<input type="radio" name="briefing_validity" value="2" <?php if ($edit['briefing_validity']==2) echo 'checked'; ?> />
									2 Days</label>
									<label>
									<input type="radio" name="briefing_validity" value="0" <?php if ($edit['briefing_validity']==0) echo 'checked'; ?> />
									Never</label>
								</div>
							</div>
							
							<div class="control-group">
								<label class="control-label">Upload Cover Picture</label>
								<div class="controls">
									<input type="file" name="team_coverphoto" accept="image/x-png,image/jpeg" />
									<p><font color="brown">Note: Only .png,.jpg,.jpeg format supported</font></p>
								</div>
						<?php  if($edit['team_coverphoto']){ ?>
								<label class="control-label">Uploaded Picture</label>
								<div class="controls">
									<img src="<?= $edit['team_coverphoto']; ?>" width="60px">
								</div>
						<?php	} ?>
							</div>
							<div class="form-actions">
                                <input type="submit" name="submit" value="Update Data" class="btn btn-success">
								<input type="reset" name="reset" value="Reset" class="btn btn-warning">
								<input type="reset" onclick="window.history.back();" value="Cancel" class="btn btn-danger">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
		</div>
        <hr>
    </div>
</div>
</div>