<div id="content">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span6">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
                        <h5>Update News Channel</h5>
                    </div>
                    <div class="widget-content nopadding">
						<?php
							echo show_err_msg($error_msg);
						?>
						<form action="" method="post" class="form-horizontal" enctype="multipart/form-data" >
                            <div class="control-group">
                                <label class="control-label">Channel Name:</label>
                                <div class="controls">
                                    <textarea type="text" name="channel_name" id="channel_name" class="span11" placeholder="Channel name" required ><?= $edit['channel_name'] ? $edit['channel_name'] : set_value('channel_name'); ?></textarea>
                                </div>
                            </div>
							<div class="control-group">
								<label class="control-label">News Channel Logo</label>
								<div class="controls">
									<input type="file" name="channel_logo" accept="image/x-png,image/jpeg"  />
									<p><font color="brown">Note: Only .png,.jpg,.jpeg format supported</font></p>
								</div>
						<?php 	if($edit['channel_logo']){ ?>
								<label class="control-label">Uploaded Logo</label>
								<div class="controls">
									<img src='<?= $edit['channel_logo']; ?>' width="50px">
								</div>
						<?php	}	?>
							</div>
							<div class="control-group">
								<label class="control-label">Channel Type</label>
								<div class="controls">
									<label>
										<input type="radio" <?= $edit['type']==2 ? "checked" : ""; ?> name="type" value="2" required /> Mandatory Channel
									</label>
									<label>
										<input type="radio" <?= $edit['type']==1 ? "checked" : ""; ?> name="type" value="1" /> Optional Channel
									</label>
									<p><font color="brown">Note: Mandatory Channel will be joined automatically.</font></p>
									
								</div>
							</div>
							
							
							<div class="form-actions">
                                <input type="submit" name="submit" value="Submit Data" class="btn btn-success">
								<input type="reset" name="reset" value="Reset" class="btn btn-warning">
								<input type="reset" onclick="window.history.back();" value="Cancel" class="btn btn-danger">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
		</div>
        <hr>
    </div>
</div>
</div>