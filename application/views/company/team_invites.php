<div id="content">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title">
                        <span class="icon" onclick="window.history.back();"><i class="icon-back"></i></span>
                        <div class="span4"><h5><a href="../invite_members/<?= $id; ?>">+ Invite Members</a></h5></div><div class="span4"><h6><?= $team_name; ?> - Team Members Invites</h6></div>
						
                    </div>
                    <div class="widget-content nopadding">
						<?php
							echo show_err_msg($this->session->flashdata('error_msg'));
							echo show_succ_msg($this->session->flashdata('success_msg'));
						?>
                        <table class="table table-bordered data-table">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Position</th>
                                    <th>Invite Date</th>
                                    <th width="10%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
						<?php $x= 1;
								foreach($data as $row){ ?>
                                <tr id="<?= $x; ?>" class="gradeX">
                                    <td>
										<div class="span2">
											<?= $row['profile_picture'] ? '<img src="'.$row['profile_picture'].'" height="100px" width="100px" onerror="this.src=\''.base_url().'assets/img/dummy.svg\'" style="background-color:#c6c6c6;">' : '<img src="'.base_url().'assets/img/dummy.svg" height="100px" width="100px" style="background-color:#c6c6c6;">' ?>
										</div>
										<div class="span10">
											<p class="top-padding-name">
												<?= $row['user_name']; ?>
											</p>
										</div>
									</td>
                                    <td><?= $row['email']; ?></td>
                                    <td><?= $row['designation'] ? $row['designation'] : "N/A" ; ?></td>
                                    <td class="center"><?= unix_to_human((human_to_unix($row['invited_on']))); ?></td>
                                    <td class="center">
                                        <div class="article-post">
                                            <div class="fr"><a class="btn btn-warning btn-mini confirm" onclick="delete_invites(<?= $x; ?>,'<?= urlencode(helper_aesEncryption($this->secret,$row['invite_id'])); ?>','<?= urlencode(helper_aesEncryption($this->secret,$row['team_id'])); ?>')" title="<b>Are you sure to delete!</b>">Delete Invite</a></div>
                                        </div>
                                    </td>
                                </tr>
						<?php $x++;
								} ?>
							</tbody>
                        </table>
                    </div>
                </div>
			</div>
        </div>
    </div>
</div>