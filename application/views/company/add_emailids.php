<div id="content">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span6">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
                        <h5>Add allowed email ids for sign-up</h5>
                    </div>
                    <div class="widget-content nopadding">
						<?php
							echo show_err_msg($error_msg);
						?>
						
                        <form action="" method="post" class="form-horizontal"  enctype="multipart/form-data" >
							<div class="control-group">
                                <label class="control-label">Email:</label>
                                <div class="controls">
                                    <input type="text" name="email" value="<?php echo set_value('email'); ?>" id="required" class="span11" placeholder="Email ids e.g. abc@gmail.com,xyx@gmail,..." required  />
									<span class="help-block">For multiple email please separate by comma(,) without any space</span>
                                </div>
                            </div>
                            <div class="form-actions">
                                <input type="submit" name="submit" value="Submit Data" class="btn btn-success">
								<input type="reset" onclick="window.history.back();" value="Cancel" class="btn btn-danger">
                            </div>
                        </form>
						<h4 class='or_devider'>Or upload CSV file</h4>
						<form action="" method="post" class="form-horizontal"  enctype="multipart/form-data" >
							<div class="control-group">
                                <label class="control-label">Upload CSV File:</label>
                                <div class="controls">
                                    <input type="file" name="csv" class="span11" required />
                                    <span class="help-block">Please check the demo format: <a href="../media/csv/demoformat.csv">download format </a></span> </div>
                            </div>
                            <div class="form-actions">
                                <input type="submit" name="submit" value="Upload File" class="btn btn-success">
								<input type="reset" onclick="window.history.back();" value="Cancel" class="btn btn-danger">
                            </div>
                        </form>
						
                    </div>
                </div>
            </div>
		</div>
        <hr>
    </div>
</div>
</div>