<div id="content">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span6">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
                        <h5>Update Business Card Template</h5>
                    </div>
                    <div class="widget-content nopadding">
						<?php
							echo show_err_msg($error_msg);
						?>
						
                        <form action="" method="post" class="form-horizontal" enctype="multipart/form-data" >
                            <div class="control-group">
								<label class="control-label">New Background Layer</label>
								<div class="controls">
									<input type="file" name="background_layer" />
									<p>(Business card background image)</p>
								</div>
								<label class="control-label">Uploaded Background Layer</label>
								<div class="controls">
									<img src='<?= base_url().$edit['background_layer']; ?>' width="200px">
								</div>
							</div>
							<div class="control-group">
								<label class="control-label">New Front Layer</label>
								<div class="controls">
									<input type="file" name="front_layer" />
									<p>(Business card front image)</p>
								</div>
								<label class="control-label">Uploaded Front Layer</label>
								<div class="controls">
									<img src='<?= base_url().$edit['front_layer']; ?>' width="200px" style="background-color:#c6c6c6;">
								</div>
							</div>
							
							<div class="form-actions">
                                <input type="submit" name="submit" value="Submit Data" class="btn btn-success">
								<input type="reset" onclick="window.history.back();" value="Cancel" class="btn btn-danger">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
		</div>
        <hr>
    </div>
</div>
</div>