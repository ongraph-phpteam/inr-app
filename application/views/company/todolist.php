<div id="content">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title">
                        <span class="icon"><i class="icon-th"></i></span>
                        <div class="span5"><h5><a href="new_team"></a></h5></div><div class="span4"><h6>Manage Todolist</h6></div>
                    </div>
                    <div class="widget-content nopadding">
						<?php
							echo show_err_msg($this->session->flashdata('error_msg'));
							echo show_succ_msg($this->session->flashdata('success_msg'));
					
						?>
                        <table class="table table-bordered data-table">
                            <thead>
                                <tr>
                                    <th width="3%" >#</th>
                                    <th >Team Name</th>
                                    <th >Accountability</th>
                                    <th >Task</th>
                                    <th >Description</th>
                                    <th >Assigned By</th>
                                    <th >Deadline</th>
                                    <th >Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
						<?php $x =1;
								foreach($data as $row){ ?>
                                <tr id="<?= $x; ?>" class="gradeX">
									<td><?= $x; ?></td>
                                    <td class="center"><?= $row['team_name']; ?></td>
									
                                    <td>
									
									<div class="span2">
										<?= $row['profile_picture'] ? '<img src="'.$row['profile_picture'].'" height="100" width="100" onerror="this.src=\''.base_url().'assets/img/dummy.svg\'" style="background-color:#c6c6c6;">' : '<img src="'.base_url().'assets/img/dummy.svg" height="100" width="100" style="background-color:#c6c6c6;">' ?>
									</div>
									<div class="span10">
										<p class=""><?= $row['user_name']."-".$row['designation']; ?><br/> <?= $row['email']; ?></p>
									</div>
									</td>
									
									<td><?= $row['action_name']; ?></td>
                                    <td><?= $row['action_description']; ?></td>
                                    <td><?= $row['assigner_name']; ?></td>
									<td><?= $row['deadline']; ?></td>
									<td><b><?= $row['status']==0 ? "Working" : "Accomplished"; ?></b></td>
									<td class="center">
                                        <div class="article-post">
                                            <div class="fr">
												<a  class="btn btn-danger btn-mini confirm" onclick="delete_assignedTask(<?= $x; ?>,'<?= urlencode(helper_aesEncryption($this->secret,$row['todoid'])); ?>','<?= urlencode(helper_aesEncryption($this->secret,$row['team_id'])); ?>')" title="<b>Are you sure to delete!</b>">Delete</a>
											</div>
                                        </div>
                                    </td>
                                </tr>
						<?php $x++;
								} ?>
							</tbody>
                        </table>
                    </div>
                </div>
			</div>
        </div>
    </div>
</div>