<div id="content">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span6">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
                        <h5>New Business Card Template</h5>
                    </div>
                    <div class="widget-content nopadding">
						<?php
							echo show_err_msg($error_msg);
						?>
						
                        <form action="" method="post" class="form-horizontal" enctype="multipart/form-data" >
                            <div class="control-group">
								<label class="control-label">Background Layer</label>
								<div class="controls">
									<input type="file" name="background_layer" required  />
									<p>(Business card background image)</p>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label">Front Layer</label>
								<div class="controls">
									<input type="file" name="front_layer" required />
									<p>(Business card front image)</p>
								</div>
							</div>
							
							<div class="form-actions">
                                <input type="submit" name="submit" value="Submit Data" class="btn btn-success">
								<input type="reset" name="reset" value="Reset" class="btn btn-warning">
								<input type="reset" onclick="window.history.back();" value="Cancel" class="btn btn-danger">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
		</div>
        <hr>
    </div>
</div>
</div>