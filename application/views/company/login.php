<!DOCTYPE html>
<html lang="en">
    
<head>
	<title>INR Company Admin</title><meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" />
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap-responsive.min.css" />
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/login.css" />
</head>
    <body>
        <div id="loginbox">     
			
            <form id="loginform" class="form-vertical" method="post" action="<?php echo base_url('company/login'); ?>">
				<div class="control-group normal_text"><h3>INR Company Admin Login</h3></div>
				<?php
					echo show_err_msg($this->session->flashdata('error_msg'));
				?>
                <div class="control-group">
                    <div class="controls">
                        <div class="main_input_box">
                            <span class="add-on"><i class="icon-user"></i></span><input type="text" name="username" placeholder="Username"  required />
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <div class="main_input_box">
                            <span class="add-on"><i class="icon-lock"></i></span><input type="password" name="password" placeholder="Password" required/>
                        </div>
                    </div>
                </div>
				
                <div class="form-actions">
                    <span class="pull-left"><a  class="flip-link btn btn-inverse" id="to-recover">Lost password?</a></span>
                    <span class="pull-right"><input type="submit" class="btn btn-success" value="Login" /></span>
                </div>
            </form>
            <form id="recoverform" action="#" class="form-vertical">
				<p class="normal_text">Enter your e-mail address below and we will send you instructions how to recover a password.</p>
				
				<?php
					echo show_err_msg($this->session->flashdata('error_msg'));
				?>
				<div class="controls">
					<div class="main_input_box">
						<span class="add-on"><i class="icon-envelope"></i></span><input type="text" placeholder="E-mail address" />
					</div>
				</div>
               
                <div class="form-actions">
                    <span class="pull-left"><a class="flip-link btn btn-inverse" id="to-login">&laquo; Back to login</a></span>
                    <span class="pull-right"><input type="submit" class="btn btn-info" value="Recover" /></span>
                </div>
            </form>
        </div>
        
        <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>  
        <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>  
        <script src="<?php echo base_url(); ?>assets/js/login.js"></script> 
        <script src="<?php echo base_url(); ?>assets/js/interface.js"></script> 
    </body>

</html>
