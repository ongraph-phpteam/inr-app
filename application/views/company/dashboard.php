<div id="content">
    <div class="quick-actions_homepage">
        <div class="widget-box widget-plain">
            <div class="center">
                <ul class="stat-boxes">
                    <li>
                        <div class="left peity_line_neutral"><span><span style="display: none;">10,15,8,14,13,10,10,15</span>
                            <canvas width="50" height="24"></canvas>
                            </span>
                            <?= $data['total_users']; ?>
                        </div>
                        <a href="users">
                            <div class="right"> <strong><?= $data['total_users']; ?></strong> Total Users </div>
                        </a>
                    </li>
                    <li>
                        <div class="left peity_line_good"><span><span style="display: none;">12,6,9,23,14,10,17</span>
                            <canvas width="50" height="24"></canvas>
                            </span>
                            <?= $data['active_users']; ?>
                        </div>
                        <a href="users">
                            <div class="right"> <strong><?= $data['active_users']; ?></strong> Active Users </div>
                        </a>
                    </li>
                    <li>
                        <div class="left peity_bar_neutral">
                            <span>
								<span style="display: none;">2,4,9,7,12,10,12</span>
                            <canvas width="50" height="30"></canvas>
                            </span>
                            <?= $data['total_todo']; ?>
                        </div>
						<a href="todolist">
							<div class="right"> <strong><?= $data['total_todo']; ?></strong>Total Assigned Tasks </div>
						</a>
                    </li>

                    <li>
                        <div class="left peity_line_good"><span>12,6,9,23,14,10,5</span>
                            <?= $data['total_ttalks']; ?>
                        </div>
                        <a href="teams">
                            <div class="right"><strong><?= $data['total_ttalks']; ?></strong> Total Teams</div>
                        </a>
                    </li>

                    <li>
                        <div class="left peity_bar_good"><span>12,6,9,23,14,10,13</span>
                            <?= $data['total_newchannel']; ?>
                        </div>
                        <a href="news_channels">
                            <div class="right"> <strong><?= $data['total_newchannel']; ?></strong>Total News Channels</div>
                        </a>
                    </li>

                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"><span class="icon"><i class="icon-signal"></i></span>
                        <h5>User Growth</h5>
                        <div class="buttons"><a href="dashboard" class="btn btn-mini"><i class="icon-refresh"></i> Update stats</a></div>
                    </div>
                    <div class="widget-content">
                        <div class="row-fluid">
                            <div class="span12">
                                <div id="new_comp_reg" style="min-width: 193px; height: 400px; margin: 0 auto"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span6">
                        <div class="widget-box">
                            <div class="widget-title"><span class="icon"><i class="icon-file"></i></span>
                                <h5>Latest News Briefs</h5>
                            </div>
                            <div class="widget-content nopadding">
                                <ul class="recent-posts">
                    <?php 	if(count($news)){ ?>
                    <?php		foreach($news as $row){ ?>
									<li>
										<div class="user-thumb"> <img width="40" height="40" alt="No Image" src="<?= $row['channel_logo'] ? $row['channel_logo'] : ($row['picture'] ? base_url().$row['picture'] : base_url().'assets/img/dummy.svg'); ?>"> </div>
										<div class="article-post"> <span class="user-info"> By: <b><?= $row['user_name']?></b> / News Channel: <b><?= $row['channel_name']; ?></b> / Date: <?= unix_to_human((human_to_unix($row['published_at']))); ?> </span>
											<p>
												<?= substr($row['news'],0,70).(strlen($row['news'])>70 ? " [..]" : ""); ?>
											</p>
										</div>
									</li>
					<?php 		} ?>
					<?php 	}else{ ?>
									<li>
										<div class=""> </div>
										<div class="article-post">
											<p style="text-align:center;color:brown;">
												<br/>
												<br/>-- No News Briefs posted yet --
												<br/>
												<br/>
											</p>
										</div>
									</li>

					<?php	}	?>
					<?php 		if(count($news)){ ?>
									<li><a href="active_newsbriefs" class="btn btn-warning btn-mini center">Manage News Briefs</a></li>
					<?php 		}	 ?>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="span6">
                        <div class="widget-box">
                            <div class="widget-title"> <span class="icon"> <i class="icon-refresh"></i> </span>
                                <h5>Most Urgent Todolist</h5>
                            </div>
                            <div class="widget-content nopadding updates">
                <?php 	if(count($todo)){ ?>
                <?php		foreach($todo as $row){ ?>
								<div class="new-update clearfix"><i class="icon-ok-sign"></i>
									<div class="update-done">
										<strong>Accountability:</strong> <?= $row['user_name']; ?> / 
										<strong>Team:</strong> <?= $row['team_name']; ?> 
										<span><strong>Task:</strong> <?= $row['action_name']; ?></span>
										<span><?= substr($row['action_description'],0,50).(strlen($row['action_description'])>50 ? ' [..]':''); ?> </span>
									</div>
									<div class="update-date">Deadline
										<span class="update-day"><?= date("d",strtotime($row['deadline']))?></span>
										<?= date("M",strtotime($row['deadline']))?>
									</div>
								</div>
                <?php 		} ?>
                <?php 	}else{ ?>
								<div class="new-update clearfix">
									<p style="text-align:center;color:brown;">
										<br/>
										<br/>-- No Todolist to show --
										<br/>
										<br/>
									</p>
								</div>
                 <?php 	} ?>
				<?php 		if(count($todo)){?>
								<div class="new-update clearfix">
									<a href="todolist" class="btn btn-warning btn-mini center">Manage Todolist</a>
								</div>
					<?php	} ?>
							</div>
                        </div>
                    </div>
				</div>
			</div>
        </div>
        <hr>
    </div>
</div>