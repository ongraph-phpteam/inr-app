<div id="content">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title">
                        <span class="icon"><i class="icon-th"></i></span>
                       <div class="span5"><h5><a href="newuser">+ New User</a></h5></div><div class="span4"><h6>Manage Users</h6></div>
                    </div>
                    <div class="widget-content nopadding">
						<?php
							echo show_err_msg($this->session->flashdata('error_msg'));
							echo show_succ_msg($this->session->flashdata('success_msg'));
						?>
                        <table class="table table-bordered data-table">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Mobile</th>
                                    <th>Position</th>
                                    <th>Last Login</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
					<?php 	$id = 1;
							foreach($data as $row){ ?>
                                <tr id='<?= $id; ?>' class="gradeX">
                                    <td>
										<div class="span2">
											<?= $row['profile_picture'] ? '<img src="'.base_url().$row['profile_picture'].'" height="100px" width="100px" onerror="this.src=\''.base_url().'assets/img/dummy.svg\'" style="background-color:#c6c6c6;">' : '<img src="'.base_url().'assets/img/dummy.svg" height="100px" width="100px" style="background-color:#c6c6c6;">' ?>
										</div>
										<div class="span10">
											<p class="top-padding-name">
												<?= $row['user_name']; ?>
											</p>
										</div>
									</td>
                                    <td><?= $row['email']; ?></td>
                                    <td><?= ltrim($row['country_code'].'-'.$row['mobile_number'],'-'); ?> 
                                    </td>
                                    <td><?= $row['designation'] ? getDesignationdata($row['designation'],'designation') : "-NA-"; ?></td>
                                    <td class="center"><?= unix_to_human((human_to_unix($row['last_login']))); ?></td>
                                    <td class="center">
                                        <div class="article-post">
                                            <div class="fr"><a href="editprofile/<?= urlencode(helper_aesEncryption($this->secret,$row['user_id'])); ?>" class="btn btn-primary btn-mini">Edit</a> <a class="btn btn-warning btn-mini confirm" id='block<?= $id; ?>' onclick="blockUser(<?= $id; ?>,'<?= urlencode(helper_aesEncryption($this->secret,$row['user_id'])); ?>')" <?php if($row['is_active']==1){ ?>  title="<b>Are you sure to block!</b>" <?php }else{ ?>title="<b>Are you sure to unblock!</b>" <?php } ?> ><?php if($row['is_active']==1){ ?>Block<?php }else{ ?>Unblock<?php } ?></a> <a class="btn btn-danger btn-mini confirm" id='delete<?= $id; ?>' onclick="deleteUser(<?= $id; ?>,'<?= urlencode(helper_aesEncryption($this->secret,$row['user_id'])); ?>')" title="<b>Are you sure to delete!</b>" >Delete</a></div>
                                        </div>
                                    </td>
                                </tr>
					<?php	$id++;
							} ?>
							</tbody>
                        </table>
                    </div>
                </div>
			</div>
        </div>
    </div>
</div>