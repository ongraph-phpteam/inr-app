<div id="content">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title">
                        <span class="icon"><i class="icon-th"></i></span>
                        <h5>All Active News Briefs</h5>
                    </div>
                    <div class="widget-content nopadding">
						<?php
							echo show_err_msg($this->session->flashdata('error_msg'));
							echo show_succ_msg($this->session->flashdata('success_msg'));
					
						?>
                        <table class="table table-bordered data-table">
                            <thead>
                                <tr>
                                    <th width="25%">Publisher</th>
                                    <th width="15%">News Channel</th>
                                    <th width="40%">Post</th>
                                    <th width="12%">Published At</th>
                                    <th width="8%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
						<?php 	$x=1;
								foreach($data as $row){ ?>
                                <tr id="<?= $x; ?>" class="gradeX">
                                    <td class="center">
										<div class="span2">
											<img src="<?= $row['channel_logo'] ? $row['channel_logo'] : ($row['picture'] ? $row['picture'] : base_url().'assets/img/dummy.svg'); ?>" height="100px" width="100px" style="background-color:#c6c6c6;" alt="Cover picture">
										</div>
										<div class="span10">	
											<p class="">
												<?= $row['user_name']; ?><br/> <?= $row['email']; ?>
											</p>
										</div>
									</td>
									
                                    <td><?= $row['channel_name']; ?></td>
                                    <td><?php if($row['picture']){ ?><div class="span2"><img src="<?= $row['picture']; ?>" width="100%" style="background-color:#c6c6c6;" alt="image link broken" ></div><div class="span10"><?= $row['news']; ?></div><?php }else{ ?><div class="span12"><?= $row['news']; ?></div><?php } ?></td>
                                    <td class="center"><?= unix_to_human((human_to_unix($row['published_at']))); ?></td>
                                    <td class="center">
                                        <div class="article-post">
                                            <div class="fr"><a class="btn btn-primary btn-mini">Edit</a> <button class="btn btn-danger btn-mini confirm" onclick="deleteAnyChannelNewsBrief(<?= $x; ?>,'<?= urlencode(helper_aesEncryption($this->secret,$row['newsbrief_id'])); ?>')"  title="<b>Are you sure to delete!</b>">Delete</button></div>
                                        </div>
                                    </td>
                                </tr>
						<?php	$x++;
								} ?>
							</tbody>
                        </table>
                    </div>
                </div>
			</div>
        </div>
    </div>
</div>