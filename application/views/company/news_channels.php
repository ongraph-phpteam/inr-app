<div id="content">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title">
                        <span class="icon"><i class="icon-th"></i></span>
                        <div class="span5"><h5><a href="new_channel">+ New Channel</a></h5></div><div class="span4"><h6>Manage News Channels</h6></div>
                    </div>
                    <div class="widget-content nopadding">
						<?php
							echo show_err_msg($this->session->flashdata('error_msg'));
							echo show_succ_msg($this->session->flashdata('success_msg'));
					
						?>
                        <table class="table table-bordered data-table">
                            <thead>
                                <tr>
                                    <th width="3%" >#</th>
                                    <th width="40%">Channel Name</th>
                                    <th >Total Followers</th>
                                    <th >Total News Brief</th>
                                    <th >Channel Type</th>
                                    <th >Createdon</th>
                                    <th width="10%" >Action</th>
                                </tr>
                            </thead>
                            <tbody>
						<?php $x =1;
								foreach($data as $row){ ?>
                                <tr id="<?= $x; ?>" class="gradeX">
									<td><?= $x; ?></td>
                                    <td class="center">
									<div class="span2">
										<img src="<?= $row['channel_logo'] !="" ? $row['channel_logo'] : ($row['profile_picture'] ? $row['profile_picture'] : base_url().'assets/img/dummy.svg'); ?>" height="70px" width="70px" style="background-color:#c6c6c6;" alt="Cover picture">
									</div>
									<div class="span10">
										<p ><b><?= $row['user_name']." - ".$row['designation']; ?></b><br>
										<?= $row['channel_name']; ?><br />
										<?= $row['email']; ?>
										</p>
									</div>
									</td>
									
                                    <td><?= $row['subscribers_count']; ?> Follower(s)<br /><a href="channel_followers/<?= urlencode(helper_aesEncryption($this->secret,$row['channel_id'])); ?>" class="btn btn-primary btn-mini">View Followers</a></td>
									<td><?php if ($row['active_newsbrief']){ ?><?= $row['active_newsbrief']; ?> News Brief(s)<br /><a href="channel_newsbriefs/<?= urlencode(helper_aesEncryption($this->secret,$row['channel_id'])); ?>" class="btn btn-primary btn-mini">View News Briefs</a><?php } else{ ?><br/><b><font color="brown">No active News Brief</font><b/><?php } ?></td>
									<td class="center"><?= $row['type']==1 ? "Optional" : "Mandatory"; ?></td>
									<td class="center"><?= unix_to_human((human_to_unix($row['created_at']))); ?></td>
									<td class="center">
                                        <div class="article-post">
                                            <div class="fr"><a href="update_channel/<?= urlencode(helper_aesEncryption($this->secret,$row['channel_id'])); ?>" class="btn btn-primary btn-mini">Edit</a> <a class="btn btn-danger btn-mini confirm" onclick="delete_newsChannel(<?= $x; ?>,'<?= urlencode(helper_aesEncryption($this->secret,$row['channel_id'])); ?>')" title="<b>Are you sure to delete!</b>">Delete</a></div>
                                        </div>
                                    </td>
                                </tr>
						<?php $x++;
								} ?>
							</tbody>
                        </table>
                    </div>
                </div>
			</div>
        </div>
    </div>
</div>