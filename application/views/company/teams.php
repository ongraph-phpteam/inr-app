<div id="content">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title">
                        <span class="icon"><i class="icon-th"></i></span>
                        <div class="span5"><h5><a href="new_team">+ New Team</a></h5></div><div class="span4"><h6>Manage Teams</h6></div>
                    </div>
                    <div class="widget-content nopadding">
						<?php
							echo show_err_msg($this->session->flashdata('error_msg'));
							echo show_succ_msg($this->session->flashdata('success_msg'));
					
						?>
                        <table class="table table-bordered data-table">
                            <thead>
                                <tr>
                                    <th width="3%" >#</th>
                                    <th >Team Name</th>
                                    <th >Total Members</th>
                                    <th >Invited Users</th>
                                    <th >Invite Declined</th>
                                    <th >Brief Validity</th>
                                    <th >Createdon</th>
                                    <th width="15%" >Action</th>
                                </tr>
                            </thead>
                            <tbody>
						<?php $x =1;
								foreach($data as $row){ ?>
                                <tr id="<?= $x; ?>" class="gradeX">
									<td><?= $x; ?></td>
                                    <td class="center">
										<div class="span2">
											<img src="<?= $row['team_coverphoto'] ? $row['team_coverphoto'] : ($row['profile_picture'] ? $row['profile_picture'] : base_url().'assets/img/dummyteam.svg'); ?>" height="100" width="100" style="background-color:#c6c6c6;" onerror='this.src="<?=  base_url().'assets/img/dummyteam.svg'; ?>"' alt="Cover picture">
										</div>
										<div class="span10">
											<p class="top-padding-name"><?= $row['team_name']; ?></p></td>
										</div>
                                    <td><?= $row['total_members']; ?> Member(s)<br /><a href="team_members/<?= urlencode(helper_aesEncryption($this->secret,$row['team_id'])); ?>" class="btn btn-primary btn-mini">View Members</a></td>
									<td><?= $row['total_invites'] ? $row['total_invites'].' Invite(s)<br /><a href="team_invites/'.urlencode(helper_aesEncryption($this->secret,$row['team_id'])).'" class="btn btn-primary btn-mini">View Invites</a>' : '<font color=blue>No Invites</font>'; ?> </td>
									<td><?= $row['total_declined'] ? $row['total_declined'].' Decline(s)<br /><a href="team_invites_decline_list/'.urlencode(helper_aesEncryption($this->secret,$row['team_id'])).'" class="btn btn-primary btn-mini">View Declines</a>' : '<font color=blue>No Decline</font>'; ?> </td>
                                    <td><?= $row['briefing_validity'] ? $row['briefing_validity'] ." days" : "Never Expire"; ?></td>
									<td class="center"><?= unix_to_human((human_to_unix($row['created_at']))); ?></td>
									<td class="center">
                                        <div class="article-post">
                                            <div class="fr"><a href="invite_members/<?= urlencode(helper_aesEncryption($this->secret,$row['team_id'])); ?>" class="btn btn-primary btn-mini">Invite users</a> <a href="update_team/<?= urlencode(helper_aesEncryption($this->secret,$row['team_id'])); ?>" class="btn btn-primary btn-mini">Edit</a> <a class="btn btn-danger btn-mini confirm" onclick="delete_team(<?= $x; ?>,'<?= urlencode(helper_aesEncryption($this->secret,$row['team_id'])); ?>')" title="<b>Are you sure to delete Team!</b>">Delete</a></div>
                                        </div>
                                    </td>
                                </tr>
						<?php $x++;
								} ?>
							</tbody>
                        </table>
                    </div>
                </div>
			</div>
        </div>
    </div>
</div>