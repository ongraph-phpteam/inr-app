<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/highcharts-3d.js"></script>
<script src="https://code.highcharts.com/modules/data.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://highcharts.github.io/export-csv/export-csv.js"></script>
<script>
	Highcharts.chart('new_comp_reg_1', {
		chart: {
			type: 'column'
		},
		credits: {
			enabled: false
		},
		title: {
			text: 'Monthly new members per'
		},
		subtitle: {
			text: 'As on <?= date("M")." ".ltrim(sufixNumber(date('d')),'0')." ".date("Y"); ?>'
		},
		xAxis: {
			categories: ['January','February','March','April', 'May', 'June', 'July', 'Auguest','September','October','November','December']
		},
		yAxis: {
			min: 0,
			title: {
				text: 'New members'
			}
		},
		tooltip: {
			pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
			shared: true
		},
		plotOptions: {
			column: {
				stacking: 'percent'
			}
		},
		series: [
		<?php 	for($nm=0;$nm<13;$nm++){ ?>
				{
					name: '<?= date("M"); ?>',
					data: [<?= 2; ?>,]
				},
		
	<?php	}	?>
				]
	});
</script>
<script>
	Highcharts.chart('new_comp_reg', {
		chart: {
			type: 'spline',
			zoomType: 'xy'
		},
	
		title: {
			text: 'Monthly Users Registration'
		},
		 credits: {
		  enabled: false
		},
		xAxis: {
			categories: [
		<?php	if(count($data)>0){ 
					for($m=0;$m<count($data);$m++){
						echo "'".date("M-Y",strtotime($data[$m]['date']))."',";
					}
				}
		?>	
			
			]
		},
		yAxis: {
			allowDecimals : false,
			title: {
				text: 'Monthly Users Registrations'
			},
			min : 0
		},
		plotOptions: {
			spline: {
				dataLabels: {
					enabled: TRUE
				},
				marker: {
					enabled: true
				},
				enableMouseTracking: true,
				
			}
		},
		series: [{
			name: 'Monthly registration',
			data: [ <?php	if(count($data)>0){ for($d=0;$d<count($data);$d++){ echo (int)$data[$d]['total'].","; } }else{ echo "" ; } ?>	]
		}]
	});

	</script>
