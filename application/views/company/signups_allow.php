<div id="content">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title">
                        <span class="icon"><i class="icon-th"></i></span>
                       <div class="span5"><h5><a href="add_emailids">+ Add Email Ids</a></h5></div><div class="span4"><h6>Manage Email Ids Allowed for Sign-up</h6></div>
                    </div>
                    <div class="widget-content nopadding">
						<?php
							echo show_err_msg($this->session->flashdata('error_msg'));
							echo show_succ_msg($this->session->flashdata('success_msg'));
						?>
                        <table class="table table-bordered data-table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Email</th>
                                    <th>Added On</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
					<?php 	$id = 1;
							foreach($data as $row){ ?>
                                <tr id='<?= $id; ?>' class="gradeX">
                                    <td><?= $id; ?></td>
                                    <td><?= $row['email']; ?></td>
                                    <td class="center"><?= unix_to_human((human_to_unix($row['invited_at']))); ?></td>
                                    <td class="center">
                                        <div class="article-post">
                                            <div class="fr"><a class="btn btn-warning btn-mini confirm" onclick="delete_allowed_email(<?= $id; ?>,'<?= urlencode(helper_aesEncryption($this->secret,$row['allowed_id'])); ?>')" title="<b>Are you sure to delete!</b>" >Delete</a></div>
                                        </div>
                                    </td>
                                </tr>
					<?php	$id++;
							} ?>
							</tbody>
                        </table>
                    </div>
                </div>
			</div>
        </div>
    </div>
</div>