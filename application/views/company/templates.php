<div id="content">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title">
                        <span class="icon"><i class="icon-th"></i></span>
                        <div class="span5"><h5><a href="new_template">+ New Template</a></h5></div><div class="span4"><h6>Manage Busuness Card Templates</h6></div>
                    </div>
                    <div class="widget-content nopadding">
						<?php
							echo show_err_msg($this->session->flashdata('error_msg'));
							echo show_succ_msg($this->session->flashdata('success_msg'));
					
						?>
                        <table class="table table-bordered data-table">
                            <thead>
                                <tr>
                                    <th width="3%" >#</th>
                                    <th width="30%">Background Layer</th>
                                    <th width="30%">Front Layer</th>
                                    <th width="10%">Used Count</th>
                                    <th width="10%" >Action</th>
                                </tr>
                            </thead>
                            <tbody>
						<?php $x =1;
								foreach($data as $row){ ?>
                                <tr id="<?= $x; ?>" class="gradeX">
									<td><?= $x; ?></td>
                                    <td class="center">
									<div class="span8">
										<img src="<?= $row['background_layer']; ?>" height="150px" width="150px">
									</div>
									</td>
									<td class="center">
									<div class="span8">
										<img src="<?= $row['front_layer']; ?>" height="150px" width="150px" style="background-color:#c6c6c6;">
									</div>									
									</td>
									<td><?= $row['used_by']; ?> card(s)</td>
									<td class="center">
                                        <div class="article-post">
                                            <div class="fr"><a href="update_template/<?= urlencode(helper_aesEncryption($this->secret,$row['template_id'])); ?>" class="btn btn-primary btn-mini">Edit</a> <a class="btn btn-warning btn-mini confirm" id='block<?= $x; ?>' onclick="blockUnblockTemplate(<?= $x; ?>,'<?= urlencode(helper_aesEncryption($this->secret,$row['template_id'])); ?>')" <?php if($row['is_active']==1){ ?>  title="<b>Are you sure to block!</b>" <?php }else{ ?>title="<b>Are you sure to unblock!</b>" <?php } ?> ><?php if($row['is_active']==1){ ?>Block<?php }else{ ?>Unblock<?php } ?></a></div>
                                        </div>
                                    </td>
                                </tr>
						<?php $x++;
								} ?>
							</tbody>
                        </table>
                    </div>
                </div>
			</div>
        </div>
    </div>
</div>