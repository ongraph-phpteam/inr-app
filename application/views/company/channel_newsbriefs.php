<div id="content">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title">
                        <span class="icon" onclick="window.history.back();"><i class="icon-back"></i></span>
                        <div class="span4"><h5></h5></div><div class="span4"><h6><?= $channel_name; ?> - News Briefs</h6></div>
                    </div>
                    <div class="widget-content nopadding">
						<?php
							echo show_err_msg($this->session->flashdata('error_msg'));
							echo show_succ_msg($this->session->flashdata('success_msg'));
					
						?>
                        <table class="table table-bordered data-table">
                            <thead>
                                <tr>
                                    <th width="25%">Publisher</th>
                                    <th width="15%">Department</th>
                                    <th width="40%">Post</th>
                                    <th width="12%">Published At</th>
                                    <th width="8%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
						<?php	$x=0;
							foreach($data as $row){ ?>
                                <tr id="<?= $x; ?>" class="gradeX">
                                    <td class="center">
										<div class="span2">
											<img src="<?= $row['channel_logo'] ? $row['channel_logo'] : ($row['picture'] ? $row['picture'] : base_url().'assets/img/dummy.svg'); ?>" height="100" width="100" style="background-color:#c6c6c6;" alt="Cover picture">
										</div>
										<div class="span10">
										<p class=""><?= $row['user_name']; ?><br/> <?= $row['email']; ?></p></td>
										</div>
                                    <td><?= $row['channel_name']; ?></td>
                                    <td><?php if($row['picture']){ ?><div class="span2"><img src="<?= $row['picture']; ?>" width="100%" ></div><div class="span10"><?= $row['news']; ?></div><?php }else{ ?><div class="span12"><?= $row['news']; ?></div><?php } ?></td>
                                    <td class="center"><?= unix_to_human((human_to_unix($row['published_at']))); ?></td>
                                    <td class="center">
                                        <div class="article-post">
                                            <div class="fr"><a class="btn btn-primary btn-mini">Edit</a> <button href="" class="btn btn-danger btn-mini confirm" onclick="deleteChannelNewsBrief(<?= $x; ?>,'<?= urlencode(helper_aesEncryption($this->secret,$row['newsbrief_id'])); ?>')" title="<b>Are you sure to delete!</b>">Delete</button></div>
                                        </div>
                                    </td>
                                </tr>
						<?php $x++;
								} ?>
							</tbody>
                        </table>
                    </div>
                </div>
			</div>
        </div>
    </div>
</div>