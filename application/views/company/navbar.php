<!--Header-part-->
<div id="header">
    <h1><a href="dashboard.html">INR Company Admin</a></h1>
</div>
<!--close-Header-part-->
<!--top-Header-menu-->
<div id="user-nav" class="navbar navbar-inverse">
    <ul class="nav">
        <li class=""><a title="" href="#"><i class="icon icon-user"></i> <span class="text">Profile</span></a></li>
        <li class=""><a title="" href="Auth/logout"><i class="icon icon-share-alt"></i> <span class="text">Logout</span></a></li>
    </ul>
</div>
<!--close-top-Header-menu-->

<div id="sidebar">
    <ul>
        <li class="active"><a href="<?= base_url(); ?>company/dashboard"><i class="icon icon-home"></i> <span>Dashboard</span></a> </li>
        <li> <a href="<?= base_url(); ?>company/users"><i class="icon-user"></i> <span>Manage Users</span></a> </li>
        <li> <a href="<?= base_url(); ?>company/signup_allow"><i class="icon-ok-sign"></i> <span>Manage Sign-ups</span></a> </li>
        <li> <a href="<?= base_url(); ?>company/teams"><i class="icon-globe"></i> <span>Manage Teams</span></a> </li>
        <li> <a href="<?= base_url(); ?>company/todolist"><i class="icon-list-alt"></i> <span>Manage Todolist</span></a> </li>
        <li> <a href="<?= base_url(); ?>company/news_channels"><i class="icon-globe"></i> <span>Manage News Channel </span></a> </li>
        <li> <a href="<?= base_url(); ?>company/active_newsbriefs"><i class="icon-comment"></i> <span>Active News Brief </span></a> </li>
        <li> <a href="<?= base_url(); ?>company/templates"><i class="icon-book"></i> <span>Templates </span></a> </li>
    </ul>
</div>