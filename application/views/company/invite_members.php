<div id="content">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span6">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
                        <h5>Invite Team Members</h5>
                    </div>
                    <div class="widget-content nopadding">
						<?php
							echo show_err_msg($error_msg);
						?>
						
                        <form action="" method="post" class="form-horizontal" >
                           <div class="control-group">
								<label class="control-label" for="rolename">Invite members</label>
								<div class="controls">
									<select id="dates-field2" name="invite_userids[]" class="multiselect-ui span6" multiple="multiple" required >
							<?php 	if(count($users)){ ?>
							<?php 		for($i=0;$i<count($users);$i++){ ?>
										<option value="<?= $users[$i]['user_id']?>"><b><?= $users[$i]['user_name']; ?></b> (<?= $users[$i]['designation'] ? $users[$i]['designation'] : $users[$i]['email'] ; ?>)</option>
									
							<?php		} ?>
							<?php	}else{ ?>
										<option value='0'>No Users found</option>
							<?php	} ?>
									</select>
								</div>
							</div>
                            <div class="form-actions">
                                <input type="submit" name="submit" value="Invite Users" class="btn btn-success">
								<input type="reset" onclick="window.history.back();" value="Cancel" class="btn btn-danger">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
		</div>
        <hr>
    </div>
</div>
</div>