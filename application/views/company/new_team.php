<div id="content">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span6">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
                        <h5>New Team</h5>
                    </div>
                    <div class="widget-content nopadding">
						<?php
							echo show_err_msg($error_msg);
						?>
						
                        <form action="" method="post" class="form-horizontal" enctype="multipart/form-data" >
                            <div class="control-group">
                                <label class="control-label">Team Name:</label>
                                <div class="controls">
                                    <textarea type="text" name="team_name" id="team_name" class="span11" placeholder="Team name" required ><?php echo set_value('team_name'); ?></textarea>
                                </div>
                            </div>
							<div class="control-group">
								<label class="control-label">Briefing Validity</label>
								<div class="controls">
									<label>
									<input type="radio" name="briefing_validity"  value="7"  required />
									7 Days</label>
									<label>
									<input type="radio" name="briefing_validity" value="5"/>
									5 Days</label>
									<label>
									<input type="radio" name="briefing_validity" value="2" />
									2 Days</label>
									<label>
									<input type="radio" name="briefing_validity" value="0" />
									Never</label>
								</div>
							</div>
							
							<div class="control-group">
								<label class="control-label">Upload Cover Picture</label>
								<div class="controls">
									<input type="file" name="team_coverphoto" accept="image/x-png,image/jpeg"  />
									<p><font color="brown">Note: Only .png,.jpg,.jpeg format supported</font></p>
								</div>
								
							</div>
							
							<div class="control-group">
								<label class="control-label" for="rolename">Invite members</label>
								<div class="controls">
									<select id="dates-field2" name="invite_userids[]" class="multiselect-ui span6" multiple="multiple">
							<?php 	if(count($users)){ ?>
							<?php 		for($i=0;$i<count($users);$i++){ ?>
										<option value="<?= $users[$i]['user_id']?>"><b><?= $users[$i]['user_name']; ?></b> (<?= $users[$i]['designation'] ? $users[$i]['designation'] : $users[$i]['email'] ; ?>)</option>
									
							<?php		} ?>
							<?php	}else{ ?>
										<option value="">No Users found</option>
							<?php	} ?>
									</select>
								</div>
							</div>

                            <div class="form-actions">
                                <input type="submit" name="submit" value="Submit Data" class="btn btn-success">
								<input type="reset" name="reset" value="Reset" class="btn btn-warning">
								<input type="reset" onclick="window.history.back();" value="Cancel" class="btn btn-danger">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
		</div>
        <hr>
    </div>
</div>
</div>