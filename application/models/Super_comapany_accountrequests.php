<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Super_comapany_accountrequests extends CI_Model {
	
	public function select_comapany_accountrequests(){
		$data = $this->db->select('my_cards.*,company_account_requests.*')->from('my_cards')->join('company_account_requests','company_account_requests.user_id=my_cards.user_id AND company_account_requests.card_id= my_cards.card_id')->get()->result_array();
		return $data;
	}
	
	public function newdesignation($input){
		$check = $this->db->select('designation_id')->from('designations')->where(array('designation'=>$input['designation']))->get()->row();
		if($check==''){
			unset($input['submit']);
			$this->db->trans_start();
			$this->db->insert('designations',$input);
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return array('status' => 500,'msg' => 'Internal server error. Please try again!');
			}else{
				$this->db->trans_commit();
				return array('status' => 200,'msg' =>'Designation added successfully.');
			}	
		}else{
			return array('status' => 400,'msg' =>'Designation name already in list');
		}
		
	}
	public function view_request($id){
		$data = $this->db->select('my_cards.*,company_account_requests.*')->from('my_cards')->join('company_account_requests','company_account_requests.user_id=my_cards.user_id AND company_account_requests.card_id= my_cards.card_id AND company_account_requests.request_id='.$id)->get()->row_array();
		if (count($data)){
			$data['profile_pic'] = $data['profile_pic'] ? base_url().$data['profile_pic']: '';
			$data['company_logo'] = $data['company_logo'] ? base_url().$data['company_logo']: '';
			return array('data'=>$data,'status' => 200,'msg' =>'');
		}else{
			return array('status' => 400,'msg' =>'Request not found!');
		}
	}
	
	
}