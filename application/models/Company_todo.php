<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Company_todo extends CI_Model {
	
	public function get_todolist(){
		$company_id = $this->session->userdata()['userdata']['company_id'];
		$data = $this->db->select('todolist.todoid, todolist.team_id, todolist.accountability, todolist.action_name, todolist.action_description, todolist.deadline, todolist.status, teams.team_name, teams.team_coverphoto, teams.user_id as team_owner, users.user_name, users.email, users.designation,users.profile_picture')->from('todolist')->where(array('todolist.is_active'=>0))->join('teams', 'todolist.team_id = teams.team_id AND company_id='.$company_id.'')->join('users', 'users.user_id = todolist.accountability')->order_by('todolist.status','ASC')->order_by('todolist.todoid','DESC')->get()->result_array();
			foreach( $data as $key => $row )
			{
				$row['team_coverphoto'] !=''  ? $row['team_coverphoto']	= base_url().$row['team_coverphoto'] : '';
				$row['profile_picture']!=''  ? $row['profile_picture']	= base_url(). $row['profile_picture']: '';
				$row['designation'] 	=  getDesignationdata($row['designation'],'designation');
				$row['assigner_name'] 	=  getusersdata($row['team_owner'],'user_name');
				$data[$key] = $row;
			}
		return $data;
	}
	
}