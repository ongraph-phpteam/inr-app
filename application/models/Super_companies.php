<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class  Super_companies extends CI_Model {
	
	public function select_allcompanies(){
		$data = $this->db->select('*,(select count(user_id) from users where company_id=companies.company_id and is_active=1) as totalusers')->from('companies')->where(array('approval_status'=>1,'is_active'=>1))->order_by('company_id','DESC')->get()->result_array();
		for($c=0;$c<count($data);$c++){
			//$data[$c]['enc_companyid'] =  urlencode($this->secret,json_encode($this->Aes_encryption($data[$c]['company_id'])));
		}
		return $data;
	}
	
	public function newcompany($input){
		$check = $this->db->select('company_id')->from('companies')->where(array('domain'=>$input['domain']))->get()->row();
		if($check==''){
			unset($input['submit']);
			$input['approval_status']	= '1';
			$input['is_active']			= '1';
			$input['company_logo']		= '';
			$config['allowed_types'] 	= '*';
			$config['max_size'] 		= '500';
			$date 						= date("Y-m");
			
			if (!empty($_FILES['company_logo'])){
				$path	=  './media/company/logo/'.$date."/";
				if (!is_dir($path)){
					mkdir($path , 0777);
					$content = "<!DOCTYPE html><html><head><title>403 Forbidden</title></head><body><p>Access denied!</p></body></html>"; 
					$fp = fopen($path."index.html","wb"); 
					fwrite($fp,$content); 
					fclose($fp); 
				}
				$config['file_name']	= date("Ymd")."_BC-".rand(1000,9999).'-'.time();
				$config['upload_path']	= $path;
				$this->upload->initialize($config);
				if (!$this->upload->do_upload('company_logo')){
					return array('status' =>400,'message' =>"Company logo uploading error: ".strip_tags($this->upload->display_errors()),'method'=>'create_businessCard');
					exit;
				}else{
					$input['company_logo'] = ltrim($path,"./").$this->upload->data()['file_name'];
				}
			}
			
			$this->db->trans_start();
			$this->db->insert('companies',$input);
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return array('status' => 500,'msg' => 'Internal server error. Please try again!');
			}else{
				$this->db->trans_commit();
				$company_id = $this->db->insert_id();
				## Default Category
				$designations = ['CEO/GM','Board Level Executive','Executive Assistant','Finance','HR','Operations','IT','Strategy','Legal Services','Sales/Marketing','R&D','Public Relations','Plant Manager','Software developer'];
				for($i=0;$i<count($designations);$i++){
					$this->db->insert('designations',array('designation'=>$designations[$i],'company_id'=>$company_id));
				}
				$check = $this->db->select("user_id")->from("users")->where(array('email'=>$input['email']))->get()->row();
				if($check!=''){
					$this->db->where('email',$input['email'])->update('users',array('account_type' =>'2','updated_at'=>today()[0]));
					$random = "Password is same as your INR-Circle mobile application password";
					$msg = "";
				}else{
					$msg = "<div>This is your temporary password. After successful login we recommend you to change your password.</div>" ;
					$random		= randomCode();
					$password 	= crypt($random,APP_SALT);
					$this->db->insert('users',array('account_type'=>'2','user_name'=>$input['contact_person'],'email'=>$input['email'],'company_id'=>$company_id,'password'=>$password,'is_active'=>1));
				}
				$this->load->library('email');
				$this->email->from('noreply@inr-circle.com', 'INR-Circle Support Team');
				$this->email->to($input['email']);
				$this->email->subject('INR-Circle Company Admin Panel Credential');
				$this->email->set_mailtype("html");
				$email_message .= "<div style='width:400px;background:#fff;color:#000;font-family:Source Sans Pro,sans-serif;padding:10px;'>";	
				$email_message .= "<div>Hi, ".$input['contact_person']."</div>" ;
				$email_message .= "<div>INR-Circle Company account for ".$input['company_name']." has been created successfully.</div>" ;
				$email_message .= "<div>Following are the login credential for you Company Administrator panel:</div>" ;
				$email_message .= "<div>Company Admin Panel Link: ".base_url()."company</div>" ;
				$email_message .= "<div>Company Admin Panel User: ".$input['email']."</div>" ;
				$email_message .= "<div>Company Admin Panel Password: ".$random."</div>" ;
				$email_message .= $msg;
				$email_message .= "<div>INR-Circle Support Team</div>" ;
				$this->email->message($email_message);
				$this->email->send();
				
				return array('status' => 200,'msg' =>'Company account created successfully.');
			}	
		}else{
			return array('status' => 400,'msg' =>'Company account already registered with '.$input['domain']);
		}
		
	}
	
	public function view_company($id){
		$data = $this->db->select('*')->from('companies')->where(array('company_id'=>$id))->get()->row_array();
		if (count($data)){
			return array('data'=>$data,'status' => 200,'msg' =>'');
			
		}else{
			return array('status' => 400,'msg' =>'Company account not exist. Please try again.');
		}
	}
	public function updateinfo($input){
		
		$check = $this->db->select('company_id')->from('companies')->where(array('domain'=>$input['domain'],'company_id!='=>$input['company_id']))->get()->row();
		if($check==''){
			unset($input['submit']);
			$input['company_logo']		= getcompanyanything($input['company_id'],'company_logo');
			$config['allowed_types'] 	= '*';
			$config['max_size'] 		= '500';
			$date 						= date("Y-m");
			
			if (!empty($_FILES['company_logo']['name'])){
				$path	=  './media/company/logo/'.$date."/";
				if (!is_dir($path)){
					mkdir($path , 0777);
					$content = "<!DOCTYPE html><html><head><title>403 Forbidden</title></head><body><p>Access denied!</p></body></html>"; 
					$fp = fopen($path."index.html","wb"); 
					fwrite($fp,$content); 
					fclose($fp); 
				}
				$config['file_name']	= date("Ymd")."_BC-".rand(1000,9999).'-'.time();
				$config['upload_path']	= $path;
				$this->upload->initialize($config);
				if (!$this->upload->do_upload('company_logo')){
					return array('status' =>400,'msg' =>"Company logo uploading error: ".strip_tags($this->upload->display_errors()));
					exit;
				}else{
					$input['company_logo'] !="" ? unlink("./".$input['company_logo']) : "";
					$input['company_logo'] = ltrim($path,"./").$this->upload->data()['file_name'];
				}
			}
			
			$this->db->trans_start();
			$this->db->where('company_id',$input['company_id'])->update('companies',array('company_name'=>$input['company_name'],'company_logo'=>$input['company_logo'],'domain'=>$input['domain'],'contact_person'=>$input['contact_person'],'contact_email'=>$input['contact_email'],'country_code'=>$input['country_code'],'contact_phone'=>$input['contact_phone'],'company_address'=>$input['company_address'],'city'=>$input['city'],'state'=>$input['state'],'zipcode'=>$input['zipcode'],'country'=>$input['country'],'last_modified_at'=>today()[0]));
			
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return array('status' => 500,'msg' => 'Internal server error. Please try again!');
			}else{
				$this->db->trans_commit();
				return array('status' => 200,'msg' =>'Company information updated successfully.');
			}	
		}else{
			return array('status' => 400,'msg' =>'<p>Company account already registered with '.$input['domain']."</p>");
		}
	}
	
}