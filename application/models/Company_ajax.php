<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Company_ajax extends CI_Model {

	public function delete_allowed_email($allowed_id){
		$allowed_id = $this->Aes_encryption->aesDecryption($this->secret,urldecode($allowed_id));
		$company_id = $this->session->userdata()['userdata']['company_id'];
		$adminid = $this->session->userdata()['userdata']['user_id'];
		$check = $this->db->select("email")->from('signup_allowed')->where(array('company_id'=>$company_id,'allowed_id'=>$allowed_id))->get()->row_array();
		
		if(count($check)){
			$this->db->trans_start();
			$this->db->where(array('allowed_id'=>$allowed_id))->delete('signup_allowed');
			
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return array('status' => 500,'msg' => 'Internal server error. Please try again!');
			}else{
				$user = $this->db->select("user_id")->from("users")->where(array("email"=>$check['email']))->get()->row_array();
				if(count($user)){
					$this->db->query("DELETE FROM team_invites WHERE user_id='".$user['user_id']."'");
					$this->db->query("DELETE FROM notifications WHERE user_id='".$user['user_id']."'");
					$this->db->query("DELETE FROM company_account_requests WHERE user_id='".$user['user_id']."'");
					$this->db->query("DELETE FROM newschannel_subscribers WHERE user_id='".$user['user_id']."'");
					$this->db->query("DELETE FROM news_deleted WHERE user_id='".$user['user_id']."'");
					$this->db->query("DELETE FROM users_authentication WHERE user_id='".$user['user_id']."'");
					$this->db->query("DELETE FROM my_cards WHERE master_card_id!=0 AND user_id='".$user['user_id']."'");
					
					## Joined Teams Relieving
					$joined_teams = $this->db->query("SELECT member_id, team_id FROM team_members WHERE user_id='".$user['user_id']."' AND  team_owner!='".$user['user_id']."' AND status=0")->result_array();
					
					if(count($joined_teams)){
						for($i=0;$i<count($joined_teams);$i++){
							$team_id	= $joined_teams[$i]['team_id'];
							$memberid	= $joined_teams[$i]['member_id'];
							$todolist = $this->db->select('todoid,assigned_by')->from('todolist')->WHERE(array('team_id'=>$team_id,'accountability'=>$user['user_id'],'status'=>'0'))->get()->result_array();
							if (count($todolist)){
								foreach($todolist as $list){
									$this->db->insert('todo_reassign_action',array('todoid'=>$list['todoid'],'team_id'=>$team_id,'prev_assigned_by'=>$list['assigned_by'],'prev_accountability'=>$user['user_id'],'updated_at'=>today()[0]));
								}
							}
							$this->db->where(array('member_id'=>$memberid))->update('team_members',array('status' =>1));
							$this->db->where(array('team_id'=>$team_id,'accountability'=>$user['user_id']))->update('todolist',array('is_active' =>1));
						}
					}

					## Created Team Relieving
					$user_teams = $this->db->query("SELECT member_id, team_id FROM team_members WHERE user_id='".$user['user_id']."' AND  team_owner='".$user['user_id']."' AND status=0")->result_array();
					
					if(count($user_teams)){
						for($i=0;$i<count($user_teams);$i++){
							$team_id	= $user_teams[$i]['team_id'];
							$memberid	= $user_teams[$i]['member_id'];
							## Check assigned assigned task to him/her self
							$todolist = $this->db->select('todoid,assigned_by')->from('todolist')->WHERE(array('team_id'=>$team_id,'accountability'=>$user['user_id'],'status'=>'0'))->get()->result_array();
							if (count($todolist)){
								foreach($todolist as $list){
									$this->db->query("DELETE FROM todolist WHERE todoid='".$list['todoid']."'");
								}
							}
							$this->db->where(array('member_id'=>$memberid,'user_id'=>$user['user_id']))->update('team_members',array('status' =>1,'member_type'=>0,'team_owner'=>0));

							$message	=	array('thread_id'=>getThreadId($team_id),"team_id "=>$team_id,"isactive"=>"1","sent_date"=>today()[0],"sender_id"=>$adminid,"privacy"=>"1","message_type"=>"1","main_sender_id"=>$adminid,"message"=>"The team leader is no longer able to manage the team so all team members have 1 week to became the Team leader by pressing Assume leadership button.");
							$this->db->insert('ttalks',$message);
							
						}
					}

					## Delete Created Channels
					$user_channels = $this->db->query("SELECT channel_id FROM news_channel WHERE channel_adminid='".$user['user_id']."'")->result_array();

					
					for($i=0;$i<count($user_channels);$i++){
						$channel_id = $user_channels[$i]['channel_id'];
						$this->db->query("DELETE FROM newschannel_subscribers WHERE channel_id=".$channel_id."");
						$this->db->query("DELETE FROM news_brief WHERE channel_id=".$channel_id."");
						$this->db->query("DELETE FROM news_channel WHERE channel_id=".$channel_id."");
					}
				
					$this->db->query("UPDATE users SET company_id=0,email='',designation='',password='',udid='',touch_login='',country_code='',mobile_number='',profile_picture='',datetime='',timezone='',rolodex_collectinginfo='',new_email_to_change='',device_change_verify_code='',email_change_verify_code='',email_varification='',last_email='".$check['email']."',updated_at=NOW(),is_active=2 WHERE user_id='".$user['user_id']."'");	
				}
				$this->db->trans_commit();
				return array('status' => 200,'msg' =>'Email deleted successfully');
			}
		}else{
			return array('status' => 400,'msg' => 'Access Denied!');
		}
	}

	## Delete User from Manage user section
	public function deleteUser($user_id){
		$user_id = $this->Aes_encryption->aesDecryption($this->secret,urldecode($user_id));
		$company_id = $this->session->userdata()['userdata']['company_id'];
		$adminid = $this->session->userdata()['userdata']['user_id'];
		$user = $this->db->select("email,user_id")->from("users")->where(array("user_id"=>$user_id))->get()->row_array();
		if(count($user)){
			$this->db->trans_start();
			$this->db->query("DELETE FROM signup_allowed WHERE email='".$user['email']."'");
			$this->db->query("DELETE FROM team_invites WHERE user_id='".$user['user_id']."'");
			$this->db->query("DELETE FROM notifications WHERE user_id='".$user['user_id']."'");
			$this->db->query("DELETE FROM company_account_requests WHERE user_id='".$user['user_id']."'");
			$this->db->query("DELETE FROM newschannel_subscribers WHERE user_id='".$user['user_id']."'");
			$this->db->query("DELETE FROM news_deleted WHERE user_id='".$user['user_id']."'");
			$this->db->query("DELETE FROM users_authentication WHERE user_id='".$user['user_id']."'");
			$this->db->query("DELETE FROM my_cards WHERE master_card_id!=0 AND user_id='".$user['user_id']."'");
			
			## Joined Teams Relieving
			$joined_teams = $this->db->query("SELECT member_id, team_id FROM team_members WHERE user_id='".$user['user_id']."' AND  team_owner!='".$user['user_id']."' AND status=0")->result_array();
			
			if(count($joined_teams)){
				for($i=0;$i<count($joined_teams);$i++){
					$team_id	= $joined_teams[$i]['team_id'];
					$memberid	= $joined_teams[$i]['member_id'];
					$todolist = $this->db->select('todoid,assigned_by')->from('todolist')->WHERE(array('team_id'=>$team_id,'accountability'=>$user['user_id'],'status'=>'0'))->get()->result_array();
					if (count($todolist)){
						foreach($todolist as $list){
							$this->db->insert('todo_reassign_action',array('todoid'=>$list['todoid'],'team_id'=>$team_id,'prev_assigned_by'=>$list['assigned_by'],'prev_accountability'=>$user['user_id'],'updated_at'=>today()[0]));
						}
					}
					$this->db->where(array('member_id'=>$memberid))->update('team_members',array('status' =>1));
					$this->db->where(array('team_id'=>$team_id,'accountability'=>$user['user_id']))->update('todolist',array('is_active' =>1));
				}
			}

			## Created Team Relieving
			$user_teams = $this->db->query("SELECT member_id, team_id FROM team_members WHERE user_id='".$user['user_id']."' AND  team_owner='".$user['user_id']."' AND status=0")->result_array();
			
			if(count($user_teams)){
				for($i=0;$i<count($user_teams);$i++){
					$team_id	= $user_teams[$i]['team_id'];
					$memberid	= $user_teams[$i]['member_id'];
					## Check assigned assigned task to him/her self
					$todolist = $this->db->select('todoid,assigned_by')->from('todolist')->WHERE(array('team_id'=>$team_id,'accountability'=>$user['user_id'],'status'=>'0'))->get()->result_array();
					if (count($todolist)){
						foreach($todolist as $list){
							$this->db->query("DELETE FROM todolist WHERE todoid='".$list['todoid']."'");
						}
					}
					$this->db->where(array('member_id'=>$memberid,'user_id'=>$user['user_id']))->update('team_members',array('status' =>1,'member_type'=>0,'team_owner'=>0));

					$message	=	array('thread_id'=>getThreadId($team_id),"team_id "=>$team_id,"isactive"=>"1","sent_date"=>today()[0],"sender_id"=>$adminid,"privacy"=>"1","message_type"=>"1","main_sender_id"=>$adminid,"message"=>"The team leader is no longer able to manage the team so all team members have 1 week to became the Team leader by pressing Assume leadership button.");
					$this->db->insert('ttalks',$message);
					
				}
			}

			## Delete Created Channels
			$user_channels = $this->db->query("SELECT channel_id FROM news_channel WHERE channel_adminid='".$user['user_id']."'")->result_array();

			
			for($i=0;$i<count($user_channels);$i++){
				$channel_id = $user_channels[$i]['channel_id'];
				$this->db->query("DELETE FROM newschannel_subscribers WHERE channel_id=".$channel_id."");
				$this->db->query("DELETE FROM news_brief WHERE channel_id=".$channel_id."");
				$this->db->query("DELETE FROM news_channel WHERE channel_id=".$channel_id."");
			}
		
			$this->db->query("UPDATE users SET company_id=0,email='',designation='',password='',udid='',touch_login='',country_code='',mobile_number='',profile_picture='',datetime='',timezone='',rolodex_collectinginfo='',new_email_to_change='',device_change_verify_code='',email_change_verify_code='',email_varification='',last_email='".$user['email']."',updated_at=NOW(),is_active=2 WHERE user_id='".$user['user_id']."'");	
		
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return array('status' => 500,'msg' => 'Internal server error. Please try again!');
			}else{
				$this->db->trans_commit();
				return array('status' => 200,'msg' =>'Email deleted successfully');
			}
		}else{
			return array('status' => 400,'msg' => 'Access Denied!');
		}
	}

	## Block User
	public function block_user($user_id){
		$user_id = $this->Aes_encryption->aesDecryption($this->secret,urldecode($user_id));
		$company_id = $this->session->userdata()['userdata']['company_id'];
		if(check_valid_users_company($company_id,$user_id)){
			
			$block_status = getusersdata($user_id,'is_active');
			$this->db->trans_start();
			if($block_status==1){
				$is_active = 2;
				$msg = 'User blocked Successfully';
				$status = 200;
			}else{
				$is_active = 1;
				$msg = 'User unblocked Successfully';
				$status = 201;
			}
			$this->db->where(array('user_id'=>$user_id))->update('users',array('is_active'=>$is_active,'updated_at'=>today()[0]));

			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return array('status' => 500,'msg' => 'Internal server error. Please try again!');
			}else{
				$this->db->trans_commit();
				return array('status' => $status,'msg' =>$msg);
			}
		}else{
			return array('status' => 400,'msg' => 'Access Denied!');
		}
	}
	
	## Delete Team
	public function delete_team($team_id){
		$team_id = $this->Aes_encryption->aesDecryption($this->secret,urldecode($team_id));
		$company_id = $this->session->userdata()['userdata']['company_id'];
		
		$check = $this->db->select('team_id')->from('teams')->WHERE(array('team_id'=>$team_id,'company_id'=>$company_id))->get()->row();
		
		if($check){
			$this->db->trans_start();
			
			$this->db->where(array('team_id'=>$team_id))->delete('teams');
			$this->db->where(array('team_id'=>$team_id))->delete('team_invites');
			$this->db->where(array('team_id'=>$team_id))->delete('team_members');
			$this->db->where(array('team_id'=>$team_id))->delete('todolist');
			$this->db->where(array('team_id'=>$team_id))->delete('todo_reassign_action');
			$this->db->where(array('team_id'=>$team_id))->delete('ttalks');
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return array('status' => 500,'msg' => 'Internal server error. Please try again!');
			}else{
				$this->db->trans_commit();
				return array('status' => 200,'msg' =>'Team deleted successfully');
			}
		}else{
			return array('status' => 400,'msg' => 'Access Denied!');
		}
	}
	
	## Remove Team Member
	public function remove_teamMember($input)
	{	
		$company_id = $this->session->userdata()['userdata']['company_id'];
		$member_id	= $this->Aes_encryption->aesDecryption($this->secret,urldecode($input['member_id']));
		$team_id	= $this->Aes_encryption->aesDecryption($this->secret,urldecode($input['team_id']));
		
		$check_valid = $this->db->select('user_id')->from('team_members')->WHERE(array('team_id'=>$team_id,'member_id'=>$member_id))->get()->row();
		
		if (count($check_valid)){
			$user_id = $check_valid->user_id;
			$todolist = $this->db->select('todoid,assigned_by')->from('todolist')->WHERE(array('team_id'=>$team_id,'accountability'=>$user_id,'status'=>'0'))->get()->result_array();
			
			if (count($todolist)){
				foreach($todolist as $list){
					$this->db->insert('todo_reassign_action',array('todoid'=>$list['todoid'],'team_id'=>$team_id,'prev_assigned_by'=>$list['assigned_by'],'prev_accountability'=>$user_id,'updated_at'=>today()[0]));
				}
			}
			$this->db->where(array('member_id'=>$member_id))->update('team_members',array('status' =>1));
			
			$this->db->where(array('team_id'=>$team_id,'accountability'=>$user_id))->update('todolist',array('is_active' =>1));
			
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return array('status' => 500,'msg' => 'Internal server error. Please try again!');
			} else {
				$this->db->trans_commit();
				return array('status' => 200,'msg' =>'Member deleted successfully');
			}
		}else{
			return array('status' => 400,'msg' => 'Invalid team member!');
		}
		
	}
	## Remove Team Member
	public function delete_invites($input)
	{	
		$company_id = $this->session->userdata()['userdata']['company_id'];
		$invite_id	= $this->Aes_encryption->aesDecryption($this->secret,urldecode($input['invite_id']));
		$team_id	= $this->Aes_encryption->aesDecryption($this->secret,urldecode($input['team_id']));
		
		$check_valid = $this->db->select('user_id')->from('team_invites')->WHERE(array('team_id'=>$team_id,'invite_id'=>$invite_id))->get()->row();
		
		if (count($check_valid)){
			$this->db->trans_start();
			$this->db->where(array('invite_id'=>$invite_id))->delete('team_invites');
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return array('status' => 500,'msg' => 'Internal server error. Please try again!');
			} else {
				$this->db->trans_commit();
				return array('status' => 200,'msg' =>'Invite deleted successfully');
			}
		}else{
			return array('status' => 400,'msg' => 'Invalid team member!');
		}
		
	}
	## Delete Assigned Task
	public function delete_assignedTask($input)
	{	
		$company_id = $this->session->userdata()['userdata']['company_id'];
		$todoid	= $this->Aes_encryption->aesDecryption($this->secret,urldecode($input['todoid']));
		$team_id	= $this->Aes_encryption->aesDecryption($this->secret,urldecode($input['team_id']));
		
		$check_valid = $this->db->select('accountability')->from('todolist')->WHERE(array('team_id'=>$team_id,'todoid'=>$todoid))->get()->row();
		
		if (count($check_valid)){
			$accountability =	$check_valid->accountability;
		
			$this->db->trans_start();
			$this->db->where(array('todoid'=>$todoid))->delete('todolist');
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return array('status' => 500,'msg' => 'Internal server error. Please try again!');
			}else{
				$notification = $this->db->select('todo_assigned')->from('notifications')->WHERE(array('user_id'=>$accountability))->get()->row();
				if($notification!=''){
					if($notification->todo_assigned > 0){
						$this->db->set('todo_assigned','todo_assigned-1',false)->where(array('user_id'=>$accountability))->update('notifications');
					}
				}
				$this->db->trans_commit();
				return array('status' => 200,'msg' =>'Deleted successfully');
			}
		}else{
			return array('status' => 400,'msg' => 'Invalid task!');
		}
	}
	## Delete News Channel
	public function delete_newsChannel($input)
	{	
		$company_id = $this->session->userdata()['userdata']['company_id'];
		$channel_id	= $this->Aes_encryption->aesDecryption($this->secret,urldecode($input['channel_id']));
		
		$check_valid = $this->db->select('channel_logo')->from('news_channel')->WHERE(array('company_id'=>$company_id,'channel_id'=>$channel_id))->get()->row();
		
		if (count($check_valid)){
			$channel_logo	= $check_valid->channel_logo;
			
			$this->db->trans_start();
			$this->db->where(array('channel_id'=>$channel_id))->delete('news_channel');
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return array('status' => 500,'msg' => 'Internal server error. Please try again!');
			} else {
				$channel_logo !="" ? unlink("./".$channel_logo) : '';
				$this->db->where(array('channel_id'=>$channel_id))->delete('news_deleted');
				$this->db->where(array('channel_id'=>$channel_id))->delete('news_brief');
				$this->db->where(array('channel_id'=>$channel_id))->delete('newschannel_subscribers');
				$this->db->trans_commit();
				return array('status' => 200,'msg' =>'Deleted successfully');
			}
		}else{
			return array('status' => 400,'msg' => 'Invalid news channel!');
		}
	}
	## Delete News Channel News Brief
	public function deleteChannelNewsBrief($input)
	{	
		$company_id		= $this->session->userdata()['userdata']['company_id'];
		$newsbrief_id	= $this->Aes_encryption->aesDecryption($this->secret,urldecode($input['newsbrief_id']));
		
		$check_valid = $this->db->select('channel_id,picture')->from('news_brief')->WHERE(array('newsbrief_id'=>$newsbrief_id,'company_id'=>$company_id))->get()->row();
		
		if (count($check_valid)){
			$this->db->trans_start();
			$this->db->where(array('newsbrief_id'=>$newsbrief_id))->delete('news_brief');
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return array('status' => 500,'msg' => 'Internal server error. Please try again!');
			} else {
				$check_valid->picture !="" ? unlink("./".$check_valid->picture) : '';
				$this->db->where(array('newsbrief_id'=>$newsbrief_id))->delete('news_deleted');
				$this->db->trans_commit();
				return array('status' => 200,'msg' =>'Deleted successfully');
			}
		}else{
			return array('status' => 400,'msg' => 'Invalid follower id!');
		}
	}
	## Delete News Channel Follower
	public function deleteChannelFollower($input)
	{	
		$company_id		= $this->session->userdata()['userdata']['company_id'];
		$channel_id		= $this->Aes_encryption->aesDecryption($this->secret,urldecode($input['channel_id']));
		$subscribe_id	= $this->Aes_encryption->aesDecryption($this->secret,urldecode($input['subscribe_id']));
		
		$check_valid = $this->db->select('user_id')->from('newschannel_subscribers')->WHERE(array('subscribe_id'=>$subscribe_id,'channel_id'=>$channel_id))->get()->row();
		
		if (count($check_valid)){
			$this->db->trans_start();
			$this->db->where(array('subscribe_id'=>$subscribe_id))->delete('newschannel_subscribers');
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return array('status' => 500,'msg' => 'Internal server error. Please try again!');
			} else {
				$this->db->where(array('channel_id'=>$channel_id,'user_id'=>$check_valid->user_id))->delete('news_deleted');
				$this->db->trans_commit();
				return array('status' => 200,'msg' =>'Deleted successfully');
			}
		}else{
			return array('status' => 400,'msg' => 'Invalid follower id!');
		}
	}
	
	## Block Unblock Templates
	public function blockUnblockTemplate($template_id){
		$template_id = $this->Aes_encryption->aesDecryption($this->secret,urldecode($template_id));
		$company_id = $this->session->userdata()['userdata']['company_id'];
		$user_id = $this->session->userdata()['userdata']['user_id'];
		$check = $this->db->query("select is_active FROM card_templates WHERE template_id='".$template_id."' AND company_id='".$company_id."'")->row_array();
		if(count($check)){
			
			$is_active = $check['is_active'];
			$this->db->trans_start();
			if($is_active==1){
				$is_active = 2;
				$msg = 'Template blocked Successfully';
				$status = 200;
			}else{
				$is_active = 1;
				$msg = 'Template unblocked Successfully';
				$status = 201;
			}
			$this->db->where(array('template_id'=>$template_id))->update('card_templates',array('is_active'=>$is_active,'added_by'=>$user_id,'updated_at'=>today()[0]));
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return array('status' => 500,'msg' => 'Internal server error. Please try again!');
			}else{
				$this->db->trans_commit();
				return array('status' => $status,'msg' =>$msg);
			}
		}else{
			return array('status' => 400,'msg' => 'Access Denied!');
		}
	}
	
	
}