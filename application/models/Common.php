<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Common extends CI_Model {
	
	
	/*
	* Get Country Code
	*/
	public function get_countryCode()
	{	
		$data = $this->db->select('countries_name, country_code, countries_iso_code as flag')->from('countries')->order_by('countries_name','ASC')->get()->result_array();
		for($i=0;$i<count($data);$i++){
			$data[$i]['flag'] = base_url().'media/flags/'.strtolower($data[$i]['flag']).'.png';
		}
		return $data;
	}
	/*
	* Get TizeZone Name
	*/
	public function get_timeZoneName()
	{	
		$data = $this->db->select('zone_id, country_code, zone_name')->from('countries_timezone')->order_by('zone_name','ASC')->get()->result_array();
		return $data;
		
	}
	
	/*
	* Get Designations List
	*/
	public function get_designationsList($company_id)
	{
		$data = $this->db->select('designation_id,designation')->from('designations')->order_by('designation','ASC')->get()->result_array();
		return $data;
	}
	/*
	* Get Company User List
	*/
	public function get_CompanyMembers($company_id,$not_included = array(0))
	{
		$data 	= $this->db->select('user_id,user_name,email,designation')->from('users')->WHERE(array('is_active'=>'1','company_id'=>$company_id))->where_not_in('user_id',$not_included)->order_by('user_name','ASC')->get()->result_array();
		foreach( $data as $key => $row )
		{
			$row['designation'] 	=  getDesignationdata($row['designation'],'designation');
			$data[$key] = $row;
		}
		return $data;
	}
	
	
}

/* End of file M_admin.php */
/* Location: ./application/models/M_admin.php */