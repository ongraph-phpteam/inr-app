<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Super_designation extends CI_Model {
	
	public function select_designations(){
		$data = $this->db->select('*')->from('designations')->where(array("company_id"=>0,'is_active'=>1))->get()->result_array();
		return $data;
	}
	
	public function newdesignation($input){
		$check = $this->db->select('designation_id')->from('designations')->where(array('designation'=>$input['designation']))->get()->row();
		if($check==''){
			unset($input['submit']);
			$this->db->trans_start();
			$this->db->insert('designations',$input);
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return array('status' => 500,'msg' => 'Internal server error. Please try again!');
			}else{
				$this->db->trans_commit();
				return array('status' => 200,'msg' =>'Designation added successfully.');
			}	
		}else{
			return array('status' => 400,'msg' =>'Designation name already in list');
		}
		
	}
	public function view_designation($id){
		$data = $this->db->select('*')->from('designations')->where(array('designation_id'=>$id))->get()->row_array();
		if (count($data)){
			return array('data'=>$data,'status' => 200,'msg' =>'');
			
		}else{
			return array('status' => 400,'msg' =>'Designation not found for update!');
		}
	}
	public function editdesignation($input){
		$check = $this->db->select('designation_id')->from('designations')->where(array('designation'=>$input['designation'],'designation_id!='=>$input['designation_id']))->get()->row();
		if($check==''){
			unset($input['submit']);
			$this->db->trans_start();
			$this->db->where('designation_id',$input['designation_id'])->update('designations',array('designation'=>$input['designation']));
			
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return array('status' => 500,'msg' => 'Internal server error. Please try again!');
			}else{
				$this->db->trans_commit();
				return array('status' => 200,'msg' =>'Designation updated!');
			}	
		}else{
			return array('status' => 400,'msg' =>'Designation name already in list!');
		}
	}
	public function delete_designation($designation_id){
		$this->db->where('designation_id',$designation_id)->update('designations',array('is_active'=>0));
		return true;
	}
}