<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Company_news_channels extends CI_Model {
	
	public function select_allChannels(){
		$company_id = $this->session->userdata()['userdata']['company_id'];
		
		$data = $this->db->select('news_channel.channel_id,news_channel.channel_name,news_channel.type, news_channel.channel_description, news_channel.channel_logo, news_channel.channel_adminid, news_channel.created_at, users.user_name, users.email, users.designation,users.profile_picture')->from('news_channel')->where(array('news_channel.company_id'=>$company_id, 'news_channel.is_active'=>'1'))->join('users', 'users.user_id = news_channel.channel_adminid')->order_by('news_channel.channel_id','DESC')->get()->result_array();
		//print $this->db->last_query(); EXIT;
		foreach( $data as $key => $row )
		{
			$expired = date('Y-m-d H:i:s', strtotime('-1 day', today()[1]));
			$this->db->where(array('company_id'=>$company_id,'published_at<'=>$expired))->update('news_brief',array('is_active'=>2));
		
			$row['channel_logo'] !=''  ? $row['channel_logo'] = base_url().$row['channel_logo'] : '';
			$row['profile_picture']!=''  ? $row['profile_picture']	= base_url(). $row['profile_picture']: '';
			$row['designation'] 	=  getDesignationdata($row['designation'],'designation');
			$row['subscribers_count'] = (string)$this->db->select('subscribe_id')->from('newschannel_subscribers')->where(array('channel_id'=>$row['channel_id']))->get()->num_rows();
			$row['active_newsbrief'] = (string)$this->db->select('newsbrief_id')->from('news_brief')->where(array('channel_id'=>$row['channel_id'],'is_active'=>1))->get()->num_rows();
			
			$data[$key] = $row;
		}
		return $data;
	}
	## Add New News Channel
	public function new_channel($input){
		$check = $this->db->select('channel_name')->from('news_channel')->where(array('channel_name'=>$input['channel_name'],'company_id'=>$input['company_id']))->get()->row();
		if($check==''){
			unset($input['submit']);
			$input['is_active'] 	= '1';
			$input['created_at']	= today()[0];
			$input['updated_at']	= today()[0];
			$input['channel_logo']	= '';
			$date = date("Y-m");
			$config['allowed_types']= '*';
			if (!empty($_FILES['channel_logo']['name'])){
				$path	=  './media/newschannel/logo/'.$date."/";
				if (!is_dir($path)){
					mkdir($path , 0777);
					$content = "<!DOCTYPE html><html><head><title>403 Forbidden</title></head><body><p>Access denied!</p></body></html>"; 
					$fp = fopen($path."index.html","wb"); 
					fwrite($fp,$content); 
					fclose($fp); 
				}
				$config['file_name']	= date("Ymd")."_NCL-".rand(100000,999999).'-'.time();
				$config['upload_path']	= $path;
				$this->upload->initialize($config);
				if (!$this->upload->do_upload('channel_logo')){
					return array('status' =>400,'msg' =>"File uploading error: ".strip_tags($this->upload->display_errors()));
					exit;
				}else{
					$image_data = $this->upload->data();
					$config['image_library'] = 'gd2';
					$config['source_image'] = $path.$image_data['file_name']; //get original image
					$config['maintain_ratio'] = TRUE;
					$config['create_thumb'] = false;
					$config['height'] = 150;
					$this->load->library('image_lib', $config);
					if (!$this->image_lib->resize()) {
						$file = $path.$image_data['file_name'];
						if (file_exists($file)) {
							unlink($file);
						}
						return array('status' =>400,'msg' =>"Image resizing error: ".strip_tags($this->image_lib->display_errors()));
						exit;
					}else{
						$input['channel_logo'] = ltrim($path,"./").$image_data['file_name'];
					}
				}
			}
			$this->db->trans_start();
			$this->db->insert('news_channel',$input);
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return array('status' => 500,'msg' =>'Internal server error.');
			} else {
				$this->db->insert('newschannel_subscribers',array('channel_id'=>$this->db->insert_id(),'user_id'=>$input['channel_adminid'],'subscribed_date'=>today()[0]));
				$this->db->trans_commit();
				return array('status' => 200,'msg' =>'News channel created.');
			}
		}else{
			return array('status' => 400,'msg' =>'News Channel already in list');
		}
	}
	### View Channel
	public function view_channel($id){
		$data = $this->db->select('*')->from('news_channel')->where(array('channel_id'=>$id))->get()->row_array();
		if (count($data)){
			$data['channel_logo'] !=''  ? $data['channel_logo'] = base_url().$data['channel_logo'] : '';
			return array('edit'=>$data,'status' => 200,'msg' =>'');
			
		}else{
			return array('status' => 400,'msg' =>'News Channel not found for update!');
		}
	}
	## Update News Channel data
	public function update_channel($input){
		$check = $this->db->select('channel_name')->from('news_channel')->where(array('channel_name'=>$input['channel_name'],'company_id'=>$input['company_id'],'channel_id!='=>$input['channel_id']))->get()->row();
		
		if($check==''){
			unset($input['submit']);
			$date = date("Y-m");
			$config['allowed_types']= '*';
			$input['channel_logo'] = getNewsChannelData($input['channel_id'],'channel_logo');
			if (!empty($_FILES['channel_logo']['name'])){
				$path	=  './media/newschannel/logo/'.$date."/";
				if (!is_dir($path)){
					mkdir($path , 0777);
					$content = "<!DOCTYPE html><html><head><title>403 Forbidden</title></head><body><p>Access denied!</p></body></html>"; 
					$fp = fopen($path."index.html","wb"); 
					fwrite($fp,$content); 
					fclose($fp); 
				}
				$config['file_name']	= date("Ymd")."_NCL-".rand(100000,999999).'-'.time();
				$config['upload_path']	= $path;
				$this->upload->initialize($config);
				if (!$this->upload->do_upload('channel_logo')){
					return array('status' =>400,'msg' =>"File uploading error: ".strip_tags($this->upload->display_errors()));
					exit;
				}else{
					$image_data = $this->upload->data();
					$config['image_library'] = 'gd2';
					$config['source_image'] = $path.$image_data['file_name']; //get original image
					$config['maintain_ratio'] = TRUE;
					$config['create_thumb'] = false;
					$config['height'] = 150;
					$this->load->library('image_lib', $config);
					if (!$this->image_lib->resize()) {
						$file = $path.$image_data['file_name'];
						if (file_exists($file)) {
							unlink($file);
						}
						return array('status' =>400,'msg' =>"Image resizing error: ".strip_tags($this->image_lib->display_errors()));
						exit;
					}else{
						if (file_exists("./".$input['channel_logo'])) {
							unlink("./".$input['channel_logo']);
						}
						$input['channel_logo'] = ltrim($path,"./").$image_data['file_name'];
					}
				}
			}
			$this->db->trans_start();
			$this->db->where('channel_id',$input['channel_id'])->update('news_channel',array('channel_name'=>$input['channel_name'],'channel_logo'=>$input['channel_logo'],'type'=>$input['type'],'updated_at'=>today()[0]));
			
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return array('status' => 500,'msg' => 'Internal server error. Please try again!');
			}else{
				$this->db->trans_commit();
				return array('status' => 200,'msg' =>'News channel updated!');
			}	
		}else{
			return array('status' => 400,'msg' =>'News Channel name already in list!');
		}
	}
	
	## View Channel Followers
	public function channel_followers($channel_id){
		if (checkValidChannel($channel_id)){
			$data = $this->db->select('newschannel_subscribers.subscribe_id, newschannel_subscribers.channel_id, newschannel_subscribers.user_id,newschannel_subscribers.subscribed_date, users.user_name, users.email, users.designation, users.profile_picture')->from('newschannel_subscribers')->WHERE(array('channel_id'=>$channel_id))->join('users','users.user_id = newschannel_subscribers.user_id')->order_by('newschannel_subscribers.subscribe_id','DESC')->get()->result_array();
			foreach( $data as $key => $row )
			{
				$row['designation'] 	=  getDesignationdata($row['designation'],'designation');
				$row['profile_picture']!=''  ? $row['profile_picture']	= base_url(). $row['profile_picture']: '';
				$data[$key] = $row;
			}
			return array('status' => 200,'msg' => '','data'=>$data);
		}else{
			return array('status' => 400,'msg'=>'Channel not exist');
		}
	}
	
	public function channel_newsbriefs($channel_id){
		$data = $this->db->select('news_brief.newsbrief_id, news_brief.channel_id, news_brief.news, news_brief.picture, news_brief.published_at, news_channel.channel_name, news_channel.channel_description, news_channel.channel_logo, news_channel.channel_adminid, users.user_name, users.profile_picture, users.designation, users.email')->from('news_brief')->where(array('news_brief.is_active'=>'1','news_brief.channel_id'=>$channel_id))->join('news_channel', 'news_brief.channel_id = news_channel.channel_id')->join('users', 'users.user_id = news_channel.channel_adminid')->order_by('news_brief.newsbrief_id','DESC')->get()->result_array();
		//print $this->db->last_query(); EXIT;
		foreach( $data as $key => $row )
		{
			$row['picture']		 = $row['picture'] !=''  ? $row['picture'] = base_url().$row['picture'] : '';
			$row['channel_logo'] = $row['channel_logo'] !=''  ? $row['channel_logo'] = base_url().$row['channel_logo'] : '';
			$row['designation']	 = getDesignationdata($row['designation'],'designation');
			$row['profile_picture']  = $row['profile_picture']!=''  ? $row['profile_picture']	= base_url(). $row['profile_picture'] : '';
			$row['published_at'] = convterUTCtoLocal($row['published_at'],'UTC');
			$data[$key] = $row;
		}
		return array('status' => 200,'data'=>$data,'msg' =>'');
	}
	
}