<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Company_auth extends CI_Model {
	
	public function login($user, $pass) {
		$this->db->select('user_id,company_id,user_name,email,password');
		$this->db->from('users');
		$this->db->where(array('email'=>$user,'account_type'=>2));
		$check = $this->db->get();
		if ($check->num_rows() == 1) {
			$data = $check->row_array();
			$hashed_password = $data['password'];
			unset($data['password']);
			if (!hash_equals($hashed_password, crypt($pass,APP_SALT))) {
			   return 400;
			}else{
				return $data;
			}
		} else {
			return 401;
		}
	}
	public function masquerade($company_id) {
		$this->db->select('company_id');
		$this->db->from('companies');
		$this->db->where('company_id', $company_id);
		$data = $this->db->get();

		if ($data->num_rows() == 1) {
			$data = array('user_id'=>'0','company_id'=>$company_id,'user_name'=>'Super Admin','email'=>'super@ceeca.com','profile_picture'=>'');
			return $data;
		} else {
			return false;
		}
	}
}

/* End of file M_auth.php */
/* Location: ./application/models/M_auth.php */