<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Company_users extends CI_Model {
	
	public function select_allusers(){
		$company_id = $this->session->userdata()['userdata']['company_id'];
		$user_id = $this->session->userdata()['userdata']['user_id'];
		$data = $this->db->select('*')->from('users')->where(array('company_id'=>$company_id,'user_id!='=>$user_id))->order_by('user_name','ASC')->get()->result_array();
		return $data;
	}
	
	public function newuser($input){
		if($input['company_id'] ==  check_valid_domain_help($input['email'])){
			$check = $this->db->select('email')->from('users')->where(array('email'=>$input['email']))->get()->row();
			if($check==''){
				$check = $this->db->select('user_id')->from('users')->where(array('mobile_number'=>$input['mobile_number'],'country_code'=>$input['country_code']))->get()->num_rows();
				if ($check){
					return array('status' => 400,'msg' =>'Account already registered with '.$input['mobile_number'])." mobile number";
				}else{
					unset($input['submit']);
					$input['is_active']			= '1';
					$input['profile_picture']	= '';
					$config['allowed_types'] 	= '*';
					$config['max_size'] 		= '500';
					$date 						= date("Y-m");
					
					if (!empty($_FILES['profile_picture']['name'])){
						$path	=  './media/profile/'.$date."/";
						if (!is_dir($path)){
							mkdir($path , 0777);
							$content = "<!DOCTYPE html><html><head><title>403 Forbidden</title></head><body><p>Access denied!</p></body></html>"; 
							$fp = fopen($path."index.html","wb"); 
							fwrite($fp,$content); 
							fclose($fp); 
						}
						$config['file_name']	= date("Ymd")."_PP-".rand(1000,9999).'-'.time();
						$config['upload_path']	= $path;
						$this->upload->initialize($config);
						if (!$this->upload->do_upload('profile_picture')){
							return array('status' =>400,'msg' =>"Profile picture uploading error: ".strip_tags($this->upload->display_errors()));
							exit;
						}else{
							$input['profile_picture'] = ltrim($path,"./").$this->upload->data()['file_name'];
						}
					}
					$rand = rand(11111111,99999999);
					$input['password']		= crypt($rand,APP_SALT); 
					$this->db->trans_start();
					$this->db->insert('users',$input);
					if ($this->db->trans_status() === FALSE){
						$this->db->trans_rollback();
						return array('status' => 500,'msg' => 'Internal server error. Please try again!');
					}else{
						$user_id = $this->db->insert_id();
						$channels = $this->db->query("SELECT `channel_id` FROM `news_channel` WHERE `company_id`='".$input['company_id']."' AND `type`='2' AND `is_active`='1'")->row_array();
						foreach($channels as $channel_id){
							$this->db->insert('newschannel_subscribers',array('channel_id'=>$channel_id,'user_id'=>$user_id,'subscribed_date'=>today()[0]));
						}
						$this->db->trans_commit();
						$this->load->library('email');
						$this->email->from('noreply@ceeca.com', 'Ceeca Support Team');
						$this->email->to($input['email']);
						$this->email->subject('Ceeca INR App login credential');
						$this->email->set_mailtype("html");
						$email_message .= "<div style='width:400px;background:#fff;color:#000;font-family:Source Sans Pro,sans-serif;padding:10px;' >";	
						$email_message .= "<div>Hi, ".$input['user_name']."</div>" ;
						$email_message .= "<div>Please find your Ceeca INR app login credential listed below:</div>" ;
						$email_message .= "<div>Username : ".$input['email']."</div>" ;
						$email_message .= "<div>Password : ".$rand."</div>" ;
						$email_message .= "<div>Please use these credential for CEECA INR app account. After successful login we recommend you to change your password.</div>" ;
						$email_message .= "<div>Thanks</div>" ;
						$this->email->message($email_message);
						$this->email->send();
						return array('status' => 200,'msg' =>'User account is created. Login credential are sent to user email successfully.');
					}
				}					
			}else{
				return array('status' => 400,'msg' =>'Account already registered with '.$input['domain']);
			}
		}else{
			return array('status' => 400,'msg' =>'Company account not registered with this domain.');
		}
	}
	
	public function view_userprofile($id){
		$data = $this->db->select('*')->from('users')->where(array('user_id'=>$id))->get()->row_array();
		if (count($data)){
			return array('data'=>$data,'status' => 200,'msg' =>'');
			
		}else{
			return array('status' => 400,'msg' =>'User account not exist!');
		}
	}
	
	public function edit_userprofile($input){
		
		$check = $this->db->select('email')->from('users')->where(array('email'=>$input['email'],'user_id!='=>$input['user_id']))->get()->row();
			if($check==''){
				$check = $this->db->select('user_id')->from('users')->where(array('mobile_number'=>$input['mobile_number'],'country_code'=>$input['country_code']))->where(array('user_id!='=>$input['user_id']))->get()->num_rows();
				if ($check){
					return array('status' => 400,'msg' =>'Account already registered with '.$input['mobile_number'])." mobile number";
				}else{
					unset($input['submit']);
					$input['profile_picture']	= getusersdata($input['user_id'],'profile_picture');
					$config['allowed_types'] 	= '*';
					$config['max_size'] 		= '500';
					$date 						= date("Y-m");
					
					if (!empty($_FILES['profile_picture']['name'])){
						$path	=  './media/profile/'.$date."/";
						if (!is_dir($path)){
							mkdir($path , 0777);
							$content = "<!DOCTYPE html><html><head><title>403 Forbidden</title></head><body><p>Access denied!</p></body></html>"; 
							$fp = fopen($path."index.html","wb"); 
							fwrite($fp,$content); 
							fclose($fp); 
						}
						$config['file_name']	= date("Ymd")."_PP-".rand(1000,9999).'-'.time();
						$config['upload_path']	= $path;
						$this->upload->initialize($config);
						if (!$this->upload->do_upload('profile_picture')){
							return array('status' =>400,'msg' =>"Profile picture uploading error: ".strip_tags($this->upload->display_errors()));
							exit;
						}else{
							$input['profile_picture'] = ltrim($path,"./").$this->upload->data()['file_name'];
						}
					}
					$this->db->trans_start();
					$this->db->where(array('user_id'=>$input['user_id']))->update('users',array('user_name'=>$input['user_name'],'profile_picture'=>$input['profile_picture'],'designation'=>$input['designation'],'email'=>$input['email'],'country_code'=>$input['country_code'],'mobile_number'=>$input['mobile_number'],'updated_at'=>today()[0]));
					if ($this->db->trans_status() === FALSE){
						$this->db->trans_rollback();
						return array('status' => 500,'msg' => 'Internal server error. Please try again!');
					}else{
						$this->db->trans_commit();
						return array('status' => 200,'msg' =>'User account is updated.');
					}
				}					
			}else{
				return array('status' => 400,'msg' =>'Account already registered with '.$input['email']);
			}
	}
	
}