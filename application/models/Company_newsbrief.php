<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Company_newsbrief extends CI_Model {
	
	public function select_allnewsbrief(){
		$company_id = $this->session->userdata()['userdata']['company_id'];
		$data = $this->db->select('news_brief.newsbrief_id, news_brief.channel_id, news_brief.news, news_brief.picture, news_brief.published_at, news_channel.channel_name, news_channel.channel_description, news_channel.channel_logo, news_channel.channel_adminid, users.user_name, users.profile_picture as profile_pic, users.designation as position, users.email')->from('news_brief')->where(array('news_brief.company_id'=>$company_id))->join('news_channel', 'news_brief.channel_id = news_channel.channel_id')->join('users', 'users.user_id = news_channel.channel_adminid')->order_by('news_brief.newsbrief_id','DESC')->get()->result_array();
		foreach( $data as $key => $row )
		{
			$row['picture']		 = $row['picture'] !=''  ? $row['picture'] = base_url().$row['picture'] : '';
			$row['channel_logo'] = $row['channel_logo'] !=''  ? $row['channel_logo'] = base_url().$row['channel_logo'] : '';
			$row['position']	 = getDesignationdata($row['position'],'designation');
			$row['profile_pic']  = $row['profile_pic']!=''  ? $row['profile_pic']	= base_url(). $row['profile_pic'] : '';
			$row['published_at'] = convterUTCtoLocal($row['published_at'],"Asia/Kolkata");
			$data[$key] = $row;
		}
		return $data;
	}
	
	
}