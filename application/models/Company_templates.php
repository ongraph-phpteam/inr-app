<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Company_templates extends CI_Model {
	
	public function select_allChannels(){
		$company_id = $this->session->userdata()['userdata']['company_id'];
		
		$data = $this->db->select('*')->from('card_templates')->where(array('company_id'=>$company_id))->order_by(' 	template_id','DESC')->get()->result_array();
		foreach( $data as $key => $row )
		{
			$row['background_layer']= base_url().$row['background_layer'];
			$row['front_layer']		= base_url().$row['front_layer'];
			$row['used_by'] = (string)$this->db->select('card_id')->from('my_cards')->where(array('template_id'=>$row['template_id'],'master_card_id'=>0,'is_active'=>1))->get()->num_rows();
			
			$data[$key] = $row;
		}
		return $data;
	}
	## Add New News Channel
	public function new_template($input){
		
		unset($input['submit']);
		$input['updated_at']	= today()[0];
		$input['background_layer']	= '';
		$input['front_layer']		= '';
		$date = date("Y-m");
		$config['allowed_types']= '*';
		if (!empty($_FILES['background_layer']['name'])){
			$path	=  './media/templates/'.$date."/";
			if (!is_dir($path)){
				mkdir($path , 0777);
				$content = "<!DOCTYPE html><html><head><title>403 Forbidden</title></head><body><p>Access denied!</p></body></html>"; 
				$fp = fopen($path."index.html","wb"); 
				fwrite($fp,$content); 
				fclose($fp); 
			}
			$config['file_name']	= date("Ymd")."_BL-".rand(100000,999999).'-'.time();
			$config['upload_path']	= $path;
			$this->upload->initialize($config);
			if (!$this->upload->do_upload('background_layer')){
				return array('status' =>400,'msg' =>"Background layer uploading error: ".strip_tags($this->upload->display_errors()));
				exit;
			}else{
				$image_data = $this->upload->data();
				$input['background_layer'] = ltrim($path,"./").$image_data['file_name'];
			}
		}
		if (!empty($_FILES['front_layer']['name'])){
			$path	=  './media/templates/'.$date."/";
			if (!is_dir($path)){
				mkdir($path , 0777);
				$content = "<!DOCTYPE html><html><head><title>403 Forbidden</title></head><body><p>Access denied!</p></body></html>"; 
				$fp = fopen($path."index.html","wb"); 
				fwrite($fp,$content); 
				fclose($fp); 
			}
			$config['file_name']	= date("Ymd")."_FL-".rand(100000,999999).'-'.time();
			$config['upload_path']	= $path;
			$this->upload->initialize($config);
			if (!$this->upload->do_upload('front_layer')){
				return array('status' =>400,'msg' =>"Front layer uploading error: ".strip_tags($this->upload->display_errors()));
				exit;
			}else{
				$image_data = $this->upload->data();
				$input['front_layer'] = ltrim($path,"./").$image_data['file_name'];
			}
		}
		if($input['background_layer']!="" && $input['front_layer']!=""){
			$this->db->trans_start();
			$this->db->insert('card_templates',$input);
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return array('status' => 500,'msg' =>'Internal server error.');
			} else {
				$this->db->trans_commit();
				return array('status' => 200,'msg' =>'Business card template added successfully');
			}
		}else{
			return array('status' => 400,'msg' =>'Background and Front layers are both required fields.');
		}
		
	}
	### View Template
	public function view_template($id){
		$data = $this->db->select('*')->from('card_templates')->where(array('template_id'=>$id))->get()->row_array();
		if (count($data)){
			return array('edit'=>$data,'status' => 200,'msg' =>'');
		}else{
			return array('status' => 400,'msg' =>'Invalid template for update!');
		}
	}
	## Update News Channel data
	public function update_template($input){
		$check = $this->db->select('*')->from('card_templates')->where(array('template_id'=>$input['template_id'],'company_id'=>$input['company_id']))->get()->row_array();
		
		if(count($check)){
			$input['updated_at']	= today()[0];
			$input['background_layer']	= $check['background_layer'];
			$input['front_layer']		= $check['front_layer'];
			$date = date("Y-m");
			$config['allowed_types']= '*';
			if (!empty($_FILES['background_layer']['name'])){
				$path	=  './media/templates/'.$date."/";
				if (!is_dir($path)){
					mkdir($path , 0777);
					$content = "<!DOCTYPE html><html><head><title>403 Forbidden</title></head><body><p>Access denied!</p></body></html>"; 
					$fp = fopen($path."index.html","wb"); 
					fwrite($fp,$content); 
					fclose($fp); 
				}
				$config['file_name']	= date("Ymd")."_BL-".rand(100000,999999).'-'.time();
				$config['upload_path']	= $path;
				$this->upload->initialize($config);
				if (!$this->upload->do_upload('background_layer')){
					return array('status' =>400,'msg' =>"Background layer uploading error: ".strip_tags($this->upload->display_errors()));
					exit;
				}else{
					$image_data = $this->upload->data();
					unlink("./".$input['background_layer']);
					$input['background_layer'] = ltrim($path,"./").$image_data['file_name'];
				}
			}
			if (!empty($_FILES['front_layer']['name'])){
				$path	=  './media/templates/'.$date."/";
				if (!is_dir($path)){
					mkdir($path , 0777);
					$content = "<!DOCTYPE html><html><head><title>403 Forbidden</title></head><body><p>Access denied!</p></body></html>"; 
					$fp = fopen($path."index.html","wb"); 
					fwrite($fp,$content); 
					fclose($fp); 
				}
				$config['file_name']	= date("Ymd")."_FL-".rand(100000,999999).'-'.time();
				$config['upload_path']	= $path;
				$this->upload->initialize($config);
				if (!$this->upload->do_upload('front_layer')){
					return array('status' =>400,'msg' =>"Front layer uploading error: ".strip_tags($this->upload->display_errors()));
					exit;
				}else{
					$image_data = $this->upload->data();
					unlink("./".$input['front_layer']);
					$input['front_layer'] = ltrim($path,"./").$image_data['file_name'];
				}
			}
			
			$this->db->trans_start();
			$this->db->where('template_id',$input['template_id'])->update('card_templates',array('background_layer'=>$input['background_layer'],'front_layer'=>$input['front_layer'],'added_by'=>$input['added_by'],'updated_at'=>today()[0]));
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return array('status' => 500,'msg' =>'Internal server error.');
			} else {
				$this->db->trans_commit();
				return array('status' => 200,'msg' =>'Updated successfully');
			}
		}else{
			return array('status' => 400,'msg' =>'Something went wrong. Try again.');
		}
	}
	
	
	
}