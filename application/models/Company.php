<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Company extends CI_Model {
	
	public function get_analytics($company_id){
		
		# Total Users
		$total_users = $this->db->select('user_id')->from('users')->where(array('is_active'=>1,'company_id'=>$company_id))->get()->num_rows();
		# Active user with in 60 days
		$sixty_days_before = date('Y-m-d H:i:s', strtotime('-60 days'));
		$total_active_users = $this->db->select('user_id')->from('users')->where(array('last_login >'=>$sixty_days_before,'company_id'=>$company_id))->get()->num_rows();
		# New user registration with in last 7 days
		$this_week = date('Y-m-d H:i:s', strtotime('-7 days'));
		$new_users = $this->db->select('user_id')->from('users')->where(array('registered_at>'=>$this_week,'company_id'=>$company_id))->get()->num_rows();
		# Total News channel
		$total_newchannel = $this->db->select('channel_id')->from('news_channel')->where(array('is_active'=>1,'company_id'=>$company_id))->get()->num_rows();
		#Total Ttalk Teams
		$total_ttalks = $this->db->select('team_id')->from('teams')->where(array('is_active'=>1,'company_id'=>$company_id))->get()->num_rows();
		# Total assigned Tasks 
		$total_todo = $this->db->select('todoid')->from('todolist')->where(array('status'=>0))->join('teams','teams.team_id=todolist.team_id AND teams.company_id='.$company_id.'')->get()->num_rows();
		
		return array('total_users'=>$total_users,'active_users'=>$total_active_users,'this_week'=>$this_week,'total_newchannel'=>$total_newchannel,'total_ttalks'=>$total_ttalks,'total_todo'=>$total_todo);
	}
	
	public function users_chart($company_id){
		$user_graph = $this->db->select("count(user_id) as total, DATE_FORMAT(registered_at, '%Y-%m-01') as date,DATE_FORMAT(registered_at, '%Y') as year,DATE_FORMAT(registered_at, '%m') as month")->from('users')->where(array('company_id'=>$company_id))->group_by("DATE_FORMAT(registered_at, '%Y-%m-01')")->order_by("year","ASC")->order_by("month","asc")->get()->result_array();
		return $user_graph;
		
	}
	
	public function latest_newsbriefs($company_id){
		$expired = date('Y-m-d H:i:s', strtotime('-1 day', today()[1]));
		$this->db->where(array('company_id'=>$company_id,'published_at<'=>$expired))->update('news_brief',array('is_active'=>2));
		$data = $this->db->select('news_brief.newsbrief_id, news_brief.channel_id, news_brief.news, news_brief.picture, news_brief.published_at, news_channel.channel_name, news_channel.channel_description, news_channel.channel_logo, news_channel.channel_adminid, users.user_name, users.profile_picture as profile_pic, users.designation as position, users.email')->from('news_brief')->where(array('news_brief.company_id'=>$company_id,'news_brief.is_active'=>1))->join('news_channel', 'news_brief.channel_id = news_channel.channel_id')->join('users', 'users.user_id = news_channel.channel_adminid')->order_by('news_brief.newsbrief_id','DESC')->limit(5,0)->get()->result_array();
		foreach( $data as $key => $row )
		{
			$row['picture']		 = $row['picture'] !=''  ? $row['picture'] = base_url().$row['picture'] : '';
			$row['channel_logo'] = $row['channel_logo'] !=''  ? $row['channel_logo'] = base_url().$row['channel_logo'] : '';
			$row['position']	 = getDesignationdata($row['position'],'designation');
			$row['profile_pic']  = $row['profile_pic']!=''  ? $row['profile_pic']	= base_url(). $row['profile_pic'] : '';
			$row['published_at'] = convterUTCtoLocal($row['published_at'],"UTC");
			$data[$key] = $row;
		}
		return $data;
	}
	
	function get_latest_todolist($company_id)
	{
		$data = $this->db->select('todolist.todoid, todolist.team_id, todolist.accountability, todolist.action_name, todolist.action_description, todolist.deadline, todolist.status, teams.team_name, teams.team_coverphoto, teams.user_id as team_owner, users.user_name, users.email, users.designation as position,users.profile_picture as profile_pic')->from('todolist')->where(array('todolist.status'=>0))->join('teams', 'todolist.team_id = teams.team_id')->join('users', 'users.user_id = todolist.accountability AND users.company_id='.$company_id.'')->order_by('todolist.status','ASC')->order_by('todolist.deadline','ASC')->limit(4,0)->get()->result_array();
		foreach( $data as $key => $row )
		{
			$row['team_coverphoto'] !=''  ? $row['team_coverphoto']	= base_url().$row['team_coverphoto'] : '';
			$row['profile_pic']!=''  ? $row['profile_pic']	= base_url(). $row['profile_pic']: '';
			$row['position'] 	=  getDesignationdata($row['position'],'designation');
			$row['total_members'] = (string)$this->db->select('member_id')->from('team_members')->where(array('team_id'=>$row['team_id'],'status!='=>'1'))->get()->num_rows();
			$data[$key] = $row;
		}
		return $data;
		
		
	}
	
}
