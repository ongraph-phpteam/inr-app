<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Super_questions extends CI_Model {
	
	public function select_questions(){
		$data = $this->db->select('*')->from('security_questions')->get()->result_array();
		return $data;
	}
	
	public function new_question($input){
		$check = $this->db->select('question_id')->from('security_questions')->where(array('question'=>$input['question']))->get()->row();
		if($check==''){
			unset($input['submit']);
			$input['isactive'] = 1;
			$this->db->trans_start();
			$this->db->insert('security_questions',$input);
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return array('status' => 500,'msg' => 'Internal server error. Please try again!');
			}else{
				$this->db->trans_commit();
				return array('status' => 200,'msg' =>'Question added successfully.');
			}	
		}else{
			return array('status' => 400,'msg' =>'Question already in list');
		}
		
	}
	public function view_question($id){
		$data = $this->db->select('*')->from('security_questions')->where(array('question_id'=>$id))->get()->row_array();
		if (count($data)){
			return array('data'=>$data,'status' => 200,'msg' =>'');
			
		}else{
			return array('status' => 400,'msg' =>'Question not found!');
		}
	}
	public function edit_question($input){
		
		$check = $this->db->select('question_id')->from('security_questions')->where(array('question'=>$input['question'],'question_id!='=>$input['question_id']))->get()->row();
		if($check==''){
			unset($input['submit']);
			$this->db->trans_start();
			$this->db->where('question_id',$input['question_id'])->update('security_questions',array('question'=>$input['question']));
			
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return array('status' => 500,'msg' => 'Internal server error. Please try again!');
			}else{
				$this->db->trans_commit();
				return array('status' => 200,'msg' =>'Question updated!');
			}	
		}else{
			return array('status' => 400,'msg' =>'Question already in list!');
		}
	}
}

/* End of file M_admin.php */
/* Location: ./application/models/M_admin.php */