<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * App Name       -    	INR-Circle APP-
 * author         -     Hem Thakur
 * created        -     10/09/2017 
**/	
class AppBackend extends CI_Model {
	public function __construct(){
		parent::__construct();
		$this->db->query("SET time_zone='+0:00'");
	}
	
	/*
	* Request Authentication
	*/
	public function ht_check_auth_client(){
		$auth_key	= $this->input->get_request_header('Auth-Key', TRUE);
		//$username	= $this->input->server('PHP_AUTH_USER');
		//$password	= $this->input->server('PHP_AUTH_PW');	
		
		//if($auth_key == APP_AUTH_KEY_SECRET && $username ==  APP_BASIC_AUTH_USERNAME && $password == APP_BASIC_AUTH_PASSWORD){
		if($auth_key == APP_AUTH_KEY_SECRET){
			return true;
        } else {
            return json_output(400,array('status' => 400,'message' => 'Unauthorized.','method'=>$this->uri->segment(3)));
        }
    }
	/*
	* Check Unique Device User
	*/
	public function ht_check_uniqueDeviceUser($input)
	{	
		if(isset($input['email'])){ // For Login section
			$check = $this->db->select('user_id,email,touch_login')->from('users')->where(array('udid'=>$input['unique_device_id']))->get()->row_array();
			if (count($check)) {
				if($check['email']==$input['email']){
					return array('status' => 200,'message' => 'Success','method'=>'check_uniqueDeviceUser');
				}else{
					$data	=	$this->Aes_encryption->aesEncryption($this->secret,json_encode($check));
					return array('status' => 400,'message' => 'Device already registered','method'=>'check_uniqueDeviceUser','data'=>$data);
				}
			}else{
				return array('status' => 200,'message' => 'Success','method'=>'check_uniqueDeviceUser');
			}
		}elseif(isset($input['pre_login_check'])){ // Pre-login check for auto email fill
			
			$check = $this->db->select('user_id,email,touch_login')->from('users')->where(array('udid'=>$input['unique_device_id']))->get()->row_array();
			if (count($check)) {
				$data	=	$this->Aes_encryption->aesEncryption($this->secret,json_encode($check));
				return array('data'=>$data,'status' => 200,'message' => 'Account exist','method'=>'check_uniqueDeviceUser');
			}else{
				## In this case at front end condition will set for migrate device id by updateAccountDeviceId() after question authentication
				return array('status' => 400,'message' => 'Account not exist','method'=>'check_uniqueDeviceUser');
			}

		}else{ // For Sign-up section
			$check = $this->db->select('user_id,email,touch_login')->from('users')->where(array('udid'=>$input['unique_device_id']))->get()->row_array();
			if (count($check)) {
				$data	=	$this->Aes_encryption->aesEncryption($this->secret,json_encode($check));
				return array('status' => 400,'message' => 'Device already registered','method'=>'check_uniqueDeviceUser','data'=>$data);
			}else{
				return array('status' => 200,'message' => 'Available','method'=>'check_uniqueDeviceUser');
			}
		}
	}
	/*
	* Mobile Number check
	*/
	public function ht_check_mobileNumber($input)
	{	
		$check = $this->db->select('user_id')->from('users')->where(array('mobile_number'=>$input['mobile_number'],'country_code'=>$input['country_code']))->get()->num_rows();
		if ($check) {
			return array('status' => 400,'message' => 'Not available','method'=>'check_mobileNumber');
		}else{
			return array('status' => 200,'message' => 'Available','method'=>'check_mobileNumber');
		}
	}
	/*
	*  Check sign up allowed for this email or account already exist 
	*/
	public function ht_check_signupAllowedOrAccountExist($input)
	{	
		$company_id = check_valid_domain_help($input['email']);
		$check = $this->db->select('user_id')->from('users')->where(array('email'=>$input['email']))->get()->num_rows();
		if ($check) {
			return array('status' => 400,'message' => 'This email address is already linked to an existing inr-circle account. Please log in with this account.','method'=>'check_signupAllowedOrAccountExist');
			
		}else{
			$check = $this->db->select('allowed_id')->from('signup_allowed')->where(array('email'=>$input['email'],'company_id'=>$company_id))->get()->num_rows();
			if($check){
				return array('status'=>200,'message'=>'Sign up allowed','method'=>'check_signupAllowedOrAccountExist');
			}else{
				return array('status' => 400,'message' => 'This email address is not allowed to sign-up for inr-circle account. Please contact your administrator.','method'=>'check_signupAllowedOrAccountExist');
			}
		}
	}
	/*
	*  Check account exist with email
	*/
	public function ht_check_emailAccountExist($input)
	{	
		$check = $this->db->select('user_id')->from('users')->where(array('email'=>$input['email']))->get()->num_rows();
		if ($check) {
			return array('status' => 200,'message' => 'Account exist','method'=>'check_emailAccountExist');
			
		}else{
			return array('status' => 400,'message' => 'This email address is not linked to any existing inr-circle account. Please try another email.','method'=>'check_emailAccountExist');
		}
	}
	/*
	*  Check Password Strength
	*/
	public function ht_check_passwordStrength($input)
	{	
		return array_merge(password_pattern_check($input['password']),array('method'=>'check_passwordStrength'));
	}
	/*
	* Check Multiple account Configured with UDID 
	*/
	public function ht_check_mulitAccountsWithUdid($input)
	{	
		$check = $this->db->select('user_id')->from('users')->where(array('udid'=>$input['udid'],'touch_login'=>'1'))->get()->result_array();
		if (count($check)==1){
			return array('status' => 201,'message' => 'Single account','method'=>'check_mulitAccountsWithUdid');
		}elseif (count($check)>1) {
			return array('status' => 200,'message' => (string)count($check).' accounts found.','method'=>'check_mulitAccountsWithUdid');
		}else{
			return array('status' => 400,'message' => 'Touch ID login not configured!','method'=>'check_mulitAccountsWithUdid');
		}
	}
	/*
	* Get Country Code
	*/
	public function ht_get_countryCode()
	{	
		$data = $this->db->select('countries_id,countries_name, country_code, countries_iso_code')->from('countries')->order_by('countries_name','ASC')->get()->result_array();
		for($i=0;$i<count($data);$i++){
			$data[$i]['flag'] = base_url().'media/flags/'.strtolower($data[$i]['countries_iso_code']).'.png';
		}
		$data	=	$this->Aes_encryption->aesEncryption($this->secret,json_encode($data));
		return array('status' => 200,'message' => 'Countries code list','method'=>'get_countryCode','data'=>$data);
	}
	/*
	* Get TizeZone Name
	*/
	public function ht_get_timeZoneName()
	{	
		$data = $this->db->select('zone_id, country_code, zone_name')->from('countries_timezone')->order_by('zone_name','ASC')->get()->result_array();
		$data	=	$this->Aes_encryption->aesEncryption($this->secret,json_encode($data));
		return array('status' => 200,'message' => 'Timezone list','method'=>'get_timeZoneName','data'=>$data);
	}
	/*
	* Update Account device Id
	*/
	public function ht_updateAccountDeviceId($input)
	{	
		if (!empty($input['unique_device_id'])) {
			$this->db->trans_start();
			$this->db->where('user_id',$input['user_id'])->update('users',array('udid' =>$input['unique_device_id'],'updated_at'=>today()[0]));
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return array('status' => 500,'message' => 'Internal server error.');
			} else {
				$this->db->trans_commit();
				return array('status' => 200,'message' => 'Device migrated successfully ','method'=>'updateAccountDeviceId');
			}
		}else{
			return array('status' => 400,'message' => 'Device id required','method'=>'updateAccountDeviceId');
		}
	}
	/*
	* Remove Account from device
	*/
	public function ht_removeAccountFromDevice($input)
	{	
		$check = $this->db->select('user_id')->from('users')->where(array('udid'=>$input['unique_device_id'],'email'=>$input['email']))->get()->result_array();
		if (count($check)) {
			$matchanswer = $this->db->select("answer_id")->from("security_answers")->where(array("user_id"=>$check[0]['user_id'],'answer'=>trim(strtolower($input['answer'])),'question_id'=>$input['question_id']))->get()->result_array();
			if(count($matchanswer)){
				$this->db->trans_start();
				$this->db->where(array('user_id'=>$check[0]['user_id']))->delete('users_authentication');
				$this->db->where('user_id',$check[0]['user_id'])->update('users',array('udid' =>'','mobile_number'=>'','country_code'=>'','updated_at'=>today()[0]));
				$this->db->where('user_id',$check[0]['user_id'])->delete('users_authentication');
				//$this->db->where('user_id',$check[0]['user_id'])->update('users',array('udid' =>'','updated_at'=>today()[0]));
				if ($this->db->trans_status() === FALSE){
					$this->db->trans_rollback();
					return array('status' => 500,'message' => 'Internal server error.');
				} else {
					$this->db->trans_commit();
					return array('status' => 200,'message' => 'Account removed successfully form this device.','method'=>'removeAccountFromDevice');
				}
			}else{
				return array('status' => 400,'message' => 'Wrong security answer! Please answer your security question carefully.','method'=>'removeAccountFromDevice');
			}
		}else{
			return array('status' => 400,'message' => 'Access denied!','method'=>'removeAccountFromDevice');
		}
	}
	/*
	* Sign Up
	*/
	public function ht_user_signup($paras)
	{
		$company_id = check_valid_domain_help($paras['email']);
		if ($company_id){ 
			$device_type = $paras['device_type'];
			$device_token = $paras['device_token'];
			unset($paras['device_type'],$paras['device_token']);
			
			$paras['password']		= crypt($paras['password'],APP_SALT); 
			$paras['company_id']	= $company_id; 
			$paras['is_active']		= 1; 
			
			$query = $this->db->select('user_id,user_name')->from('users')->where('email',$paras['email'])->get()->row();
			
			if ($query) {
				return array('status' => 400,'message' => 'This email address is already linked to an existing inr-circle account. Please log in with this account.','method'=>'user_signup');
			} else {
				$random = randomCode(6,false,'m');
				$paras['email_varification'] = $random;
				$this->db->trans_start();
				//User Table
				$this->db->insert('users',$paras);
				$user_id = $this->db->insert_id();
				
				$token = crypt((substr( md5(rand()), 0, 7)),APP_SALT);
				//User Authentication Table
				$this->db->insert('users_authentication',array('user_id' => $user_id,'token' => $token,'device_type' => $device_type,'device_token' => $device_token));
				if ($this->db->trans_status() === FALSE){
					$this->db->trans_rollback();
					return array('status' => 500,'message' => 'Internal server error.');
				} else {
					
					$forward = $this->db->select('card_id')->from('rolodex_forwarded_cards')->where(array('phoneno'=>$paras['mobile_number']))->get()->result_array();
					
					if(count($forward)){
						$notification = $this->db->select('user_id')->from('notifications')->WHERE(array('user_id'=>$user_id))->get()->num_rows();
						if($notification==0){
							$this->db->insert('notifications',array('user_id'=>$user_id,'forward_cards'=>'0','todo_assigned'=>'0','new_ttalks'=>'0','new_onetoone_msg'=>'0'));
						}
						for($i=0;$i<count($forward);$i++){
							$validid = check_valid_cardid($forward[$i]['card_id']);
							if($validid!=$user_id){
								$this->db->insert('rolodex',array('user_id'=>$user_id,'card_id'=>$forward[$i]['card_id'],'added_via'=>'1','received_at'=>today()[0]));
								$this->db->set('forward_cards','forward_cards+1',false)->where(array('user_id'=>$validid))->update('notifications');
							}
						}
						$this->db->where(array('phoneno'=>$paras['mobile_number']))->delete('rolodex_forwarded_cards');
					}
					$touch_login = "0";
					if(isset($paras['touch_login'])){
						$touch_login = "1";
					}
					
					## clear data from Signup allowed table
					$this->db->where(array('email'=>$paras['email']))->delete('users_authentication');
					
					$this->db->trans_commit();
					## email_verification = 1 Required verification = 0 Not required
					## User data
					$data = array('is_device_changed'=>'0','user_id' => (string)$user_id, 'user_name'=>$paras['user_name'], 'token' => $token,'card_id'=>'','card_picture'=>'','isUpdated'=>'0','email'=>$paras['email'],'profile_picture'=>'','designation'=>'','country_code'=>$paras['country_code'],'mobile_number'=>$paras['mobile_number'],'rolodex_collectinginfo'=>'1','company_id'=>$company_id,'security_answers'=>'0','touch_login'=>$touch_login,'email_verification'=>'1');
					$data = $this->Aes_encryption->aesEncryption($this->secret,json_encode($data));
					## Send Verification code
					$this->load->library('email');
					$this->email->from('noreply@inr-circle.com', 'INR-Circle Support Team');
					$this->email->to($paras['email']);
					$this->email->subject('INR-Circle Application account email verification code');
					$this->email->set_mailtype("html");
					$email_message .= "<div style='width:400px;background:#fff;color:#000;font-family:Source Sans Pro,sans-serif;padding:10px;' >";	
					$email_message .= "<div>Your email verification code is: ".$random."</div>" ;
					$email_message .= "<div>Please use this code to verify your email of INR-Circle Application.</div>" ;
					$email_message .= "<div>Thanks</div>" ;
					$this->email->message($email_message);

					$this->email->send();
					$this->email->clear(TRUE);
					return array('status' => 200,'message' => 'Signup successful. A verification code has been sent at your email. Please check your inbox. ','method'=>'user_signup','data'=>$data);
				}
			}
		}else{
			return array('status' => 400,'message' => 'Your company may not be registered with us or please check once email you have entered!','method'=>'user_signup');
		}
	}
	/*
	* Sign In
	*/
	public function ht_user_login($paras)
    {	
		$udid		 = $paras['udid'];
		$touch_login = $paras['touch_login'];
		unset($paras['udid']);
		unset($paras['touch_login']);
		if ($udid && $touch_login){ ## Touch Login IOS
			$condition = array('is_active'=>'1','udid'=>$udid);
			unset($paras['password']);
			$pass = "";
		}else{ ## Normal Login
			$condition = array('is_active'=>'1','email'=>$paras['username']);
			$pass = urldecode($paras['password']);
			
		}
		$data  = $this->db->select('password,user_id,designation,user_name,email,country_code,mobile_number,profile_picture,rolodex_collectinginfo,company_id,udid,touch_login,device_change_verify_code,email_varification')->from('users')->where($condition)->get()->row();
		
		if($data == ""){
			return array('status' => 400,'message' => 'Account not found.','method'=>'user_login');
        } else {
			if($pass){
				$hashed_password = $data->password;
				if (!hash_equals($hashed_password, crypt($pass,APP_SALT))) {
				   return array('status' => 400,'message' => 'Wrong password.','method'=>'user_login');
				   exit;
				}
			}
			$id	= $data->user_id;
			$last_login = date('Y-m-d H:i:s');
			$token = crypt((substr(md5(rand()), 0, 7)),APP_SALT);
			$this->db->trans_start();
			//update User table
			$this->db->where('user_id',$id)->update('users',array('last_login' =>today()[0]));
			
			## Check device changed
			$check = $this->db->select('id,device_token')->from('users_authentication')->where(array('user_id'=>$id))->get()->row();
			
			$devicechanged = '0';
			if ($check !=""){
				if ($udid!="" && $data->udid!="" && $data->udid!=$udid){
					$devicechanged = '1';
				}else{
					if ($check->device_token != $paras['device_token']){
						$devicechanged = '1';
					}
					$this->db->where(array('user_id' => $id))->update('users_authentication',array('token'=>$token,'device_token' => $paras['device_token']));
				}
			}else{
				unset($paras['username'],$paras['password'],$paras['mobile_number']);
				$paras['user_id'] = $id;
				$paras['token'] = $token;
				$this->db->insert('users_authentication',$paras);
			}
			
			if($data->mobile_number == "" ){
				$devicechanged = '1';
			}
			
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return array('status' => 500,'message' => 'Internal server error.','method'=>'user_login');
			} else {
				$this->db->trans_commit();
				
				$card = $this->db->select('card_id,card_picture')->from('my_cards')->where(array('user_id'=>$id,'is_active'=>'1','master_card_id'=>'0'))->order_by('card_id','DESC')->get()->result_array();
				$card_id = count($card) ? $card[0]['card_id'] : '';
				$card_picture = $card[0]['card_picture'] !='' ? base_url().$card[0]['card_picture'] : '';
				$isUpdated = $data->designation !=0 ? '1' : '0';
				
				$forward = $this->db->select('card_id')->from('rolodex_forwarded_cards')->where(array('phoneno'=>$data->mobile_number))->get()->result_array();
				
				if(count($forward)){
					
					$notification = $this->db->select('user_id')->from('notifications')->WHERE(array('user_id'=>$id))->get()->num_rows();
					if($notification == 0){
						$this->db->insert('notifications',array('user_id'=>$id,'forward_cards'=>'0','todo_assigned'=>'0','new_ttalks'=>'0','new_onetoone_msg'=>'0'));
					}
					
					for($i=0;$i<count($forward);$i++){
						$validid = check_valid_cardid($forward[$i]['card_id']);
						if($validid!=$id){
							if(check_CardAlreadyInRolodex($forward[$i]['card_id'],$id)==0){
								$this->db->insert('rolodex',array('user_id'=>$id,'card_id'=>$forward[$i]['card_id'],'added_via'=>'1','received_at'=>today()[0]));
								$this->db->set('forward_cards','forward_cards+1',false)->where(array('user_id'=>$validid))->update('notifications');
							}
						}
					}
					$this->db->where(array('phoneno'=>$data->mobile_number))->delete('rolodex_forwarded_cards');
				}
				
				if($devicechanged==1){
					$device_change_verify_code = $data->device_change_verify_code;
					if(empty($device_change_verify_code)){
						$device_change_verify_code = rand(111111,999999);
					}
					$this->db->where('user_id',$id)->update('users',array('device_change_verify_code' =>$device_change_verify_code));
					
					$this->load->library('email');
					$this->email->from('noreply@inr-circle.com', 'INR-Circle Support Team');
					$this->email->to($data->email);
					$this->email->subject('INR-Circle App device change verification code');
					$this->email->set_mailtype("html");
					$email_message .= "<div style='width:400px;background:#fff;color:#000;font-family:Source Sans Pro,sans-serif;padding:10px;' >";	
					$email_message .= "<div>Hi ".$data->user_name.",</div>" ;
					$email_message .= "<div>This is your device change verification code: <b>".$device_change_verify_code."</b></div>" ;
					$email_message .= "<div>Please use this code to verify your INR-Circle app account to complete device change process.</div>" ;
					$email_message .= "<div>Thanks</div>" ;
					$this->email->message($email_message);
					$this->email->send();
					$this->email->clear(TRUE);
				}
				
				$data->profile_picture != '' ? $data->profile_picture = base_url().$data->profile_picture : '';
				$security_answers = count(getUsersSecurityAnswers($id)) ? '1' : '0';
				
				##User Data
				if($data->email_varification){
					$email_verification = '1'; ## Email verification required
				}else{
					$email_verification = '0'; ## Email verification not required
				}
				$final = array('is_device_changed'=>$devicechanged,'user_id' => $id,'user_name'=>$data->user_name,'token' => $token,'card_id'=>$card_id,'card_picture'=>$card_picture,'isUpdated'=>$isUpdated,'email'=>$data->email,'profile_picture'=>$data->profile_picture,'designation'=>getDesignationdata($data->designation,'designation'),'mobile_number'=>$data->mobile_number,'country_code'=>$data->country_code,'rolodex_collectinginfo'=>$data->rolodex_collectinginfo,'company_id'=>$data->company_id,'security_answers'=>$security_answers,'udid'=>$data->udid,'touch_login'=>$data->touch_login,'email_verification'=>$email_verification);
				
				$final = $this->Aes_encryption->aesEncryption($this->secret,json_encode($final));
				
				return array('status' => 200,'message' => 'Successfully login.','method'=>'user_login','data'=>$final);
			}
		}
	}
	/*
	* Verify Email id
	*/
    public function ht_verifyEmailId($input)
    {
		$check  = $this->db->select("email_varification")->from("users")->where(array('email'=>$input['email']))->get()->row();
		if($check){
			if($check->email_varification==""){
				return array('status' => 400,'message' =>'Your account is already verified','method'=>'verifyEmailId');
			}else{
				if( $check->email_varification==$input['verification_code'] ){
					$this->db->trans_start();
					$this->db->where(array('email'=>$input['email']))->update('users',array('email_varification'=>''));
					if ($this->db->trans_status() === FALSE){
						$this->db->trans_rollback();
						return  array('status' => 500,'message' =>'Internal server error.','method'=>'verifyEmailId');
					} else {
						$this->db->trans_commit();
						
						##User data
						$data  = $this->db->select('user_id,designation,user_name,email,country_code,mobile_number,profile_picture,rolodex_collectinginfo,company_id,udid,touch_login,device_change_verify_code,email_varification')->from('users')->where(array('email'=>$input['email']))->get()->row();
						$id = $data->user_id;
						
						## Join Mandatory News Channels 
						$channels = $this->db->query("SELECT `channel_id` FROM `news_channel` WHERE `company_id`='".$data->company_id."' AND `type`='2' AND `is_active`='1'")->row_array();
						foreach($channels as $channel_id){
							$this->db->insert('newschannel_subscribers',array('channel_id'=>$channel_id,'user_id'=>$id,'subscribed_date'=>today()[0]));
						}
						
						## Get Card
						$card = $this->db->select('card_id,card_picture')->from('my_cards')->where(array('user_id'=>$id,'is_active'=>'1','master_card_id'=>'0'))->order_by('card_id','DESC')->get()->result_array();
						$card_id = count($card) ? $card[0]['card_id'] : '';
						$card_picture = $card[0]['card_picture'] !='' ? base_url().$card[0]['card_picture'] : '';
						$isUpdated = $data->designation !=0 ? '1' : '0';
						
						## Add forwarded cards to Rolodex
						$forward = $this->db->select('card_id')->from('rolodex_forwarded_cards')->where(array('phoneno'=>$data->mobile_number))->get()->result_array();
						
						if(count($forward)){
							
							$notification = $this->db->select('user_id')->from('notifications')->WHERE(array('user_id'=>$id))->get()->num_rows();
							if($notification == 0){
								$this->db->insert('notifications',array('user_id'=>$id,'forward_cards'=>'0','todo_assigned'=>'0','new_ttalks'=>'0','new_onetoone_msg'=>'0'));
							}
							
							for($i=0;$i<count($forward);$i++){
								$validid = check_valid_cardid($forward[$i]['card_id']);
								if($validid!=$id){
									if(check_CardAlreadyInRolodex($forward[$i]['card_id'],$id)==0){
										$this->db->insert('rolodex',array('user_id'=>$id,'card_id'=>$forward[$i]['card_id'],'added_via'=>'1','received_at'=>today()[0]));
										$this->db->set('forward_cards','forward_cards+1',false)->where(array('user_id'=>$validid))->update('notifications');
									}
								}
							}
							$this->db->where(array('phoneno'=>$data->mobile_number))->delete('rolodex_forwarded_cards');
						}
						## Final User data
						$token = $this->db->select('token')->from('users_authentication')->where(array('user_id'=>$id))->get()->row();
			
						$security_answers = count(getUsersSecurityAnswers($id)) ? '1' : '0';
						$data->profile_picture != '' ? $data->profile_picture = base_url().$data->profile_picture : '';
						
						$final = array('is_device_changed'=>'0','user_id' => $id,'user_name'=>$data->user_name,'token' => $token->token,'card_id'=>$card_id,'card_picture'=>$card_picture,'isUpdated'=>$isUpdated,'email'=>$data->email,'profile_picture'=>$data->profile_picture,'designation'=>getDesignationdata($data->designation,'designation'),'mobile_number'=>$data->mobile_number,'country_code'=>$data->country_code,'rolodex_collectinginfo'=>$data->rolodex_collectinginfo,'company_id'=>$data->company_id,'security_answers'=>$security_answers,'udid'=>$data->udid,'touch_login'=>$data->touch_login,'email_verification'=>'0');
				
						$final = $this->Aes_encryption->aesEncryption($this->secret,json_encode($final));
						
						return array('status' => 200,'data'=>$final,'message' =>'Email verified','method'=>'verifyEmailId');
					}
				}else{
					return array('status' => 400,'message' =>'Wrong code entered! Please try again.','method'=>'verifyEmailId');
				}
			}
		}else{
			return array('status' => 400,'message' =>'Account not found.','method'=>'verifyEmailId');
		}
	}
	/*
	* Verify code for email and device change
	*/
    public function ht_verifyCode($input)
    {
		if($input['section']==1){ ##Device Change verification
			if ($input['step']==1){ # Before mobile number verificatin
			$check  = $this->db->select("user_id")->from("users")->where(array('user_id'=>$input['user_id'],'device_change_verify_code'=>$input['verification_code']))->get()->row();
			}else{ ## step = 2 after mobile verification
				$check = 1;
				$update = array("device_change_verify_code"=>"");
			}
		}else{ ##Email change verification
			$check  = $this->db->select("user_id,email,new_email_to_change")->from("users")->where(array('user_id'=>$input['user_id'],'email_change_verify_code'=>$input['verification_code']))->get()->row();
		}
		if($check){
			if($input['section']==2){
				$update = array('email_change_verify_code'=>'','email'=>$check->new_email_to_change);
			}
			if($input['step']==1){
				$update = array("device_change_verify_code"=>$input['verification_code']);	
			}
			$this->db->trans_start();
			$this->db->where(array('user_id'=>$input['user_id']))->update('users',$update);
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return  array('status' => 500,'message' =>'Internal server error.','method'=>'verifyCode');
			} else {
				$this->db->trans_commit();
				return array('status' => 200,'message' =>'Verified','method'=>'verifyCode');
			}
		}else{
			return array('status' => 400,'message' =>'Wrong code entered! Please try again.','method'=>'verifyCode');
		}
	}
	/*
	* Forgot Password
	*/
	public function ht_forgot_password($input)
    {
		$data  = $this->db->select('user_id,company_id,user_name')->from('users')->where(array('email'=>$input['email'],'is_active'=>'1'))->get()->row();
		if($data == ""){
			return array('status' => 400,'message' => 'Account not found.','method'=>'forgot_password');
        } else {
			$attempt = 0;
			$check = $this->db->select("wrong_attempt, blocked_untill")->from("reset_password")->where(array("email"=>$input['email']))->get()->row_array();
			if(count($check)){
				if ($check['blocked_untill'] > today()[0] && $check['wrong_attempt']==3){
					return array('status' => 400,'message' => 'You already got blocked for 1 hour due to 3 wrong attempt. Please wait for unblock or contact to your administrator.','method'=>'forgot_password');
					exit;
				}elseif($check['blocked_untill'] < today()[0] && $check['wrong_attempt']==3){
					$this->db->where(array('email'=>$input['email']))->delete('reset_password');
				}else{
					$attempt = $check['wrong_attempt'];
				}
			}
			$matchanswer = $this->db->select("answer_id")->from("security_answers")->where(array("user_id"=>$data->user_id,'answer'=>trim(strtolower($input['answer'])),'question_id'=>$input['question_id']))->get()->result_array();
			if(count($matchanswer)){
				$random		= randomCode();
				$password 	= crypt($random,APP_SALT);
				$this->db->trans_start();
				$this->db->where(array('email'=>$input['email']))->update('users',array('password'=>$password,'updated_at'=>today()[0]));
				if ($this->db->trans_status() === FALSE){
					$this->db->trans_rollback();
					return array('status' => 500,'message' => 'Internal server error.');
				} else {
					$this->db->trans_commit();
					
					$this->db->where(array('email'=>$input['email']))->delete('reset_password');
					
					$this->load->library('email');
					$this->email->from('noreply@inr-circle.com', 'INR-Circle Support Team');
					$this->email->to($input['email']);
					$this->email->subject('INR-Circle Application temporary password');
					$this->email->set_mailtype("html");
					$email_message .= "<div style='width:400px;background:#fff;color:#000;font-family:Source Sans Pro,sans-serif;padding:10px;' >";	
					$email_message .= "<div>Temporary login password: ".$random."</div>" ;
					$email_message .= "<div>Please use this password for login to your INR-Circle Application. After successful login we recommend you to change your password.</div>" ;
					$email_message .= "<div>Thanks</div>" ;
					$this->email->message($email_message);

					$this->email->send();
					return array('status' => 200,'message' => 'A temporary password has sent at your email. Please check your inbox.','method'=>'forgot_password');
				}
			}else{
				$blocked_untill = date("Y-m-d H:i:s", strtotime('+1 hours'));
				$attempt = $attempt+1;
				if ($attempt==1){
					$this->db->insert('reset_password',array('email'=>$input['email'],'wrong_attempt'=>$attempt,'blocked_untill'=>$blocked_untill));
					$msg = "Wrong security answer attempt 1 out of 3! Please answer your security question carefully.";
				}else{
					$this->db->where('email',$input['email'])->update('reset_password',array('wrong_attempt' =>$attempt,'blocked_untill'=>$blocked_untill));
					if($attempt==3){
						$this->load->library('email');
						$this->email->from('noreply@inr-circle.com', 'INR-Circle Support Team');
						$this->email->to($input['email']);
						$this->email->subject('Alert : Attempt to request temporary password');
						$this->email->set_mailtype("html");
						$email_message .= "<div style='width:400px;background:#fff;color:#000;font-family:Source Sans Pro,sans-serif;padding:10px;' >";	
						$email_message .= "<div>You have just attempt 3 wrong security answer for request temporary password. For security reason you are blocked for 1 hour to request temporary password. Please wait or contact to your administrator.</div>" ;
						$email_message .= "<div>Thanks</div>" ;
						$this->email->message($email_message);
						$this->email->send();
						$msg = "Wrong security answer attempt 3 out of 3! Your are blocked for 1 hour.";
					}else{
						$msg = "Wrong security answer attempt 2 out of 3! Please answer your security question carefully.";
					}
				}
				return array('status' => 400,'message' =>$msg,'method'=>'forgot_password');
			}
		}
    }
	/*
	public function ht_forgot_password($input)
    {
		$data  = $this->db->select('company_id,user_name')->from('users')->where(array('email'=>$input['email'],'is_active'=>'1'))->get()->row();
		if($data == ""){
			return array('status' => 400,'message' => 'Account not found.','method'=>'forgot_password');
        } else {
			$random		= randomCode();
			$password 	= crypt($random,APP_SALT);
			$this->db->trans_start();
			$this->db->where(array('email'=>$input['email']))->update('users',array('password'=>$password,'updated_at'=>today()[0]));
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return array('status' => 500,'message' => 'Internal server error.');
			} else {
				$this->db->trans_commit();
				$this->load->library('email');
				$this->email->from('noreply@inr-circle.com', 'INR-Circle Support Team');
				$this->email->to($input['email']);
				$this->email->subject('INR-Circle App temporary passowrd');
				$this->email->set_mailtype("html");
				$email_message .= "<div style='width:400px;background:#fff;color:#000;font-family:Source Sans Pro,sans-serif;padding:10px;' >";	
				$email_message .= "<div>Temporary login password: ".$random."</div>" ;
				$email_message .= "<div>Please use this passowrd for login to your INR-Circle app. After successful login we recommend you to change your password.</div>" ;
				$email_message .= "<div>Thanks</div>" ;
				$this->email->message($email_message);

				$this->email->send();
				return array('status' => 200,'message' => 'A temporary password has sent at your email. Please check your inbox.','method'=>'forgot_password');
			}
		}
    } */
	
	/*
	* Get Security Question
	*/
    public function ht_get_SecurityQuestions()
    {
		$data = $this->db->select("security_questions.question_id, security_questions.question")->from('security_questions')->where('security_questions.isactive','1')->get()->result_array();
		$data = $this->Aes_encryption->aesEncryption($this->secret,json_encode($data));
		return array('status' => 200,'message' => 'Security question.','method'=>'get_SecurityQuestions','data'=>$data);
    }
	/*
	* Get User Security Question Answers
	*/
    public function ht_get_mySecurityQuestionAnswers($input)
    {
		$data = $this->db->select("security_questions.question_id, security_questions.question, IFNULL(security_answers.answer,'') as answer")->from('security_questions')->join('security_answers','security_questions.question_id = security_answers.question_id AND security_answers.user_id='.$input['user_id'].'','left')->where('security_questions.isactive','1')->get()->result_array();
		$data = $this->Aes_encryption->aesEncryption($this->secret,json_encode($data));
		return array('status' => 200,'message' => 'Security question answers.','method'=>'get_mySecurityQuestionAnswers','data'=>$data);
    }
	/*
	* Set User Security Answers
	*/
    public function ht_set_mySecurityAnswers($input)
    {
		$answers = $input['data'];
		if (count($answers)){
			foreach($answers as $data){
				$question_id= $data['question_id'];
				$answer		= trim(strtolower($data['answer']));
				$check		= getUsersSecurityAnswers($input['user_id'],$question_id);
				$this->db->trans_start();
				if($check){
					$this->db->where(array('answer_id'=>$check))->update('security_answers',array('answer'=>$answer));
				}else{
					$this->db->insert('security_answers',array('question_id'=>$question_id,'user_id'=>$input['user_id'],'answer'=>$answer));
				}
				if ($this->db->trans_status() === FALSE){
					$this->db->trans_rollback();
					$message =  array('status' => 500,'message' =>'Internal server error.','method'=>'set_mySecurityAnswers');
					 break;
				} else {
					$this->db->trans_commit();
					$message =  array('status' => 200,'message' =>'Security answers updated','method'=>'set_mySecurityAnswers');
				}
			}
			return $message;
			
		}else{
			return array('status' => 400,'message' => 'Answers can\'t empty!','method'=>'set_mySecurityAnswers');
		}
	}
	/*
	* Change Login Type
	*/
    public function ht_change_loginType($input)
    {
		
		if ($input['login_type'] == 1){ // Normal login with password
			$password 	= crypt($input['pssword_or_udid'],APP_SALT);
			$update = array('touch_login'=>'0','updated_at'=>today()[0]);
		}else{ // With Touch Id IOS
			$update = array('touch_login'=>'1','updated_at'=>today()[0]);
		}
		$this->db->trans_start();
		$this->db->where(array('user_id'=>$input['user_id']))->update('users',$update);
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return  array('status' => 500,'message' =>'Internal server error.','method'=>'change_loginType');
		} else {
			$this->db->trans_commit();
			return array('status' => 200,'message' =>'Login type changed','method'=>'change_loginType');
		}
	}
	/*
	* Log out
	*/
    public function ht_logout()
    {
        $user_id  = $this->Aes_encryption->aesDecryption($this->secret,$this->input->get_request_header('userid', TRUE));
        $token     = $this->input->get_request_header('token', TRUE);
        $this->db->where(array('user_id'=>$user_id,'token'=>$token))->delete('users_authentication');
        return array('status' => 200,'message' => 'Successfully logout.','method'=>'logout');
    }
	/*
	* Authenticate User
	*/
    public function ht_auth()
    {
        $user_id	= $this->Aes_encryption->aesDecryption($this->secret,$this->input->get_request_header('userid', TRUE));
        $token		= $this->input->get_request_header('token', TRUE);
        $q  = $this->db->select('id')->from('users_authentication')->where(array('user_id'=>$user_id,'token'=>$token))->get()->num_rows();
        if($q){
            return array('id'=>$user_id,'status' => 200,'message' => 'Authorized.','method'=>$this->uri->segment(3));
        } else {
            return json_output(401,array('status' => 401,'message' => 'Unauthorized.','method'=>$this->uri->segment(3)));
        }
    }
	/*
	* View Profile
	*/
	public function ht_view_profile($input)
	{
		$data = $this->db->select('user_id,user_name,email,mobile_number,designation as designation_id,profile_picture,datetime,timezone,udid')->from('users')->where(array('user_id'=>$input['user_id']))->get()->row();
		$data->profile_picture != '' ? $data->profile_picture = base_url().$data->profile_picture : '';
		$data->designation =  getDesignationdata($data->designation_id,'designation');
		$data	=	$this->Aes_encryption->aesEncryption($this->secret,json_encode($data));
		return array('status' => 200,'message' =>'Profile detail.','method'=>'view_profile','data'=>$data);
	}
	
	/*
	* Get Designations List
	*/
	public function ht_get_designationsList($input)
	{
		$company_id	=	getusersdata($input['user_id'],'company_id');
		$data = $this->db->select('designation_id,designation')->from('designations')->order_by('designation_id','ASC')->get()->result_array();
		$data	=	$this->Aes_encryption->aesEncryption($this->secret,json_encode($data));
		return array('status' => 200,'message' =>'Designations list.','method'=>'get_designationsList','data'=>$data);
	}
	/*
	* Update Profile
	*/
	public function ht_update_profile($input)
	{	
		$input['profile_picture']	= getusersdata($input['user_id'],'profile_picture');
		$date = date("Y-m");
		$config['allowed_types']= '*';
		//$config['max_size']		= '200';
		if (!empty($_FILES['profile_picture']['name'])){
			$path	=  './media/profile/'.$date."/";
			if (!is_dir($path)){
				mkdir($path , 0777);
				$content = "<!DOCTYPE html><html><head><title>403 Forbidden</title></head><body><p>Access denied!</p></body></html>"; 
				$fp = fopen($path."index.html","wb"); 
				fwrite($fp,$content); 
				fclose($fp); 
			}
			$config['file_name']	= date("Ymd")."_PP-".rand(100000,999999).'-'.time();
			$config['upload_path']	= $path;
			$this->upload->initialize($config);
			if (!$this->upload->do_upload('profile_picture')){
				return array('status' =>400,'message' =>"File uploading error: ".strip_tags($this->upload->display_errors()),'method'=>$this->uri->segment(3));
				exit;
			}else{
				$input['profile_picture'] !="" ? unlink("./".$input['profile_picture']) : '';
				$input['profile_picture'] = ltrim($path,"./").$this->upload->data()['file_name'];
			}
		}
		$this->db->trans_start();
		$this->db->where(array('user_id'=>$input['user_id']))->update('users',array('user_name'=>$input['user_name'],'profile_picture'=>$input['profile_picture'],'designation'=>$input['designation_id'],'datetime'=>$input['datetime'],'timezone'=>$input['timezone'],'updated_at'=>today()[0]));
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return array('status' => 500,'message' =>'Internal server error.','method'=>'update_profile');
		} else {
			$this->db->trans_commit();
			return array('status' => 200,'message' =>'Profile updated.','method'=>'update_profile');
		}
	}
	/*
	* Update Collection Info Setting 
	*/
	public function ht_update_RolodexCollectionInfoSetting($input)
	{	
		$input['rolodex_collectinginfo']	= getusersdata($input['user_id'],'rolodex_collectinginfo');
		if ($input['rolodex_collectinginfo'] == 1){
			$input['rolodex_collectinginfo'] = 0;
		}else{
			$input['rolodex_collectinginfo'] = 1;
		}
		$this->db->trans_start();
		$this->db->where(array('user_id'=>$input['user_id']))->update('users',array('rolodex_collectinginfo'=>$input['rolodex_collectinginfo']));
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return array('status' => 500,'message' =>'Internal server error.','method'=>'update_RolodexCollectionInfoSetting');
		} else {
			$this->db->trans_commit();
			return array('rolodex_collectinginfo'=>$input['rolodex_collectinginfo'],'status' => 200,'message' =>'Updated successfully.','method'=>'update_RolodexCollectionInfoSetting');
		}
	}
	/*
	* Change Password
	*/
	public function ht_change_password($input)
	{	
		$data  = $this->db->select('password')->from('users')->where(array('user_id'=>$input['user_id']))->get()->row();
		if($data == ""){
			return array('status' => 400,'message' => 'Account not found.','method'=>'change_password');
        } else {
			$oldpassword = $data->password;
			if (hash_equals($oldpassword, crypt($input['oldpassword'],APP_SALT))) {
				if ($input['oldpassword'] !=$input['newpassword']){
					$password 	= crypt($input['newpassword'],APP_SALT);
					$this->db->trans_start();
					$this->db->where(array('user_id'=>$input['user_id']))->update('users',array('password'=>$password,'updated_at'=>today()[0]));
					if ($this->db->trans_status() === FALSE){
						$this->db->trans_rollback();
						return array('status' => 500,'message' => 'Internal server error.');
					} else{
						$this->db->trans_commit();
						return array('status' => 200,'message' =>'Password changed successfully.','method'=>'change_password');
					}	
				}else{
					return array('status' => 400,'message' => 'New password must be different then old password.','method'=>'change_password');
				}
				
            } else {
               return array('status' => 400,'message' => 'Wrong old password.','method'=>'change_password');
            }
        }
	}
	/*
	* Send Change email verification code
	*/
	public function ht_send_EmailChangeVerificationCode($input)
	{	  
		$data  = $this->db->select('device_change_verify_code,email_change_verify_code,user_name,email,new_email_to_change')->from('users')->where(array('user_id'=>$input['user_id']))->get()->row();
		if(isset($input['device_change'])){
			$device_change_verify_code = $data->device_change_verify_code;
			if(empty($device_change_verify_code)){
				$device_change_verify_code = rand(111111,999999);
			}
			$this->db->where('user_id',$input['user_id'])->update('users',array('device_change_verify_code' =>$device_change_verify_code));
			$this->load->library('email');
			$this->email->from('noreply@inr-circle.com', 'INR-Circle Support Team');
			$this->email->to($data->email);
			$this->email->subject('INR-Circle App account verification code');
			$this->email->set_mailtype("html");
			$email_message .= "<div style='width:400px;background:#fff;color:#000;font-family:Source Sans Pro,sans-serif;padding:10px;' >";	
			$email_message .= "<div>Hi ".$data->user_name.",</div>" ;
			$email_message .= "<div>This is your account verification code: <b>".$device_change_verify_code."</b></div>" ;
			$email_message .= "<div>Please use this code to verify your INR-Circle app account to complete device change process.</div>" ;
			$email_message .= "<div>Thanks</div>" ;
			$this->email->message($email_message);
			$this->email->send();
		}else{
			$email_change_verify_code = $data->email_change_verify_code;
			if(empty($email_change_verify_code)){
				$email_change_verify_code = rand(111111,999999);
			}
			$this->db->where('user_id',$input['user_id'])->update('users',array('email_change_verify_code' =>$email_change_verify_code));
			$this->load->library('email');
			$this->email->from('noreply@inr-circle.com', 'INR-Circle Support Team');
			$this->email->to($data->email);
			$this->email->subject('INR-Circle App email change verification code');
			$this->email->set_mailtype("html");
			$email_message .= "<div style='width:400px;background:#fff;color:#000;font-family:Source Sans Pro,sans-serif;padding:10px;' >";	
			$email_message .= "<div>Hi ".$data->user_name.",</div>" ;
			$email_message .= "<div>This is your email change verification code: <b>".$email_change_verify_code."</b></div>" ;
			$email_message .= "<div>Please use this code to verify your INR-Circle app account to change your linked email id.</div>" ;
			$email_message .= "<div>Thanks</div>" ;
			$this->email->message($email_message);
			$this->email->send();	
		}
		return array('status' => 200,'message' =>'Verification code sent','method'=>'send_EmailChangeVerificationCode');
	}
	/*
	* Change email
	*/
	public function ht_change_Email($input)
	{	
		$data  = $this->db->select('email')->from('users')->where(array('user_id!='=>$input['user_id'],'email'=>$input['email']))->get()->row();
		if($data){
			return array('status' => 400,'message' => 'This email address is already linked to an existing inr-circle account.','method'=>'change_Email');
        } else {
			$company_id 	= check_valid_domain_help($input['email']);
			$user_company 	= getusersdata($input['user_id'],'company_id');
			$user_name 		= getusersdata($input['user_id'],'user_name');
			if ($company_id == $user_company){ 
				$email_change_verify_code = getusersdata($input['user_id'],'email_change_verify_code');
				if(empty($email_change_verify_code)){
					$email_change_verify_code = rand(111111,999999);
				}
				$this->db->trans_start();
				$this->db->where(array('user_id'=>$input['user_id']))->update('users',array('new_email_to_change'=>$input['email'],'email_change_verify_code'=>$email_change_verify_code,'updated_at'=>today()[0]));
				if ($this->db->trans_status() === FALSE){
					$this->db->trans_rollback();
					return array('status' => 500,'message' => 'Internal server error.');
				}else{
					$this->db->trans_commit();
					$this->load->library('email');
					$this->email->from('noreply@inr-circle.com', 'INR-Circle Support Team');
					$this->email->to($input['email']);
					$this->email->subject('INR-Circle App email change verification code');
					$this->email->set_mailtype("html");
					$email_message .= "<div style='width:400px;background:#fff;color:#000;font-family:Source Sans Pro,sans-serif;padding:10px;' >";	
					$email_message .= "<div>Hi ".$user_name.",</div>" ;
					$email_message .= "<div>This is your email change verification code: <b>".$email_change_verify_code."</b></div>" ;
					$email_message .= "<div>Please use this code to verify your INR-Circle app account to change your linked email id.</div>" ;
					$email_message .= "<div>Thanks</div>" ;
					$this->email->message($email_message);
					$this->email->send();
					return array('status' => 200,'message' =>'Verification code sent.','method'=>'change_Email');
				}
			}else{
				return array('status' => 400,'message' => 'Email you have entered not belongs to your company account. Please try again with your company email.','method'=>'change_Email');
			}
        }
	}
	/*
	* Change Mobile Number
	*/
	public function ht_change_mobileNumber($input)
	{	
		if($input['new_number'] == ""){
			return array('status' => 400,'message' => 'New mobile number required!','method'=>'change_mobileNumber');
        } else {
			$this->db->trans_start();
			$this->db->where(array('user_id'=>$input['user_id']))->update('users',array('mobile_number'=>$input['new_number'],'country_code'=>$input['new_country_code'],'updated_at'=>today()[0]));
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return array('status' => 500,'message' => 'Internal server error.');
			}else{
				$this->db->trans_commit();
				return array('status' => 200,'message' =>'Mobile number updated successfully.','method'=>'change_mobileNumber');
			}	
		}
	}
	/*
	* Create Business Card
	*/
	public function ht_create_businessCard($input)
	{
		$input['is_active'] 	= 1;
		$input['card_picture']	= '';
		$input['profile_pic']	= '';
		$input['company_logo']	= '';
		$input['created_at'] 	= today()[0];
		$input['updated_at']	= today()[0];
		$config['allowed_types'] = '*';
		$date = date("Y-m");
		$this->load->library('image_lib');
		if (!empty($_FILES['card_picture']['name'])){
			$path	=  './media/cards/card_picture/'.$date."/";
			if (!is_dir($path)){
				mkdir($path , 0777);
				$content = "<!DOCTYPE html><html><head><title>403 Forbidden</title></head><body><p>Access denied!</p></body></html>"; 
				$fp = fopen($path."index.html","wb"); 
				fwrite($fp,$content); 
				fclose($fp); 
			}
			$config['file_name'] = date("Ymd")."_PP-".rand(1000,9999).'-'.time();
			$config['upload_path'] = $path;
			$this->upload->initialize($config);
			if (!$this->upload->do_upload('card_picture')){
				return array('status' =>400,'message' =>"Card Pic uploading error: ".strip_tags($this->upload->display_errors()),'method'=>'create_businessCard');
				exit;
			}else{
				$card_picture_data = $this->upload->data();
				$config['image_library'] = 'gd2';
				$config['source_image'] = $path.$card_picture_data['file_name']; //get original image
				$config['maintain_ratio'] = TRUE;
				$config['create_thumb'] = false;
				$config['height'] = 400;
				$this->image_lib->initialize($config);
				if (!$this->image_lib->resize()) {
					$card = $path.$card_picture_data['file_name'];
					if (file_exists($card)) {
						unlink($card);
					}
					return array('status' =>400,'msg' =>"Card picture resizing error: ".strip_tags($this->image_lib->display_errors()));
					exit;
				}else{
					$input['card_picture'] = ltrim($path,"./").$card_picture_data['file_name'];
				}
				$this->image_lib->clear();
			}
		}
		if (!empty($_FILES['profile_pic']['name'])){
			$path	=  './media/cards/profile_pic/'.$date."/";
			if (!is_dir($path)){
				mkdir($path , 0777);
				$content = "<!DOCTYPE html><html><head><title>403 Forbidden</title></head><body><p>Access denied!</p></body></html>"; 
				$fp = fopen($path."index.html","wb"); 
				fwrite($fp,$content); 
				fclose($fp); 
			}
			$config['file_name'] = date("Ymd")."_PP-".rand(1000,9999).'-'.time();
			$config['upload_path'] = $path;
			$this->upload->initialize($config);
			if (!$this->upload->do_upload('profile_pic')){
				return array('status' =>400,'message' =>"Profile Pic uploading error: ".strip_tags($this->upload->display_errors()),'method'=>'create_businessCard');
				exit;
			}else{
				$pro_picture_data	=	$this->upload->data();
				$config['image_library'] = 'gd2';
				$config['source_image'] = $path.$pro_picture_data['file_name']; //get original image
				$config['maintain_ratio'] = TRUE;
				$config['create_thumb'] = false;
				$config['height'] = 150;
				$this->image_lib->initialize($config);
				if (!$this->image_lib->resize()) {
					$card = $path.$pro_picture_data['file_name'];
					if (file_exists($card)) {
						unlink($card);
					}
					return array('status' =>400,'msg' =>"Profile picture resizing error: ".strip_tags($this->image_lib->display_errors()));
					exit;
				}else{
					$input['profile_pic'] = ltrim($path,"./").$pro_picture_data['file_name'];
				}
				$this->image_lib->clear();
			}
		}
		
		if (!empty($_FILES['company_logo']['name'])){
			$path	=  './media/cards/company_logo/'.$date."/";
			if (!is_dir($path)){
				mkdir($path , 0777);
				$content = "<!DOCTYPE html><html><head><title>403 Forbidden</title></head><body><p>Access denied!</p></body></html>"; 
				$fp = fopen($path."index.html","wb"); 
				fwrite($fp,$content); 
				fclose($fp); 
			}
			$config['file_name']	= date("Ymd")."_BC-".rand(1000,9999).'-'.time();
			$config['upload_path']	= $path;
			$this->upload->initialize($config);
			if (!$this->upload->do_upload('company_logo')){
				return array('status' =>400,'message' =>"Company logo uploading error: ".strip_tags($this->upload->display_errors()),'method'=>'create_businessCard');
				exit;
			}else{
				$company_logo_data	=	$this->upload->data();
				$config['image_library'] = 'gd2';
				$config['source_image'] = $path.$company_logo_data['file_name']; //get original image
				$config['maintain_ratio'] = TRUE;
				$config['create_thumb'] = false;
				$config['height'] = 150;
				$this->image_lib->initialize($config);
				if (!$this->image_lib->resize()) {
					$card = $path.$company_logo_data['file_name'];
					if (file_exists($card)) {
						unlink($card);
					}
					return array('status' =>400,'msg' =>"Company logo resizing error: ".strip_tags($this->image_lib->display_errors()));
					exit;
				}else{
					$input['company_logo'] = ltrim($path,"./").$company_logo_data['file_name'];
				}
				$this->image_lib->clear();
			}
		}
		$this->db->trans_start();
		$this->db->insert('my_cards',$input);
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return array('status' => 500,'message' =>'Internal server error.','method'=>'create_businessCard');
		} else {
			$this->db->trans_commit();
			return array('status' => 200,'message' =>'Card created.','method'=>'create_businessCard');
		}
	}
	
	/*
	* View Business Card
	*/
	
	public function ht_view_businessCard($input)
	{
		$company_id	= getusersdata($input['user_id'],'company_id');
		$data = $this->db->select('card_id, user_id, template_id, name, middle_name, last_name, title, position,card_picture, profile_pic, company_name, company_logo, postal, building_name, street_address, house_number, state, city, country, fax_number, office_number, mobile_number, fax_country_code, office_country_code, mobile_country_code, email_address, website, UNIX_TIMESTAMP(created_at) as created_at')->from('my_cards')->where(array('card_id'=>$input['card_id'],'is_active'=>'1'))->get();
		
		if ($data->num_rows()){
			$final = $data->row();
			$final->card_picture != '' ? $final->card_picture = base_url().$final->card_picture : '';
			$final->profile_pic != '' ? $final->profile_pic = base_url().$final->profile_pic : '';
			$final->company_logo != '' ? $final->company_logo = base_url().$final->company_logo : '';
			$final->template 	= getTemplateImages($final->template_id,$company_id);
			$final	=	$this->Aes_encryption->aesEncryption($this->secret,json_encode($final));
			return array('status' => 200,'message' => 'Business card detail.','method'=>'view_businessCard','data'=>$final);
		}else{
			return array('status' =>400,'message' =>'Card not exist','method'=>'view_businessCard');
		}
	}
	/*
	* Edit Business Card
	*/
	public function ht_edit_businessCard($input)
	{
		$check = $this->db->select('user_id, template_id, name, middle_name, last_name, title, position, is_cardpic_deleted, card_picture, profile_pic, company_name, company_logo, postal, building_name, street_address, house_number, state, city, country, fax_number, office_number, mobile_number, fax_country_code, office_country_code, mobile_country_code, email_address, website, created_at, updated_at, is_active')->from('my_cards')->where(array('card_id'=>$input['card_id'],'user_id'=>$input['user_id']))->get()->row();
		
		if ($check!=""){
			$card_picture			= '';
			$input['card_picture']	= $check->card_picture;
			$input['profile_pic']	= $check->profile_pic;
			$input['company_logo']	= $check->company_logo;
			$input['is_cardpic_deleted']= $check->is_cardpic_deleted;
			
			$config['allowed_types'] = '*';
			$date = date("Y-m");
			$this->load->library('image_lib');
			if (!empty($_FILES['card_picture']['name'])){
				$path	=  './media/cards/card_picture/'.$date."/";
				if (!is_dir($path)){
					mkdir($path , 0777);
					$content = "<!DOCTYPE html><html><head><title>403 Forbidden</title></head><body><p>Access denied!</p></body></html>"; 
					$fp = fopen($path."index.html","wb"); 
					fwrite($fp,$content); 
					fclose($fp); 
				}
				$config['file_name'] = date("Ymd")."_PP-".rand(1000,9999).'-'.time();
				$config['upload_path'] = $path;
				$this->upload->initialize($config);
				if (!$this->upload->do_upload('card_picture')){
					return array('status' =>400,'message' =>"Card Pic uploading error: ".strip_tags($this->upload->display_errors()),'method'=>$this->uri->segment(3));
					exit;
				}else{
					//$input['card_picture'] !="" ? unlink("./".$input['card_picture']) : '';
					//$card_picture = ltrim($path,"./").$this->upload->data()['file_name'];
					$input['is_cardpic_deleted']= '0';
					$card_picture_data = $this->upload->data();
					$config['image_library'] = 'gd2';
					$config['source_image'] = $path.$card_picture_data['file_name']; //get original image
					$config['maintain_ratio'] = TRUE;
					$config['create_thumb'] = false;
					$config['height'] = 400;
					$this->image_lib->clear();
					$this->image_lib->initialize($config);
					if (!$this->image_lib->resize()) {
						$card = $path.$card_picture_data['file_name'];
						if (file_exists($card)) {
							unlink($card);
						}
						return array('status' =>400,'msg' =>"Card picture resizing error: ".strip_tags($this->image_lib->display_errors()));
						exit;
					}else{
						$card_picture = ltrim($path,"./").$card_picture_data['file_name'];
					}
					$this->image_lib->clear();
				}
			}
			if (!empty($_FILES['profile_pic']['name'])){
				$path	=  './media/cards/profile_pic/'.$date."/";
				if (!is_dir($path)){
					mkdir($path , 0777);
					$content = "<!DOCTYPE html><html><head><title>403 Forbidden</title></head><body><p>Access denied!</p></body></html>"; 
					$fp = fopen($path."index.html","wb"); 
					fwrite($fp,$content); 
					fclose($fp); 
				}
				$config['file_name'] = date("Ymd")."_PP-".rand(1000,9999).'-'.time();
				$config['upload_path'] = $path;
				$this->upload->initialize($config);
				if (!$this->upload->do_upload('profile_pic')){
					return array('status' =>400,'message' =>"Profile Pic uploading error: ".strip_tags($this->upload->display_errors()),'method'=>$this->uri->segment(3));
					exit;
				}else{
					$input['profile_pic'] !="" ? unlink("./".$input['profile_pic']) : '';
					//$input['profile_pic'] = ltrim($path,"./").$this->upload->data()['file_name'];
					
					$pro_picture_data	=	$this->upload->data();
					$config['image_library'] = 'gd2';
					$config['source_image'] = $path.$pro_picture_data['file_name']; //get original image
					$config['maintain_ratio'] = TRUE;
					$config['create_thumb'] = false;
					$config['height'] = 150;
					$this->image_lib->clear();
					$this->image_lib->initialize($config);
					if (!$this->image_lib->resize()) {
						$card = $path.$pro_picture_data['file_name'];
						if (file_exists($card)) {
							unlink($card);
						}
						return array('status' =>400,'msg' =>"Profile picture resizing error: ".strip_tags($this->image_lib->display_errors()));
						exit;
					}else{
						$input['profile_pic'] = ltrim($path,"./").$pro_picture_data['file_name'];
					}
					$this->image_lib->clear();
				}
			}
			
			if (!empty($_FILES['company_logo']['name'])){
				$path	=  './media/cards/company_logo/'.$date."/";
				if (!is_dir($path)){
					mkdir($path , 0777);
					$content = "<!DOCTYPE html><html><head><title>403 Forbidden</title></head><body><p>Access denied!</p></body></html>"; 
					$fp = fopen($path."index.html","wb"); 
					fwrite($fp,$content); 
					fclose($fp); 
				}
				$config['file_name']	= date("Ymd")."_BC-".rand(1000,9999).'-'.time();
				$config['upload_path']	= $path;
				$this->upload->initialize($config);
				if (!$this->upload->do_upload('company_logo')){
					return array('status' =>400,'message' =>"Company logo uploading error: ".strip_tags($this->upload->display_errors()),'method'=>$this->uri->segment(3));
					exit;
				}else{
					$input['company_logo'] !="" ? unlink("./".$input['company_logo']) : '';
					//$input['company_logo'] = ltrim($path,"./").$this->upload->data()['file_name'];
					
					$company_logo_data	=	$this->upload->data();
					$config['image_library'] = 'gd2';
					$config['source_image'] = $path.$company_logo_data['file_name']; //get original image
					$config['maintain_ratio'] = TRUE;
					$config['create_thumb'] = false;
					$config['height'] = 150;
					$this->image_lib->clear();
					$this->image_lib->initialize($config);
					if (!$this->image_lib->resize()) {
						$card = $path.$company_logo_data['file_name'];
						if (file_exists($card)) {
							unlink($card);
						}
						return array('status' =>400,'msg' =>"Company logo resizing error: ".strip_tags($this->image_lib->display_errors()));
						exit;
					}else{
						$input['company_logo'] = ltrim($path,"./").$company_logo_data['file_name'];
					}
					$this->image_lib->clear();
				}
			}
			$this->db->trans_start();
			$check->master_card_id = $input['card_id'];
			$check->created_at = today()[0];
			$check->updated_at = today()[0];
			//Linked Card
			$this->db->insert('my_cards',$check);
			//Update main Card
			if ($card_picture!=""){
				$input['card_picture'] = $card_picture;
			}
			if(isset($input['middle_name'])){
				$input['middle_name'] = $input['middle_name'];
			}else{
				$input['middle_name'] = '';
			}
			
			if(isset($input['last_name'])){
				$input['last_name'] = $input['last_name'];
			}else{
				$input['last_name'] = '';
			}
			if(isset($input['template_id'])){
				$input['template_id'] = $input['template_id'];
			}else{
				$input['template_id'] = '';
			}
			$this->db->where('card_id',$input['card_id'])->update('my_cards',array('template_id'=>$input['template_id'],'name'=>$input['name'],'middle_name'=>$input['middle_name'],'last_name'=>$input['last_name'],'title'=>$input['title'],'position'=>$input['position'],'card_picture'=>$input['card_picture'],'profile_pic'=>$input['profile_pic'],'company_name'=>$input['company_name'],'company_logo'=>$input['company_logo'],'postal'=>$input['postal'],'building_name'=>$input['building_name'],'street_address'=>$input['street_address'],'house_number'=>$input['house_number'],'state'=>$input['state'],'city'=>$input['city'],'country'=>$input['country'],'fax_number'=>$input['fax_number'],'office_number'=>$input['office_number'],'mobile_number'=>$input['mobile_number'],'fax_country_code'=>$input['fax_country_code'], 'office_country_code'=>$input['office_country_code'], 'mobile_country_code'=>$input['mobile_country_code'], 'email_address'=>$input['email_address'],'website'=>$input['website'],'is_cardpic_deleted'=>$input['is_cardpic_deleted'],'updated_at'=>today()[0]));
			
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return array('status' => 500,'message' =>'Internal server error.','method'=>'edit_businessCard');
			} else {
				$this->db->trans_commit();
				return array('status' => 200,'message' =>'Information updated.','method'=>'edit_businessCard');
			}
		}else{
			return array('status' =>400,'message' =>'You are not authorized!','method'=>'edit_businessCard');
		}
	}
	/*
	* Delete Business Card Picture
	*/
	public function ht_delete_businessCardPicture($input)
	{
		$check = $this->db->select('card_picture')->from('my_cards')->where(array('card_id'=>$input['card_id'],'user_id'=>$input['user_id']))->get()->row();
		
		if ($check!=""){
			$this->db->trans_start();
			$this->db->where('card_id',$input['card_id'])->update('my_cards',array('is_cardpic_deleted'=>'1','card_picture'=>''));
			//$this->db->where('card_id',$input['card_id'])->update('my_cards',array('is_cardpic_deleted'=>'1'));
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return array('status' => 500,'message' =>'Internal server error.','method'=>'delete_businessCardPicture');
			} else {
				$this->db->trans_commit();
				if($check->card_picture){
					unlink("./".$check->card_picture);
				}
				return array('status' => 200,'message' =>'Picture deleted.','method'=>'delete_businessCardPicture');
			}
		}else{
			return array('status' =>400,'message' =>'You are not authorized!','method'=>'delete_businessCardPicture');
		}
	}
	
	/*
	* My All Business Cards
	*/
	
	public function ht_get_myBusinessCards($input)
	{
		$company_id	= getusersdata($input['user_id'],'company_id');
		$page		= $input['page'];
		$user_id	= $input['user_id'];
		$limit		= 10;
		$start		= ($page - 1)* $limit; 
		$totalrows 	= $this->db->select('card_id')->from('my_cards')->WHERE (array('user_id'=>$user_id,'is_active'=>'1'))->get()->num_rows();
		$totalpages = ceil($totalrows/$limit);
		$previous	=  $totalpages > 1 ? "1" : "0"; 
		$next		=  $page < $totalpages ? "1" : "0";
		
		$data = $this->db->select('card_id, user_id, template_id, name, middle_name, last_name, title, position, card_picture, profile_pic, company_name, company_logo, postal, building_name, street_address, house_number, state, city, country, fax_number, office_number, mobile_number, fax_country_code, office_country_code, mobile_country_code, email_address, website, UNIX_TIMESTAMP(created_at) as created_at,is_cardpic_deleted')->from('my_cards')->where(array('user_id'=>$user_id,'is_active'=>'1','master_card_id'=>'0'))->order_by('card_id','DESC')->limit($limit,$start)->get()->result_array();
		
		foreach( $data as $key => $row )
		{
			$row['card_picture'] !=''  ? $row['card_picture']	= ($row['is_cardpic_deleted'] == '0' ? base_url().$row['card_picture'] : '') : '';
			$row['profile_pic'] !=''  ? $row['profile_pic']	= base_url().$row['profile_pic'] : '';
			$row['company_logo'] !='' ? $row['company_logo']= base_url().$row['company_logo']: '';
			$row['template']		=	getTemplateImages($row['template_id'],$company_id);
			
			$data[$key] = $row;
		}
		$data	=	$this->Aes_encryption->aesEncryption($this->secret,json_encode($data));
		return array('status' => 200,'message' => 'My all business cards.','method'=>'get_myBusinessCards','data'=>$data,'totalpage'=>(string)$totalpages,'previous'=>$previous,'next'=>$next);
	}
	
	/*
	* Add Business Card to Rolodex
	*/
	public function ht_add_cardToRolodex($input)
	{	
		$validid = check_valid_cardid($input['card_id']);
		if ($validid){
			
			if ($validid != $input['user_id']){
				if (check_CardAlreadyInRolodex($input['card_id'],$input['user_id'])){
					return array('status' =>400,'message' =>'Already collected in Rolodex!','method'=>'add_cardToRolodex');
				}else{
					$input['received_at']= today()[0];
					$this->db->trans_start();
					$this->db->insert('rolodex',$input);
					if ($this->db->trans_status() === FALSE){
						$this->db->trans_rollback();
						return array('status' => 500,'message' =>'Internal server error.','method'=>'add_cardToRolodex');
					} else {
						$this->db->trans_commit();
						return array('status' => 200,'message' =>'Rolodex updated.','method'=>'add_cardToRolodex');
					}
				}
			}else{
				return array('status' =>400,'message' =>'You can\'t add your own card to rolodex!','method'=>'add_cardToRolodex');
			}
		}else{
			return array('status' =>400,'message' =>'Card is not valid!','method'=>'add_cardToRolodex');
		}
	}
	/*
	* Forword Business Card
	*/
	public function ht_forward_businesscard($input)
	{	
		$validid = check_valid_cardid($input['card_id']);
		if ($validid){
			$contacts 	= array_filter(explode(',',$input['contact_numbers']));
			$ceecausers = array_filter(explode(',',$input['ceeca_users']));
			//print_r($contacts); exit;
			for($i=0;$i<count($contacts);$i++){
				$number = $contacts[$i];
				$userid = isNumberRegistered($number);
				if ($userid){
					if ($validid != $userid){
						if (check_CardAlreadyInRolodex($input['card_id'],$userid)==0){
							$this->db->trans_start();
							$this->db->insert('rolodex',array('user_id'=>$userid,'card_id'=>$input['card_id'],'added_via'=>'1','received_at'=>today()[0]));
							if ($this->db->trans_status() === FALSE){
								$this->db->trans_rollback();
							} else {
								$this->db->set('forward_cards','forward_cards+1',false)->where(array('user_id'=>$validid))->update('notifications');
								$this->db->trans_commit();
							}
						}
					}
				}else {
					$check = $this->db->select("forward_id")->from('rolodex_forwarded_cards')->where(array('phoneno'=>$number))->get()->num_rows();
					
					if($check==0){
						$this->db->trans_start();
						$this->db->insert('rolodex_forwarded_cards',array('phoneno'=>$number,'card_id'=>$input['card_id'],'forward_by'=>$input['user_id']));
						if ($this->db->trans_status() === FALSE){
							$this->db->trans_rollback();
						} else {
							$this->db->trans_commit();
						}
					}
				}
			}
			for($x=0;$x<count($ceecausers);$x++){
				$userid = $ceecausers[$x];
				if ($validid != $userid){
					if (check_CardAlreadyInRolodex($input['card_id'],$userid)==0){
						$this->db->trans_start();
						$this->db->insert('rolodex',array('user_id'=>$userid,'card_id'=>$input['card_id'],'added_via'=>'1','received_at'=>today()[0]));
						if ($this->db->trans_status() === FALSE){
							$this->db->trans_rollback();
						} else {
							$this->db->set('forward_cards','forward_cards+1',false)->where(array('user_id'=>$validid))->update('notifications');
							$this->db->trans_commit();
						}
					}
				}
			}
			return array('status' =>200,'message' =>'Card forwarded!','method'=>'forward_businesscard');
		}else{
			return array('status' =>400,'message' =>'Card is not valid!','method'=>'forward_businesscard');
		}
	}
	/*
	* Remove Business Card - Rolodex
	*/
	public function ht_removeRolodex_BusinessCard($input)
	{	
		if (check_valid_rolodex_id($input['rolodex_id'],$input['user_id'])){
			
			$this->db->trans_start();
			$this->db->where(array('rolodex_id'=>$input['rolodex_id']))->delete('rolodex');
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return array('status' => 500,'message' =>'Internal server error.','method'=>'removeRolodex_BusinessCard');
			} else {
				$this->db->trans_commit();
				return array('status' => 200,'message' =>'Card removed.','method'=>'removeRolodex_BusinessCard');
			}
		}else{
			return array('status' =>400,'message' =>'Access denied!','method'=>'removeRolodex_BusinessCard');
		}
	}
	/*
	* Add Note on Business Card - Rolodex
	*/
	public function ht_addNote_BusinessCard($input)
	{	
		if (check_valid_rolodex_id($input['rolodex_id'],$input['user_id'])){
			unset($input['user_id']);
			$this->db->trans_start();
			$this->db->insert('rolodexcard_notes',$input);
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return array('status' => 500,'message' =>'Internal server error.','method'=>'addNote_BusinessCard');
			} else {
				$this->db->trans_commit();
				return array('status' => 200,'message' =>'Note added.','method'=>'addNote_BusinessCard');
			}
		}else{
			return array('status' =>400,'message' =>'Business card is not on your rolodex!','method'=>'addNote_BusinessCard');
		}
	}
	/*
	* Update Note on Business Card - Rolodex
	*/
	public function ht_updateNote_BusinessCard($input)
	{	
		if (check_valid_noteid($input['noteid'],$input['rolodex_id'])){
			$this->db->trans_start();
			$this->db->where('noteid',$input['noteid'])->update('rolodexcard_notes',array('note'=>$input['note']));
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return array('status' => 500,'message' =>'Internal server error.','method'=>'updateNote_BusinessCard');
			} else {
				$this->db->trans_commit();
				return array('status' => 200,'message' =>'Note updated.','method'=>'updateNote_BusinessCard');
			}
		}else{
			return array('status' =>400,'message' =>'Access denied!','method'=>'updateNote_BusinessCard');
		}
	}
	/*
	* Remove Note on Business Card - Rolodex
	*/
	public function ht_removeNote_BusinessCard($input)
	{	
		if (check_valid_noteid($input['noteid'],$input['rolodex_id'])){
			
			$this->db->trans_start();
			$this->db->where(array('noteid'=>$input['noteid']))->delete('rolodexcard_notes');
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return array('status' => 500,'message' =>'Internal server error.','method'=>'removeNote_BusinessCard');
			} else {
				$this->db->trans_commit();
				return array('status' => 200,'message' =>'Note removed.','method'=>'removeNote_BusinessCard');
			}
		}else{
			return array('status' =>400,'message' =>'Access denied!','method'=>'removeNote_BusinessCard');
		}
	}
	
	/*
	* Get My Rolodex
	*/
	
	public function ht_get_myRolodex($input)
	{
		$company_id	= getusersdata($input['user_id'],'company_id');
		$page		= $input['page'];
		$user_id	= $input['user_id'];
		$limit		= 10;
		$start		= ($page - 1)* $limit; 
		$totalrows 	= $this->db->select('card_id')->from('my_cards')->WHERE (array('user_id'=>$user_id,'is_active'=>'1'))->get()->num_rows();
		$totalpages = ceil($totalrows/$limit);
		$previous	=  $totalpages > 1 ? "1" : "0"; 
		$next		=  $page < $totalpages ? "1" : "0";
		
		$data = $this->db->select('rolodex.rolodex_id,rolodex.latitude,rolodex.longitude, rolodex.received_at, my_cards.card_id, my_cards.user_id, my_cards.template_id, my_cards.name, my_cards.middle_name, my_cards.last_name, my_cards.title, my_cards.position, my_cards.card_picture, my_cards.profile_pic, my_cards.company_name, my_cards.company_logo, my_cards.postal, my_cards.building_name, my_cards.street_address, my_cards.house_number, my_cards.state, my_cards.city, my_cards.country, my_cards.fax_number, my_cards.office_number, my_cards.mobile_number, my_cards.fax_country_code,  my_cards.office_country_code,  my_cards.mobile_country_code, my_cards.email_address, website, UNIX_TIMESTAMP(my_cards.created_at) as created_at')->from('rolodex')->join('my_cards', 'my_cards.card_id = rolodex.card_id')->where(array('rolodex.user_id'=>$user_id))->order_by('rolodex_id','DESC')->limit($limit,$start)->get()->result_array();
		
		foreach( $data as $key => $row )
		{
			$row['card_picture'] !=''  ? $row['card_picture']	= base_url().$row['card_picture'] : '';
			$row['profile_pic'] !=''  ? $row['profile_pic']	= base_url().$row['profile_pic'] : '';
			$row['company_logo'] !='' ? $row['company_logo']= base_url().$row['company_logo']: '';
			$row['template']		=	getTemplateImages($row['template_id'],$company_id);
			
			$row['notes'] = $this->db->select('noteid,note')->from('rolodexcard_notes')->WHERE (array('rolodex_id'=>$row['rolodex_id']))->order_by('noteid','ASC')->get()->result_array();
			$row['pictures'] = $this->db->select('picture_id,picture')->from('rolodexcard_pictures')->WHERE (array('rolodex_id'=>$row['rolodex_id']))->order_by('picture_id','ASC')->get()->result_array();
			for($p=0;$p<count($row['pictures']);$p++){
				$row['pictures'][$p]['picture'] = base_url().$row['pictures'][$p]['picture'];
			}
			
			$linked = $this->db->select('card_id, master_card_id, user_id, template_id, name, middle_name, last_name, title, position, card_picture, profile_pic, company_name, company_logo, postal,building_name, street_address, house_number, state, city, country, fax_number, office_number, mobile_number, fax_country_code, office_country_code, mobile_country_code, email_address, my_cards.website, UNIX_TIMESTAMP(created_at) as created_at')->from('my_cards')->where(array('master_card_id'=>$row['card_id'],'created_at>'=>$row['received_at']))->order_by('card_id','DESC')->get()->result_array();
			//print $this->db->last_query(); EXIT;
			foreach( $linked as $keyl => $rowl )
			{
				$rowl['card_picture'] !=''  ? $rowl['card_picture']	= base_url().$rowl['card_picture'] : '';
				$rowl['profile_pic'] !=''  ? $rowl['profile_pic']	= base_url().$rowl['profile_pic'] : '';
				$rowl['company_logo'] !='' ? $rowl['company_logo']= base_url().$rowl['company_logo']: '';
				$rowl['template']		=	getTemplateImages($rowl['template_id'],$company_id);
				$linked[$keyl] = $rowl;
			}
			$row['linked_cards'] = $linked;
			$data[$key] = $row;
		}
		$this->db->set('forward_cards','0',false)->where(array('user_id'=>$user_id))->update('notifications');
		$data	=	$this->Aes_encryption->aesEncryption($this->secret,json_encode($data));			
		return array('status' => 200,'message' => 'My all business cards rolodex.','method'=>'get_myRolodex','data'=>$data,'totalpage'=>(string)$totalpages,'previous'=>$previous,'next'=>$next);
	}
	/*
	* Search My Rolodex Cards
	*/
	public function ht_search_myRolodexCard($input)
	{
		$company_id	= getusersdata($input['user_id'],'company_id');
		$user_id		= $input['user_id'];
		$name			= !empty($input['name']) ? " and my_cards.name LIKE '%".$input['name']."%'" : "";
		$middle_name	= !empty($input['middle_name']) ? " and my_cards.middle_name LIKE '%".$input['middle_name']."%'" : "";
		
		$last_name		= !empty($input['last_name']) ? " and my_cards.last_name LIKE '%".$input['last_name']."%'" : "";
		$title 			= !empty($input['title']) ? " and my_cards.title LIKE '%".$input['title']."%'" : "";
		$position 		= !empty($input['position']) ? " and my_cards.position LIKE '%".$input['position']."%'" : "";
		$company_name 	= !empty($input['company_name']) ? " and my_cards.company_name LIKE '%".$input['company_name']."%'" : "";
		$building_name	= !empty($input['building_name']) ? " and my_cards.building_name LIKE '%".$input['building_name']."%'" : "";
		$city 			= !empty($input['city']) ? " and my_cards.city LIKE '%".$input['city']."%'" : "";
		$country		= !empty($input['country']) ? " and my_cards.country LIKE '%".$input['country']."%'" : "";
		$fax_number 	= !empty($input['fax_number']) ? " and my_cards.fax_number LIKE '%".$input['fax_number']."%'" : "";
		$office_number 	= !empty($input['office_number']) ? " and my_cards.office_number LIKE '%".$input['office_number']."%'" : "";
		$mobile_number 	= !empty($input['mobile_number']) ? " and my_cards.mobile_number LIKE '%".$input['mobile_number']."%'" : "";
		$email_address 	= !empty($input['email_address']) ? " and my_cards.email_address LIKE '%".$input['email_address']."%'" : "";
		$website 		= !empty($input['website']) ? " and my_cards.website LIKE '%".$input['website']."%'" : "";
		$postal 		= !empty($input['postal']) ? " and my_cards.postal LIKE '%".$input['postal']."%'" : "";
		$street_address	= !empty($input['street_address']) ? " and my_cards.street_address LIKE '%".$input['street_address']."%'" : "";
		$house_number	= !empty($input['house_number']) ? " and my_cards.house_number LIKE '%".$input['house_number']."%'" : "";
		$state			= !empty($input['state']) ? " and my_cards.state LIKE '%".$input['state']."%'" : "";
		
		$search = $name . $middle_name. $last_name. $title . $position . $company_name. $building_name . $city . $country . $fax_number . $office_number . $mobile_number. $email_address . $website . $postal . $street_address . $house_number .$state ;
		$data = $this->db->select('rolodex.rolodex_id,rolodex.latitude,rolodex.longitude,my_cards.card_id, my_cards.user_id, my_cards.template_id, my_cards.name, my_cards.middle_name, my_cards.last_name, my_cards.title, my_cards.position, my_cards.card_picture, my_cards.profile_pic, my_cards.company_name, my_cards.company_logo, my_cards.postal, my_cards.building_name, my_cards.street_address, my_cards.house_number, my_cards.state, my_cards.city, my_cards.country, my_cards.fax_number, my_cards.office_number, my_cards.mobile_number, my_cards.fax_country_code,  my_cards.office_country_code, my_cards.mobile_country_code, my_cards.email_address, my_cards.website, UNIX_TIMESTAMP(my_cards.created_at) as created_at')->from('rolodex')->join('my_cards', 'my_cards.card_id = rolodex.card_id')->where("rolodex.user_id=".$user_id. $search)->order_by('rolodex_id','DESC')->get()->result_array();
		//print $this->db->last_query(); EXIT;
		foreach( $data as $key => $row )
		{
			$row['card_picture'] !=''  ? $row['card_picture']	= base_url().$row['card_picture'] : '';
			$row['profile_pic'] !=''  ? $row['profile_pic']	= base_url().$row['profile_pic'] : '';
			$row['company_logo'] !='' ? $row['company_logo']= base_url().$row['company_logo']: '';
			$row['template']		=	getTemplateImages($row['template_id'],$company_id);
			$row['notes'] = $this->db->select('noteid,note')->from('rolodexcard_notes')->WHERE (array('rolodex_id'=>$row['rolodex_id']))->order_by('noteid','ASC')->get()->result_array();
			$row['pictures'] = $this->db->select('picture_id,picture')->from('rolodexcard_pictures')->WHERE (array('rolodex_id'=>$row['rolodex_id']))->order_by('picture_id','ASC')->get()->result_array();
			for($p=0;$p<count($row['pictures']);$p++){
				$row['pictures'][$p]['picture'] = base_url().$row['pictures'][$p]['picture'];
			}
			$linked = $this->db->select('card_id, master_card_id, user_id, template_id, name, title, position, card_picture, profile_pic, company_name, company_logo, postal, building_name, street_address, house_number, state, city, country, fax_number, office_number, mobile_number, fax_country_code, office_country_code,  mobile_country_code, email_address, website, UNIX_TIMESTAMP(created_at) as created_at')->from('my_cards')->where(array('master_card_id'=>$row['card_id']))->order_by('card_id','DESC')->get()->result_array();
			foreach( $linked as $keyl => $rowl )
			{
				$rowl['card_picture'] !=''  ? $rowl['card_picture']	= base_url().$rowl['card_picture'] : '';
				$rowl['profile_pic'] !=''  ? $rowl['profile_pic']	= base_url().$rowl['profile_pic'] : '';
				$rowl['company_logo'] !='' ? $rowl['company_logo']= base_url().$rowl['company_logo']: '';
				$rowl['template']		=	getTemplateImages($rowl['template_id'],$company_id);
				$linked[$keyl] = $rowl;
			}
			$row['linked_cards'] = $linked;
			$data[$key] = $row;
		}
		$data	=	$this->Aes_encryption->aesEncryption($this->secret,json_encode($data));
		return array('status' => 200,'message' => 'Search Data.','method'=>'get_myRolodex','data'=>$data);
	}
	/*
	* Create Business Card
	*/
	public function ht_addPictures_BusinessCard($input)
	{	
		if (check_valid_rolodex_id($input['rolodex_id'],$input['user_id'])){
			unset($input['user_id']);
			//$config['allowed_types'] = 'gif|jpg|jpeg|png';
			$config['allowed_types'] = '*';
			//$config['max_size'] = '200';
			$message = array('status' => 400,'message' =>'Select pitures to upload!','method'=>'addPictures_BusinessCard');
			$date = date("Y-m");
			$files = $_FILES;
			$count = count($_FILES['picture']['name']);
			for($i=0;$i<$count;$i++){
				$_FILES['picture']['name']		= $files['picture']['name'][$i];
				$_FILES['picture']['type']		= $files['picture']['type'][$i];
				$_FILES['picture']['tmp_name']	= $files['picture']['tmp_name'][$i];
				$_FILES['picture']['error']		= $files['picture']['error'][$i];
				$_FILES['picture']['size']		= $files['picture']['size'][$i];    
				if ($_FILES['picture']['name']!=''){
					$path	=  './media/cards/rolodex_picture/'.$date."/";
					if (!is_dir($path)){
						mkdir($path , 0777);
						$content = "<!DOCTYPE html><html><head><title>403 Forbidden</title></head><body><p>Access denied!</p></body></html>"; 
						$fp = fopen($path."index.html","wb"); 
						fwrite($fp,$content); 
						fclose($fp); 
					}
					$config['file_name'] = date("Ymd")."_ROLXP-".rand(1000,9999).'-'.time().$input['rolodex_id'].$i;
					$config['upload_path'] = $path;
					$this->upload->initialize($config);
					if (!$this->upload->do_upload('picture')){
						$message = array('status' =>400,'message' =>"Picture(".($i+1).") uploading error: ".strip_tags($this->upload->display_errors()),'method'=>'addPictures_BusinessCard');
						break;
					}else{
						$input['picture'] = ltrim($path,"./").$this->upload->data()['file_name'];
					}
					$this->db->trans_start();
					$this->db->insert('rolodexcard_pictures',$input);
					if ($this->db->trans_status() === FALSE){
						$this->db->trans_rollback();
						$message = array('status' => 500,'message' =>'Internal server error.','method'=>'addPictures_BusinessCard');
						break;
					} else {
						$this->db->trans_commit();
						$message = array('status' => 200,'message' =>'Picture(s) uploaded.','picture'=>base_url().$input['picture'],'method'=>'addPictures_BusinessCard');
					}
				}
			}
			return $message;
		}else{
			return array('status' =>400,'message' =>'Card is not valid!','method'=>'addPictures_BusinessCard');
		}
	}
	/*
	* Remove Pro Picture Or Logo of Business Card 
	*/
	public function ht_removeProPicOrLogoBusinessCard($input)
	{	
		$check = $this->db->select('user_id,profile_pic,company_logo')->from('my_cards')->where(array('card_id'=>$input['card_id'],'user_id'=>$input['user_id']))->get()->row();
		
		if ($check!=""){
			
			$this->db->trans_start();
			
			if($input['section']==1){
				$this->db->where('card_id',$input['card_id'])->update('my_cards',array('profile_pic'=>'','updated_at'=>today()[0]));
			}else{
				$this->db->where('card_id',$input['card_id'])->update('my_cards',array('company_logo'=>'','updated_at'=>today()[0]));
			}
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				$message = array('status' => 500,'message' =>'Internal server error.','method'=>'emoveProPicOrLogoBusinessCard');
				break;
			} else {
				if($input['section']==1){
					unlink("./".$check->picture);
				}else{
					unlink("./".$check->company_logo);
				}
				$this->db->trans_commit();
				$message =  array('status' => 200,'message' =>'Removed successfully.','method'=>'emoveProPicOrLogoBusinessCard');
			}
			return $message;
		}else{
			return array('status' =>400,'message' =>'Access denied!','method'=>'removePictures_BusinessCard');
		}
	}
	/*
	* Remove Picture of Business Card - Rolodex
	*/
	public function ht_removePictures_BusinessCard($input)
	{	
		if (check_valid_rolodex_id($input['rolodex_id'],$input['user_id'])){
			$picids = explode(',',$input['picture_id']);
			$message = array('status' => 400,'message' =>'Select piture(s) to delete!','method'=>'removePictures_BusinessCard');
			for($i=0;$i<count($picids);$i++){
				$picture = $this->db->select('picture')->from('rolodexcard_pictures')->WHERE (array('picture_id'=>$picids[$i],'rolodex_id'=>$input['rolodex_id']))->get()->row();
				$this->db->trans_start();
				$this->db->where(array('picture_id'=>$picids[$i],'rolodex_id'=>$input['rolodex_id']))->delete('rolodexcard_pictures');
				if ($this->db->trans_status() === FALSE){
					$this->db->trans_rollback();
					$message = array('status' => 500,'message' =>'Internal server error.','method'=>'removePictures_BusinessCard');
					break;
				} else {
					unlink("./".@$picture->picture);
					$this->db->trans_commit();
					$message =  array('status' => 200,'message' =>'Picture(s) removed.','method'=>'removePictures_BusinessCard');
				}
			}
			return $message;
		}else{
			return array('status' =>400,'message' =>'Access denied!','method'=>'removePictures_BusinessCard');
		}
	}
	/*
	* Remove Picture of Business Card - Rolodex
	*/
	public function ht_create_newTeam($input)
	{	
		$input['is_active']		=	1;
		$input['company_id']	=	getusersdata($input['user_id'],'company_id');
		$input['team_coverphoto'] = "";
		$config['allowed_types']=	'*';
		//$config['max_size'] 	=	'200';
		$date = date("Y-m");
		if (!empty($_FILES['team_coverphoto'])){
			$path	=  './media/teams/team_coverphoto/'.$date."/";
			if (!is_dir($path)){
				mkdir($path , 0777);
				$content = "<!DOCTYPE html><html><head><title>403 Forbidden</title></head><body><p>Access denied!</p></body></html>"; 
				$fp = fopen($path."index.html","wb"); 
				fwrite($fp,$content); 
				fclose($fp); 
			}
			$config['file_name'] = date("Ymd")."_TCP-".rand(1000,9999).'-'.time();
			$config['upload_path'] = $path;
			$this->upload->initialize($config);
			if (!$this->upload->do_upload('team_coverphoto')){
				return array('status' =>400,'message' =>"Cover Pic uploading error: ".strip_tags($this->upload->display_errors()),'method'=>'create_newTeam');
				exit;
			}else{
				$input['team_coverphoto'] = ltrim($path,"./").$this->upload->data()['file_name'];
			}
		}
		$invitedusers = array_merge(explode(',',$input['invite_userids']));
		unset($input['invite_userids']);
		$this->db->trans_start();
		$this->db->insert('teams',$input);
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return array('status' => 500,'message' =>'Internal server error.','method'=>'create_newTeam');
		} else {
			$teamid = $this->db->insert_id();
			$this->db->insert('team_members',array('team_id'=>$teamid,'user_id'=>$input['user_id'],'team_owner'=>$input['user_id'],'member_type'=>'2'));
			for($i=0;$i<count($invitedusers);$i++){
				$this->db->insert('team_invites',array('team_id'=>$teamid,'user_id'=>$invitedusers[$i],'inviter_id'=>$input['user_id']));
			}
			$this->db->trans_commit();
			return array('status' => 200,'message' =>'Team created.','method'=>'create_newTeam');
		}
	}
	/*
	* Get My Team
	*/
	
	public function ht_get_myTeams($input)
	{
		$page		= $input['page'];
		$user_id	= $input['user_id'];
		$limit		= 10;
		$start		= ($page - 1)* $limit; 
		$totalrows 	= $this->db->select('member_id')->from('team_members')->WHERE (array('user_id'=>$user_id,'status!='=>'1'))->get()->num_rows();
		$totalpages = ceil($totalrows/$limit);
		$previous	=  $totalpages > 1 ? "1" : "0"; 
		$next		=  $page < $totalpages ? "1" : "0";
		
		$data = $this->db->select('team_members.member_id,team_members.user_id,team_members.member_type,team_members.team_owner,teams.team_id,teams.team_name,teams.team_coverphoto,teams.briefing_validity,users.user_name, users.profile_picture as profile_pic')->from('team_members')->join('teams', 'team_members.team_id = teams.team_id')->join('users', 'users.user_id = team_members.team_owner')->where(array('team_members.user_id'=>$user_id,'team_members.status!='=>'1'))->order_by('team_members.member_type','DESC' )->order_by('teams.team_name', 'ASC' )->limit($limit,$start)->get()->result_array();
		
		foreach( $data as $key => $row )
		{
			$row['team_coverphoto'] !=''  ? $row['team_coverphoto']	= base_url().$row['team_coverphoto'] : '';
			$row['profile_pic']!=''  ? $row['profile_pic']	= base_url(). $row['profile_pic'] : '';
			$row['total_members'] = (string)$this->db->select('member_id')->from('team_members')->where(array('team_id'=>$row['team_id'],'status!='=>'1'))->get()->num_rows();
			$data[$key] = $row;
		}
		$data	=	$this->Aes_encryption->aesEncryption($this->secret,json_encode($data));
		// My New Invites
		$newinvites = $this->db->select('team_invites.invite_id,team_invites.team_id,teams.team_id,teams.user_id,teams.team_name,teams.team_coverphoto,users.user_name, users.profile_picture as profile_pic')->from('team_invites')->where(array('team_invites.user_id'=>$user_id,'team_invites.status'=>1))->join('teams', 'teams.team_id = team_invites.team_id')->join('users', 'users.user_id = teams.user_id')->order_by('team_invites.invite_id', 'desc' )->get()->result_array();
		//print $this->db->last_query(); EXIT;
		for($i=0;$i<count($newinvites);$i++){
			$newinvites[$i]['profile_pic'] !=''  ? $newinvites[$i]['profile_pic'] = base_url(). $newinvites[$i]['profile_pic'] : '';
			$newinvites[$i]['total_members'] = (string)$this->db->select('member_id')->from('team_members')->where(array('team_id'=>$newinvites[$i]['team_id'],'status!='=>'1'))->get()->num_rows();
		}
		$newinvites	=	$this->Aes_encryption->aesEncryption($this->secret,json_encode($newinvites));
		return array('status' => 200,'message' => 'My Teams.','method'=>'get_myTeams','data'=>$data,'new_invites'=>$newinvites,'totalpage'=>(string)$totalpages,'previous'=>$previous,'next'=>$next);
	}
	/*
	* Get Team Invites to means
	*/
	function ht_get_MyInvites($user_id)
	{
		$newinvites = $this->db->select('team_invites.invite_id,team_invites.team_id,teams.team_id,teams.user_id,teams.team_name,teams.team_coverphoto,users.user_name, users.profile_picture as profile_pic')->from('team_invites')->where(array('team_invites.user_id'=>$user_id,'team_invites.status'=>1))->join('teams', 'teams.team_id = team_invites.team_id')->join('users', 'users.user_id = teams.user_id')->order_by('team_invites.invite_id', 'desc' )->get()->result_array();
		//print $this->db->last_query(); EXIT;
		for($i=0;$i<count($newinvites);$i++){
			$newinvites[$i]['profile_pic'] !=''  ? $newinvites[$i]['profile_pic'] = base_url(). $newinvites[$i]['profile_pic'] : '';
			$newinvites[$i]['total_members'] = (string)$this->db->select('member_id')->from('team_members')->where(array('team_id'=>$newinvites[$i]['team_id'],'status!='=>'1'))->get()->num_rows();
		}
		$newinvites	=	$this->Aes_encryption->aesEncryption($this->secret,json_encode($newinvites));
		return array('status' => 200,'message' => 'My invites.','method'=>'get_MyInvites','data'=>$newinvites);
	}
	/*
	* Remove Picture of Business Card - Rolodex
	*/
	public function ht_search_usersForTeam($input)
	{	
		$search		= $input['search'];
		$page		= $input['page'];
		$user_id	= $input['user_id'];
		$company_id	= getusersdata($user_id,'company_id');
		$limit		= 50;
		$start		= ($page - 1)* $limit; 
		$hidden = array(0);
		if($input['team_id']){
			$check_hidden = $this->db->select('GROUP_CONCAT(user_id) as user_ids')->from('team_members')->WHERE(array('team_id'=>$input['team_id'],'status'=>'0'))->get()->row();
			if($check_hidden){
				$hidden = explode(',',$check_hidden->user_ids);
			}
		}
		
		$totalrows 	= $this->db->select('user_id,user_name')->from('users')->WHERE(array('is_active'=>'1','user_id!='=>$user_id,'company_id'=>$company_id))->where_not_in('user_id',$hidden)->group_start()->or_like('user_name',$search)->or_like('mobile_number',$search)->group_end()->get()->num_rows();
		$totalpages = ceil($totalrows/$limit);
		$previous	=  $totalpages > 1 ? "1" : "0"; 
		$next		=  $page < $totalpages ? "1" : "0";
		
		$data		=  $this->db->select('user_id,user_name, country_code, mobile_number, designation as position,  profile_picture as profile_pic')->from('users')->WHERE(array('is_active'=>'1','user_id!='=>$user_id,'company_id'=>$company_id))->where_not_in('user_id',$hidden)->group_start()->or_like('user_name',$search)->or_like('mobile_number',$search)->group_end()->order_by('users.user_name','ASC')->limit($limit,$start)->get()->result_array();
		//print $this->db->last_query(); EXIT;
		foreach( $data as $key => $row )
		{
			$row['position'] 	=  getDesignationdata($row['position'],'designation');
			$row['profile_pic']!=''  ? $row['profile_pic']	= base_url(). $row['profile_pic'] : '';
			$data[$key] = $row;
		}
		$data	=	$this->Aes_encryption->aesEncryption($this->secret,json_encode($data));
		return array('status' => 200,'message' => 'Search results.','method'=>'search_usersForTeam','data'=>$data,'totalpage'=>(string)$totalpages,'previous'=>$previous,'next'=>$next);
	}
	/*
	* Remove Picture of Business Card - Rolodex
	*/
	public function ht_send_teamJoinInvites($input)
	{	
		if (check_valid_team($input['team_id'],$input['user_id'])){
			
			$invited_userids = explode(',',$input['invite_userid']);
			
			foreach($invited_userids as $user_id){
			
				$check = $this->db->select('invite_id,status')->from('team_invites')->where(array('team_id'=>$input['team_id'],'user_id'=>$user_id))->get()->row();
				
				if($check){
					if($check->status!=1){ ## Already requested
						$check = $this->db->select('member_id')->from('team_members')->where(array('team_id'=>$input['team_id'],'user_id'=>$user_id,'status'=>'0'))->get()->num_rows();
						if($check==0){
							$this->db->where('invite_id',$check->invite_id)->update('team_invites',array('status'=>'1'));
						}
					}	
				}else{
					$check = $this->db->select('member_id')->from('team_members')->where(array('team_id'=>$input['team_id'],'user_id'=>$user_id,'status'=>'0'))->get()->num_rows();
					if($check==0){
						$fields = array('team_id'=>$input['team_id'],'user_id'=>$user_id,'inviter_id'=>$input['user_id']);
						$this->db->insert('team_invites',$fields);
					}
				}
			}
			return array('status' => 200,'message' =>'Invitation sent.','method'=>'send_teamJoinInvites');
		}else{
			return array('status' =>400,'message' =>'Access denied!','method'=>'send_teamJoinInvites');
		}
	}
	/*
	* Accept or reject Team invites
	*/
	public function ht_acceptOrReject_teamJoinInvite($input)
	{	
		$check = check_valid_invite($input['invite_id'],$input['user_id']);
		if ($check){
			$team_id	=	$check->team_id;
			$inviter_id	=	$check->inviter_id;
			$action		=	$input['action'];
			
			if($action==1){ // Accept invites
				$fields = array('team_id'=>$team_id,'user_id'=>$input['user_id'],'team_owner'=>$inviter_id);
				
				$member_id = check_valid_memeber($team_id,$input['user_id']); // Check for previous membership
				
				$this->db->trans_start();
				
				if ($member_id){
					$this->db->where(array('member_id'=>$member_id))->update('team_members',array('status' =>0));
					
					$this->db->where(array('team_id'=>$team_id,'accountability'=>$input['user_id']))->update('todolist',array('is_active' =>0));
					
					$this->db->where(array('team_id'=>$team_id,'prev_accountability'=>$input['user_id']))->delete('todo_reassign_action');
				}else{
					$this->db->insert('team_members',$fields);
				}
				
				if ($this->db->trans_status() === FALSE){
					$this->db->trans_rollback();
					return array('status' => 500,'message' =>'Internal server error.','method'=>'acceptOrReject_teamJoinInvite');
				} else {
					$this->db->where(array('invite_id'=>$input['invite_id']))->delete('team_invites');
					$this->db->trans_commit();
					return array('status' => 200,'message' =>'Invitation accepted.','method'=>'acceptOrReject_teamJoinInvite');
				}
			}else if ($action == 2){ // Reject Invites
				$this->db->where(array('invite_id'=>$input['invite_id']))->update('team_invites',array('status' =>0,'invited_on'=>today()[0]));
				return array('status' => 200,'message' =>'Invitation rejected.','method'=>'acceptOrReject_teamJoinInvite');
			}else{
				return array('status' =>400,'message' =>'Bad request','method'=>'acceptOrReject_teamJoinInvite');
			}
		}else{
			return array('status' =>400,'message' =>'Invites not exit!','method'=>'acceptOrReject_teamJoinInvite');
		}
	}
	/*
	* Get Team Members
	*/
	public function ht_getTeamMembers($input)
	{	
		$page		= $input['page'];
		$user_id	= $input['user_id'];
		$team_id	= $input['team_id'];
		$search 	= $input['search'];
		$limit		= 50;
		$start		= ($page - 1)* $limit; 
		if (check_valid_teamId($team_id)){
			
			$totalrows 	= $this->db->select('team_members.user_id,users.user_name')->from('team_members')->WHERE(array('team_id'=>$team_id,'status!='=>'1'))->join('users','users.user_id = team_members.user_id')->group_start()->like('users.user_name',$search)->group_end()->get()->num_rows();
			$totalpages = ceil($totalrows/$limit);
			$previous	=  $totalpages > 1 ? "1" : "0"; 
			$next		=  $page < $totalpages ? "1" : "0";
			$data		=  $this->db->select('team_members.member_id, team_members.team_id, team_members.user_id, team_members.team_owner, team_members.member_type, team_members.joined_at, users.user_name, users.email,users.designation as position, users.profile_picture as profile_pic')->from('team_members')->WHERE(array('team_id'=>$team_id,'status!='=>'1'))->join('users','users.user_id = team_members.user_id')->group_start()->like('users.user_name',$search)->group_end()->order_by('team_members.member_type','DESC')->order_by('team_members.member_id', 'desc' )->limit($limit,$start)->get()->result_array();
			//print $this->db->last_query(); EXIT;
			foreach( $data as $key => $row )
			{
				$row['position'] 	=  getDesignationdata($row['position'],'designation');
				$row['profile_pic']!=''  ? $row['profile_pic']	= base_url(). $row['profile_pic']: '';
				$data[$key] = $row;
			}
			$data	=	$this->Aes_encryption->aesEncryption($this->secret,json_encode($data));
			return array('status' => 200,'message' => 'Team members.','method'=>'getTeamMembers','data'=>$data,'totalpage'=>(string)$totalpages,'previous'=>$previous,'next'=>$next);
		}else{
			return array('status' => 400,'message'=>'Team not exist','method'=>'getTeamMembers');
		}
	}
	/*
	* Get Team Invites and Declined Requests 
	*/
	public function ht_getTeamInvitesAndDeclinedRequests($input)
	{	
		$page		= $input['page'];
		$user_id	= $input['user_id'];
		$team_id	= $input['team_id'];
		$search 	= $input['search'];
		$limit		= 50;
		$start		= ($page - 1)* $limit; 
		if (getTeamdata($team_id,'user_id')== $user_id){
			
			## Get All invites
			$totalrows 	= $this->db->select('team_invites.invite_id,team_invites.team_id,users.user_name')->from('team_invites')->where(array('team_invites.team_id'=>$team_id,'team_invites.status'=>1))->join('teams', 'teams.team_id = team_invites.team_id')->join('users', 'users.user_id = team_invites.user_id')->group_start()->like('users.user_name',$search)->group_end()->get()->num_rows();
			$totalpages = ceil($totalrows/$limit);
			$previous	=  $totalpages > 1 ? "1" : "0"; 
			$next		=  $page < $totalpages ? "1" : "0";
			
			$newinvites = $this->db->select('team_invites.invite_id,team_invites.team_id,team_invites.user_id,teams.team_id,teams.team_name,teams.team_coverphoto,users.user_name,users.email,users.designation as position, users.profile_picture as profile_pic')->from('team_invites')->where(array('team_invites.team_id'=>$team_id,'team_invites.status'=>1))->join('teams', 'teams.team_id = team_invites.team_id')->join('users', 'users.user_id = team_invites.user_id')->group_start()->like('users.user_name',$search)->group_end()->order_by('team_invites.invite_id', 'desc' )->limit($limit,$start)->get()->result_array();
			//print $this->db->last_query(); EXIT;
			for($i=0;$i<count($newinvites);$i++){
				$newinvites[$i]['position'] 	=  getDesignationdata($newinvites[$i]['position'],'designation');
				$newinvites[$i]['profile_pic'] !=''  ? $newinvites[$i]['profile_pic'] = base_url(). $newinvites[$i]['profile_pic'] : '';
			}
			$newinvites	=	$this->Aes_encryption->aesEncryption($this->secret,json_encode($newinvites));
			
			## Get All Declined
			$declined = $this->db->select('team_invites.invite_id,team_invites.team_id,team_invites.user_id,teams.team_id,teams.team_name,teams.team_coverphoto,users.user_name, users.email, users.designation as position, users.profile_picture as profile_pic')->from('team_invites')->where(array('team_invites.team_id'=>$team_id,'team_invites.status'=>0))->join('teams', 'teams.team_id = team_invites.team_id')->join('users', 'users.user_id = team_invites.user_id')->group_start()->like('users.user_name',$search)->group_end()->order_by('team_invites.invite_id', 'desc' )->limit($limit,$start)->get()->result_array();
			//print $this->db->last_query(); EXIT;
			for($j=0;$j<count($declined);$j++){
				$declined[$j]['position'] 	=  getDesignationdata($declined[$j]['position'],'designation');
				$declined[$j]['profile_pic'] !=''  ? $declined[$j]['profile_pic'] = base_url(). $declined[$j]['profile_pic'] : '';
			}
			$declined	=	$this->Aes_encryption->aesEncryption($this->secret,json_encode($declined));
			return array('status' => 200,'message' => 'Data featched.','method'=>'getTeamInvitesAndDeclinedRequests','invites'=>$newinvites,'declined'=>$declined,'totalpage'=>(string)$totalpages,'previous'=>$previous,'next'=>$next);
		}else{
			return array('status' => 400,'message'=>'Access denied!','method'=>'getTeamInvitesAndDeclinedRequests');
		}
	}
	/*
	* Remove Team Member
	*/
	public function ht_remove_TeamMember($input)
	{	
		if (check_valid_team($input['team_id'],$input['user_id'])){
			$memberid = check_valid_memeber($input['team_id'],$input['member_id']); ## member_id means members user_id
			
			if ($memberid){
				$todolist = $this->db->select('todoid,assigned_by')->from('todolist')->WHERE(array('team_id'=>$input['team_id'],'accountability'=>$input['member_id'],'status'=>'0'))->get()->result_array();
				
				$this->db->trans_start();
				if (count($todolist)){
					foreach($todolist as $list){
						$this->db->insert('todo_reassign_action',array('todoid'=>$list['todoid'],'team_id'=>$input['team_id'],'prev_assigned_by'=>$list['assigned_by'],'prev_accountability'=>$input['member_id'],'updated_at'=>today()[0]));
					}
				}
				$this->db->where(array('member_id'=>$memberid))->update('team_members',array('status' =>1));
				$this->db->where(array('team_id'=>$input['team_id'],'accountability'=>$input['member_id']))->update('todolist',array('is_active' =>1));
				
				if ($this->db->trans_status() === FALSE){
					$this->db->trans_rollback();
					return array('status' => 500,'message' =>'Internal server error.','method'=>'remove_TeamMember');
				} else {
					$this->db->trans_commit();
					return array('status' => 200,'message' =>'Member removed successfully.','method'=>'remove_TeamMember');
				}
			}else{
				return array('status' =>400,'message' =>'Invalid team member','method'=>'remove_TeamMember');
			}
		}else{
			return array('status' =>400,'message' =>'Access denied!','method'=>'remove_TeamMember');
		}
	}
	
	/*
	* Set validity of briefings
	*/
	public function ht_set_briefingValidity($input)
	{	
		if (check_valid_team($input['team_id'],$input['user_id'])){
			$this->db->trans_start();
			$this->db->where(array('team_id'=>$input['team_id']))->update('teams',array('briefing_validity' =>$input['briefing_validity'],'updated_at'=>today()[0]));
			
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return array('status' => 500,'message' =>'Internal server error.','method'=>'set_briefingValidity');
			} else {
				$this->db->trans_commit();
				return array('status' => 200,'message' =>'Updated successfully.','method'=>'set_briefingValidity');
			}
		}else{
			return array('status' =>400,'message' =>'Access denied!','method'=>'set_briefingValidity');
		}
	}
	/*
	* Appoint New Team Leader
	*/
	## member_type = 0 means Member, 1 means Secretory, 2 Means Team Leader
	public function ht_appoint_newTeamLeader($input)
	{	
		if (check_valid_team($input['team_id'],$input['user_id'])){
			
			$check = $this->db->select("member_type")->from("team_members")->where(array('user_id'=>$input['appoint_userid'],'team_id'=>$input['team_id']))->get()->row();
			
			if($check){ // Accept invites
				$this->db->trans_start();
				
				$this->db->where(array('team_id' =>$input['team_id']))->update('teams',array('user_id'=>$input['appoint_userid'],'updated_at'=>today()[0]));
				
				$this->db->where(array('team_id' =>$input['team_id']))->update('team_invites',array('inviter_id'=>$input['appoint_userid']));
				
				$this->db->where(array('team_id' =>$input['team_id'],'user_id'=>$input['appoint_userid']))->update('team_members',array('member_type'=>'2','ttalk_status'=>'0','status'=>'0'));
				
				$this->db->where(array('team_id' =>$input['team_id'],'user_id'=>$input['user_id']))->update('team_members',array('member_type'=>'0','ttalk_status'=>'0','status'=>'0'));
				
				$this->db->where(array('team_id' =>$input['team_id']))->update('team_members',array('team_owner'=>$input['appoint_userid']));
				
				$this->db->where(array('team_id' =>$input['team_id']))->update('todolist',array('assigned_by'=>$input['appoint_userid']));
				
				$this->db->where(array('team_id' =>$input['team_id']))->update('todo_reassign_action',array('prev_accountability'=>$input['appoint_userid']));
				
				if ($this->db->trans_status() === FALSE){
					$this->db->trans_rollback();
					return array('status' => 500,'message' =>'Internal server error.','method'=>'appoint_newTeamLeader');
				} else {
					$this->db->trans_commit();
					return array('status' => 200,'message' =>'Updated successfully','method'=>'appoint_newTeamLeader');
				}
			}else{
				return array('status' =>400,'message' =>'Invited user not belongs to this team!','method'=>'appoint_newTeamLeader');
			}
		}else{
			return array('status' =>400,'message' =>'Access denied!','method'=>'appoint_newTeamLeader');
		}
	}
	/*
	* Appoint or reject secretary
	*/
	## member_type = 0 means Member, 1 means Secretory, 2 Means Team Leader
	public function ht_appoint_reject_secretary($input)
	{	
		if (check_valid_team($input['team_id'],$input['user_id'])){
			
			$check = $this->db->select("member_type")->from("team_members")->where(array('user_id'=>$input['appoint_userid'],'team_id'=>$input['team_id']))->get()->row();
			
			if($check){ // Accept invites
				$this->db->trans_start();
				if ($check->member_type== 1){
					$this->db->where(array('user_id'=>$input['appoint_userid'],'team_id'=>$input['team_id']))->update('team_members',array('member_type' =>'0'));
				}else if ($check->member_type== 2){
					$this->db->where(array('user_id'=>$input['appoint_userid'],'team_id'=>$input['team_id']))->update('team_members',array('member_type' =>'2'));
					
				}else{
					$this->db->where(array('user_id'=>$input['appoint_userid'],'team_id'=>$input['team_id']))->update('team_members',array('member_type' =>'1'));
				}
				if ($this->db->trans_status() === FALSE){
					$this->db->trans_rollback();
					return array('status' => 500,'message' =>'Internal server error.','method'=>'appoint_reject_secretary');
				} else {
					$this->db->trans_commit();
					return array('status' => 200,'message' =>'Updated successfully','method'=>'appoint_reject_secretary');
				}
			}else{
				return array('status' =>400,'message' =>'Invited user not belong to this team!','method'=>'appoint_reject_secretary');
			}
		}else{
			return array('status' =>400,'message' =>'Access denied!','method'=>'appoint_reject_secretary');
		}
	}
	/*
	* Assing new Task
	*/
	public function ht_assign_NewTask($input)
	{	
		if (check_valid_team($input['team_id'],$input['assigned_by'])){
			$input['attachment'] = "";
			if(isset($input['link'])){
				$input['link'] = $input['link'];
			}else{
				$input['link'] = "";
			}
			$config['allowed_types']=	'*';
			if (!empty($_FILES['attachment'])){
				$path	=  './media/todo/'.$date."/";
				if (!is_dir($path)){
					mkdir($path , 0777);
					$content = "<!DOCTYPE html><html><head><title>403 Forbidden</title></head><body><p>Access denied!</p></body></html>"; 
					$fp = fopen($path."index.html","wb"); 
					fwrite($fp,$content); 
					fclose($fp); 
				}
				$config['file_name'] = date("Ymd")."_todo-".rand(1000,9999).'-'.time();
				$config['upload_path'] = $path;
				$this->upload->initialize($config);
				if (!$this->upload->do_upload('attachment')){
					return array('status' =>400,'message' =>"Uploading error: ".strip_tags($this->upload->display_errors()),'method'=>'assign_NewTask');
					exit;
				}else{
					$input['attachment'] = ltrim($path,"./").$this->upload->data()['file_name'];
				}
			}
			$this->db->trans_start();
			$this->db->insert('todolist',$input);
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return array('status' => 500,'message' =>'Internal server error.','method'=>'assign_NewTask');
			} else {
				$notification = $this->db->select('user_id')->from('notifications')->WHERE(array('user_id'=>$input['accountability']))->get()->num_rows();
				if($notification==0){
					$this->db->insert('notifications',array('user_id'=>$input['accountability'],'forward_cards'=>'0','todo_assigned'=>'1','new_ttalks'=>'0','new_onetoone_msg'=>'0'));
				}else{
					$this->db->set('todo_assigned','todo_assigned+1',false)->where(array('user_id'=>$input['accountability']))->update('notifications');
				}
				$this->db->trans_commit();
				return array('status' => 200,'message' =>'Task assigned.','method'=>'assign_NewTask');
			}
		}else{
			return array('status' =>400,'message' =>'Access denied!','method'=>'assign_NewTask');
		}
	}
	/*
	* Edit Assigned Task
	*/
	public function ht_edit_assignedTask($input)
	{	
		if (check_valid_todoid_assigner($input['todoid'],$input['assigned_by'])){
			$this->db->trans_start();
			
			## Check Reassign 
			$check = $this->db->select("reassignaction_id")->from("todo_reassign_action")->where(array('todoid'=>$input['todoid']))->get()->result_array();
			if(count($check)){
				$this->db->where(array('reassignaction_id'=>$check[0]['reassignaction_id']))->delete('todo_reassign_action');
			}
			$input['attachment'] = getTodoData($input['todoid'],'attachment');
			$config['allowed_types']=	'*';
			if (!empty($_FILES['attachment'])){
				$path	=  './media/todo/'.$date."/";
				if (!is_dir($path)){
					mkdir($path , 0777);
					$content = "<!DOCTYPE html><html><head><title>403 Forbidden</title></head><body><p>Access denied!</p></body></html>"; 
					$fp = fopen($path."index.html","wb"); 
					fwrite($fp,$content); 
					fclose($fp); 
				}
				$config['file_name'] = date("Ymd")."_todo-".rand(1000,9999).'-'.time();
				$config['upload_path'] = $path;
				$this->upload->initialize($config);
				if (!$this->upload->do_upload('attachment')){
					return array('status' =>400,'message' =>"Uploading error: ".strip_tags($this->upload->display_errors()),'method'=>'assign_NewTask');
					exit;
				}else{
					$input['attachment']!="" ? unlink('./'.$input['attachment']): '';
					$input['attachment'] = ltrim($path,"./").$this->upload->data()['file_name'];
				}
			}
			$input['link'] = '';
			if(isset($input['link'])){
				$input['link'] = $input['link'];
			}
			$this->db->where(array('todoid'=>$input['todoid']))->update('todolist',array('is_active' =>0,'accountability'=>$input['accountability'],'team_id'=>$input['team_id'],'action_name'=>$input['action_name'],'action_description'=>$input['action_description'],'deadline'=>$input['deadline'],'link'=>$input['link'],'attachment'=>$input['attachment']));
			
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return array('status' => 500,'message' =>'Internal server error.','method'=>'edit_assignedTask');
			}else{
				if ($input['accountable_changed']){
					$notification = $this->db->select('user_id')->from('notifications')->WHERE(array('user_id'=>$input['accountability']))->get()->num_rows();
					if($notification){
						$this->db->set('todo_assigned','todo_assigned+1',false)->where(array('user_id'=>$input['accountability']))->update('notifications');
					}else{
						$this->db->insert('notifications',array('user_id'=>$input['accountability'],'forward_cards'=>'0','todo_assigned'=>'1','new_ttalks'=>'0','new_onetoone_msg'=>'0'));
					}
				}
				$this->db->trans_commit();
				return array('status' => 200,'message' =>'Task updated.','method'=>'edit_assignedTask');
			}
		}else{
			return array('status' =>400,'message' =>'Access denied!','method'=>'edit_assignedTask');
		}
	}
	/*
	* Delete Assigned Task
	*/
	public function ht_delete_assignedTask($input)
	{	
		$accountability = check_valid_todoid_assigner($input['todoid'],$input['user_id']);
		if ($accountability){
			$this->db->trans_start();
			$this->db->where(array('todoid'=>$input['todoid']))->delete('todolist');
			
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return array('status' => 500,'message' =>'Internal server error.','method'=>'delete_assignedTask');
			}else{
				$notification = $this->db->select('todo_assigned')->from('notifications')->WHERE(array('user_id'=>$accountability))->get()->row();
				if($notification!=''){
					if($notification->todo_assigned > 0){
						$this->db->set('todo_assigned','todo_assigned-1',false)->where(array('user_id'=>$accountability))->update('notifications');
					}
				}
				$this->db->trans_commit();
				return array('status' => 200,'message' =>'Task updated.','method'=>'delete_assignedTask');
			}
		}else{
			return array('status' =>400,'message' =>'Access denied!','method'=>'delete_assignedTask');
		}
	}
	/*
	* Get My Todo list
	*/
	## status = 0 Working progress, 1 means accomplished
	## is_active = 0 means user active team member, 1 means user left team
	public function ht_get_myTodolist($input)
	{	
		$max 	= $this->db->select('IFNULL(MAX(todoid),0) as todo_id')->from('todolist')->WHERE(array('accountability'=>$input['user_id'],'is_active!='=>'1','status'=>'0'))->order_by('todoid','DESC')->get()->row();
		
		$data = $this->db->select('todolist.todoid, todolist.team_id, todolist.accountability, todolist.action_name, todolist.action_description, todolist.deadline, todolist.attachment, todolist.link, todolist.status, todolist.is_active, teams.team_name, teams.team_coverphoto, teams.user_id as team_owner, users.user_name, users.email, users.designation as position,users.profile_picture as profile_pic')->from('todolist')->where(array('accountability'=>$input['user_id'],'todolist.is_active!='=>'1','todolist.status'=>'0'))->join('teams', 'todolist.team_id = teams.team_id')->join('users', 'users.user_id = todolist.accountability')->order_by('todolist.status','ASC')->order_by('todolist.deadline','ASC')->get()->result_array();
		foreach( $data as $key => $row )
		{
			$row['attachment']!=''  ? $row['attachment']	= base_url(). $row['attachment']: '';
			$row['filetype'] = end(explode('.',$row['attachment']));
			$row['team_coverphoto'] !=''  ? $row['team_coverphoto']	= base_url().$row['team_coverphoto'] : '';
			
			$row['profile_pic']!=''  ? $row['profile_pic']	= base_url(). $row['profile_pic']: '';
			$row['position'] 	=  getDesignationdata($row['position'],'designation');
			$row['total_members'] = (string)$this->db->select('member_id')->from('team_members')->where(array('team_id'=>$row['team_id'],'status!='=>'1'))->get()->num_rows();
			$data[$key] = $row;
		}
		$this->db->set('todo_assigned','0',false)->where(array('user_id'=>$input['user_id']))->update('notifications');
		$data	=	$this->Aes_encryption->aesEncryption($this->secret,json_encode($data));
		return array('status' => 200,'data'=>$data,'max_todoid'=>$max->todo_id,'message' =>'My todo list.','method'=>'get_myTodolist');
	}
	/*
	* Refresh My Todo list
	*/
	public function ht_refresh_myTodolist($input)
	{	
		$data = $this->db->select('todolist.todoid, todolist.team_id, todolist.accountability, todolist.action_name, todolist.action_description, todolist.deadline, todolist.attachment, todolist.link, todolist.status,todolist.is_active, teams.team_name, teams.team_coverphoto, teams.user_id as team_owner, users.user_name, users.email, users.designation as position,users.profile_picture as profile_pic')->from('todolist')->where(array('accountability'=>$input['user_id'],'todolist.is_active!='=>'1','todolist.status'=>'0','todolist.todoid>'=>$input['todoid']))->join('teams', 'todolist.team_id = teams.team_id')->join('users', 'users.user_id = todolist.accountability')->order_by('todolist.deadline','ASC')->get()->result_array();
		foreach( $data as $key => $row )
		{	
			$row['attachment']!=''  ? $row['attachment']	= base_url(). $row['attachment']: '';
			$row['filetype'] = end(explode('.',$row['attachment']));
			$row['team_coverphoto'] !=''  ? $row['team_coverphoto']	= base_url().$row['team_coverphoto'] : '';
			$row['profile_pic']!=''  ? $row['profile_pic']	= base_url(). $row['profile_pic']: '';
			$row['position'] 	=  getDesignationdata($row['position'],'designation');
			$row['total_members'] = (string)$this->db->select('member_id')->from('team_members')->where(array('team_id'=>$row['team_id'],'status!='=>'1'))->get()->num_rows();
			$data[$key] = $row;
		}
		$max = 0;
		if(count($data)){
			$max = $data[0]['todoid'];
		}
		$this->db->set('todo_assigned','0',false)->where(array('user_id'=>$input['user_id']))->update('notifications');
		$data	=	$this->Aes_encryption->aesEncryption($this->secret,json_encode($data));
		return array('status' => 200,'data'=>$data,'max_todoid'=>$max,'message' =>'New actions.','method'=>'refresh_myTodolist');
	}
	/*
	* Get Team Action list
	*/
	## status = 0 Working progress, 1 means accomplished
	## is_active = 0 means user active team member, 1 means user left team
	public function ht_get_Actionlist($input)
	{	
		$page 		= $input['page'];
		$limit		= 50;
		$start		= ($page - 1)* $limit; 
		if (check_valid_memeber($input['team_id'],$input['user_id'])){
			$all_rows 	= $this->db->select('todoid')->from('todolist')->WHERE(array('team_id'=>$input['team_id']))->order_by('todoid','DESC')->get();
			$totalrows 	= $all_rows->num_rows();
			$totalpages = ceil($totalrows/$limit);
			$previous	=  $totalpages > 1 ? "1" : "0"; 
			$next		=  $page < $totalpages ? "1" : "0";
			
			$data = $this->db->select('todolist.todoid, todolist.team_id, todolist.accountability, todolist.action_name, todolist.action_description, todolist.deadline, todolist.attachment, todolist.link, todolist.status,todolist.is_active, teams.team_name, teams.team_coverphoto, teams.user_id as team_owner, users.user_name, users.email, users.designation as position,users.profile_picture as profile_pic')->from('todolist')->where(array('todolist.team_id'=>$input['team_id']))->join('teams', 'todolist.team_id = teams.team_id')->join('users', 'users.user_id = todolist.accountability')->order_by('todolist.todoid','DESC')->order_by('todolist.deadline','ASC')->limit($limit,$start)->get()->result_array();
			foreach( $data as $key => $row )
			{
				$row['attachment']!=''  ? $row['attachment']	= base_url(). $row['attachment']: '';
				$row['filetype'] = end(explode('.',$row['attachment']));
				$row['team_coverphoto'] !=''  ? $row['team_coverphoto']	= base_url().$row['team_coverphoto'] : '';
				$row['profile_pic']!=''  ? $row['profile_pic']	= base_url(). $row['profile_pic']: '';
				$row['position'] 	=  getDesignationdata($row['position'],'designation');
				$row['total_members'] = (string)$this->db->select('member_id')->from('team_members')->where(array('team_id'=>$row['team_id'],'status!='=>'1'))->get()->num_rows();
				$data[$key] = $row;
			}
			$max = 0;
			if(count($all_rows->row())){
				$max = $all_rows->row()->todoid;
			}
			$data	=	$this->Aes_encryption->aesEncryption($this->secret,json_encode($data));
			return array('status' => 200,'data'=>$data,'max_todoid'=>$max,'message' =>'Action list.','method'=>'get_ActionList','totalpage'=>(string)$totalpages,'previous'=>$previous,'next'=>$next);
		
		}else{
			return array('status' => 400,'message'=>'Access denied!','method'=>'get_ActionList');
		}
	}
	/*
	* Refresh Team Action list
	*/
	public function ht_refresh_ActionList($input)
	{	
		if (check_valid_memeber($input['team_id'],$input['user_id'])){
			
			$data = $this->db->select('todolist.todoid, todolist.team_id, todolist.accountability, todolist.action_name, todolist.action_description, todolist.deadline, todolist.status,todolist.is_active, teams.team_name, teams.team_coverphoto, teams.user_id as team_owner, users.user_name, users.email, users.designation as position,users.profile_picture as profile_pic')->from('todolist')->where(array('todolist.team_id'=>$input['team_id'],'todolist.todoid>'=>$input['todoid']))->join('teams', 'todolist.team_id = teams.team_id')->join('users', 'users.user_id = todolist.accountability')->order_by('todolist.deadline','ASC')->get()->result_array();
			foreach( $data as $key => $row )
			{
				$row['team_coverphoto'] !=''  ? $row['team_coverphoto']	= base_url().$row['team_coverphoto'] : '';
				$row['profile_pic']!=''  ? $row['profile_pic']	= base_url(). $row['profile_pic']: '';
				$row['position'] 	=  getDesignationdata($row['position'],'designation');
				$row['total_members'] = (string)$this->db->select('member_id')->from('team_members')->where(array('team_id'=>$row['team_id'],'status!='=>'1'))->get()->num_rows();
				$data[$key] = $row;
			}
			$max = 0;
			if(count($data)){
				$max = $data[0]['todoid'];
			}
			$data	=	$this->Aes_encryption->aesEncryption($this->secret,json_encode($data));
			return array('status' => 200,'data'=>$data,'max_todoid'=>$max,'message' =>'New Action list.','method'=>'refresh_ActionList');
		
		}else{
			return array('status' => 400,'message'=>'Access denied!','method'=>'refresh_ActionList');
		}
	}
	/*
	* Mark Action Accomplish
	*/
	public function ht_mark_ActionAccomplished($input)
	{	
		if (check_valid_todoid($input['todoid'],$input['user_id'])){
			$check 	= $this->db->select('status')->from('todolist')->WHERE(array('todoid'=>$input['todoid']))->get()->row();
			if ($check->status ==1){
				$status = 0;
			}else{
				$status = 1;
			}
			$this->db->trans_start();
			
			$this->db->where(array('todoid'=>$input['todoid']))->update('todolist',array('status' =>$status,'complited_at'=>date("Y-m-d H:i:s")));
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return array('status' => 500,'message' =>'Internal server error.','method'=>'mark_ActionAccomplished');
			} else {
				$this->db->trans_commit();
				return array('status' => 200,'message' =>'Status updated.','method'=>'mark_ActionAccomplished');
			}
		}else{
			return array('status' =>400,'message' =>'Access denied!','method'=>'mark_ActionAccomplished');
		}
	}
	/*
	* Get Ttalk Inbox
	*/
	public function ht_get_TtalkInbox($input)
	{	 
		$user_id = $input['user_id'];
		$leftteams = $this->db->query("SELECT GROUP_CONCAT(team_id) as team_id FROM `team_members` WHERE `user_id`='".$user_id."' and `status`=1 ")->row_array();
		$leftteamids = 0;
		if($leftteams['team_id']){
			$leftteamids = $leftteams['team_id'];
		}
		$talks = $this->db->query("SELECT DISTINCT(thread_id) AS thread_id, team_id, (SELECT MAX(ttalkid) AS ttalkid FROM ttalks WHERE thread_id= a.thread_id) as  ttalkid, IFNULL((select team_name from teams where team_id=a.team_id),'') as team_name, IFNULL((select team_coverphoto from teams where team_id=a.team_id),'') as team_coverphoto, IFNULL((select user_id from teams where team_id=a.team_id),'') as team_owner, IFNULL((select briefing_validity from teams where team_id=a.team_id),'') as briefing_validity FROM ttalks as a WHERE (a.sender_id='".$input['user_id']."' or a.receiver_id='".$input['user_id']."') AND team_id NOT IN($leftteamids) order by ttalkid desc" )->result_array();
		//print $this->db->last_query(); EXIT;
		$ids = array(0);
		if(count($talks)){
			$ids = array_column($talks,'team_id');
		}
		foreach( $talks as $keys => $rows )
		{
			$rows['team_coverphoto']!=''  ? $rows['team_coverphoto']	= base_url().$rows['team_coverphoto'] : '';
			if($rows['team_id']){
				$other_id = $rows['team_owner'];
				$rows['unread_count']	=	messageUnreadcount($rows['thread_id'],$input['user_id'],$rows['team_id']);
			}else{
				$other_id	= getSenderOrReceiverId($rows['thread_id'],$input['user_id']);
				$rows['unread_count']	=	messageUnreadcount($rows['thread_id'],$input['user_id'],'o2o');
			}
			$profile = $this->db->select('user_name,profile_picture,email,designation')->from('users')->where(array('user_id'=>$other_id))->get()->row();
			
			//print_r($profile); exit;
			$rows['receiver_id']	=	$other_id;
			$rows['user_name']		=	$profile->user_name;
			$rows['email']			=	$profile->email;
			$rows['profile_pic']	=	$profile->profile_picture !=''  ?  base_url().$profile->profile_picture : '';
			$rows['position'] 		=	getDesignationdata($profile->designation,'designation');
			$rows['total_members'] 	=	(string)$this->db->select('member_id')->from('team_members')->where(array('team_id'=>$rows['team_id'],'status!='=>'1'))->get()->num_rows();
			$talks[$keys] = $rows;
		}
		
		$teams = $this->db->select('team_owner,teams.team_id,teams.team_name,teams.team_coverphoto,teams.briefing_validity,users.user_name, users.profile_picture as profile_pic,users.email,users.designation as position')->from('team_members')->join('teams', 'team_members.team_id = teams.team_id')->join('users', 'users.user_id = team_members.team_owner')->where(array('team_members.user_id'=>$user_id,'team_members.status!='=>'1'))->where_not_in('teams.team_id',$ids)->order_by('team_members.member_type','DESC' )->order_by('team_members.member_id', 'desc' )->get()->result_array();
		foreach( $teams as $key => $row )
		{
			$row['team_coverphoto'] !=''  ? $row['team_coverphoto']	= base_url().$row['team_coverphoto'] : '';
			$row['unread_count']	= '0';
			$row['receiver_id']	= '0';
			$row['profile_pic']!=''  ? $row['profile_pic']	= base_url(). $row['profile_pic'] : '';
			$row['total_members'] 	= (string)$this->db->select('member_id')->from('team_members')->where(array('team_id'=>$row['team_id'],'status!='=>'1'))->get()->num_rows();
			$teams[$key] = $row;
		}
		$data=array_merge($talks,$teams);
		$data	=	$this->Aes_encryption->aesEncryption($this->secret,json_encode($data));
		return array('status' => 200,'data'=>$data,'message' =>'Inbox.','method'=>'get_TtalkInbox');
	}
	## For Get All group where chat is initiated 
	/*
	public function ht_get_TtalkInbox($input)
	{	
		$user_id = $input['user_id'];
		
		$myteams = $this->db->select('DISTINCT(team_id) as team_id')->from('team_members')->WHERE('user_id='.$input['user_id'].' and status!=1')->get()->result_array();
		
		$teamids = array(NULL);
		if(count($myteams)){
			$teamids = array_column($myteams,'team_id');
		}
		## One To One
		$one = $this->db->select("DISTINCT(thread_id) AS thread_id")->from('ttalks')->where("(sender_id='".$input['user_id']."' OR receiver_id='".$input['user_id']."') AND team_id=0")->get()->result_array();
		$threads = array("NULL");
		if(count($one)){
			$threads = array_column($one,'thread_id');
		}
		$talks = $this->db->select("DISTINCT(thread_id) AS thread_id, team_id,(SELECT MAX(ttalkid) AS ttalkid FROM ttalks WHERE thread_id= a.thread_id) as  ttalkid, IFNULL((select team_name from teams where team_id=a.team_id),'') as team_name, IFNULL((select team_coverphoto from teams where team_id=a.team_id),'') as team_coverphoto, IFNULL((select user_id from teams where team_id=a.team_id),'') as team_owner, IFNULL((select briefing_validity from teams where team_id=a.team_id),'') as briefing_validity")->from('ttalks as a')->where_in('team_id',$teamids)->or_where_in('thread_id',$threads)->order_by('ttalkid','desc')->get()->result_array();
		$ids = array_column($talks,'team_id');
		
		foreach( $talks as $keys => $rows )
		{
			$rows['team_coverphoto']!=''  ? $rows['team_coverphoto']	= base_url().$rows['team_coverphoto'] : '';
			if($rows['team_id']){
				$other_id = $rows['team_owner'];
				$rows['unread_count']	=	messageUnreadcount($rows['thread_id'],$input['user_id'],$rows['team_id']);
			}else{
				$other_id	= getSenderOrReceiverId($rows['thread_id'],$input['user_id']);
				$rows['unread_count']	=	messageUnreadcount($rows['thread_id'],$input['user_id'],'o2o');
			}
			$profile = $this->db->select('user_name,profile_picture,email,designation')->from('users')->where(array('user_id'=>$other_id))->get()->row();
			//print $this->db->last_query(); EXIT;
			//print_r($profile); exit;
			$rows['receiver_id']	=	$other_id;
			$rows['user_name']		=	$profile->user_name;
			$rows['email']			=	$profile->email;
			$rows['profile_pic']	=	$profile->profile_picture !=''  ?  base_url().$profile->profile_picture : '';
			$rows['position'] 		=	getDesignationdata($profile->designation,'designation');
			$rows['total_members'] 	=	(string)$this->db->select('member_id')->from('team_members')->where(array('team_id'=>$rows['team_id'],'status!='=>'1'))->get()->num_rows();
			$talks[$keys] = $rows;
		}
		$data	=	$this->Aes_encryption->aesEncryption($this->secret,json_encode($talks));
		return array('status' => 200,'data'=>$data,'message' =>'Inbox.','method'=>'get_TtalkInbox');
	}
	 */ 
	 
	 
	 
	 
	/*
	* Send New Message
	*/
	public function ht_send_newMessage($input)
	{	
		if (check_valid_memeber($input['team_id'],$input['sender_id'])){
			$input['thread_id'] = getThreadId($input['team_id']);
			$input['isactive']	= '1';
			$input['filelink'] = '';
			$input['thumbnail']= '';
			$input['filesize'] = 0;
			$input['main_sender_id'] = $input['sender_id'];
			
			if($input['message_type']!=1){
				$date = date("Y-m");
				$config['allowed_types'] = '*';
				
				if (!empty($_FILES['filelink'])){
					if($input['message_type']==2){
						$folder = 'images';						
					}elseif($input['message_type']==3){
						$folder = 'videos';
					}else{
						$folder = 'documents';
					}
					$input['filesize'] = $_FILES['filelink']['size'];
					$path	=  './media/ttalk/'.$folder.'/'.$date."/";
					if (!is_dir($path)){
						mkdir($path , 0777);
						$content = "<!DOCTYPE html><html><head><title>403 Forbidden</title></head><body><p>Access denied!</p></body></html>"; 
						$fp = fopen($path."index.html","wb"); 
						fwrite($fp,$content); 
						fclose($fp); 
					}
					$config['file_name']	= date("Ymd")."_TTK-".rand(100000,999999).'-'.time();
					$config['upload_path']	= $path;
					$this->upload->initialize($config);
					if (!$this->upload->do_upload('filelink')){
						return array('status' =>400,'message' =>"File uploading error: ".strip_tags($this->upload->display_errors()),'method'=>$this->uri->segment(3));
						exit;
					}else{
						$input['filelink'] = ltrim($path,"./").$this->upload->data()['file_name'];
						if (!empty($_FILES['thumbnail'])){
							$config['file_name'] = $config['file_name']."_thumb";
							$config['upload_path'] = $path;
							$this->upload->initialize($config);
							if ($this->upload->do_upload('thumbnail')){
								$input['thumbnail'] = ltrim($path,"./").$this->upload->data()['file_name'];
							}
						}
					}
				}else{
					return array('status' =>400,'message' =>'Select a file to upload!','method'=>$this->uri->segment(3));
					exit;
				}
			}
			$input['sent_date'] = today()[0];
			$this->db->trans_start();
			$this->db->insert('ttalks',$input);
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return array('status' => 500,'message' =>'Internal server error.','method'=>'send_newMessage');
			} else {
				$ttalkid = $this->db->insert_id();
				$this->db->set('unread_briefcount','unread_briefcount+1',false)->where('team_id='.$input['team_id'].' and user_id!='.$input['sender_id'].' and status=0')->update('team_members');
				$this->db->trans_commit();
				//print $this->db->last_query(); EXIT;
				return array('status' => 200,'thread_id'=>$input['thread_id'],'ttalkid'=>$ttalkid,'message' =>'Message sent.','method'=>'send_newMessage');
			}
		}else{
			return array('status' =>400,'message' =>'Access denied!','method'=>'send_newMessage');
		}
	}
	/*
	* Send New Message
	*/
	public function ht_send_newMessageOnetoOne($input)
	{	
		$receivers = explode(',',$input['receiver_id']);
		unset($input['receiver_id']);
		$message =  array('status' =>400,'message' =>'No action performed!','method'=>'send_newMessageOnetoOne');
		for($i=0;$i<count($receivers);$i++){
			$input['receiver_id'] = $receivers[$i];
			$input['thread_id'] = getThreadId($input['sender_id'],$input['receiver_id']);
			$input['isactive']	= '1';
			$input['privacy']	= '1';
			$input['filelink'] = '';
			$input['thumbnail']= '';
			$input['filesize'] = 0;
			$input['main_sender_id'] = $input['sender_id'];
			if($input['message_type']!=1){
				$date = date("Y-m");
				$config['allowed_types'] = '*';
				if (!empty($_FILES['filelink'])){
					if($input['message_type']==2){
						$folder = 'images';						
					}elseif($input['message_type']==3){
						$folder = 'videos';
					}else{
						$folder = 'documents';
					}
					$path	=  './media/ttalk/'.$folder.'/'.$date."/";
					if (!is_dir($path)){
						mkdir($path , 0777);
						$content = "<!DOCTYPE html><html><head><title>403 Forbidden</title></head><body><p>Access denied!</p></body></html>"; 
						$fp = fopen($path."index.html","wb"); 
						fwrite($fp,$content); 
						fclose($fp); 
					}
					$input['filesize'] = $_FILES['filelink']['size'];
					$config['file_name']	= date("Ymd")."_oto-".rand(100000,999999).'-'.time();
					$config['upload_path']	= $path;
					$this->upload->initialize($config);
					if (!$this->upload->do_upload('filelink')){
						return array('status' =>400,'message' =>"File uploading error: ".strip_tags($this->upload->display_errors()),'method'=>$this->uri->segment(3));
						exit;
					}else{
						$input['filelink'] = ltrim($path,"./").$this->upload->data()['file_name'];
						if (!empty($_FILES['thumbnail'])){
							$config['file_name'] = $config['file_name']."_thumb";
							$config['upload_path'] = $path;
							$this->upload->initialize($config);
							if ($this->upload->do_upload('thumbnail')){
								$input['thumbnail'] = ltrim($path,"./").$this->upload->data()['file_name'];
							}
						}
					}
				}else{
					$message = array('status' =>400,'message' =>'Select a file to upload!','method'=>$this->uri->segment(3));
					break;
				}
			}
			$input['sent_date'] = today()[0];
			$this->db->trans_start();
			$this->db->insert('ttalks',$input);
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				$message =  array('status' => 500,'message' =>'Internal server error.','method'=>'send_newMessageOnetoOne');
				break;
			} else {
				$ttalkid = $this->db->insert_id();
				$this->db->trans_commit();
				$message =  array('status' => 200,'thread_id'=>$input['thread_id'],'ttalkid'=>$ttalkid,'message' =>'Message sent.','method'=>'send_newMessageOnetoOne');
			}
		}
		return $message;
	}
	/*
	* Forword messages
	*/
	public function ht_forward_Messages($input)
	{	
		$receivers		= explode(',',$input['receiver_id']);
		$ttalkids		= explode(',',$input['ttalkid']);
		$forward_type	= $input['forward_type'];
		unset($input['receiver_id']);
		unset($input['ttalkid']);
		unset($input['forward_type']);
		$message =  array('status' =>400,'message' =>'No action performed!','method'=>'forward_Messages');
		for($i=0;$i<count($receivers);$i++){
			if($forward_type == 1){
				$input['team_id'] 	= $receivers[$i];
				$input['thread_id'] = getThreadId($input['team_id']);
			}else{
				$input['receiver_id'] = $receivers[$i];
				$input['thread_id'] = getThreadId($input['sender_id'],$input['receiver_id']);
			}
			$input['privacy']	= '1';
			$input['filelink']	= '';
			$input['thumbnail']	= '';
			$input['filesize'] 	= 0;
			$input['isactive']	= '1';
			$input['re_breaf']	= '1';
			$input['sent_date'] = today()[0];
			for($t=0;$t<count($ttalkids);$t++){
				$ttalkid = $ttalkids[$t];
				$check = $this->db->select("message_type, message, filelink, thumbnail, filesize,main_sender_id")->from('ttalks')->where(array('ttalkid'=>$ttalkid))->get();
				
				if($check->num_rows()){
					$row = $check->row_array();
					$input['message_type']	= $row['message_type'];
					$input['filelink'] 		= $row['filelink'];
					$uname = date("Ymd")."_frwd-".rand(100000,999999).'-'.time();
					
					if($row['filelink']){
						$file = explode('.',$row['filelink']);
						$newname = substr($file[0], 0, strrpos($file[0], '/')).'/'.$uname.'.'.$file[1];
						$input['filelink']	= copy(('./'.$row['filelink']), ('./'.$newname)) ? $newname : $row['filelink'];
					}
					$input['thumbnail']		= $row['thumbnail'];
					if($input['thumbnail']){
						$file = explode('.',$row['thumbnail']);
						$newname = substr($file[0], 0, strrpos($file[0], '/')).'/'.$uname.'_thumb.'.$file[1];
						$input['thumbnail']		= copy(('./'.$row['thumbnail']), ('./'.$newname)) ? $newname : $row['thumbnail'];
					}
					
					$input['filesize']	= $row['filesize'];
					$input['message']	= $row['message'];
					$input['main_sender_id']	= $row['main_sender_id'];
					$this->db->trans_start();
					$this->db->insert('ttalks',$input);
					if ($this->db->trans_status() === FALSE){
						$this->db->trans_rollback();
						$message =  array('status' => 500,'message' =>'Internal server error.','method'=>'forward_Messages');
						break;
					} else {
						$ttalkid = $this->db->insert_id();
						$this->db->trans_commit();
						$message =  array('status' => 200,'message' =>'Message forwarded.','method'=>'forward_Messages');
					}
				}
			}
		}	
		return $message;
	}
	
	
	/*
	* Get Ttalk Messages
	*/
	public function ht_get_tTalkMessage($input)
	{	
		$page 		= $input['page'];
		$limit		= 50;
		$start		= ($page - 1)* $limit; 
		if (check_valid_memeber($input['team_id'],$input['sender_id'])){
			
			$briefing_validity 	=  getTeamdata($input['team_id'],'briefing_validity');
			if($briefing_validity){ 
				$expired 			=  date('Y-m-d H:i:s', strtotime('-'.$briefing_validity.' day', today()[1]));
				
				$this->db->where(array('team_id'=>$input['team_id'],'sent_date<'=>$expired))->update('ttalks',array('isactive'=>2));
			}
			$totalrows 	= $this->db->select('ttalkid')->from('ttalks')->WHERE(array('team_id'=>$input['team_id'],'isactive'=>'1'))->get()->num_rows();
			//print $this->db->last_query(); EXIT;
			$totalpages = ceil($totalrows/$limit);
			$previous	=  $totalpages > 1 ? "1" : "0"; 
			$next		=  $page < $totalpages ? "1" : "0";
			
			$data = $this->db->select('ttalks.ttalkid, ttalks.thread_id, ttalks.team_id, ttalks.sender_id, ttalks.privacy, ttalks.private_receiverid, ttalks.message_type, ttalks.message, ttalks.filelink, ttalks.thumbnail, ttalks.filesize, ttalks.re_breaf, ttalks.main_sender_id, ttalks.sent_date, teams.team_name, teams.team_coverphoto, users.user_name, users.email, users.designation as position,users.profile_picture as profile_pic')->from('ttalks')->where(array('ttalks.team_id'=>$input['team_id'],'ttalks.isactive'=>'1'))->join('teams', 'ttalks.team_id = teams.team_id')->join('users', 'users.user_id = ttalks.sender_id')->order_by('ttalks.ttalkid','DESC')->limit($limit,$start)->get()->result_array();
			$data = array_reverse($data);
			foreach( $data as $key => $row )
			{
				$row['filelink'] !=''  ? $row['filelink']	= base_url().$row['filelink'] : '';
				$row['thumbnail'] !=''  ? $row['thumbnail']	= base_url().$row['thumbnail'] : '';
				$row['team_coverphoto'] !=''  ? $row['team_coverphoto']	= base_url().$row['team_coverphoto'] : '';
				$row['profile_pic']!=''  ? $row['profile_pic']	= base_url(). $row['profile_pic']: '';
				$row['position'] 	=  getDesignationdata($row['position'],'designation');
				$row['sent_date'] = convterUTCtoLocal($row['sent_date'],$input['timezone']);
				$row['main_sender_name'] =  getusersdata($row['main_sender_id'],'user_name');;
				$ids = explode(',',$row['private_receiverid']);
				$recipent = $this->db->select('user_id, user_name')->from('users')->where_in('user_id',$ids)->get()->result_array();
				$row['private_recipient'] = "";
				if(count($recipent)){
					$row['private_recipient'] = implode(', ',array_column($recipent,'user_name'));
				}
				$data[$key] = $row;
			}
			$this->db->where('team_id='.$input['team_id'].' and user_id='.$input['sender_id'])->update('team_members',array('unread_briefcount'=>'0'));
			$data	=	$this->Aes_encryption->aesEncryption($this->secret,json_encode($data));
			return array('status' => 200,'data'=>$data,'message' =>'Messages.','method'=>'get_tTalkMessage','totalpage'=>(string)$totalpages,'previous'=>$previous,'next'=>$next);
		
		}else{
			return array('status' => 400,'message'=>'Access denied!','method'=>'get_tTalkMessage');
		}
	}
	/*
	* Get One to one Messages
	*/
	public function ht_get_oneToOneChat($input)
	{	
		$thread_id  = getThreadId($input['user_id'],$input['receiver_id'],'validate');
		if(	$thread_id){
			
			$page 		= $input['page'];
			$limit		= 50;
			$start		= ($page - 1)* $limit; 
			$totalrows 	= $this->db->select('ttalkid')->from('ttalks')->WHERE('thread_id='.$thread_id.' and ((sender_id='.$input['user_id'].' and sender_trash=1) or (receiver_id='.$input['user_id'].' and receiver_trash=1))')->get()->num_rows();
			$totalpages = ceil($totalrows/$limit);
			$previous	=  $totalpages > 1 ? "1" : "0"; 
			$next		=  $page < $totalpages ? "1" : "0";
			
			$data = $this->db->select('ttalkid, thread_id, team_id, sender_id, receiver_id,message_type, message, thumbnail, filesize, sent_date')->from('ttalks')->WHERE('thread_id='.$thread_id.' and ((sender_id='.$input['user_id'].' and sender_trash=1) or (receiver_id='.$input['user_id'].' and receiver_trash=1))')->order_by('ttalkid','DESC')->limit($limit,$start)->get()->result_array();
			//print $this->db->last_query(); EXIT;
			
			$data = array_reverse($data);
			foreach( $data as $key => $row )
			{
				$row['filelink'] !=''  ? $row['filelink']	= base_url().$row['filelink'] : '';
				$row['thumbnail'] !=''  ? $row['thumbnail']	= base_url().$row['thumbnail'] : '';
				$row['sent_date'] = convterUTCtoLocal($row['sent_date'],$input['timezone']);
				$data[$key] = $row;
			}
			$designationid 	=  getusersdata($input['receiver_id'],'designation');
			$profile_picture=  getusersdata($input['receiver_id'],'profile_picture');
			$profile_picture!=''  ? $profile_picture	= base_url(). $profile_picture: '';
			$designation 	=  getDesignationdata($designationid,'designation');
			
			$receiver_data = array('user_name'=>getusersdata($input['receiver_id'],'user_name'),'email'=>getusersdata($input['receiver_id'],'email'),'designation'=>$designation,'profile_picture'=>$profile_picture);
			
			$designationid 	=  getusersdata($input['user_id'],'designation');
			$profile_picture=  getusersdata($input['user_id'],'profile_picture');
			$profile_picture!=''  ? $profile_picture	= base_url(). $profile_picture: '';
			$designation 	=  getDesignationdata($designationid,'designation');
			
			$sender_data = array('user_name'=>getusersdata($input['user_id'],'user_name'),'email'=>getusersdata($input['user_id'],'email'),'designation'=>$designation,'profile_picture'=>$profile_picture);
			
			$this->db->where(array('thread_id'=>$thread_id,'receiver_id'=>$input['user_id']))->update('ttalks',array('is_read'=>1));
			$sender_data	=	$this->Aes_encryption->aesEncryption($this->secret,json_encode($sender_data));
			$receiver_data	=	$this->Aes_encryption->aesEncryption($this->secret,json_encode($receiver_data));
			
			$data			=	$this->Aes_encryption->aesEncryption($this->secret,json_encode($data));
			return array('status' => 200,'sender_data'=>$sender_data,'receiver_data'=>$receiver_data,'data'=>$data,'message' =>'Messages.','method'=>'get_oneToOneChat','totalpage'=>(string)$totalpages,'previous'=>$previous,'next'=>$next);
		}else{
			return array('status' => 400,'message'=>'Access denied!','method'=>'get_tTalkMessage');
		}
	}
	/*
	* Refresh chat
	*/
	public function ht_refreshChat($input)
	{	
		if ($input['section']==1){ // One to One Section
			$data = $this->db->select('ttalkid, thread_id, team_id, sender_id, receiver_id,message_type, private_receiverid, message, filelink, thumbnail, filesize, re_breaf,sent_date')->from('ttalks')->WHERE('thread_id='.$input['thread_id'].' and isactive=1 and ttalkid >'.$input['ttalkid'])->order_by('ttalkid','DESC')->get()->result_array();
			$data = array_reverse($data);
			//print $this->db->last_query(); EXIT;
			foreach( $data as $key => $row )
			{
				$row['filelink'] !=''  ? $row['filelink']	= base_url().$row['filelink'] : '';
				$row['thumbnail'] !=''  ? $row['thumbnail']	= base_url().$row['thumbnail'] : '';
				$row['profile_pic']!=''  ? $row['profile_pic']	= base_url(). $row['profile_pic']: '';
				$row['position'] 	=  getDesignationdata($row['position'],'designation');
				$row['sent_date'] = convterUTCtoLocal($row['sent_date'],$input['timezone']);
				$data[$key] = $row;
			}
			
			$this->db->where(array('thread_id'=>$thread_id,'receiver_id'=>$input['user_id']))->update('ttalks',array('is_read'=>1));
			$data	=	$this->Aes_encryption->aesEncryption($this->secret,json_encode($data));
			return array('status' => 200,'data'=>$data,'message' =>'Messages.','method'=>'refreshChat');
		
		}else{ // Brief Section
			$data = $this->db->select('ttalks.ttalkid, ttalks.thread_id, ttalks.team_id, ttalks.sender_id, ttalks.privacy, ttalks.private_receiverid, ttalks.message_type, ttalks.message, ttalks.filelink, ttalks.filelink, ttalks.thumbnail, ttalks.re_breaf, ttalks.sent_date, teams.team_name, teams.team_coverphoto, users.user_name, users.email, users.designation as position,users.profile_picture as profile_pic')->from('ttalks')->where(array('ttalks.thread_id'=>$input['thread_id'],'ttalks.ttalkid > '=>$input['ttalkid'],'ttalks.isactive'=>'1'))->join('teams', 'ttalks.team_id = teams.team_id')->join('users', 'users.user_id = ttalks.sender_id')->order_by('ttalks.ttalkid','DESC')->get()->result_array();
			$data = array_reverse($data);
			foreach( $data as $key => $row )
			{
				$row['filelink'] !=''  ? $row['filelink']	= base_url().$row['filelink'] : '';
				$row['thumbnail'] !=''  ? $row['thumbnail']	= base_url().$row['thumbnail'] : '';
				$row['team_coverphoto'] !=''  ? $row['team_coverphoto']	= base_url().$row['team_coverphoto'] : '';
				$row['profile_pic']!=''  ? $row['profile_pic']	= base_url(). $row['profile_pic']: '';
				$row['position'] 	=  getDesignationdata($row['position'],'designation');
				$row['sent_date'] = convterUTCtoLocal($row['sent_date'],$input['timezone']);
				
				$ids = explode(',',$row['private_receiverid']);
				$recipent = $this->db->select('user_id, user_name')->from('users')->where_in('user_id',$ids)->get()->result_array();
				$row['private_recipient'] = "";
				if(count($recipent)){
					$row['private_recipient'] = implode(', ',array_column($recipent,'user_name'));
				}
				
				$data[$key] = $row;
			}
			$data	=	$this->Aes_encryption->aesEncryption($this->secret,json_encode($data));
			return array('status' => 200,'data'=>$data,'message' =>'Messages.','method'=>'refreshChat',);
		}
	}
	/*
	* Delete Message
	*/
	public function ht_delete_Message($input)
	{	
		$ttalkids = explode(',',$input['ttalkid']);
		$message =  array('status' => 400,'message'=>'No action selected!','method'=>'delete_Message');	
		for($i=0;$i<count($ttalkids);$i++){
			$id = $ttalkids[$i];
			$check = $this->db->select('ttalkid')->from('ttalks')->WHERE(array('sender_id'=>$input['user_id'],'ttalkid'=>$id))->get()->num_rows();
			if($check){
				$this->db->where(array('ttalkid'=>$id))->update('ttalks',array('isactive'=>2,'sender_trash'=>0,'receiver_id'=>0));
				$message = array('status' => 200,'message' =>'Message deleted','method'=>'delete_Message');
			}else{
				$message =  array('status' => 400,'message'=>'Access denied!','method'=>'delete_Message');	
				break;
			}
		}
		return $message;
	}
	/*
	* Print Protocol
	*/
	## Section = 1 for Brief, 2 for one to one
	
	public function ht_print_Protocol($input)
	{	
		$thread_id  = $input['thread_id'];
		if($thread_id){
			if ($input['section']==1){
				$data = $this->db->select('ttalks.ttalkid, ttalks.thread_id, ttalks.team_id, ttalks.sender_id, ttalks.privacy, ttalks.private_receiverid, ttalks.message_type, ttalks.message, ttalks.filelink, ttalks.sent_date, teams.team_name, teams.team_coverphoto, users.user_name, users.email, users.designation as position,users.profile_picture as profile_pic')->from('ttalks')->where(array('ttalks.thread_id'=>$thread_id,'ttalks.isactive'=>'1'))->join('teams', 'ttalks.team_id = teams.team_id')->join('users', 'users.user_id = ttalks.sender_id')->order_by('ttalks.ttalkid','DESC')->get()->result_array();
				
				$attachment .="<html><Body><table style='width:100%;text-align:left;'><caption>TTalk ".@$data[0]['team_name']." Team</caption><tr><th colspan='3'><hr /><th></tr><tr><th style='width:35%;text-align:left;'>Sender</th><th style='width:50%;text-align:left;'>Messages</th><th style='width:15%;text-align:left;'>Date / Time</th></tr>";
				foreach( $data as $key => $row )
				{
					$attachment .= '<tr>';
					//$row['filelink'] !=''  ? $row['filelink']	= "<a href='".base_url().$row['filelink']."' >".end(explode("/",$row['filelink']))."</a>" : '';
					$row['filelink'] !=''  ? $row['filelink']	= end(explode("/",$row['filelink'])) : '';
					$row['sent_date'] = convterUTCtoLocal($row['sent_date'],$input['timezone']);
					if ($row['sender_id'] == $input['user_id']){
						$sender  = "You";
					}else{
						$sender = $row['user_name']." : ".$row['email'];
					}
					if ($row['filelink']){
						$row['message'] = $row['message']." File: ".$row['filelink'];
					}
					$attachment .= "<td>".$sender."</td><td>".$row['message']."</td><td>".$row['sent_date']."</td></tr>";
					
				}
				$attachment .= "</table></body></html>";
			}else{
				$data = $this->db->select('ttalkid, thread_id, team_id, sender_id, receiver_id,message_type, message, filelink, sent_date')->from('ttalks')->WHERE('thread_id='.$thread_id.' and ((sender_id='.$input['user_id'].' and sender_trash=1) or (receiver_id='.$input['user_id'].' and receiver_trash=1))')->order_by('ttalkid','DESC')->get()->result_array();
				$attachment .="<html><Body><table style='width:100%;text-align:left;'><caption>Chat with ".getusersdata($input['receiver_id'],'user_name')."</caption><tr><th colspan='3'><hr /><th></tr><tr><th style='width:25%;text-align:left;'>Sender</th><th style='width:50%;text-align:left;'>Messages</th><th style='width:25%;text-align:left;'>Date / Time</th></tr>";
				foreach( $data as $key => $row )
				{
					$attachment .= '<tr>';
					//$row['filelink'] !=''  ? $row['filelink']	= "<a href='".base_url().$row['filelink']."' >".end(explode("/",$row['filelink']))."</a>" : '';
					$row['filelink'] !=''  ? $row['filelink']	= end(explode("/",$row['filelink'])) : '';
					$row['sent_date'] = convterUTCtoLocal($row['sent_date'],$input['timezone']);
					if ($row['sender_id'] == $input['user_id']){
						$sender  = "You";
					}else{
						$sender = getusersdata($input['receiver_id'],'user_name')." : ".getusersdata($input['receiver_id'],'email');
					}
					if ($row['filelink']){
						$row['message'] = $row['message']." File: ".$row['filelink'];
					}
					$attachment .= "<td>".$sender."</td><td>".$row['message']."</td><td>".$row['sent_date']."</td></tr>";
					
				}
				$attachment .= "</table></body></html>";
			}
			if (count($data)){
				$this->load->library('pdf');
				$pdf = new DOMPDF();;
				$pdf->load_html($attachment, 'UTF-8');
				// Render the HTML as PDF
				$pdf->render();
				$pdf = $pdf->output();
				$this->load->library('email');
				$this->email->initialize($this->config->item("email_config"));
				$from = $this->config->item("email_from");
				$this->email->from('mailer@inr-circle.com', 'INR Mailer');
				$this->email->to(getusersdata($input['user_id'],'email'));
				$this->email->subject('INR-Circle TTalk/Chat history');
				$this->email->message('Please find the attachment for chat history.');
				$this->email->attach($pdf, 'application/pdf', "File".time().$input['user_id']. ".pdf", false);

				$r = $this->email->send();
				if (!$r) {
					return array('status' => 400,'message'=>strip_tags($this->email->print_debugger(array("header"))),'method'=>'print_Protocol');
				} else {
					return array('status' => 200,'message' =>'Please check your mail box for chat history.','method'=>'print_Protocol');
				}
			}else{
				return array('status' => 400,'message'=>'No data found','method'=>'print_Protocol');
			}
		}else{
			return array('status' => 400,'message'=>'Access denied!','method'=>'print_Protocol');
		}
	}
	/*
	* Print Protocol Ttalk
	*/
	## Section = 1 for Brief, 2 for one to one
	
	public function ht_print_ProtocolTtalk($input)
	{	
		$thread_id  = $input['thread_id'];
		$attachment = "";
		if($thread_id){
			if ($input['section']==1){
				if(!empty($input['start']) && !empty($input['end'])){
					$data = $this->db->select('ttalks.ttalkid, ttalks.thread_id, ttalks.team_id, ttalks.sender_id, ttalks.privacy, ttalks.private_receiverid, ttalks.message_type, ttalks.message, ttalks.thumbnail,ttalks.filelink, ttalks.sent_date, teams.team_name, teams.team_coverphoto, users.user_name, users.email, users.designation as position,users.profile_picture as profile_pic')->from('ttalks')->where(array('ttalks.thread_id'=>$thread_id,'ttalks.isactive'=>'1'))->where(array('ttalks.sent_date>='=>$input['start'].' 00:00:00'))->where(array('ttalks.sent_date<='=>$input['end'].' 23:59:59'))->join('teams', 'ttalks.team_id = teams.team_id')->join('users', 'users.user_id = ttalks.sender_id')->order_by('ttalks.ttalkid','DESC')->get()->result_array();
					
				}else{
					$data = $this->db->select('ttalks.ttalkid, ttalks.thread_id, ttalks.team_id, ttalks.sender_id, ttalks.privacy, ttalks.private_receiverid, ttalks.message_type, ttalks.message, ttalks.filelink, ttalks.thumbnail, ttalks.sent_date, teams.team_name, teams.team_coverphoto, users.user_name, users.email, users.designation as position,users.profile_picture as profile_pic')->from('ttalks')->where(array('ttalks.thread_id'=>$thread_id,'ttalks.isactive'=>'1'))->join('teams', 'ttalks.team_id = teams.team_id')->join('users', 'users.user_id = ttalks.sender_id')->order_by('ttalks.ttalkid','DESC')->get()->result_array();
				}
				$attachment .="<html><Body><table style='width:100%;text-align:left;'><caption>TTalk ".@$data[0]['team_name']." Team</caption><tr><th colspan='3'><hr /><th></tr><tr><th style='width:35%;text-align:left;'>Sender</th><th style='width:50%;text-align:left;'>Messages</th><th style='width:15%;text-align:left;'>Date / Time</th></tr>";
				$i = 0;
				foreach( $data as $key => $row )
				{
					if ($i%2 == 0) 
						$attachment .= '<tr style="background-color:#d6d6d6;">';
					else 
						$attachment .= '<tr style="background-color:#f6f6f6;">';
			
					//$row['filelink'] !=''  ? $row['filelink']	= "<a href='".base_url().$row['filelink']."' >".end(explode("/",$row['filelink']))."</a>" : '';
					if($row['thumbnail']){
						$image = "<img src=./".$row['thumbnail']." height='80px'>";
					}elseif($row['filelink'] && $row['message_type']==2){
						$image = "<img src=./".$row['filelink']." height='80px'>";
					}else{
						$image = "";
					}
					$row['filelink'] !=''  ? $row['filelink']	= end(explode("/",$row['filelink'])) : '';
					$row['sent_date'] = convterUTCtoLocal($row['sent_date'],$input['timezone'],1);
					if ($row['sender_id'] == $input['user_id']){
						$sender  = "You";
					}else{
						$sender = $row['user_name']." : ".$row['email'];
					}
					if ($row['filelink']){
						$row['message'] = $row['message']." <br />".$image."<br />".$row['filelink'];
					}
					$attachment .= "<td>".$sender."</td><td>".$row['message']."</td><td>".$row['sent_date']."</td></tr>";
					$i++;
				}
				$attachment .= "</table></body></html>";
			}else{
				if(!empty($input['start']) && !empty($input['end'])){
					$data = $this->db->select('ttalkid, thread_id, team_id, sender_id, receiver_id,message_type, message, filelink, sent_date')->from('ttalks')->WHERE('thread_id='.$thread_id.' and ((sender_id='.$input['user_id'].' and sender_trash=1) or (receiver_id='.$input['user_id'].' and receiver_trash=1))')->where(array('ttalks.sent_date>='=>$input['start'].' 00:00:00'))->where(array('ttalks.sent_date<='=>$input['end'].' 23:59:59'))->order_by('ttalkid','DESC')->get()->result_array();
					
				}else{
					$data = $this->db->select('ttalkid, thread_id, team_id, sender_id, receiver_id,message_type, message, filelink, sent_date')->from('ttalks')->WHERE('thread_id='.$thread_id.' and ((sender_id='.$input['user_id'].' and sender_trash=1) or (receiver_id='.$input['user_id'].' and receiver_trash=1))')->order_by('ttalkid','DESC')->get()->result_array();
				}
				$attachment .="<html><Body><table style='width:100%;text-align:left;'><caption>Chat with ".getusersdata($input['receiver_id'],'user_name')."</caption><tr><th colspan='3'><hr /><th></tr><tr><th style='width:25%;text-align:left;'>Sender</th><th style='width:50%;text-align:left;'>Messages</th><th style='width:25%;text-align:left;'>Date / Time</th></tr>";
				$i=0;
				foreach( $data as $key => $row )
				{
					if ($i%2 == 0) 
						$attachment .= '<tr style="background-color:#d6d6d6;">';
					else 
						$attachment .= '<tr style="background-color:#f6f6f6;">';
			
					$attachment .= '<tr>';
					//$row['filelink'] !=''  ? $row['filelink']	= "<a href='".base_url().$row['filelink']."' >".end(explode("/",$row['filelink']))."</a>" : '';
					if($row['thumbnail']){
						$image = "<img src=./".$row['thumbnail']." height='80px'>";
					}elseif($row['filelink']){
						$image = "<img src=./".$row['filelink']." height='80px'>";
					}else{
						$image = "";
					}
					$row['filelink'] = end(explode("/",$row['filelink']));
					$row['sent_date'] = convterUTCtoLocal($row['sent_date'],$input['timezone'],1);
					if ($row['sender_id'] == $input['user_id']){
						$sender  = "You";
					}else{
						$sender = getusersdata($input['receiver_id'],'user_name')." : ".getusersdata($input['receiver_id'],'email');
					}
					if ($row['filelink']){
						$row['message'] = $row['message']." <br />".$image."<br />".$row['filelink'];
					}
					$attachment .= "<td>".$sender."</td><td>".$row['message']."</td><td>".$row['sent_date']."</td></tr>";
					$i++;
				}
				$attachment .= "</table></body></html>";
			}
			if (count($data)){
				$this->load->library('pdf');
				$pdf = new DOMPDF();;
				$pdf->load_html($attachment, 'UTF-8');
				// Render the HTML as PDF
				$pdf->render();
				$pdf = $pdf->output();
				$this->load->library('email');
				$this->email->initialize($this->config->item("email_config"));
				$from = $this->config->item("email_from");
				$this->email->from('mailer@inr-circle.com', 'INR Mailer');
				$this->email->to(getusersdata($input['user_id'],'email'));
				$this->email->subject('INR-Circle TTalk/Chat history');
				$this->email->message('Please find the attachment for chat history.');
				$this->email->attach($pdf, 'application/pdf', "File".time().$input['user_id']. ".pdf", false);

				$r = $this->email->send();
				if (!$r) {
					return array('status' => 400,'message'=>strip_tags($this->email->print_debugger(array("header"))),'method'=>'print_ProtocolTtalk');
				} else {
					return array('status' => 200,'message' =>'Please check your mail box for chat history. Please allow a minute for its preparation and check your spam folder in case it was not received in your inbox. ','method'=>'print_ProtocolTtalk');
				}
			}else{
				return array('status' => 400,'message'=>'No data found','method'=>'print_ProtocolTtalk');
			}
		}else{
			return array('status' => 400,'message'=>'Access denied!','method'=>'print_ProtocolTtalk');
		}
	}
	/*
	* Print Protocol Todo action list
	*/
	public function ht_print_ProtocolTodo($input)
	{	
		$team_id 		 = $input['team_id'];
		$accountability  = $input['accountability'];
		$attachment = "";
		if($team_id || $accountability){
			if ($accountability){ ## Particular Users Action list
			
				if(!empty($input['start']) && !empty($input['end'])){
					$data = $this->db->select('todolist.todoid, todolist.team_id, todolist.accountability, todolist.action_name, todolist.action_description, todolist.deadline, todolist.attachment, todolist.link, todolist.status, todolist.is_active, teams.team_name, teams.team_coverphoto, teams.user_id as team_owner, users.user_name, users.email, users.designation as position,users.profile_picture as profile_pic')->from('todolist')->where(array('accountability'=>$accountability,'todolist.is_active!='=>'1','todolist.status'=>'0'))->where(array('deadline>='=>$input['start']))->where(array('deadline<='=>$input['end']))->join('teams', 'todolist.team_id = teams.team_id')->join('users', 'users.user_id = todolist.accountability')->order_by('todolist.status','ASC')->order_by('todolist.deadline','ASC')->get()->result_array();
				}else{
					$data = $this->db->select('todolist.todoid, todolist.team_id, todolist.accountability, todolist.action_name, todolist.action_description, todolist.deadline, todolist.attachment, todolist.link, todolist.status, todolist.is_active, teams.team_name, teams.team_coverphoto, teams.user_id as team_owner, users.user_name, users.email, users.designation as position,users.profile_picture as profile_pic')->from('todolist')->where(array('accountability'=>$accountability,'todolist.is_active!='=>'1','todolist.status'=>'0'))->join('teams', 'todolist.team_id = teams.team_id')->join('users', 'users.user_id = todolist.accountability')->order_by('todolist.status','ASC')->order_by('todolist.deadline','ASC')->get()->result_array();
				}
				$attachment .="<html><Body><table style='width:100%;text-align:left;'><caption>Action List</caption><tr><th colspan='6'><hr /><th></tr>
				<tr>
					<th style='width:15%;text-align:left;'>Team</th>
					<th style='width:15%;text-align:left;'>Accountable</th>
					<th style='width:20%;text-align:left;'>Action</th>
					<th style='width:35%;text-align:left;'>Description</th>
					<th style='width:10%;text-align:left;'>Deadline</th>
					<th style='width:5%;text-align:left;'>Status</th>
				</tr>";
				$link = ""; 
				$file = "";
				$i=0;
				foreach( $data as $key => $row )
				{
					if ($i%2 == 0) 
						$attachment .= '<tr style="background-color:#d6d6d6;">';
					else 
						$attachment .= '<tr style="background-color:#f6f6f6;">';
				
					if($row['link']){
						$link = "<br/>Link: ".$row['link'];
					}
					if($row['attachment']){
						$file = "<br />Attachment: ".end(explode("/",$row['attachment']));
					}
					if($row['status']){
						$status = "Done";
					}else{
						$status = "Working";
					}
					$attachment .= "<td>".$row['team_name']."</td><td>".$row['user_name']."<br/>".$row['email']."</td><td>".$row['action_name'].$link.$file."</td><td>".$row['action_description']."</td><td>".$row['deadline']."</td><td>".$status."</td></tr>";
					$i++;
				}
				$attachment .= "</table></body></html>";
			}else{ ## Whole Team Action List
				if(!empty($input['start']) && !empty($input['end'])){
					$data = $this->db->select('todolist.todoid, todolist.team_id, todolist.accountability, todolist.action_name, todolist.action_description, todolist.deadline, todolist.attachment, todolist.link, todolist.status,todolist.is_active, teams.team_name, teams.team_coverphoto, teams.user_id as team_owner, users.user_name, users.email, users.designation as position,users.profile_picture as profile_pic')->from('todolist')->where(array('todolist.team_id'=>$team_id))->where(array('deadline>='=>$input['start']))->where(array('deadline<='=>$input['end']))->join('teams', 'todolist.team_id = teams.team_id')->join('users', 'users.user_id = todolist.accountability')->order_by('todolist.status','ASC')->order_by('todolist.deadline','ASC')->get()->result_array();
				}else{
					$data = $this->db->select('todolist.todoid, todolist.team_id, todolist.accountability, todolist.action_name, todolist.action_description, todolist.deadline, todolist.attachment, todolist.link, todolist.status,todolist.is_active, teams.team_name, teams.team_coverphoto, teams.user_id as team_owner, users.user_name, users.email, users.designation as position,users.profile_picture as profile_pic')->from('todolist')->where(array('todolist.team_id'=>$team_id))->join('teams', 'todolist.team_id = teams.team_id')->join('users', 'users.user_id = todolist.accountability')->order_by('todolist.status','ASC')->order_by('todolist.deadline','ASC')->get()->result_array();
				}
				
				$attachment .="<html><Body><table style='width:100%;text-align:left;'><caption>Action List of ".getTeamdata($team_id,'team_name')." Team</caption><tr><th colspan='6'><hr /><th></tr>
				<tr>
					<th style='width:15%;text-align:left;'>Team</th>
					<th style='width:15%;text-align:left;'>Accountable</th>
					<th style='width:20%;text-align:left;'>Action</th>
					<th style='width:35%;text-align:left;'>Description</th>
					<th style='width:10%;text-align:left;'>Deadline</th>
					<th style='width:5%;text-align:left;'>Status</th>
				</tr>";
				$link = ""; 
				$file = "";
				$i=0;
				foreach( $data as $key => $row )
				{
					if ($i%2 == 0) 
						$attachment .= '<tr style="background-color:#d6d6d6;">';
					else 
						$attachment .= '<tr style="background-color:#f6f6f6;">';
					
					if($row['link']){
						$link = "<br/>Link: ".$row['link'];
					}
					if($row['attachment']){
						$file = "<br />Attachment: ".end(explode("/",$row['attachment']));
					}
					if($row['status']){
						$status = "Done";
					}else{
						$status = "Working";
					}
					$attachment .= "<td>".$row['team_name']."</td><td>".$row['user_name']."<br/>".$row['email']."</td><td>".$row['action_name'].$link.$file."</td><td>".$row['action_description']."</td><td>".$row['deadline']."</td><td>".$status."</td></tr>";
					$i++;
				}
				$attachment .= "</table></body></html>";
			}
			
			if (count($data)){
				$this->load->library('pdf');
				$pdf = new DOMPDF();;
				$pdf->load_html($attachment, 'UTF-8');
				$dompdf->setPaper('A4', 'landscape'); ## landscape or portrait
				// Render the HTML as PDF
				$pdf->render();
				$pdf = $pdf->output();
				$this->load->library('email');
				$this->email->initialize($this->config->item("email_config"));
				$from = $this->config->item("email_from");
				$this->email->from('mailer@inr-circle.com', 'INR Mailer');
				$this->email->to(getusersdata($input['user_id'],'email'));
				$this->email->subject('INR-Circle Todo list data');
				$this->email->message('Please find the attachment for todo action list data.');
				$this->email->attach($pdf, 'application/pdf', "File".time().$input['user_id']. ".pdf", false);

				$r = $this->email->send();
				if (!$r) {
					return array('status' => 400,'message'=>strip_tags($this->email->print_debugger(array("header"))),'method'=>'print_ProtocolTodo');
				} else {
					return array('status' => 200,'message' =>'Please check your mail box.','method'=>'print_ProtocolTodo');
				}
			}else{
				return array('status' => 400,'message'=>'No data found','method'=>'print_ProtocolTodo');
			}
		}else{
			return array('status' => 400,'message'=>'Access denied!','method'=>'print_ProtocolTodo');
		}
	}
	/*
	* Leave Team
	*/
	public function ht_leaveTeam($input)
	{	
		$member_id	=	check_valid_memeber($input['team_id'],$input['user_id']);
		if ($member_id){
			$check 	= $this->db->select('member_type')->from('team_members')->WHERE(array('team_id'=>$input['team_id'],'user_id'=>$input['user_id']))->get()->row();
			
			if ($check->member_type){
				return array('status' =>400,'message' =>'Release your position from team first!','method'=>'leaveTeam');
			}else{
				$todolist = $this->db->select('todoid,assigned_by')->from('todolist')->WHERE(array('team_id'=>$input['team_id'],'accountability'=>$input['user_id'],'status'=>'0'))->get()->result_array();
				
				if (count($todolist)){
					foreach($todolist as $list){
						$this->db->insert('todo_reassign_action',array('todoid'=>$list['todoid'],'team_id'=>$input['team_id'],'prev_assigned_by'=>$list['assigned_by'],'prev_accountability'=>$input['user_id'],'updated_at'=>today()[0]));
					}
				}
				$this->db->trans_start();
				$this->db->where(array('member_id'=>$member_id))->update('team_members',array('status' =>1));
				$this->db->where(array('team_id'=>$input['team_id'],'accountability'=>$input['user_id']))->update('todolist',array('is_active' =>1));
				
				if ($this->db->trans_status() === FALSE){
					$this->db->trans_rollback();
					return array('status' => 500,'message' =>'Internal server error.','method'=>'leaveTeam');
				} else {
					$this->db->trans_commit();
					return array('status' => 200,'message' =>'Team left successfully.','method'=>'leaveTeam');
				}
			}
		}else{
			return array('status' =>400,'message' =>'Access denied!','method'=>'leaveTeam');
		}
	}
	/*
	* Task Reassign list to Admin
	*/
	public function ht_get_reassignTaskList($input)
	{	
		$myteams = $this->db->select('GROUP_CONCAT(team_id) as team_id')->from('team_members')->WHERE('user_id='.$input['user_id'].' and member_type!=0')->get()->row();
		$teamids = array(0);
		if($myteams){
			$teamids = explode(',',$myteams->team_id);
		}
		$data = $this->db->select('todo_reassign_action.reassignaction_id, todo_reassign_action.todoid, todo_reassign_action.team_id,todo_reassign_action.prev_accountability,todolist.action_name,todolist.action_description,todolist.deadline,teams.team_name,teams.team_coverphoto,users.user_name,users.email,users.profile_picture as profile_pic')->from('todo_reassign_action')->where_in('todo_reassign_action.team_id',$teamids)->join('todolist','todolist.todoid=todo_reassign_action.todoid')->join('teams','todo_reassign_action.team_id=teams.team_id')->join('users','users.user_id=todo_reassign_action.prev_accountability')->get()->result_array();
		foreach( $data as $key => $row )
		{
			$row['team_coverphoto'] !=''  ? $row['team_coverphoto'] = base_url().$row['team_coverphoto'] : '';
			$row['profile_pic']!=''  ? $row['profile_pic']	= base_url(). $row['profile_pic']: '';
			$data[$key] = $row;
		}
		$data	=	$this->Aes_encryption->aesEncryption($this->secret,json_encode($data));
		return array('status' => 200,'message' =>'Task re-assign action list.','method'=>'get_reassignTaskList','data'=>$data);
	}
	/*
	* Assing new Task
	*/
	public function ht_reassign_task($input)
	{	
		if (check_valid_todoid_assigner($input['todoid'],$input['assigned_by'])){
			$this->db->trans_start();
			
			$this->db->where(array('todoid'=>$input['todoid']))->update('todolist',array('is_active' =>0,'accountability'=>$input['accountability'],'deadline'=>$input['deadline'],'action_name'=>$input['action_name'],'action_description'=>$input['action_description']));
			
			$this->db->where(array('reassignaction_id'=>$input['reassignaction_id']))->delete('todo_reassign_action');
			
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return array('status' => 500,'message' =>'Internal server error.','method'=>'reassign_task');
			} else {
				$notification = $this->db->select('user_id')->from('notifications')->WHERE(array('user_id'=>$input['accountability']))->get()->num_rows();
				if($notification==0){
					$this->db->insert('notifications',array('user_id'=>$input['accountability'],'forward_cards'=>'0','todo_assigned'=>'1','new_ttalks'=>'0','new_onetoone_msg'=>'0'));
				}else{
					$this->db->set('todo_assigned','todo_assigned+1',false)->where(array('user_id'=>$input['accountability']))->update('notifications');
				}
				$this->db->trans_commit();
				return array('status' => 200,'message' =>'Task reassigned.','method'=>'reassign_task');
			}
		}else{
			return array('status' =>400,'message' =>'Access denied!','method'=>'reassign_task');
		}
	}
	/*
	* Dissolve Team
	*/
	public function ht_dissolveTeam($input)
	{	
		if (check_valid_team($input['team_id'],$input['user_id'])){
			
			$this->db->trans_start();
			$this->db->where(array('team_id'=>$input['team_id']))->delete('teams');
			$this->db->where(array('team_id'=>$input['team_id']))->delete('team_invites');
			$this->db->where(array('team_id'=>$input['team_id']))->delete('team_members');
			$this->db->where(array('team_id'=>$input['team_id']))->delete('todolist');
			$this->db->where(array('team_id'=>$input['team_id']))->delete('todo_reassign_action');
			$this->db->where(array('team_id'=>$input['team_id']))->delete('ttalks');
			
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return array('status' => 500,'message' =>'Internal server error.','method'=>'dissolveTeam');
			} else {
				$this->db->trans_commit();
				return array('status' => 200,'message' =>'Team dissolved successfully.','method'=>'dissolveTeam');
			}
			
		}else{
			return array('status' =>400,'message' =>'Access denied!','method'=>'dissolveTeam');
		}
	}
	
	/*
	* Create New News Channel
	*/
	public function ht_create_NewsChannel($input)
	{	
		$input['is_active'] 	= '1';
		$input['created_at']	= today()[0];
		$input['updated_at']	= today()[0];
		$input['type'] 			= '1';
		$input['company_id']	= getusersdata($input['channel_adminid'],'company_id');
		$input['channel_logo']	= '';
		$date = date("Y-m");
		$config['allowed_types']= '*';
		//$config['max_size']		= '200';
		if (!empty($_FILES['channel_logo']['name'])){
			$path	=  './media/newschannel/logo/'.$date."/";
			if (!is_dir($path)){
				mkdir($path , 0777);
				$content = "<!DOCTYPE html><html><head><title>403 Forbidden</title></head><body><p>Access denied!</p></body></html>"; 
				$fp = fopen($path."index.html","wb"); 
				fwrite($fp,$content); 
				fclose($fp); 
			}
			$config['file_name']	= date("Ymd")."_NCL-".rand(100000,999999).'-'.time();
			$config['upload_path']	= $path;
			$this->upload->initialize($config);
			if (!$this->upload->do_upload('channel_logo')){
				return array('status' =>400,'message' =>"File uploading error: ".strip_tags($this->upload->display_errors()),'method'=>$this->uri->segment(3));
				exit;
			}else{
				$input['channel_logo'] = ltrim($path,"./").$this->upload->data()['file_name'];
			}
		}
		$this->db->trans_start();
		$this->db->insert('news_channel',$input);
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return array('status' => 500,'message' =>'Internal server error.','method'=>'create_NewsChannel');
		} else {
			$this->db->insert('newschannel_subscribers',array('channel_id'=>$this->db->insert_id(),'user_id'=>$input['channel_adminid'],'subscribed_date'=>today()[0]));
			$this->db->trans_commit();
			return array('status' => 200,'message' =>'News channel created.','method'=>'create_NewsChannel');
		}
	}
	/*
	* Update News Channel Info
	*/
	public function ht_update_NewsChannelInfo($input)
	{	
		if (checkValidChannelAdmin($input['channel_id'],$input['channel_adminid'])){
			$input['channel_description']	= @$input['channel_description'];
			$input['channel_logo']	= getNewsChannelData($input['channel_id'],'channel_logo');
			$date = date("Y-m");
			$config['allowed_types']= '*';
			//$config['max_size']		= '200';
			if (!empty($_FILES['channel_logo']['name'])){
				$path	=  './media/newschannel/logo/'.$date."/";
				if (!is_dir($path)){
					mkdir($path , 0777);
					$content = "<!DOCTYPE html><html><head><title>403 Forbidden</title></head><body><p>Access denied!</p></body></html>"; 
					$fp = fopen($path."index.html","wb"); 
					fwrite($fp,$content); 
					fclose($fp); 
				}
				$config['file_name']	= date("Ymd")."_NCL-".rand(100000,999999).'-'.time();
				$config['upload_path']	= $path;
				$this->upload->initialize($config);
				if (!$this->upload->do_upload('channel_logo')){
					return array('status' =>400,'message' =>"File uploading error: ".strip_tags($this->upload->display_errors()),'method'=>$this->uri->segment(3));
					exit;
				}else{
					$input['channel_logo'] !="" ? unlink("./".$input['channel_logo']) : '';
					$input['channel_logo'] = ltrim($path,"./").$this->upload->data()['file_name'];
				}
			}
			$this->db->trans_start();
			$this->db->where('channel_id',$input['channel_id'])->update('news_channel',array('channel_name'=>$input['channel_name'],'channel_logo'=>$input['channel_logo'],'channel_description'=>$input['channel_description'],'updated_at' =>today()[0]));
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return array('status' => 500,'message' =>'Internal server error.','method'=>'update_NewsChannelInfo');
			} else {
				$this->db->trans_commit();
				return array('status' => 200,'message' =>'Information updated.','method'=>'update_NewsChannelInfo');
			}
		}else{
			return array('status' =>400,'message' =>'Access denied!','method'=>'update_NewsChannelInfo');
		}
	}
	/*
	* Delete News Channel 
	*/
	public function ht_delete_NewsChannel($input)
	{	
		if (checkValidChannelAdmin($input['channel_id'],$input['channel_adminid'])){
			$channel_logo	= getNewsChannelData($input['channel_id'],'channel_logo');
			
			$this->db->trans_start();
			$this->db->where(array('channel_id'=>$input['channel_id']))->delete('news_channel');
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return array('status' => 500,'message' =>'Internal server error.','method'=>'delete_NewsChannel');
			} else {
				$channel_logo !="" ? unlink("./".$channel_logo) : '';
				$this->db->where(array('channel_id'=>$input['channel_id']))->delete('news_deleted');
				$this->db->where(array('channel_id'=>$input['channel_id']))->delete('news_brief');
				$this->db->where(array('channel_id'=>$input['channel_id']))->delete('newschannel_subscribers');
				$this->db->trans_commit();
				return array('status' => 200,'message' =>'News Channel deleted.','method'=>'delete_NewsChannel');
			}
		}else{
			return array('status' =>400,'message' =>'Access denied!','method'=>'delete_NewsChannel');
		}
	}
	/*
	* Search News Channel
	*/
	public function ht_get_myNewsChannel($input)
	{	
		$data = $this->db->select('news_channel.channel_id,news_channel.channel_name, news_channel.channel_description, news_channel.channel_logo, news_channel.channel_adminid,users.user_name, users.email, users.designation as position,users.profile_picture as profile_pic')->from('news_channel')->where(array('news_channel.channel_adminid'=>$input['channel_adminid'], 'news_channel.is_active'=>'1'))->join('users', 'users.user_id = news_channel.channel_adminid')->order_by('news_channel.channel_name','ASC')->get()->result_array();
		//print $this->db->last_query(); EXIT;
		foreach( $data as $key => $row )
		{
			$row['profile_pic']		!=''  ? $row['profile_pic']	= base_url(). $row['profile_pic']: '';
			$row['position'] 		=  getDesignationdata($row['position'],'designation');
			$row['subscribe_status']=  "";
			$row['channel_logo'] !=''  ? $row['channel_logo'] = base_url().$row['channel_logo'] : '';
			$row['subscribers_count'] = (string)$this->db->select('subscribe_id')->from('newschannel_subscribers')->where(array('channel_id'=>$row['channel_id']))->get()->num_rows();
			
			$data[$key] = $row;
		}
		$data	=	$this->Aes_encryption->aesEncryption($this->secret,json_encode($data));
		return array('status' => 200,'data'=>$data,'message' =>'My Channels.','method'=>'get_myNewsChannel');
	}
	/*
	* Search News Channel
	*/
	public function ht_search_newsChannel($input)
	{	
		$keyword 	= $input['search'];
		$company_id = getusersdata($input['user_id'],'company_id');
		## My Channel
		$mydata = $this->db->select('news_channel.channel_id,news_channel.channel_name, news_channel.channel_description, news_channel.channel_logo, news_channel.channel_adminid, users.user_name, users.email, users.designation as position,users.profile_picture as profile_pic')->from('news_channel')->where(array('news_channel.company_id'=>$company_id, 'news_channel.channel_adminid'=>$input['user_id'],'news_channel.is_active'=>'1'))->join('users', 'users.user_id = news_channel.channel_adminid')->group_start()->or_like('news_channel.channel_name',$keyword)->or_like('users.user_name',$keyword)->or_like('users.email',$keyword)->group_end()->order_by('news_channel.channel_name','ASC')->get()->result_array();
		foreach( $mydata as $mykey => $myrow )
		{
			$myrow['channel_logo'] !=''  ? $myrow['channel_logo'] = base_url().$myrow['channel_logo'] : '';
			$myrow['profile_pic']!=''  ? $myrow['profile_pic']	= base_url(). $myrow['profile_pic']: '';
			$myrow['position'] 	=  getDesignationdata($myrow['position'],'designation');
			$myrow['subscribe_status'] = "";
			$myrow['subscribers_count'] = (string)$this->db->select('subscribe_id')->from('newschannel_subscribers')->where(array('channel_id'=>$myrow['channel_id']))->get()->num_rows();
			
			$mydata[$mykey] = $myrow;
		}
		
		$my_channel = array(0);
		if(count($mydata)){
			$my_channel = array_column($mydata,'channel_id');
		}
		$page 		= $input['page'];
		$limit		= 30;
		$start		= ($page - 1)* $limit;
		## Other than my channel
		$total 	= $this->db->select('news_channel.channel_id,users.user_name, users.email')->from('news_channel')->WHERE(array('news_channel.company_id'=>$company_id,'news_channel.is_active'=>'1'))->where_not_in('news_channel.channel_id',$my_channel)->join('users', 'users.user_id = news_channel.channel_adminid')->group_start()->or_like('news_channel.channel_name',$keyword)->or_like('users.user_name',$keyword)->or_like('users.email',$keyword)->group_end()->get();
		$totalpages = ceil(($total->num_rows())/$limit);
		$previous	=  $totalpages > 1 ? "1" : "0"; 
		$next		=  $page < $totalpages ? "1" : "0";
		
		$data = $this->db->select('news_channel.channel_id,news_channel.channel_name, news_channel.channel_description, news_channel.channel_logo, news_channel.channel_adminid, news_channel.type, users.user_name, users.email, users.designation as position,users.profile_picture as profile_pic')->from('news_channel')->where(array('news_channel.company_id'=>$company_id, 'news_channel.is_active'=>'1'))->where_not_in('news_channel.channel_id',$my_channel)->join('users', 'users.user_id = news_channel.channel_adminid')->group_start()->or_like('news_channel.channel_name',$keyword)->or_like('users.user_name',$keyword)->or_like('users.email',$keyword)->group_end()->order_by('news_channel.channel_name','ASC')->limit($limit,$start)->get()->result_array();
		//print $this->db->last_query(); EXIT;
		foreach( $data as $key => $row )
		{
			$row['channel_logo'] !=''  ? $row['channel_logo'] = base_url().$row['channel_logo'] : '';
			$row['profile_pic']!=''  ? $row['profile_pic']	= base_url(). $row['profile_pic']: '';
			$row['position'] 	=  getDesignationdata($row['position'],'designation');
			$row['subscribe_status'] = (string)$this->db->select('subscribe_id')->from('newschannel_subscribers')->where(array('user_id'=>$input['user_id'],'channel_id'=>$row['channel_id']))->get()->num_rows();
			$row['subscribers_count'] = (string)$this->db->select('subscribe_id')->from('newschannel_subscribers')->where(array('channel_id'=>$row['channel_id']))->get()->num_rows();
			## Join Mandatory Channel atuotmatically 
			if($row['subscribe_status']==0 && $row['type']== 2){
				$this->db->insert('newschannel_subscribers',array('channel_id'=>$row['channel_id'],'user_id'=>$input['user_id'],'subscribed_date'=>today()[0]));
			}
			$data[$key] = $row;
		}
		$data	=	$this->Aes_encryption->aesEncryption($this->secret,json_encode($data));
		$mydata	=	$this->Aes_encryption->aesEncryption($this->secret,json_encode($mydata));
		return array('status' => 200,'data'=>$data,'my_channels'=>$mydata,'message' =>'Channels list.','method'=>'search_newsChannel','totalpage'=>(string)$totalpages,'previous'=>$previous,'next'=>$next);
	}
	/*
	* Subscribe-unsubscribe news channel
	*/
	public function ht_subs_unsbuscribeNewsChannel($input)
	{	
		if (getNewsChannelData($input['channel_id'],'company_id')== getusersdata($input['user_id'],'company_id')){
			$check 	= $this->db->select('subscribe_id')->from('newschannel_subscribers')->WHERE(array('channel_id'=>$input['channel_id'],'user_id'=>$input['user_id']))->get()->row();
			if ($check){
				$subscribe_id = $check->subscribe_id;
			}else{
				$subscribe_id = '0';
			}
			$this->db->trans_start();
			if($subscribe_id){
				$this->db->where(array('subscribe_id'=>$subscribe_id))->delete('newschannel_subscribers');
				$msg = "Unsubscribed successfully.";
			}else{
				$this->db->insert('newschannel_subscribers',array('channel_id'=>$input['channel_id'],'user_id'=>$input['user_id'],'subscribed_date'=>today()[0]));
				$msg = "Subscribed successfully.";
			}
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return array('status' => 500,'message' =>'Internal server error.','method'=>'subs_unsbuscribeNewsChannel');
			} else {
				$this->db->trans_commit();
				return array('status' => 200,'message' =>$msg,'method'=>'subs_unsbuscribeNewsChannel');
			}
		}else{
			return array('status' =>400,'message' =>'Access denied!','method'=>'subs_unsbuscribeNewsChannel');
		}
	}
	/*
	* Publish News
	*/
	public function ht_publish_NewsBrief($input)
	{	
		if (checkValidChannelAdmin($input['channel_id'],$input['user_id'])){
			$input['is_active'] 	= '1';
			$input['published_at']	= today()[0];
			$input['company_id']	= getusersdata($input['user_id'],'company_id');
			$input['picture']	= '';
			$date = date("Y-m");
			$config['allowed_types']= '*';
			//$config['max_size']		= '200';
			if (!empty($_FILES['picture']['name'])){
				$path	=  './media/newschannel/newsbrief/'.$date."/";
				if (!is_dir($path)){
					mkdir($path , 0777);
					$content = "<!DOCTYPE html><html><head><title>403 Forbidden</title></head><body><p>Access denied!</p></body></html>"; 
					$fp = fopen($path."index.html","wb"); 
					fwrite($fp,$content); 
					fclose($fp); 
				}
				$config['file_name']	= date("Ymd")."_NBP-".rand(100000,999999).'-'.time();
				$config['upload_path']	= $path;
				$this->upload->initialize($config);
				if (!$this->upload->do_upload('picture')){
					return array('status' =>400,'message' =>"File uploading error: ".strip_tags($this->upload->display_errors()),'method'=>$this->uri->segment(3));
					exit;
				}else{
					
					$input['picture'] = ltrim($path,"./").$this->upload->data()['file_name'];
				}
			}
			$this->db->trans_start();
			$this->db->insert('news_brief',$input);
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return array('status' => 500,'message' =>'Internal server error.','method'=>'publish_NewsBrief');
			} else {
				$this->db->trans_commit();
				return array('status' => 200,'message' =>'News published.','method'=>'publish_NewsBrief');
			}
		
		}else{
			return array('status' =>400,'message' =>'Access denied!','method'=>'publish_NewsBrief');
		}	
	}
	/*
	* Publish News
	*/
	public function ht_edit_NewsBrief($input)
	{	
		if (getNewsBriefData($input['newsbrief_id'],'user_id') == $input['user_id']){
			$input['picture']	= getNewsBriefData($input['newsbrief_id'],'picture');
			$date = date("Y-m");
			$config['allowed_types']= '*';
			//$config['max_size']		= '200';
			if (!empty($_FILES['picture']['name'])){
				$path	=  './media/newschannel/newsbrief/'.$date."/";
				if (!is_dir($path)){
					mkdir($path , 0777);
					$content = "<!DOCTYPE html><html><head><title>403 Forbidden</title></head><body><p>Access denied!</p></body></html>"; 
					$fp = fopen($path."index.html","wb"); 
					fwrite($fp,$content); 
					fclose($fp); 
				}
				$config['file_name']	= date("Ymd")."_NBP-".rand(100000,999999).'-'.time();
				$config['upload_path']	= $path;
				$this->upload->initialize($config);
				if (!$this->upload->do_upload('picture')){
					return array('status' =>400,'message' =>"File uploading error: ".strip_tags($this->upload->display_errors()),'method'=>$this->uri->segment(3));
					exit;
				}else{
					$input['picture'] !="" ? unlink("./".$input['picture']) : '';
					$input['picture'] = ltrim($path,"./").$this->upload->data()['file_name'];
				}
			}
			$this->db->trans_start();
			$this->db->where(array('newsbrief_id'=>$input['newsbrief_id']))->update('news_brief',array('news'=>$input['news'],'picture'=>$input['picture'],'modified_at'=>today()[0]));
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return array('status' => 500,'message' =>'Internal server error.','method'=>'edit_NewsBrief');
			} else {
				$this->db->trans_commit();
				return array('status' => 200,'message' =>'Updated.','method'=>'edit_NewsBrief');
			}
		
		}else{
			return array('status' =>400,'message' =>'Access denied!','method'=>'edit_NewsBrief');
		}	
	}
	
	/*
	* Get Latest Brief News Channel
	*/
	public function ht_get_latest_newsBriefs($input)
	{	
	
		$expired = date('Y-m-d H:i:s', strtotime('-1 day', today()[1]));
		$input['company_id']	= getusersdata($input['user_id'],'company_id');
		
		$this->db->where(array('company_id'=>$input['company_id'],'published_at<'=>$expired))->update('news_brief',array('is_active'=>2));
		
		$check_hidden = $this->db->select('GROUP_CONCAT(newsbrief_id) as newsbrief_ids')->from('news_deleted')->WHERE(array('user_id'=>$input['user_id']))->get()->row();
		$hidden = array(0);
		if($check_hidden){
			$hidden = explode(',',$check_hidden->newsbrief_ids);
		}
		
		$checksubs = $this->db->select('GROUP_CONCAT(channel_id) as channel_ids')->from('newschannel_subscribers')->WHERE(array('user_id'=>$input['user_id']))->get()->row();
		$subscribed = array(0);
		if($checksubs){
			$subscribed = explode(',',$checksubs->channel_ids);
		}
		
		$page 		= $input['page'];
		$limit		= 30;
		$start		= ($page - 1)* $limit;
		$totalrows 	= $this->db->select('newsbrief_id')->from('news_brief')->WHERE('is_active','1')->where_in('channel_id',$subscribed)->where_not_in('newsbrief_id',$hidden)->where_not_in('newsbrief_id',$hidden)->get()->num_rows();
		
		$totalpages = ceil($totalrows/$limit);
		$previous	=  $totalpages > 1 ? "1" : "0"; 
		$next		=  $page < $totalpages ? "1" : "0";
		
		$data = $this->db->select('news_brief.newsbrief_id, news_brief.channel_id, news_brief.news, news_brief.picture, news_brief.published_at, news_channel.channel_name, news_channel.channel_description, news_channel.channel_logo, news_channel.channel_adminid, users.user_name, users.profile_picture as profile_pic, users.designation as position, users.email')->from('news_brief')->where(array('news_brief.is_active'=>'1'))->where_in('news_brief.channel_id',$subscribed)->where_not_in('news_brief.newsbrief_id',$hidden)->join('news_channel', 'news_brief.channel_id = news_channel.channel_id')->join('users', 'users.user_id = news_channel.channel_adminid')->order_by('news_brief.newsbrief_id','DESC')->limit($limit,$start)->get()->result_array();
		//print $this->db->last_query(); EXIT;
		foreach( $data as $key => $row )
		{
			$row['picture']		 = $row['picture'] !=''  ? $row['picture'] = base_url().$row['picture'] : '';
			$row['channel_logo'] = $row['channel_logo'] !=''  ? $row['channel_logo'] = base_url().$row['channel_logo'] : '';
			$row['position']	 = getDesignationdata($row['position'],'designation');
			$row['profile_pic']  = $row['profile_pic']!=''  ? $row['profile_pic']	= base_url(). $row['profile_pic'] : '';
			$row['published_at'] = convterUTCtoLocal($row['published_at'],$input['timezone']);
			$data[$key] = $row;
		}
		$data	=	$this->Aes_encryption->aesEncryption($this->secret,json_encode($data));
		return array('status' => 200,'data'=>$data,'message' =>'Latest news feeds.','method'=>'get_latest_newsBriefs','totalpage'=>(string)$totalpages,'previous'=>$previous,'next'=>$next);
	}
	/*
	* Refresh Latest news Brief 
	*/
	public function ht_refresh_newsBriefs($input)
	{	
		$checksubs = $this->db->select('GROUP_CONCAT(channel_id) as channel_ids')->from('newschannel_subscribers')->WHERE(array('user_id'=>$input['user_id']))->get()->row();
		$subscribed = array(0);
		if($checksubs){
			$subscribed = explode(',',$checksubs->channel_ids);
		}
		$data = $this->db->select('news_brief.newsbrief_id, news_brief.channel_id, news_brief.news, news_brief.picture, news_brief.published_at, news_channel.channel_name, news_channel.channel_description, news_channel.channel_logo, news_channel.channel_adminid, users.user_name, users.profile_picture as profile_pic, users.designation as position, users.email')->from('news_brief')->where(array('news_brief.is_active'=>'1','news_brief.newsbrief_id >'=>$input['newsbrief_id']))->where_in('news_brief.channel_id',$subscribed)->join('news_channel', 'news_brief.channel_id = news_channel.channel_id')->join('users', 'users.user_id = news_channel.channel_adminid')->order_by('news_brief.newsbrief_id','DESC')->get()->result_array();
		foreach( $data as $key => $row )
		{
			$row['picture']		 = $row['picture'] !=''  ? $row['picture'] = base_url().$row['picture'] : '';
			$row['channel_logo'] = $row['channel_logo'] !=''  ? $row['channel_logo'] = base_url().$row['channel_logo'] : '';
			$row['position']	 = getDesignationdata($row['position'],'designation');
			$row['profile_pic']  = $row['profile_pic']!=''  ? $row['profile_pic']	= base_url(). $row['profile_pic'] : '';
			$row['published_at'] = convterUTCtoLocal($row['published_at'],$input['timezone']);
			$data[$key] = $row;
		}
		$data	=	$this->Aes_encryption->aesEncryption($this->secret,json_encode($data));
		return array('status' => 200,'data'=>$data,'message' =>'New news briefs.','method'=>'refresh_newsBriefs');
	}
	/*
	* Delete News Briefs 
	*/
	public function ht_delete_newsBrief($input)
	{	
		if ($input['hide']){
			$channel_id	= getNewsBriefData($input['newsbrief_id'],'channel_id');
			$this->db->trans_start();
			$this->db->insert('news_deleted',array('channel_id'=>$channel_id,'newsbrief_id'=>$input['newsbrief_id'],'user_id'=>$input['user_id'],' 	deleted_at'=>today()[0]));
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return array('status' => 500,'message' =>'Internal server error.','method'=>'delete_newsBrief');
			} else {
				$this->db->trans_commit();
				return array('status' => 200,'message' =>'News deleted.','method'=>'delete_newsBrief');
			}
		}else{
			if (checkValidNewsBrifPubisher($input['newsbrief_id'],$input['user_id'])){
				$picture	= getNewsBriefData($input['newsbrief_id'],'picture');
				$this->db->trans_start();
				$this->db->where(array('newsbrief_id'=>$input['newsbrief_id']))->delete('news_brief');
				if ($this->db->trans_status() === FALSE){
					$this->db->trans_rollback();
					return array('status' => 500,'message' =>'Internal server error.','method'=>'delete_newsBrief');
				} else {
					$picture !="" ? unlink("./".$picture) : '';
					$this->db->trans_commit();
					return array('status' => 200,'message' =>'News deleted.','method'=>'delete_newsBrief');
				}
			}else{
				return array('status' =>400,'message' =>'Access denied!','method'=>'delete_newsBrief');
			}
		}
	}
	/*
	* GET New Notification Alerts
	*/
	public function ht_get_notificationsAlerts($input)
	{	
		$data = $this->db->select('user_id,forward_cards,todo_assigned,new_ttalks,new_onetoone_msg')->from('notifications')->WHERE(array('user_id'=>$input['user_id']))->get()->row();
		//print $this->db->last_query(); EXIT;
		if($data==''){
			$data = array('user_id'=>$input['user_id'],'forward_cards'=>'0','todo_assigned'=>'0','new_ttalks'=>'0','new_onetoone_msg'=>'0');
			$this->db->insert('notifications',$data);
		}
		$data	=	$this->Aes_encryption->aesEncryption($this->secret,json_encode($data));
		return array('status' => 200,'message' =>'New notifications.','method'=>'get_notificationsAlerts','data'=>$data);
	}
	/*
	* GET Company Information
	*/
	public function ht_get_company_information($input)
	{	
		if (check_valid_users_company($input['company_id'],$input['user_id'])){
			$row = $this->db->select('company_id, company_name, company_logo, domain, contact_person, contact_email, contact_phone, company_address, city, state, zipcode, country')->from('companies')->WHERE(array('company_id'=>$input['company_id']))->get()->row_array();
			$row['company_logo'] = $row['company_logo'] !=''  ? $row['company_logo'] = base_url().$row['company_logo'] : '';
			$row	=	$this->Aes_encryption->aesEncryption($this->secret,json_encode($row));
			return array('data'=>$row,'status' => 200,'message' =>'Company information','method'=>'get_company_information');
		}else{
			return array('status' =>400,'message' =>'Access denied!','method'=>'delete_newsBrief');
		}
	}
	/*
	* Send Company Account Request
	*/
	public function ht_send_company_account_request($input)
	{	
		$data = $this->db->select('user_id, template_id, name, title, position, is_cardpic_deleted, card_picture, profile_pic, company_name, company_logo, postal, building_name, street_address, house_number, state, city, country, fax_number, office_number, mobile_number, email_address, website, created_at, updated_at')->from('my_cards')->where(array('card_id'=>$input['card_id'],'user_id'=>$input['user_id']))->get()->row_array();
		if (count($data)){
			$input['request_date'] = today()[0];
			$check = $this->db->select("request_id")->from("company_account_requests")->where(array("user_id"=>$input['user_id']))->get()->num_rows();
			if ($check){
				return array('status' =>400,'message' =>'You have already sent request. Please wait.','method'=>'company_account_request');
			}else{ 
				$this->db->trans_start();
				$this->db->insert('company_account_requests',$input);
				if ($this->db->trans_status() === FALSE){
					$this->db->trans_rollback();
					return array('status' => 500,'message' => 'Internal server error.');
				} else {
					$this->db->trans_commit();
					$this->load->library('email');
					$this->email->from($data['email_address'], $data['name']);
					$this->email->to('iphone.techwinlabs@gmail.com');
					$this->email->subject('INR-Circle company account request');
					$this->email->set_mailtype("html");
					$email_message .= "<div style='width:400px;background:#fff;color:#000;font-family:Source Sans Pro,sans-serif;padding:10px;' >";	
					$email_message .= "<div><b>Requester information</b></div>";
					$email_message .= "<div></div>";
					$email_message .= "<div>Name	: ".$data['name']."</div>" ;
					$email_message .= "<div>Email	: ".$data['email_address']."</div>" ;
					$email_message .= "<div>Mobile	: ".$data['mobile_number']."</div>" ;
					$email_message .= "<div>Office	: ".$data['office_number']."</div>" ;
					$email_message .= "<div>Fax		: ".$data['fax_number']."</div>" ;
					$email_message .= "<div>Company	: ".$data['company_name']."</div>" ;
					$email_message .= "<div>Address	: ".$data['house_number'].", ". $data['street_address']."</div>" ;
					$email_message .= "<div>City	: ".$data['city']."</div>" ;
					$email_message .= "<div>State	: ".$data['state']."</div>" ;
					$email_message .= "<div>Zipcode	: ".$data['postal']."</div>" ;
					$email_message .= "<div>country	: ".$data['country']."</div>" ;
					$email_message .= "<div></div>" ;
					$email_message .= "<div>Thanks</div>" ;
					$email_message .= "</div>" ;
					$this->email->message($email_message);

					$this->email->send();
					return array('status' => 200,'message' => 'Your request has been sent successfully. We will contact you very soon.','method'=>'send_company_account_request');
				}
			}
		}else{
			return array('status' =>400,'message' =>'Access denied!','method'=>'company_account_request');
		}
	}
	/*
	* Export Data To CSV Google contact and Excel Outlook contact Format
	*/
	public function ht_exportContactsTo_GoogleOrOutlook($input)
	{	
		if($input['rolodex_id']){
			$data = $this->db->select('rolodex.rolodex_id,rolodex.latitude,rolodex.longitude,my_cards.card_id, my_cards.user_id, my_cards.template_id, my_cards.name, my_cards.middle_name, my_cards.last_name, my_cards.title, my_cards.position, my_cards.card_picture, my_cards.profile_pic, my_cards.company_name, my_cards.company_logo, my_cards.postal, my_cards.building_name, my_cards.street_address, my_cards.house_number, my_cards.state, my_cards.city, my_cards.country, my_cards.fax_number, my_cards.office_number, my_cards.mobile_number, my_cards.email_address, website, UNIX_TIMESTAMP(my_cards.created_at) as created_at')->from('rolodex')->join('my_cards', 'my_cards.card_id = rolodex.card_id')->where(array('rolodex.user_id'=>$input['user_id'],'rolodex.rolodex_id'=>$input['rolodex_id']))->get()->result_array();
		}else{
			$data = $this->db->select('rolodex.rolodex_id,rolodex.latitude,rolodex.longitude,my_cards.card_id, my_cards.user_id, my_cards.template_id, my_cards.name, my_cards.middle_name, my_cards.last_name, my_cards.title, my_cards.position, my_cards.card_picture, my_cards.profile_pic, my_cards.company_name, my_cards.company_logo, my_cards.postal, my_cards.building_name, my_cards.street_address, my_cards.house_number, my_cards.state, my_cards.city, my_cards.country, my_cards.fax_number, my_cards.office_number, my_cards.mobile_number, my_cards.email_address, website, UNIX_TIMESTAMP(my_cards.created_at) as created_at')->from('rolodex')->join('my_cards', 'my_cards.card_id = rolodex.card_id')->where(array('rolodex.user_id'=>$input['user_id']))->order_by('rolodex_id','DESC')->get()->result_array();
		}
		$path	=  './media/export/';
		if (count($data)){
			
			$content = "<!DOCTYPE html><html><head><title>403 Forbidden</title></head><body><p>Access denied!</p></body></html>"; 
			$fp = fopen($path."index.html","wb"); 
			fwrite($fp,$content); 
			fclose($fp);
			if ($input['export_to']==1){ // Google
				
				$csvData[] =array("Name","Given Name","Additional Name","Family Name","Yomi Name","Given Name Yomi","Additional Name Yomi","Family Name Yomi","Name Prefix","Name Suffix","Initials","Nickname","Short Name","Maiden Name","Birthday","Gender","Location","Billing Information","Directory Server","Mileage","Occupation","Hobby","Sensitivity","Priority","Subject","Notes","Group Membership","E-mail 1 - Type","E-mail 1 - Value","IM 1 - Type","IM 1 - Service","IM 1 - Value","Phone 1 - Type","Phone 1 - Value","Phone 2 - Type","Phone 2 - Value","Phone 3 - Type","Phone 3 - Value","Address 1 - Type","Address 1 - Formatted","Address 1 - Street","Address 1 - City","Address 1 - PO Box","Address 1 - Region","Address 1 - Postal Code","Address 1 - Country","Address 1 - Extended Address","Organization 1 - Type","Organization 1 - Name","Organization 1 - Yomi Name","Organization 1 - Title","Organization 1 - Department","Organization 1 - Symbol","Organization 1 - Location","Organization 1 - Job Description");
				
				foreach($data as $csv){
					$csvData[] = array($csv['name'],'','','','','','','','','',$csv['title'],'','','','','','','','','','','','','','','','','',$csv['email_address'],'','','','',$csv['mobile_number'],'','','','','','',$csv['street_address'],$csv['city'],'','',$csv['postal'],$csv['country'],'','',$csv['company_name'],'','',$csv['position'],'',$csv['building_name'],'');
				}
				$google =fopen(($path."googlecontact_".$input['user_id'].".csv"), 'wb');

				foreach ($csvData as $fields) {
					fputcsv($google,$fields);
				}
				fclose($google);
				
				$subject = 'INR-Circle contacts export to Google contacts';
				$attachment = './media/export/googlecontact_'.$input['user_id'].'.csv';
				$msg = "Please find the attachment for your INR-Circle mobile application Rolodex contacts in Google csv format.<br />Please download this file and import these contacts from your Google contacts.";
			}else{ // Outlook
				
				$outlookData[] =array("Title", "First Name", "Middle Name", "Last Name", "Title", "Suffix", "Initials", "Web Page", "Gender", "Birthday", "Anniversary", "Location", "Language", "Internet Free Busy", "Notes", "E-mail Address", "E-mail 2 Address", "E-mail 3 Address", "Primary Phone", "Home Phone", "Home Phone 2", "Mobile Phone", "Pager", "Home Fax", "Home Address", "Home Street", "Home Street 2", "Home Street 3", "Home Address POB ox", "Home City", "Home State", "Home Postal Code", "Home Country", "Spouse", "Children", "Manager's Name", "Assistant's Name", "Referred By", "Company Main Phone", "Business Phone", "Business Phone 2", "Business Fax", "Assistant's Phone", "Company", "Job Title", "Department", "Office Location", "Organizational ID Number", "Profession", "Account", "Business Address", "Business Street", "Business Street 2", "Business Street 3", "Business Address PO Box", "Business City", "Business State", "Business Postal Code", "Business Country", "Other Phone", "Other Fax", "Other Address", "Other Street", "Other Street 2", "Other Street 3", "Other Address PO Box", "Other City", "Other State", "Other Postal Code", "Other Country", "Callback", "Car Phone", "ISDN", "Radio Phone", "TTY/TDD Phone", "Telex", "User 1", "User 2", "User 3", "User 4", "Keywords", "Mileage", "Hobby", "Billing Information", "Directory Server", "Sensitivity", "Priority", "Private", "Categories");
				
				foreach($data as $outlook){
					$name = explode(" ",$outlook['name']);
					$fname = $name[0];
					$lname = !empty($name[1]) ? $name[1] : "";
					$outlookData[] = array($outlook['title'],$fname,'',$lname, $outlook['title'], '', '', $outlook['website'], 'Unspecified', '0/0/00', '0/0/00', '', '', '', '', $outlook['email_address'], '', '', '', '', '', $outlook['mobile_number'], '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', $outlook['office_number'], '', $outlook['fax_number'], '', $outlook['company_name'], $outlook['position'], '', $outlook['building_name'], '', '', '', $outlook['street_address'], $outlook['street_address'], '', '', '', $outlook['city'], $outlook['state'], $outlook['postal'], $outlook['country'], '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Normal', 'Normal', '', '');
				}
				
				$outlookCSV =fopen(($path."outllookcontact".$input['user_id'].".csv"), 'wb');

				foreach ($outlookData as $fields) {
					fputcsv($outlookCSV,$fields);
				}
				fclose($outlookCSV);
				
				$attachment = './media/export/outllookcontact'.$input['user_id'].'.csv';
				
				## Export To Excel
				/*
				
				// Starting the PHPExcel library
				$this->load->library('PHPExcel');
				//$this->load->library('PHPExcel/IOFactory');
				$objPHPExcel = new PHPExcel();
				$objPHPExcel->getProperties()->setTitle("Contacts")->setDescription("none");
		 
				$objPHPExcel->setActiveSheetIndex(0);
		 		$r =1;
				foreach ($outlookData as $row)
				{	
					$c = 0;
					foreach ($row as $value){
						$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($c, $r, $value);
						$c++;
					}
					$r++;
				}
				$objPHPExcel->setActiveSheetIndex(0);
				$objPHPExcel->getSecurity()->setLockWindows(false);
				$objPHPExcel->getSecurity()->setLockStructure(false);
				$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
				
				$objWriter->save($path."outlookcontact_".$input['user_id'].".xls");
				$attachment = "./media/export/outlookcontact_".$input['user_id'].".xls";
				/* // Sending headers to force the user to download the file
				header('Content-Type: application/vnd.ms-excel');
				header('Content-Disposition: attachment;filename="outlookcontact_"'.$input['user_id'].'".xls"');
				header('Cache-Control: max-age=0');
				$objWriter->save('php://output'); */
				## End Of Export to excel 
	
				$subject = 'INR-Circle contacts export to Outlook contacts';
				$msg = "Please find the attachment for your INR-Circle mobile application Rolodex contacts in Outlook CSV format.<br />Please download this file and import these contacts from your Outlook contacts.";
			}
			$this->load->library('email');
			$this->email->from("noreply@inr-circle.com", 'INR-Circle Support');
			$this->email->to(getusersdata($input['user_id'],'email'));
			$this->email->subject($subject);
			$this->email->set_mailtype("html");
			$email_message .= "<div style='width:800px;background:#fff;color:#000;font-family:Source Sans Pro,sans-serif;padding:10px;' >";	
			$email_message .= "<div><b>Hello ".getusersdata($input['user_id'],'user_name').",</b></div>";
			$email_message .= "<div><p>".$msg."</p></div>";
			$email_message .= "<div><br /></div>";
			$email_message .= "<div>Thanks</div>" ;
			$email_message .= "</div>" ;
			$this->email->message($email_message);
			$this->email->attach($attachment);
			if ($this->email->send()){
				$this->email->clear(TRUE);
				return array('status' => 200,'message' => 'Data exported successfully.','method'=>'exportContactsTo_GoogleOrOutlook');
			}else{
				return array('status' => 400,'message' => 'There are some problem to sending email! Please try again.','method'=>'exportContactsTo_GoogleOrOutlook');
			}
		}else{
			return array('status' => 400,'message' => 'No contacts to export!','method'=>'exportContactsTo_GoogleOrOutlook');
		}
	}
	
	/*
	* Delete Invites Or Rejection 
	*/
	public function ht_delete_InvitesOrRejection($input)
	{	
		$check = $this->db->select("team_id,inviter_id,status")->from("team_invites")->where(array('invite_id'=>$input['invite_id']))->get()->row();
		if($check){
			if($check->inviter_id==$input['user_id']){
				$this->db->trans_start();
				
				$this->db->where(array('invite_id'=>$input['invite_id']))->delete('team_invites');
				
				if ($this->db->trans_status() === FALSE){
					$this->db->trans_rollback();
					return array('status' => 500,'message' =>'Internal server error.','method'=>'delete_InvitesOrRejection');
				} else {
					$this->db->trans_commit();
					if($check->status){
						return array('status' => 200,'message' =>'Invitation canceled.','method'=>'delete_InvitesOrRejection');
					}else{
						return array('status' => 200,'message' =>'Data removed','method'=>'delete_InvitesOrRejection');
					}
				}
				
			}else{
				return array('status' =>400,'message' =>'Access denied!','method'=>'delete_InvitesOrRejection');
			}
		
		}else{
			return array('status' =>400,'message' =>'Invalid action!','method'=>'delete_InvitesOrRejection');
		}
	}
	
	/*
	* Get INR User Info By Mobile Number 
	*/
	public function ht_get_UserInfoByMobileNumber($input)
	{	
		$data = $this->db->select('user_id,user_name,email,profile_picture')->from('users')->where(array('mobile_number'=>$input['mobile_number']))->get()->row();
		if(count($data)){
			$data->profile_picture != '' ? $data->profile_picture = base_url().$data->profile_picture : '';
			$data =	$this->Aes_encryption->aesEncryption($this->secret,json_encode($data));
			return array('data'=>$data,'status' => 200,'message' =>'User data','method'=>'get_UserInfoByMobileNumber');
		}else{
			return array('status' =>400,'message' =>'No data','method'=>'get_UserInfoByMobileNumber');
		}
	}
	/*
	* Get Business Card Template
	*/
	public function ht_get_businessCardTemplate($input)
	{	
		$company_id	= getusersdata($input['user_id'],'company_id');
		$data = $this->db->select('template_id, background_layer, front_layer')->from('card_templates')->where(array('company_id'=>$company_id,'is_active'=>1))->get()->result_array();
		for($i=0;$i<count($data);$i++){
			$data[$i]['background_layer']	=	base_url().$data[$i]['background_layer']; 
			$data[$i]['front_layer']		=	base_url().$data[$i]['front_layer']; 
		}
		$data =	$this->Aes_encryption->aesEncryption($this->secret,json_encode($data));
		return array('data'=>$data,'status' => 200,'message' =>'Business Card Templates','method'=>'get_businessCardTemplate');
	}
	
	
	
	
	
// end of model	
}