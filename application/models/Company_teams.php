<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Company_teams extends CI_Model {
	
	public function select_allteams(){
		$company_id = $this->session->userdata()['userdata']['company_id'];
		$data = $this->db->select('teams.team_id, teams.company_id, teams.user_id, teams.team_name, teams.team_coverphoto, teams.briefing_validity, teams.created_at, teams.updated_at, teams.is_active,users.user_name, users.user_name, users.profile_picture ')->from('teams')->where(array('teams.company_id'=>$company_id))->join('users', 'users.user_id = teams.user_id')->order_by('teams.team_id','DESC')->get()->result_array();
		foreach( $data as $key => $row )
		{
			$row['team_coverphoto'] = $row['team_coverphoto'] !=''  ? $row['team_coverphoto'] = base_url().$row['team_coverphoto'] : '';
			$row['profile_picture']  = $row['profile_picture']!=''  ? $row['profile_picture']	= base_url(). $row['profile_picture'] : '';
			$row['created_at'] = convterUTCtoLocal($row['created_at'],"UTC");
			
			$row['total_members'] = $this->db->select('member_id')->from('team_members')->where(array('team_id'=>$row['team_id'],'status!='=>'1'))->get()->num_rows();
			$row['total_invites'] = $this->db->select('invite_id')->from('team_invites')->where(array('team_id'=>$row['team_id'],'status'=>1))->get()->num_rows();
			$row['total_declined'] = $this->db->select('invite_id')->from('team_invites')->where(array('team_id'=>$row['team_id'],'status'=>0))->get()->num_rows();
			$data[$key] = $row;
		}
		return $data;
	}
	## Add New Team
	public function new_team($input){
		$check = $this->db->select('team_name')->from('teams')->where(array('team_name'=>$input['team_name']))->get()->row();
		if($check==''){
			$input['is_active']		=	1;
			$input['team_coverphoto'] = "";
			if(isset($input['invite_userids'])){
				$invitedusers = $input['invite_userids'];
			}else{
				$invitedusers = array();
			}
			unset($input['invite_userids']);
			unset($input['submit']);
			$config['allowed_types']=	'*';
			//$config['max_size'] 	=	'200';
			$image_data = "";
			$date = date("Y-m");
			if (!empty($_FILES['team_coverphoto']['name'])){
				$path	=  './media/teams/team_coverphoto/'.$date."/";
				if (!is_dir($path)){
					mkdir($path , 0777);
					$content = "<!DOCTYPE html><html><head><title>403 Forbidden</title></head><body><p>Access denied!</p></body></html>"; 
					$fp = fopen($path."index.html","wb"); 
					fwrite($fp,$content); 
					fclose($fp); 
				}
				$config['file_name'] = date("Ymd")."_TCP-".rand(1000,9999).'-'.time();
				$config['upload_path'] = $path;
				$this->upload->initialize($config);
				if (!$this->upload->do_upload('team_coverphoto')){
					return array('status' =>400,'msg' =>"Uploading error: ".strip_tags($this->upload->display_errors()));
					exit;
				}else{
					$image_data = $this->upload->data();
					
					$config['image_library'] = 'gd2';
					$config['source_image'] = $path.$image_data['file_name']; //get original image
					$config['maintain_ratio'] = TRUE;
					$config['create_thumb'] = false;
					$config['height'] = 150;
					$this->load->library('image_lib', $config);
					if (!$this->image_lib->resize()) {
						$file = $path.$image_data['file_name'];
						if (file_exists($file)) {
							unlink($file);
						}
						return array('status' =>400,'msg' =>"Image resizing error: ".strip_tags($this->image_lib->display_errors()));
						exit;
					}else{
						$input['team_coverphoto'] = ltrim($path,"./").$image_data['file_name'];
					}
				}
			}
			$this->db->trans_start();
			$this->db->insert('teams',$input);
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return array('status' => 500,'msg' =>'Internal server error.');
			} else {
				$teamid = $this->db->insert_id();
				$this->db->insert('team_members',array('team_id'=>$teamid,'user_id'=>$input['user_id'],'team_owner'=>$input['user_id'],'member_type'=>'2'));
				for($i=0;$i<count($invitedusers);$i++){
					$this->db->insert('team_invites',array('team_id'=>$teamid,'user_id'=>$invitedusers[$i],'inviter_id'=>$input['user_id']));
				}
				$this->db->trans_commit();
				return array('status' => 200,'msg' =>'Team created and invites are sent successfully.');
			}
		}else{
			return array('status' => 400,'msg' =>'Team name already in list');
		}
	}
	### View Team
	public function view_team($id){
		$data = $this->db->select('*')->from('teams')->where(array('team_id'=>$id))->get()->row_array();
		if (count($data)){
			$data['team_coverphoto'] =  $data['team_coverphoto'] ? base_url().$data['team_coverphoto'] : '';
			return array('edit'=>$data,'status' => 200,'msg' =>'');
		}else{
			return array('status' => 400,'msg' =>'Team not found for update!');
		}
	}
	## Update Team data
	public function update_team($input){
		
		$check = $this->db->select('team_id')->from('teams')->where(array('team_name'=>$input['team_name'],'team_id!='=>$input['team_id']))->get()->row();
		if($check==''){
			unset($input['submit']);
			$input['team_coverphoto'] = getTeamdata($input['team_id'],'team_coverphoto');
			if (!empty($_FILES['team_coverphoto']['name'])){
				$config['allowed_types']=	'*';
				$date = date("Y-m");
				$path	=  './media/teams/team_coverphoto/'.$date."/";
				if (!is_dir($path)){
					mkdir($path , 0777);
					$content = "<!DOCTYPE html><html><head><title>403 Forbidden</title></head><body><p>Access denied!</p></body></html>"; 
					$fp = fopen($path."index.html","wb"); 
					fwrite($fp,$content); 
					fclose($fp); 
				}
				$config['file_name'] = date("Ymd")."_TCP-".rand(1000,9999).'-'.time();
				$config['upload_path'] = $path;
				$this->upload->initialize($config);
				if (!$this->upload->do_upload('team_coverphoto')){
					return array('status' =>400,'msg' =>"Uploading error: ".strip_tags($this->upload->display_errors()));
					exit;
				}else{
					$image_data = $this->upload->data();
					$config['image_library'] = 'gd2';
					$config['source_image'] = $path.$image_data['file_name']; //get original image
					$config['maintain_ratio'] = TRUE;
					$config['create_thumb'] = false;
					$config['height'] = 150;
					$this->load->library('image_lib', $config);
					if (!$this->image_lib->resize()) {
						$file = $path.$image_data['file_name'];
						if (file_exists($file)) {
							unlink($file);
						}
						return array('status' =>400,'msg' =>"Image resizing error: ".strip_tags($this->image_lib->display_errors()));
						exit;
					}else{
						if (file_exists('./'.$input['team_coverphoto'])){
							unlink('./'.$input['team_coverphoto']);
						}
						$input['team_coverphoto'] = ltrim($path,"./").$image_data['file_name'];
					}
				}
			}
			
			$this->db->trans_start();
			$this->db->where('team_id',$input['team_id'])->update('teams',array('team_name'=>$input['team_name'],'briefing_validity'=>$input['briefing_validity'],'team_coverphoto'=>$input['team_coverphoto'],'updated_at'=>today()[0]));
			
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return array('status' => 500,'msg' => 'Internal server error. Please try again!');
			}else{
				$this->db->trans_commit();
				return array('status' => 200,'msg' =>'Team updated!');
			}	
		}else{
			return array('status' => 400,'msg' =>'Team name already in list!');
		}
	}
	
	## View Team Members
	public function team_members($team_id){
		
		if (check_valid_teamId($team_id)){
			
			$data = $this->db->select('team_members.member_id, team_members.team_id, team_members.user_id, team_members.team_owner, team_members.member_type, team_members.joined_at, users.user_name, users.email,users.designation, users.profile_picture')->from('team_members')->WHERE(array('team_id'=>$team_id,'status!='=>'1'))->join('users','users.user_id = team_members.user_id')->order_by('team_members.member_type','DESC')->order_by('team_members.member_id', 'desc' )->get()->result_array();
			foreach( $data as $key => $row )
			{
				$row['designation'] 	=  getDesignationdata($row['designation'],'designation');
				$row['profile_picture']!=''  ? $row['profile_picture']	= base_url(). $row['profile_picture']: '';
				$data[$key] = $row;
			}
			return array('status' => 200,'msg' => '','data'=>$data);
		}else{
			return array('status' => 400,'msg'=>'Team not exist');
		}
	}
	## View Team Invites
	public function team_invites($team_id){
		
		if (check_valid_teamId($team_id)){
			
			$data = $this->db->select('team_invites.invite_id,team_invites.team_id,team_invites.user_id, team_invites.invited_on, users.user_name, users.email,users.designation , users.profile_picture')->from('team_invites')->WHERE(array('team_invites.team_id'=>$team_id,'team_invites.status'=>1))->join('users','users.user_id = team_invites.user_id')->order_by('team_invites.invite_id','DESC')->get()->result_array();
			foreach( $data as $key => $row )
			{
				$row['designation'] 	=  getDesignationdata($row['designation'],'designation');
				$row['profile_picture']!=''  ? $row['profile_picture']	= base_url(). $row['profile_picture']: '';
				$data[$key] = $row;
			}
			return array('status' => 200,'msg' => '','data'=>$data);
		}else{
			return array('status' => 400,'msg'=>'Team not exist');
		}
	}
	## View Team Invites declines list
	public function team_invites_decline_list($team_id){
		
		if (check_valid_teamId($team_id)){
			
			$data = $this->db->select('team_invites.invite_id,team_invites.team_id,team_invites.user_id, team_invites.invited_on, users.user_name, users.email,users.designation , users.profile_picture')->from('team_invites')->WHERE(array('team_invites.team_id'=>$team_id,'team_invites.status'=>0))->join('users','users.user_id = team_invites.user_id')->order_by('team_invites.invite_id','DESC')->get()->result_array();
			foreach( $data as $key => $row )
			{
				$row['designation'] 	=  getDesignationdata($row['designation'],'designation');
				$row['profile_picture']!=''  ? $row['profile_picture']	= base_url(). $row['profile_picture']: '';
				$data[$key] = $row;
			}
			return array('status' => 200,'msg' => '','data'=>$data);
		}else{
			return array('status' => 400,'msg'=>'Team not exist');
		}
	}
	## Invite Members
	public function invite_members($input){
		if (check_valid_team($input['team_id'],$input['user_id'])){
			$invitedusers = $input['invite_userids'];
			for($i=0;$i<count($invitedusers);$i++){
				$invited_id = $invitedusers[$i];
				$fields = array('team_id'=>$input['team_id'],'user_id'=>$invited_id,'inviter_id'=>$input['user_id']);
				$this->db->insert('team_invites',$fields);
			}
			return array('status' => 200,'msg' =>'Invitation sent.');
		}else{
			return array('status' =>400,'msg' =>'Access denied!');
		}
	}
	
}