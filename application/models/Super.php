<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Super extends CI_Model{
	
	public function get_analytics(){
		# Total Companies
		$total_companies = $this->db->select('company_id')->from('companies')->where(array('is_active'=>1))->get()->num_rows();
		# Total Users
		$total_users = $this->db->select('user_id')->from('users')->where(array('is_active'=>1))->get()->num_rows();
		# Active user with in 60 days
		$sixty_days_before = date('Y-m-d H:i:s', strtotime('-60 days'));
		$total_active_users = $this->db->select('user_id')->from('users')->where(array('last_login >'=>$sixty_days_before))->get()->num_rows();
		# New user registration with in last 7 days
		$this_week = date('Y-m-d H:i:s', strtotime('-7 days'));
		$new_users = $this->db->select('user_id')->from('users')->where(array('registered_at>'=>$this_week))->get()->num_rows();
		# Company account registration requests
		$account_requests = $this->db->select('request_id')->from('company_account_requests')->get()->num_rows();
		# Total News channel
		$total_newchannel = $this->db->select('channel_id')->from('news_channel')->where(array('is_active'=>1))->get()->num_rows();
		#Total Ttalk Teams
		$total_ttalks = $this->db->select('team_id')->from('teams')->where(array('is_active'=>1))->get()->num_rows();
		
		return array('total_company'=>$total_companies,'total_users'=>$total_users,'active_users'=>$total_active_users,'new_requests'=>$account_requests,'total_newchannel'=>$total_newchannel,'total_ttalks'=>$total_ttalks);
	}
	
	public function users_chart(){
		$user_graph = $this->db->select("count(user_id) as total, DATE_FORMAT(registered_at, '%Y-%m-01') as date,DATE_FORMAT(registered_at, '%Y') as year,DATE_FORMAT(registered_at, '%m') as month")->from('users')->group_by("DATE_FORMAT(registered_at, '%Y-%m-01')")->order_by("year","ASC")->order_by("month","asc")->get()->result_array();
		return $user_graph;
		
	}
	
}