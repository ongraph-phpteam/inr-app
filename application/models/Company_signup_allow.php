<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Company_signup_allow extends CI_Model {
	
	public function select_signupAllowed(){
		$company_id = $this->session->userdata()['userdata']['company_id'];
		$data = $this->db->select('*')->from('signup_allowed')->where(array('company_id'=>$company_id))->order_by('allowed_id','DESC')->get()->result_array();
		return $data;
	}
	
	public function add_emailids($input){
		$company_id = $this->session->userdata()['userdata']['company_id'];
		$user_id = $this->session->userdata()['userdata']['user_id'];
		if(!empty($input['email'])){
			$emails = explode(',',$input['email']);
			$message =  array('status' => 400,'msg' =>'Please enter a valid email id.');
			for($i=0;$i<count($emails);$i++){
				$email = $emails[$i];
				if (valid_email($email)){
					$check = $this->db->select("allowed_id")->from("signup_allowed")->where(array('email'=>$email))->get()->num_rows();
					$this->db->trans_start();
					if($check){
						$this->db->where(array('email'=>$email))->update('signup_allowed',array('invited_at'=>today()[0]));
					}else{
						$this->db->insert('signup_allowed',array('email'=>$email,'company_id'=>$company_id,'invited_by'=>$user_id,'invited_at'=>today()[0],'is_active'=>'1'));
					}
					if ($this->db->trans_status() === FALSE){
						$this->db->trans_rollback();
						$message =  array('status' => 500,'msg' => 'Internal server error. Please try again!');
						break;
					}else{
						$this->db->trans_commit();
						$message =  array('status' => 200,'msg' =>'Email added successfully.');
					}
				}else{
					$message =  array('status' => 400,'msg' =>"<u>".$email."</u> is not a valid email format. Data not inserted after this email id. Please do correction.");
					break;
				}
			}
			return $message;
		}else{
			if ($_FILES['csv']['name']!="" || $_FILES['csv']['name']!=null){   
				$path	=  './media/csv/';
				$config['allowed_types']= '*';
				$config['file_name']	= $company_id;
				$config['upload_path']	= $path;
				$this->upload->initialize($config);
				if (!$this->upload->do_upload('csv')){
					return array('status' =>400,'msg' =>"Profile picture uploading error: ".strip_tags($this->upload->display_errors()));
					exit;
				}else{
					$csv_file = $path.$this->upload->data()['file_name'];
					$message = array('status' => 400,'msg' =>"File permission is not accessible!");
				
					if ($this->csvimport->get_array($csv_file)) {
						$csv_array = $this->csvimport->get_array($csv_file);
						foreach ($csv_array as $row) {
							$email = $row['email'];
							if (valid_email($email)){
								$check = $this->db->select("allowed_id")->from("signup_allowed")->where(array('email'=>$email))->get()->num_rows();
								$this->db->trans_start();
								if($check){
									$this->db->where(array('email'=>$email))->update('signup_allowed',array('invited_at'=>today()[0]));
								}else{
									$this->db->insert('signup_allowed',array('email'=>$email,'company_id'=>$company_id,'invited_by'=>$user_id,'invited_at'=>today()[0],'is_active'=>'1'));
								}
								if ($this->db->trans_status() === FALSE){
									$this->db->trans_rollback();
									$message =  array('status' => 500,'msg' => 'Internal server error. Please try again!');
									break;
								}else{
									$this->db->trans_commit();
									$message =  array('status' => 200,'msg' =>'Email added successfully.');
								}
							}else{
								$message =  array('status' => 400,'msg' =>"<u>".$email."</u> is not a valid email format. Data not inserted after this email id. Please do correction.");
								break;
							}
						}
					}
					unlink($csv_file);
					return $message;
				}
			}else{
				array('status' => 400,'msg' =>'Please select csv file for upload!');
			}
		}
	}
	public function view_userprofile($id){
		$data = $this->db->select('*')->from('users')->where(array('user_id'=>$id))->get()->row_array();
		if (count($data)){
			return array('data'=>$data,'status' => 200,'msg' =>'');
			
		}else{
			return array('status' => 400,'msg' =>'User account not exist!');
		}
	}
	
	public function edit_userprofile($input){
		
		$check = $this->db->select('email')->from('users')->where(array('email'=>$input['email'],'user_id!='=>$input['user_id']))->get()->row();
			if($check==''){
				$check = $this->db->select('user_id')->from('users')->where(array('mobile_number'=>$input['mobile_number'],'country_code'=>$input['country_code']))->where(array('user_id!='=>$input['user_id']))->get()->num_rows();
				if ($check){
					return array('status' => 400,'msg' =>'Account already registered with '.$input['mobile_number'])." mobile number";
				}else{
					unset($input['submit']);
					$input['profile_picture']	= getusersdata($input['user_id'],'profile_picture');
					$config['allowed_types'] 	= '*';
					$config['max_size'] 		= '500';
					$date 						= date("Y-m");
					
					if (!empty($_FILES['profile_picture']['name'])){
						$path	=  './media/profile/'.$date."/";
						if (!is_dir($path)){
							mkdir($path , 0777);
							$content = "<!DOCTYPE html><html><head><title>403 Forbidden</title></head><body><p>Access denied!</p></body></html>"; 
							$fp = fopen($path."index.html","wb"); 
							fwrite($fp,$content); 
							fclose($fp); 
						}
						$config['file_name']	= date("Ymd")."_PP-".rand(1000,9999).'-'.time();
						$config['upload_path']	= $path;
						$this->upload->initialize($config);
						if (!$this->upload->do_upload('profile_picture')){
							return array('status' =>400,'msg' =>"Profile picture uploading error: ".strip_tags($this->upload->display_errors()));
							exit;
						}else{
							$input['profile_picture'] = ltrim($path,"./").$this->upload->data()['file_name'];
						}
					}
					$this->db->trans_start();
					$this->db->where(array('user_id'=>$input['user_id']))->update('users',array('user_name'=>$input['user_name'],'profile_picture'=>$input['profile_picture'],'designation'=>$input['designation'],'email'=>$input['email'],'country_code'=>$input['country_code'],'mobile_number'=>$input['mobile_number'],'updated_at'=>today()[0]));
					if ($this->db->trans_status() === FALSE){
						$this->db->trans_rollback();
						return array('status' => 500,'msg' => 'Internal server error. Please try again!');
					}else{
						$this->db->trans_commit();
						return array('status' => 200,'msg' =>'User account is updated.');
					}
				}					
			}else{
				return array('status' => 400,'msg' =>'Account already registered with '.$input['email']);
			}
	}
	
}