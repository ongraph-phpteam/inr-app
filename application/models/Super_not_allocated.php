<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Super_not_allocated extends CI_Model {
	
	public function select_allusers(){
		$company_id = $this->session->userdata()['userdata']['company_id'];
		$user_id = $this->session->userdata()['userdata']['user_id'];
		$data = $this->db->query('SELECT users.user_id,my_cards.* FROM `users` JOIN my_cards ON users.user_id=my_cards.user_id AND my_cards.master_card_id=0 WHERE users.company_id=0')->result_array();
		return $data;
	}
	
	
}