<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ADMIN_Controller extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->secret = $this->config->item('encryption_key');
		$this->Aes_encryption = new Aes_encryption();
		//$this->load->model('Company');

		$this->userdata = $this->session->userdata('userdata');
		
		$this->session->set_flashdata('segment', explode('/', $this->uri->uri_string()));
		if ($this->session->userdata('status') != 'ADMIN') {
			redirect(base_url().'company');
		}
	}

	public function load_view($template_name, $vars = array(), $return = FALSE)
	{
		
		if($return):
			$content  = $this->load->view('company/header', $vars, $return);
			$content  = $this->load->view('company/navbar', $vars, $return);
			$content  = $this->load->view('company/'.$template_name, $vars, $return);
			$content = $this->load->view('company/footer', $vars, $return);
			return $content;
		else:
			$content  = $this->load->view('company/header', $vars, $return);
			$content  = $this->load->view('company/navbar', $vars, $return);
			$content  = $this->load->view('company/'.$template_name, $vars, $return);
			$content  = $this->load->view('company/footer', $vars, $return);
		endif;
    }
	
	
	
	public function updateProfil() {
		if ($this->userdata != '') {
			$data = $this->Company->select($this->userdata->id);

			$this->session->set_userdata('userdata', $data);
			$this->userdata = $this->session->userdata('userdata');
		}
	}
	
	
	
}

/* End of file MY_Auth.php */
/* Location: ./application/core/MY_Auth.php */