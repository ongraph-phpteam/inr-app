<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SUPER_Controller extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->secret = $this->config->item('encryption_key');
		$this->Aes_encryption = new Aes_encryption();
		
		$this->userdata = $this->session->userdata('superdata');
		$this->session->set_flashdata('segment', explode('/', $this->uri->uri_string()));
		if ($this->session->userdata('superstatus') != 'SUPER') {
			redirect(base_url().'admin');
		}
	}

	public function load_view($template_name, $vars = array(), $return = FALSE)
	{
		
		if($return):
			$content  = $this->load->view('admin/header', $vars, $return);
			$content  = $this->load->view('admin/navbar', $vars, $return);
			$content  = $this->load->view('admin/'.$template_name, $vars, $return);
			$content  = $this->load->view('admin/footer', $vars, $return);
			return $content;
		else:
			$content  = $this->load->view('admin/header', $vars, $return);
			$content  = $this->load->view('admin/navbar', $vars, $return);
			$content  = $this->load->view('admin/'.$template_name, $vars, $return);
			$content  = $this->load->view('admin/footer', $vars, $return);
		endif;
    }
	
	
	
	
}

/* End of file MY_Auth.php */
/* Location: ./application/core/MY_Auth.php */