<?php
defined('BASEPATH') OR exit('No direct script access allowed');

function json_output($statusHeader,$response)
{
	$ci =& get_instance();
	$ci->output->set_content_type('application/json');
	$ci->output->set_status_header($statusHeader);
	$ci->output->set_output(json_encode($response));
}

function check_required($array)
{
	$requried = "";
	foreach($array as $key => $value) {
		if ($value==''){
			$requried .= $key.',';
		}
	}
	if($requried!=""){
		return rtrim($requried,',');
	}else{
		return false;
	}
}

function today(){
	$date = array();
	$date[0] = date('Y-m-d H:i:s', time());
	$date[1] = strtotime(date('Y-m-d H:i:s', time()));
	return $date;
}

//function for get domain
function getDomain($email){
	$break=explode('@',$email);
	$domain=@$break[1];
	return $domain;
}//end

// Check valid company Email
function check_valid_domain_help($email){
	$CI =& get_instance();
	$check = $CI->db->select('company_id')->from('companies')->WHERE (array('domain'=>getDomain($email),'is_active'=>'1','approval_status'=>'1'))->get()->result_array();
	if(count($check)){
		return $check[0]['company_id'];
	}else{
		return 0;
	}
}
// Get users security Answers
function getUsersSecurityAnswers($user_id,$question_id = 0){
	$CI =& get_instance();
	if($question_id){
		$data = $CI->db->select('answer_id')->from('security_answers')->WHERE (array('user_id'=>$user_id,'question_id'=>$question_id))->get()->result_array();
		if(count($data)){
			return $data[0]['answer_id'];
		}else{
			return 0;
		}
	}else{
		$data = $CI->db->select('answer_id, question_id, answer')->from('security_answers')->WHERE (array('user_id'=>$user_id))->get()->result_array();
		return $data;
	}
	
}



// Check User belongs to company
function check_valid_users_company($company_id,$user_id){
	$CI =& get_instance();
	$check = $CI->db->select('company_id')->from('users')->WHERE (array('company_id'=>$company_id,'user_id'=>$user_id))->get()->result_array();
	if(count($check)){
		return $check[0]['company_id'];
	}else{
		return 0;
	}
}
//Check Designation anything
function getcompanyanything($company_id,$whatever){
	$CI =& get_instance();
	$check = $CI->db->select($whatever)->from('companies')->WHERE(array('company_id'=>$company_id))->get()->row();
	if(count($check)){
		return $check->$whatever;
	}else{
		return "";
	}
}
//Check Designation anything
function getDesignationdata($designation_id,$whatever){
	$CI =& get_instance();
	$check = $CI->db->select($whatever)->from('designations')->WHERE(array('designation_id'=>$designation_id))->get()->row();
	if(count($check)){
		return $check->$whatever;
	}else{
		return "";
	}
}
// Check valid Card id
function check_valid_cardid($card_id){
	$CI =& get_instance();
	$check = $CI->db->select('user_id')->from('my_cards')->WHERE(array('card_id'=>$card_id,'is_active'=>'1'))->get()->result_array();
	if(count($check)){
		return $check[0]['user_id'];
	}else{
		return 0;
	}
}

//Check Card already in Rolodex
function check_CardAlreadyInRolodex($card_id,$user_id){
	$CI =& get_instance();
	$check = $CI->db->select('rolodex_id')->from('rolodex')->WHERE(array('card_id'=>$card_id,'user_id'=>$user_id))->get()->result_array();
	if(count($check)){
		return $check[0]['rolodex_id'];
	}else{
		return 0;
	}
}
//Check valid rolodex id
function check_valid_rolodex_id($rolodex_id,$user_id){
	$CI =& get_instance();
	$check = $CI->db->select('rolodex_id')->from('rolodex')->WHERE(array('rolodex_id'=>$rolodex_id,'user_id'=>$user_id))->get()->result_array();
	if(count($check)){
		return $check[0]['rolodex_id'];
	}else{
		return 0;
	}
}
//Check valid noteid
function check_valid_noteid($noteid,$rolodex_id){
	$CI =& get_instance();
	$check = $CI->db->select('noteid')->from('rolodexcard_notes')->WHERE(array('noteid'=>$noteid,'rolodex_id'=>$rolodex_id))->get()->result_array();
	if(count($check)){
		return $check[0]['noteid'];
	}else{
		return 0;
	}
}

//Check Users anything
function getusersdata($user_id,$whatever){
	$CI =& get_instance();
	$check = $CI->db->select($whatever)->from('users')->WHERE(array('user_id'=>$user_id))->get()->row();
	if(count($check)){
		return $check->$whatever;
	}else{
		return "";
	}
}
//Check Number is registered Or not
function isNumberRegistered($mobile_number){
	$CI =& get_instance();
	$check = $CI->db->select('user_id')->from('users')->WHERE(array('mobile_number'=>$mobile_number))->get()->row();
	if(count($check)){
		return $check->user_id;
	}else{
		return "";
	}
}
//Check Valid Team ownership
function check_valid_team($team_id,$user_id){
	$CI =& get_instance();
	$check = $CI->db->select('team_id')->from('teams')->WHERE(array('team_id'=>$team_id,'user_id'=>$user_id))->get()->row();
	if(count($check)){
		return $check->team_id;
	}else{
		return 0;
	}
}
//Check Valid Team ownership
function check_valid_memeber($team_id,$user_id){
	$CI =& get_instance();
	$check = $CI->db->select('member_id')->from('team_members')->WHERE(array('team_id'=>$team_id,'user_id'=>$user_id))->get()->row();
	if(count($check)){
		return $check->member_id;
	}else{
		return 0;
	}
}
//Check Valid Team id
function check_valid_teamId($team_id){
	$CI =& get_instance();
	$check = $CI->db->select('team_id')->from('teams')->WHERE(array('team_id'=>$team_id,'is_active'=>'1'))->get()->row();
	if(count($check)){
		return $check->team_id;
	}else{
		return 0;
	}
}


//Get Teams anything
function getTeamdata($team_id,$whatever){
	$CI =& get_instance();
	$check = $CI->db->select($whatever)->from('teams')->WHERE(array('team_id'=>$team_id))->get()->row();
	if(count($check)){
		return $check->$whatever;
	}else{
		return "";
	}
}
//Check Valid Invites
function check_valid_invite($invite_id,$user_id){
	$CI =& get_instance();
	$check = $CI->db->select('team_id,inviter_id')->from('team_invites')->WHERE(array('invite_id'=>$invite_id,'user_id'=>$user_id))->get()->row();
	if(count($check)){
		return $check;
	}else{
		return 0;
	}
}
## Anything of ToDo
function getTodoData($todoid,$whatever){
	$CI =& get_instance();
	$check = $CI->db->select($whatever)->from('todolist')->WHERE(array('todoid'=>$todoid))->get()->row();
	if(count($check)){
		return $check->$whatever;
	}else{
		return "";
	}
}

//Check Valid todoid
function check_valid_todoid($todoid,$user_id){
	$CI =& get_instance();
	$check = $CI->db->select('todoid')->from('todolist')->WHERE(array('todoid'=>$todoid,'(accountability'=>$user_id))->or_where('assigned_by ='.$user_id.')')->get()->row();
	//print $CI->db->last_query(); EXIT;
	if(count($check)){
		return $check;
	}else{
		return 0;
	}
}
//Check Valid todoid admin
function check_valid_todoid_assigner($todoid,$user_id){
	$CI =& get_instance();
	$check = $CI->db->select('accountability')->from('todolist')->WHERE(array('todoid'=>$todoid,'assigned_by' =>$user_id))->get()->row();
	//print $CI->db->last_query(); EXIT;
	if($check != ""){
		return $check->accountability;
	}else{
		return 0;
	}
}

//convert UTC or server time to Local
function convterUTCtoLocal($datetime,$timezone,$read = 0){
	$timestamp = strtotime($datetime);
	$daylight_saving = false;
	$f =  gmt_to_local($timestamp, $timezone, $daylight_saving);
	date_default_timezone_set($timezone);
	if($read){
		$date =  date("Y-m-d G:i A",$f);
	}else{
		$date =  date("Y-m-d H:i:s",$f);
	}
	date_default_timezone_set('UTC');
	return $date;
}
// Get News Channel anything
function getNewsChannelData($channel_id,$whatever){
	$CI =& get_instance();
	
	$check = $CI->db->select($whatever)->from('news_channel')->WHERE(array('channel_id'=>$channel_id))->get()->row();
	if($check){
		return $check->$whatever;
	}else{
		return "";
	}
}
// Check valid Channel 
function checkValidChannel($channel_id){
	$CI =& get_instance();
	$check = $CI->db->select('channel_id')->from('news_channel')->WHERE(array('channel_id'=>$channel_id))->get()->row();
	if($check){
		return $check->channel_id;
	}else{
		return 0;
	}
}
// check valid Channel admin
function checkValidChannelAdmin($channel_id,$channel_adminid){
	$CI =& get_instance();
	$check = $CI->db->select('channel_id')->from('news_channel')->WHERE(array('channel_id'=>$channel_id,'channel_adminid'=>$channel_adminid,'is_active'=>'1'))->get()->num_rows();
	return $check;
}
// Get News Briefs anything
function getNewsBriefData($newsbrief_id,$whatever){
	$CI =& get_instance();
	$check = $CI->db->select($whatever)->from('news_brief')->WHERE(array('newsbrief_id'=>$newsbrief_id))->get()->row();
	if($check){
		return $check->$whatever;
	}else{
		return "";
	}
}

// check valid News Publisher
function checkValidNewsBrifPubisher($newsbrief_id,$user_id){
	$CI =& get_instance();
	$check = $CI->db->select('newsbrief_id')->from('news_brief')->WHERE(array('newsbrief_id'=>$newsbrief_id,'user_id'=>$user_id))->get()->num_rows();
	return $check;
}

// Get random password
function randomCode($length = 9, $add_dashes = false, $available_sets = 'hems'){
	$sets = array();
    if(strpos($available_sets, 'h') !== false)
        $sets[] = 'abcdefghjkmnpqrstuvwxyz';
    if(strpos($available_sets, 'e') !== false)
        $sets[] = 'ABCDEFGHJKMNPQRSTUVWXYZ';
    if(strpos($available_sets, 'm') !== false)
        $sets[] = '23456789';
    if(strpos($available_sets, 's') !== false)
        $sets[] = '@#$&*?';

    $all = '';
    $password = '';
    foreach($sets as $set)
    {
        $password .= $set[array_rand(str_split($set))];
        $all .= $set;
    }

    $all = str_split($all);
    for($i = 0; $i < $length - count($sets); $i++)
        $password .= $all[array_rand($all)];

    $password = str_shuffle($password);

    if(!$add_dashes)
        return $password;

    $dash_len = floor(sqrt($length));
    $dash_str = '';
    while(strlen($password) > $dash_len)
    {
        $dash_str .= substr($password, 0, $dash_len) . '-';
        $password = substr($password, $dash_len);
    }
    $dash_str .= $password;
    return $dash_str;
}

## check password patron 
## Rule At least : Length - 8 Character -  1 Number - 1 Upper - 1 Lower - 1 Special Character
function password_pattern_check($str) 
{
	if (strlen($str)<8){
		$status		= 400;
		$message	= "Password must be minimum 8 characters long and contain at least 1 uppercase, 1 lowercase, 1 number and 1 special character";
	}else{
		$error	=	"";
		if (!preg_match('#[0-9]#', $str)) {
			$error	=  $error." 1 number,";
		}
		if(!preg_match('#[A-Z]#', $str)){
			$error	=  $error." 1 uppercase character,";
		}
		if(!preg_match('#[a-z]#', $str)){
			$error	=  $error." 1 lowercase character,";
		}
		if(!preg_match("#[\~\`\!\@\#\$\%\^\&\*\(\)\_\-\+\=\{\}\[\\]\|\:\;\<\>\.\?\/\\\\]+#",$str)){
			$error	=  $error." 1 special character";
		}
		if ($error){
			$status		= 400;
			$message	= "Password must contain at least".rtrim($error,',');
		}else{
			$status		= 200;
			$message	= "Success";
		}
	}
	return array('status'=>$status,'message'=>$message);
}
// Get Thread id 

function getThreadId(){
	$args = func_get_args();
	$CI =& get_instance();
	if (count($args)){ // team chat
		if(count($args)==1){
			$check = $CI->db->select('thread_id')->from('ttalks')->WHERE(array('team_id'=>$args[0]))->get()->row();
			if($check){
				return $check->thread_id;
				exit;
			}
		}else if(count($args)==3){
			$check = $CI->db->select('thread_id')->from('ttalks')->WHERE('(sender_id='.$args[0].' and receiver_id='.$args[1].')')->or_where('(sender_id='.$args[1].' and receiver_id='.$args[0].')')->get()->row();
			if($check){
				return $check->thread_id;
				exit;
			}else{
				return '0';
				exit;
			}
			
		}else{
			$check = $CI->db->select('thread_id')->from('ttalks')->WHERE('(sender_id='.$args[0].' and receiver_id='.$args[1].')')->or_where('(sender_id='.$args[1].' and receiver_id='.$args[0].')')->get()->row();
			if($check){
				return $check->thread_id;
				exit;
			}
		}
		$check = $CI->db->select('max(thread_id) as thread_id')->from('ttalks')->get()->row();
		if($check){
			return ($check->thread_id+1);
		}else{
			return 1;
		}
		
		
	}else{// 1 to 1
		return 0;
	}
}
// Get sender or Receiver Id 
function getSenderOrReceiverId($thread_id,$user_id){
	$CI =& get_instance();
	$check = $CI->db->select('sender_id,receiver_id')->from('ttalks')->WHERE(array('thread_id'=>$thread_id))->get()->row();
	if ($check){
		if ($check->sender_id == $user_id){
			return $check->receiver_id;
		}else{
			return $check->sender_id;
		}
	}else{
		return '0';
	}
}

// function message unread count
function messageUnreadcount($thread_id,$user_id,$condition_id){ // type 1 for onetoone, 2 for brief chat
	$CI =& get_instance();
	if ($condition_id == 'o2o'){
		$count = $CI->db->select('ttalkid')->from('ttalks')->WHERE('thread_id='.$thread_id.' and receiver_id='.$user_id.' and is_read=0')->get()->num_rows();
		return (string)$count;
	}else{
		$count = $CI->db->select('unread_briefcount')->from('team_members')->WHERE('team_id='.$condition_id.' and user_id='.$user_id)->get()->row();
		return $count->unread_briefcount;
	}
}

## Function for get Template Images
function getTemplateImages($template_id,$company_id){
	$CI =& get_instance();
	$data = $CI->db->select('background_layer, front_layer')->from('card_templates')->WHERE(array('company_id'=>$company_id,'template_id'=>$template_id,'is_active'=>1))->get()->row_array();
	if(count($data)){
		$data['background_layer']	= base_url().$data['background_layer'];
		$data['front_layer']		= base_url().$data['front_layer'];
		return $data;
	}else{
		$data['background_layer']	= "";
		$data['front_layer']		="";
		return $data;
	}
	
}


// Number Suffix
function sufixNumber($number) {
    $ends = array('th','st','nd','rd','th','th','th','th','th','th');
    if ((($number % 100) >= 11) && (($number%100) <= 13)){
        return $number. 'th';
    }else{
        return $number. $ends[$number % 10];
	}
}
###### for WEB Section only
function helper_aesEncryption($key,$data,$encpt = true) {
	if ($encpt == true){
		if(16 !== strlen($key)) $key = hash('MD5', $key, true);
		$padding = 16 - (strlen($data) % 16);
		$data .= str_repeat(chr($padding), $padding);
		return base64_encode(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key, $data, MCRYPT_MODE_ECB, str_repeat("\0", 16))));
	}else{
		return json_decode($data,TRUE);
	}
}
function helper_aesDecryption($key, $data, $decpt = true) {
	if ($decpt== true){
		$data = base64_decode(base64_decode($data));
		if(16 !== strlen($key)) $key = hash('MD5', $key, true);
		$data = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key, $data, MCRYPT_MODE_ECB, str_repeat("\0", 16));
		$padding = ord($data[strlen($data) - 1]); 
		return substr($data, 0, -$padding); 
	}else{
		return $data;
	}
}

function helper_decryptPostCall($key,$data=array()){
	foreach ( $data as $index=>$value)
	{
		$data[$index] = $this->aesDecryption($key,$value);
	}
	return $data;
}

####### End web Section
