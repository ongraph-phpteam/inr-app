<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
// Super Admin Section START
$route['admin'] = 'admin/Auth/index';
$route['admin/login'] = 'admin/Auth/login';
$route['admin/dashboard'] = 'admin/Dashboard/index';
$route['admin/companies'] = 'admin/Companies/index';
$route['admin/newcompany'] = 'admin/Companies/newcompany';
$route['admin/updateinfo/(:any)'] = 'admin/Companies/updateinfo/$1';
$route['admin/designations'] = 'admin/Designations/index';
$route['admin/newdesignation'] = 'admin/Designations/newdesignation';
$route['admin/editdesignation/(:any)'] = 'admin/Designations/editdesignation/$1';
$route['admin/accountrequest'] = 'admin/Comapany_accountrequests/index';
$route['admin/viewrequest/(:any)'] = 'admin/Comapany_accountrequests/view_request/$1';
$route['admin/process_request/(:any)'] = 'admin/Comapany_accountrequests/process_request/$1';
$route['admin/questions'] = 'admin/Questions/index';
$route['admin/new_question'] = 'admin/Questions/new_question';
$route['admin/edit_question/(:any)'] = 'admin/Questions/edit_question/$1';
$route['admin/delete_designation/(:any)'] = 'admin/Designations/delete_designation/$1';
$route['admin/not_allocated'] = 'admin/Not_allocated/index';

// Super Admin Section End
// Company admin start 
$route['company'] = 'company/Auth/index';
$route['company/masquerade/(:any)'] = 'company/Auth/masquerade/$1';
$route['company/login'] = 'company/Auth/login';
$route['company/password_recover'] = 'company/Auth/password_recover';
$route['company/dashboard'] = 'company/Dashboard/index';
$route['company/users'] = 'company/Users/index';
$route['company/newuser'] = 'company/Users/newuser';
$route['company/editprofile/(:any)'] = 'company/Users/edit_userprofile/$1';
$route['company/active_newsbriefs'] = 'company/Active_newsbriefs/index';
$route['company/teams'] = 'company/Teams/index';
$route['company/new_team'] = 'company/Teams/new_team';
$route['company/update_team/(:any)'] = 'company/Teams/update_team/$1';
$route['company/team_members/(:any)'] = 'company/Teams/team_members/$l';
$route['company/team_invites/(:any)'] = 'company/Teams/team_invites/$l';
$route['company/team_invites_decline_list/(:any)'] = 'company/Teams/team_invites_decline_list/$l';
$route['company/invite_members/(:any)'] = 'company/Teams/invite_members/$l';
$route['company/news_channels'] = 'company/News_channels/index';
$route['company/new_channel'] = 'company/News_channels/new_channel';
$route['company/update_channel/(:any)'] = 'company/News_channels/update_channel/$1';
$route['company/channel_followers/(:any)'] = 'company/News_channels/channel_followers/$l';
$route['company/channel_newsbriefs/(:any)'] = 'company/News_channels/channel_newsbriefs/$l';
$route['company/new_channel'] = 'company/News_channels/new_channel';
$route['company/todolist'] = 'company/Todo/index';
$route['company/signup_allow'] = 'company/Signup_allow/index';
$route['company/add_emailids'] = 'company/Signup_allow/add_emailids';
$route['company/templates'] = 'company/Templates/index';
$route['company/new_template'] = 'company/Templates/new_template';
$route['company/update_template/(:any)'] = 'company/Templates/update_template/$l';


$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
