<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * App Name       -    	INR APP- CEECA
 * auther         -     Hem Thakur
 * created        -     17/12/2017 
**/
class Aes_encryption{

	public function aesEncryption($key,$data,$encpt = true) {
		if ($encpt == true){
			if(16 !== strlen($key)) $key = hash('MD5', $key, true);
			$padding = 16 - (strlen($data) % 16);
			$data .= str_repeat(chr($padding), $padding);
			return base64_encode(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key, $data, MCRYPT_MODE_ECB, str_repeat("\0", 16))));
		}else{
			return json_decode($data,TRUE);
		}
	}
	public function aesDecryption($key, $data, $decpt = true) {
		if ($decpt== true){
			$data = base64_decode(base64_decode($data));
			if(16 !== strlen($key)) $key = hash('MD5', $key, true);
			$data = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key, $data, MCRYPT_MODE_ECB, str_repeat("\0", 16));
			$padding = ord($data[strlen($data) - 1]); 
			return (substr($data, 0, -$padding)); 
		}else{
			return $data;
		}
	}
	
	public function decryptPostCall($key,$data=array()){
		foreach ( $data as $index=>$value)
		{
			$data[$index] = $this->aesDecryption($key,$value);
		}
		return $data;
	}
}