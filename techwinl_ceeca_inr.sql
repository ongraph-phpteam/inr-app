-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 30, 2018 at 05:58 AM
-- Server version: 5.6.38
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `techwinl_ceeca_inr`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(500) NOT NULL,
  `createdon` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `isactive` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `name`, `email`, `password`, `createdon`, `isactive`) VALUES
(1, 'Ceeca Amdin', 'super@ceeca.com', 'e10adc3949ba59abbe56e057f20f883e', '2017-12-06 13:53:37', 1);

-- --------------------------------------------------------

--
-- Table structure for table `card_templates`
--

CREATE TABLE `card_templates` (
  `template_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `background_layer` varchar(500) NOT NULL,
  `front_layer` varchar(500) NOT NULL,
  `added_by` tinyint(4) NOT NULL,
  `updated_at` datetime NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `card_templates`
--

INSERT INTO `card_templates` (`template_id`, `company_id`, `background_layer`, `front_layer`, `added_by`, `updated_at`, `is_active`) VALUES
(1, 1, 'media/templates/2018-03/20180309_BL-371190-1520597808.png', 'media/templates/2018-03/20180309_FL-147678-1520597808.png', 1, '2018-03-09 12:19:25', 1);

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

CREATE TABLE `companies` (
  `company_id` int(11) NOT NULL,
  `company_name` varchar(255) NOT NULL,
  `company_logo` varchar(255) NOT NULL,
  `domain` varchar(100) NOT NULL,
  `contact_person` varchar(50) NOT NULL,
  `contact_email` varchar(255) NOT NULL,
  `country_code` varchar(20) NOT NULL,
  `contact_phone` varchar(30) NOT NULL,
  `company_address` varchar(255) NOT NULL,
  `city` varchar(100) NOT NULL,
  `state` varchar(100) NOT NULL,
  `zipcode` varchar(20) NOT NULL,
  `country` varchar(100) NOT NULL,
  `registered_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `registeration_type` int(11) NOT NULL COMMENT '0 for norml',
  `approval_status` int(11) NOT NULL COMMENT '0 for not approved, 1 for approved',
  `last_modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_active` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `companies`
--

INSERT INTO `companies` (`company_id`, `company_name`, `company_logo`, `domain`, `contact_person`, `contact_email`, `country_code`, `contact_phone`, `company_address`, `city`, `state`, `zipcode`, `country`, `registered_at`, `registeration_type`, `approval_status`, `last_modified_at`, `is_active`) VALUES
(1, 'Ceeca', 'media/company/logo/2017-12/ceeca.png', 'gmail.com', 'Daniel Janouschek', 'ceecaadmin@gmail.com', '', '7898646598', 'Plot 10', 'Chandigarh', 'Chandigarh', '160017', 'India', '2017-10-03 17:49:29', 0, 1, '2017-10-03 12:19:29', 1);

-- --------------------------------------------------------

--
-- Table structure for table `company_account_requests`
--

CREATE TABLE `company_account_requests` (
  `request_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `card_id` int(11) NOT NULL,
  `empoloyees_count` int(11) NOT NULL,
  `request_date` datetime NOT NULL,
  `isread` tinyint(4) NOT NULL COMMENT '0 for not read 1 for read'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `company_admin`
--

CREATE TABLE `company_admin` (
  `id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(500) NOT NULL,
  `password` varchar(500) NOT NULL,
  `profile_picture` varchar(500) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_active` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company_admin`
--

INSERT INTO `company_admin` (`id`, `company_id`, `name`, `email`, `password`, `profile_picture`, `created_at`, `updated_at`, `is_active`) VALUES
(1, 1, 'Daniel Janouschek', 'dani@gmail.com', '202cb962ac59075b964b07152d234b70', '', '2017-12-08 17:41:19', '2017-12-08 17:41:19', 1);

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `countries_id` int(11) NOT NULL,
  `countries_name` varchar(64) NOT NULL DEFAULT '',
  `countries_iso_code` varchar(2) NOT NULL,
  `country_code` varchar(7) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code`, `country_code`) VALUES
(1, 'Afghanistan', 'AF', '93'),
(2, 'Albania', 'AL', '355'),
(3, 'Algeria', 'DZ', '213'),
(4, 'American Samoa', 'AS', '1-684'),
(5, 'Andorra', 'AD', '376'),
(6, 'Angola', 'AO', '244'),
(7, 'Anguilla', 'AI', '1-264'),
(8, 'Antarctica', 'AQ', '672'),
(9, 'Antigua and Barbuda', 'AG', '1-268'),
(10, 'Argentina', 'AR', '54'),
(11, 'Armenia', 'AM', '374'),
(12, 'Aruba', 'AW', '297'),
(13, 'Australia', 'AU', '61'),
(14, 'Austria', 'AT', '43'),
(15, 'Azerbaijan', 'AZ', '994'),
(16, 'Bahamas', 'BS', '1-242'),
(17, 'Bahrain', 'BH', '973'),
(18, 'Bangladesh', 'BD', '880'),
(19, 'Barbados', 'BB', '1-246'),
(20, 'Belarus', 'BY', '375'),
(21, 'Belgium', 'BE', '32'),
(22, 'Belize', 'BZ', '501'),
(23, 'Benin', 'BJ', '229'),
(24, 'Bermuda', 'BM', '1-441'),
(25, 'Bhutan', 'BT', '975'),
(26, 'Bolivia', 'BO', '591'),
(27, 'Bosnia and Herzegowina', 'BA', '387'),
(28, 'Botswana', 'BW', '267'),
(29, 'Bouvet Island', 'BV', '47'),
(30, 'Brazil', 'BR', '55'),
(31, 'British Indian Ocean Territory', 'IO', '246'),
(32, 'Brunei Darussalam', 'BN', '673'),
(33, 'Bulgaria', 'BG', '359'),
(34, 'Burkina Faso', 'BF', '226'),
(35, 'Burundi', 'BI', '257'),
(36, 'Cambodia', 'KH', '855'),
(37, 'Cameroon', 'CM', '237'),
(38, 'Canada', 'CA', '1'),
(39, 'Cape Verde', 'CV', '238'),
(40, 'Cayman Islands', 'KY', '1-345'),
(41, 'Central African Republic', 'CF', '236'),
(42, 'Chad', 'TD', '235'),
(43, 'Chile', 'CL', '56'),
(44, 'China', 'CN', '86'),
(45, 'Christmas Island', 'CX', '61'),
(46, 'Cocos (Keeling) Islands', 'CC', '61'),
(47, 'Colombia', 'CO', '57'),
(48, 'Comoros', 'KM', '269'),
(49, 'Congo Democratic Republic of', 'CG', '242'),
(50, 'Cook Islands', 'CK', '682'),
(51, 'Costa Rica', 'CR', '506'),
(52, 'Cote D\'Ivoire', 'CI', '225'),
(53, 'Croatia', 'HR', '385'),
(54, 'Cuba', 'CU', '53'),
(55, 'Cyprus', 'CY', '357'),
(56, 'Czech Republic', 'CZ', '420'),
(57, 'Denmark', 'DK', '45'),
(58, 'Djibouti', 'DJ', '253'),
(59, 'Dominica', 'DM', '1-767'),
(60, 'Dominican Republic', 'DO', '1-809'),
(61, 'Timor-Leste', 'TL', '670'),
(62, 'Ecuador', 'EC', '593'),
(63, 'Egypt', 'EG', '20'),
(64, 'El Salvador', 'SV', '503'),
(65, 'Equatorial Guinea', 'GQ', '240'),
(66, 'Eritrea', 'ER', '291'),
(67, 'Estonia', 'EE', '372'),
(68, 'Ethiopia', 'ET', '251'),
(69, 'Falkland Islands (Malvinas)', 'FK', '500'),
(70, 'Faroe Islands', 'FO', '298'),
(71, 'Fiji', 'FJ', '679'),
(72, 'Finland', 'FI', '358'),
(73, 'France', 'FR', '33'),
(75, 'French Guiana', 'GF', '594'),
(76, 'French Polynesia', 'PF', '689'),
(77, 'French Southern Territories', 'TF', '262'),
(78, 'Gabon', 'GA', '241'),
(79, 'Gambia', 'GM', '220'),
(80, 'Georgia', 'GE', '995'),
(81, 'Germany', 'DE', '49'),
(82, 'Ghana', 'GH', '233'),
(83, 'Gibraltar', 'GI', '350'),
(84, 'Greece', 'GR', '30'),
(85, 'Greenland', 'GL', '299'),
(86, 'Grenada', 'GD', '1-473'),
(87, 'Guadeloupe', 'GP', '590'),
(88, 'Guam', 'GU', '1-671'),
(89, 'Guatemala', 'GT', '502'),
(90, 'Guinea', 'GN', '224'),
(91, 'Guinea-bissau', 'GW', '245'),
(92, 'Guyana', 'GY', '592'),
(93, 'Haiti', 'HT', '509'),
(94, 'Heard Island and McDonald Islands', 'HM', '011'),
(95, 'Honduras', 'HN', '504'),
(96, 'Hong Kong', 'HK', '852'),
(97, 'Hungary', 'HU', '36'),
(98, 'Iceland', 'IS', '354'),
(99, 'India', 'IN', '91'),
(100, 'Indonesia', 'ID', '62'),
(101, 'Iran (Islamic Republic of)', 'IR', '98'),
(102, 'Iraq', 'IQ', '964'),
(103, 'Ireland', 'IE', '353'),
(104, 'Israel', 'IL', '972'),
(105, 'Italy', 'IT', '39'),
(106, 'Jamaica', 'JM', '1-876'),
(107, 'Japan', 'JP', '81'),
(108, 'Jordan', 'JO', '962'),
(109, 'Kazakhstan', 'KZ', '7'),
(110, 'Kenya', 'KE', '254'),
(111, 'Kiribati', 'KI', '686'),
(112, 'Korea, Democratic People\'s Republic of', 'KP', '850'),
(113, 'South Korea', 'KR', '82'),
(114, 'Kuwait', 'KW', '965'),
(115, 'Kyrgyzstan', 'KG', '996'),
(116, 'Lao People\'s Democratic Republic', 'LA', '856'),
(117, 'Latvia', 'LV', '371'),
(118, 'Lebanon', 'LB', '961'),
(119, 'Lesotho', 'LS', '266'),
(120, 'Liberia', 'LR', '231'),
(121, 'Libya', 'LY', '218'),
(122, 'Liechtenstein', 'LI', '423'),
(123, 'Lithuania', 'LT', '370'),
(124, 'Luxembourg', 'LU', '352'),
(125, 'Macao', 'MO', '853'),
(126, 'Macedonia, The Former Yugoslav Republic of', 'MK', '389'),
(127, 'Madagascar', 'MG', '261'),
(128, 'Malawi', 'MW', '265'),
(129, 'Malaysia', 'MY', '60'),
(130, 'Maldives', 'MV', '960'),
(131, 'Mali', 'ML', '223'),
(132, 'Malta', 'MT', '356'),
(133, 'Marshall Islands', 'MH', '692'),
(134, 'Martinique', 'MQ', '596'),
(135, 'Mauritania', 'MR', '222'),
(136, 'Mauritius', 'MU', '230'),
(137, 'Mayotte', 'YT', '262'),
(138, 'Mexico', 'MX', '52'),
(139, 'Micronesia, Federated States of', 'FM', '691'),
(140, 'Moldova', 'MD', '373'),
(141, 'Monaco', 'MC', '377'),
(142, 'Mongolia', 'MN', '976'),
(143, 'Montserrat', 'MS', '1-664'),
(144, 'Morocco', 'MA', '212'),
(145, 'Mozambique', 'MZ', '258'),
(146, 'Myanmar', 'MM', '95'),
(147, 'Namibia', 'NA', '264'),
(148, 'Nauru', 'NR', '674'),
(149, 'Nepal', 'NP', '977'),
(150, 'Netherlands', 'NL', '31'),
(151, 'Netherlands Antilles', 'AN', '599'),
(152, 'New Caledonia', 'NC', '687	'),
(153, 'New Zealand', 'NZ', '64'),
(154, 'Nicaragua', 'NI', '505'),
(155, 'Niger', 'NE', '227'),
(156, 'Nigeria', 'NG', '234'),
(157, 'Niue', 'NU', '683'),
(158, 'Norfolk Island', 'NF', '672'),
(159, 'Northern Mariana Islands', 'MP', '1-670'),
(160, 'Norway', 'NO', '47'),
(161, 'Oman', 'OM', '968'),
(162, 'Pakistan', 'PK', '92'),
(163, 'Palau', 'PW', '680'),
(164, 'Panama', 'PA', '507'),
(165, 'Papua New Guinea', 'PG', '675'),
(166, 'Paraguay', 'PY', '595'),
(167, 'Peru', 'PE', '51'),
(168, 'Philippines', 'PH', '63'),
(169, 'Pitcairn', 'PN', '64'),
(170, 'Poland', 'PL', '48'),
(171, 'Portugal', 'PT', '351'),
(172, 'Puerto Rico', 'PR', '1-787'),
(173, 'Qatar', 'QA', '974'),
(174, 'Reunion', 'RE', '262'),
(175, 'Romania', 'RO', '40'),
(176, 'Russian Federation', 'RU', '7'),
(177, 'Rwanda', 'RW', '250'),
(178, 'Saint Kitts and Nevis', 'KN', '1-869'),
(179, 'Saint Lucia', 'LC', '1-758'),
(180, 'Saint Vincent and the Grenadines', 'VC', '1-784'),
(181, 'Samoa', 'WS', '685'),
(182, 'San Marino', 'SM', '378'),
(183, 'Sao Tome and Principe', 'ST', '239'),
(184, 'Saudi Arabia', 'SA', '966'),
(185, 'Senegal', 'SN', '221'),
(186, 'Seychelles', 'SC', '248'),
(187, 'Sierra Leone', 'SL', '232'),
(188, 'Singapore', 'SG', '65'),
(189, 'Slovakia (Slovak Republic)', 'SK', '421'),
(190, 'Slovenia', 'SI', '386'),
(191, 'Solomon Islands', 'SB', '677'),
(192, 'Somalia', 'SO', '252'),
(193, 'South Africa', 'ZA', '27'),
(194, 'South Georgia and the South Sandwich Islands', 'GS', '500'),
(195, 'Spain', 'ES', '34'),
(196, 'Sri Lanka', 'LK', '94'),
(197, 'Saint Helena, Ascension and Tristan da Cunha', 'SH', '290'),
(198, 'St. Pierre and Miquelon', 'PM', '508'),
(199, 'Sudan', 'SD', '249'),
(200, 'Suriname', 'SR', '597'),
(201, 'Svalbard and Jan Mayen Islands', 'SJ', '47'),
(202, 'Swaziland', 'SZ', '268'),
(203, 'Sweden', 'SE', '46'),
(204, 'Switzerland', 'CH', '41'),
(205, 'Syrian Arab Republic', 'SY', '963'),
(206, 'Taiwan', 'TW', '886'),
(207, 'Tajikistan', 'TJ', '992'),
(208, 'Tanzania, United Republic of', 'TZ', '255'),
(209, 'Thailand', 'TH', '66'),
(210, 'Togo', 'TG', '228'),
(211, 'Tokelau', 'TK', '690'),
(212, 'Tonga', 'TO', '676'),
(213, 'Trinidad and Tobago', 'TT', '1-868'),
(214, 'Tunisia', 'TN', '216'),
(215, 'Turkey', 'TR', '90'),
(216, 'Turkmenistan', 'TM', '993'),
(217, 'Turks and Caicos Islands', 'TC', '1-649'),
(218, 'Tuvalu', 'TV', '688'),
(219, 'Uganda', 'UG', '256'),
(220, 'Ukraine', 'UA', '380'),
(221, 'United Arab Emirates', 'AE', '971'),
(222, 'United Kingdom', 'GB', '44'),
(223, 'United States', 'US', '1'),
(224, 'United States Minor Outlying Islands', 'UM', '246'),
(225, 'Uruguay', 'UY', '598'),
(226, 'Uzbekistan', 'UZ', '998'),
(227, 'Vanuatu', 'VU', '678'),
(228, 'Vatican City State (Holy See)', 'VA', '379'),
(229, 'Venezuela', 'VE', '58'),
(230, 'Vietnam', 'VN', '84'),
(231, 'Virgin Islands (British)', 'VG', '1-284'),
(232, 'Virgin Islands (U.S.)', 'VI', '1-340'),
(233, 'Wallis and Futuna Islands', 'WF', '681'),
(234, 'Western Sahara', 'EH', '212'),
(235, 'Yemen', 'YE', '967'),
(236, 'Serbia', 'RS', '381'),
(238, 'Zambia', 'ZM', '260'),
(239, 'Zimbabwe', 'ZW', '263'),
(240, 'Aaland Islands', 'AX', '358'),
(241, 'Palestine', 'PS', '970'),
(242, 'Montenegro', 'ME', '382'),
(243, 'Guernsey', 'GG', '44-1481'),
(244, 'Isle of Man', 'IM', '44-1624'),
(245, 'Jersey', 'JE', '44-1534'),
(247, 'Curaçao', 'CW', '599'),
(248, 'Ivory Coast', 'CI', '225'),
(249, 'Kosovo', 'XK', '383');

-- --------------------------------------------------------

--
-- Table structure for table `countries_timezone`
--

CREATE TABLE `countries_timezone` (
  `zone_id` int(10) NOT NULL,
  `country_code` char(2) COLLATE utf8_bin NOT NULL,
  `zone_name` varchar(35) COLLATE utf8_bin NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `countries_timezone`
--

INSERT INTO `countries_timezone` (`zone_id`, `country_code`, `zone_name`) VALUES
(1, 'AD', 'Europe/Andorra'),
(2, 'AE', 'Asia/Dubai'),
(3, 'AF', 'Asia/Kabul'),
(4, 'AG', 'America/Antigua'),
(5, 'AI', 'America/Anguilla'),
(6, 'AL', 'Europe/Tirane'),
(7, 'AM', 'Asia/Yerevan'),
(8, 'AO', 'Africa/Luanda'),
(9, 'AQ', 'Antarctica/McMurdo'),
(10, 'AQ', 'Antarctica/Casey'),
(11, 'AQ', 'Antarctica/Davis'),
(12, 'AQ', 'Antarctica/DumontDUrville'),
(13, 'AQ', 'Antarctica/Mawson'),
(14, 'AQ', 'Antarctica/Palmer'),
(15, 'AQ', 'Antarctica/Rothera'),
(16, 'AQ', 'Antarctica/Syowa'),
(17, 'AQ', 'Antarctica/Troll'),
(18, 'AQ', 'Antarctica/Vostok'),
(19, 'AR', 'America/Argentina/Buenos_Aires'),
(20, 'AR', 'America/Argentina/Cordoba'),
(21, 'AR', 'America/Argentina/Salta'),
(22, 'AR', 'America/Argentina/Jujuy'),
(23, 'AR', 'America/Argentina/Tucuman'),
(24, 'AR', 'America/Argentina/Catamarca'),
(25, 'AR', 'America/Argentina/La_Rioja'),
(26, 'AR', 'America/Argentina/San_Juan'),
(27, 'AR', 'America/Argentina/Mendoza'),
(28, 'AR', 'America/Argentina/San_Luis'),
(29, 'AR', 'America/Argentina/Rio_Gallegos'),
(30, 'AR', 'America/Argentina/Ushuaia'),
(31, 'AS', 'Pacific/Pago_Pago'),
(32, 'AT', 'Europe/Vienna'),
(33, 'AU', 'Australia/Lord_Howe'),
(34, 'AU', 'Antarctica/Macquarie'),
(35, 'AU', 'Australia/Hobart'),
(36, 'AU', 'Australia/Currie'),
(37, 'AU', 'Australia/Melbourne'),
(38, 'AU', 'Australia/Sydney'),
(39, 'AU', 'Australia/Broken_Hill'),
(40, 'AU', 'Australia/Brisbane'),
(41, 'AU', 'Australia/Lindeman'),
(42, 'AU', 'Australia/Adelaide'),
(43, 'AU', 'Australia/Darwin'),
(44, 'AU', 'Australia/Perth'),
(45, 'AU', 'Australia/Eucla'),
(46, 'AW', 'America/Aruba'),
(47, 'AX', 'Europe/Mariehamn'),
(48, 'AZ', 'Asia/Baku'),
(49, 'BA', 'Europe/Sarajevo'),
(50, 'BB', 'America/Barbados'),
(51, 'BD', 'Asia/Dhaka'),
(52, 'BE', 'Europe/Brussels'),
(53, 'BF', 'Africa/Ouagadougou'),
(54, 'BG', 'Europe/Sofia'),
(55, 'BH', 'Asia/Bahrain'),
(56, 'BI', 'Africa/Bujumbura'),
(57, 'BJ', 'Africa/Porto-Novo'),
(58, 'BL', 'America/St_Barthelemy'),
(59, 'BM', 'Atlantic/Bermuda'),
(60, 'BN', 'Asia/Brunei'),
(61, 'BO', 'America/La_Paz'),
(62, 'BQ', 'America/Kralendijk'),
(63, 'BR', 'America/Noronha'),
(64, 'BR', 'America/Belem'),
(65, 'BR', 'America/Fortaleza'),
(66, 'BR', 'America/Recife'),
(67, 'BR', 'America/Araguaina'),
(68, 'BR', 'America/Maceio'),
(69, 'BR', 'America/Bahia'),
(70, 'BR', 'America/Sao_Paulo'),
(71, 'BR', 'America/Campo_Grande'),
(72, 'BR', 'America/Cuiaba'),
(73, 'BR', 'America/Santarem'),
(74, 'BR', 'America/Porto_Velho'),
(75, 'BR', 'America/Boa_Vista'),
(76, 'BR', 'America/Manaus'),
(77, 'BR', 'America/Eirunepe'),
(78, 'BR', 'America/Rio_Branco'),
(79, 'BS', 'America/Nassau'),
(80, 'BT', 'Asia/Thimphu'),
(81, 'BW', 'Africa/Gaborone'),
(82, 'BY', 'Europe/Minsk'),
(83, 'BZ', 'America/Belize'),
(84, 'CA', 'America/St_Johns'),
(85, 'CA', 'America/Halifax'),
(86, 'CA', 'America/Glace_Bay'),
(87, 'CA', 'America/Moncton'),
(88, 'CA', 'America/Goose_Bay'),
(89, 'CA', 'America/Blanc-Sablon'),
(90, 'CA', 'America/Toronto'),
(91, 'CA', 'America/Nipigon'),
(92, 'CA', 'America/Thunder_Bay'),
(93, 'CA', 'America/Iqaluit'),
(94, 'CA', 'America/Pangnirtung'),
(95, 'CA', 'America/Atikokan'),
(96, 'CA', 'America/Winnipeg'),
(97, 'CA', 'America/Rainy_River'),
(98, 'CA', 'America/Resolute'),
(99, 'CA', 'America/Rankin_Inlet'),
(100, 'CA', 'America/Regina'),
(101, 'CA', 'America/Swift_Current'),
(102, 'CA', 'America/Edmonton'),
(103, 'CA', 'America/Cambridge_Bay'),
(104, 'CA', 'America/Yellowknife'),
(105, 'CA', 'America/Inuvik'),
(106, 'CA', 'America/Creston'),
(107, 'CA', 'America/Dawson_Creek'),
(108, 'CA', 'America/Fort_Nelson'),
(109, 'CA', 'America/Vancouver'),
(110, 'CA', 'America/Whitehorse'),
(111, 'CA', 'America/Dawson'),
(112, 'CC', 'Indian/Cocos'),
(113, 'CD', 'Africa/Kinshasa'),
(114, 'CD', 'Africa/Lubumbashi'),
(115, 'CF', 'Africa/Bangui'),
(116, 'CG', 'Africa/Brazzaville'),
(117, 'CH', 'Europe/Zurich'),
(118, 'CI', 'Africa/Abidjan'),
(119, 'CK', 'Pacific/Rarotonga'),
(120, 'CL', 'America/Santiago'),
(121, 'CL', 'America/Punta_Arenas'),
(122, 'CL', 'Pacific/Easter'),
(123, 'CM', 'Africa/Douala'),
(124, 'CN', 'Asia/Shanghai'),
(125, 'CN', 'Asia/Urumqi'),
(126, 'CO', 'America/Bogota'),
(127, 'CR', 'America/Costa_Rica'),
(128, 'CU', 'America/Havana'),
(129, 'CV', 'Atlantic/Cape_Verde'),
(130, 'CW', 'America/Curacao'),
(131, 'CX', 'Indian/Christmas'),
(132, 'CY', 'Asia/Nicosia'),
(133, 'CY', 'Asia/Famagusta'),
(134, 'CZ', 'Europe/Prague'),
(135, 'DE', 'Europe/Berlin'),
(136, 'DE', 'Europe/Busingen'),
(137, 'DJ', 'Africa/Djibouti'),
(138, 'DK', 'Europe/Copenhagen'),
(139, 'DM', 'America/Dominica'),
(140, 'DO', 'America/Santo_Domingo'),
(141, 'DZ', 'Africa/Algiers'),
(142, 'EC', 'America/Guayaquil'),
(143, 'EC', 'Pacific/Galapagos'),
(144, 'EE', 'Europe/Tallinn'),
(145, 'EG', 'Africa/Cairo'),
(146, 'EH', 'Africa/El_Aaiun'),
(147, 'ER', 'Africa/Asmara'),
(148, 'ES', 'Europe/Madrid'),
(149, 'ES', 'Africa/Ceuta'),
(150, 'ES', 'Atlantic/Canary'),
(151, 'ET', 'Africa/Addis_Ababa'),
(152, 'FI', 'Europe/Helsinki'),
(153, 'FJ', 'Pacific/Fiji'),
(154, 'FK', 'Atlantic/Stanley'),
(155, 'FM', 'Pacific/Chuuk'),
(156, 'FM', 'Pacific/Pohnpei'),
(157, 'FM', 'Pacific/Kosrae'),
(158, 'FO', 'Atlantic/Faroe'),
(159, 'FR', 'Europe/Paris'),
(160, 'GA', 'Africa/Libreville'),
(161, 'GB', 'Europe/London'),
(162, 'GD', 'America/Grenada'),
(163, 'GE', 'Asia/Tbilisi'),
(164, 'GF', 'America/Cayenne'),
(165, 'GG', 'Europe/Guernsey'),
(166, 'GH', 'Africa/Accra'),
(167, 'GI', 'Europe/Gibraltar'),
(168, 'GL', 'America/Godthab'),
(169, 'GL', 'America/Danmarkshavn'),
(170, 'GL', 'America/Scoresbysund'),
(171, 'GL', 'America/Thule'),
(172, 'GM', 'Africa/Banjul'),
(173, 'GN', 'Africa/Conakry'),
(174, 'GP', 'America/Guadeloupe'),
(175, 'GQ', 'Africa/Malabo'),
(176, 'GR', 'Europe/Athens'),
(177, 'GS', 'Atlantic/South_Georgia'),
(178, 'GT', 'America/Guatemala'),
(179, 'GU', 'Pacific/Guam'),
(180, 'GW', 'Africa/Bissau'),
(181, 'GY', 'America/Guyana'),
(182, 'HK', 'Asia/Hong_Kong'),
(183, 'HN', 'America/Tegucigalpa'),
(184, 'HR', 'Europe/Zagreb'),
(185, 'HT', 'America/Port-au-Prince'),
(186, 'HU', 'Europe/Budapest'),
(187, 'ID', 'Asia/Jakarta'),
(188, 'ID', 'Asia/Pontianak'),
(189, 'ID', 'Asia/Makassar'),
(190, 'ID', 'Asia/Jayapura'),
(191, 'IE', 'Europe/Dublin'),
(192, 'IL', 'Asia/Jerusalem'),
(193, 'IM', 'Europe/Isle_of_Man'),
(194, 'IN', 'Asia/Kolkata'),
(195, 'IO', 'Indian/Chagos'),
(196, 'IQ', 'Asia/Baghdad'),
(197, 'IR', 'Asia/Tehran'),
(198, 'IS', 'Atlantic/Reykjavik'),
(199, 'IT', 'Europe/Rome'),
(200, 'JE', 'Europe/Jersey'),
(201, 'JM', 'America/Jamaica'),
(202, 'JO', 'Asia/Amman'),
(203, 'JP', 'Asia/Tokyo'),
(204, 'KE', 'Africa/Nairobi'),
(205, 'KG', 'Asia/Bishkek'),
(206, 'KH', 'Asia/Phnom_Penh'),
(207, 'KI', 'Pacific/Tarawa'),
(208, 'KI', 'Pacific/Enderbury'),
(209, 'KI', 'Pacific/Kiritimati'),
(210, 'KM', 'Indian/Comoro'),
(211, 'KN', 'America/St_Kitts'),
(212, 'KP', 'Asia/Pyongyang'),
(213, 'KR', 'Asia/Seoul'),
(214, 'KW', 'Asia/Kuwait'),
(215, 'KY', 'America/Cayman'),
(216, 'KZ', 'Asia/Almaty'),
(217, 'KZ', 'Asia/Qyzylorda'),
(218, 'KZ', 'Asia/Aqtobe'),
(219, 'KZ', 'Asia/Aqtau'),
(220, 'KZ', 'Asia/Atyrau'),
(221, 'KZ', 'Asia/Oral'),
(222, 'LA', 'Asia/Vientiane'),
(223, 'LB', 'Asia/Beirut'),
(224, 'LC', 'America/St_Lucia'),
(225, 'LI', 'Europe/Vaduz'),
(226, 'LK', 'Asia/Colombo'),
(227, 'LR', 'Africa/Monrovia'),
(228, 'LS', 'Africa/Maseru'),
(229, 'LT', 'Europe/Vilnius'),
(230, 'LU', 'Europe/Luxembourg'),
(231, 'LV', 'Europe/Riga'),
(232, 'LY', 'Africa/Tripoli'),
(233, 'MA', 'Africa/Casablanca'),
(234, 'MC', 'Europe/Monaco'),
(235, 'MD', 'Europe/Chisinau'),
(236, 'ME', 'Europe/Podgorica'),
(237, 'MF', 'America/Marigot'),
(238, 'MG', 'Indian/Antananarivo'),
(239, 'MH', 'Pacific/Majuro'),
(240, 'MH', 'Pacific/Kwajalein'),
(241, 'MK', 'Europe/Skopje'),
(242, 'ML', 'Africa/Bamako'),
(243, 'MM', 'Asia/Yangon'),
(244, 'MN', 'Asia/Ulaanbaatar'),
(245, 'MN', 'Asia/Hovd'),
(246, 'MN', 'Asia/Choibalsan'),
(247, 'MO', 'Asia/Macau'),
(248, 'MP', 'Pacific/Saipan'),
(249, 'MQ', 'America/Martinique'),
(250, 'MR', 'Africa/Nouakchott'),
(251, 'MS', 'America/Montserrat'),
(252, 'MT', 'Europe/Malta'),
(253, 'MU', 'Indian/Mauritius'),
(254, 'MV', 'Indian/Maldives'),
(255, 'MW', 'Africa/Blantyre'),
(256, 'MX', 'America/Mexico_City'),
(257, 'MX', 'America/Cancun'),
(258, 'MX', 'America/Merida'),
(259, 'MX', 'America/Monterrey'),
(260, 'MX', 'America/Matamoros'),
(261, 'MX', 'America/Mazatlan'),
(262, 'MX', 'America/Chihuahua'),
(263, 'MX', 'America/Ojinaga'),
(264, 'MX', 'America/Hermosillo'),
(265, 'MX', 'America/Tijuana'),
(266, 'MX', 'America/Bahia_Banderas'),
(267, 'MY', 'Asia/Kuala_Lumpur'),
(268, 'MY', 'Asia/Kuching'),
(269, 'MZ', 'Africa/Maputo'),
(270, 'NA', 'Africa/Windhoek'),
(271, 'NC', 'Pacific/Noumea'),
(272, 'NE', 'Africa/Niamey'),
(273, 'NF', 'Pacific/Norfolk'),
(274, 'NG', 'Africa/Lagos'),
(275, 'NI', 'America/Managua'),
(276, 'NL', 'Europe/Amsterdam'),
(277, 'NO', 'Europe/Oslo'),
(278, 'NP', 'Asia/Kathmandu'),
(279, 'NR', 'Pacific/Nauru'),
(280, 'NU', 'Pacific/Niue'),
(281, 'NZ', 'Pacific/Auckland'),
(282, 'NZ', 'Pacific/Chatham'),
(283, 'OM', 'Asia/Muscat'),
(284, 'PA', 'America/Panama'),
(285, 'PE', 'America/Lima'),
(286, 'PF', 'Pacific/Tahiti'),
(287, 'PF', 'Pacific/Marquesas'),
(288, 'PF', 'Pacific/Gambier'),
(289, 'PG', 'Pacific/Port_Moresby'),
(290, 'PG', 'Pacific/Bougainville'),
(291, 'PH', 'Asia/Manila'),
(292, 'PK', 'Asia/Karachi'),
(293, 'PL', 'Europe/Warsaw'),
(294, 'PM', 'America/Miquelon'),
(295, 'PN', 'Pacific/Pitcairn'),
(296, 'PR', 'America/Puerto_Rico'),
(297, 'PS', 'Asia/Gaza'),
(298, 'PS', 'Asia/Hebron'),
(299, 'PT', 'Europe/Lisbon'),
(300, 'PT', 'Atlantic/Madeira'),
(301, 'PT', 'Atlantic/Azores'),
(302, 'PW', 'Pacific/Palau'),
(303, 'PY', 'America/Asuncion'),
(304, 'QA', 'Asia/Qatar'),
(305, 'RE', 'Indian/Reunion'),
(306, 'RO', 'Europe/Bucharest'),
(307, 'RS', 'Europe/Belgrade'),
(308, 'RU', 'Europe/Kaliningrad'),
(309, 'RU', 'Europe/Moscow'),
(310, 'RU', 'Europe/Simferopol'),
(311, 'RU', 'Europe/Volgograd'),
(312, 'RU', 'Europe/Kirov'),
(313, 'RU', 'Europe/Astrakhan'),
(314, 'RU', 'Europe/Saratov'),
(315, 'RU', 'Europe/Ulyanovsk'),
(316, 'RU', 'Europe/Samara'),
(317, 'RU', 'Asia/Yekaterinburg'),
(318, 'RU', 'Asia/Omsk'),
(319, 'RU', 'Asia/Novosibirsk'),
(320, 'RU', 'Asia/Barnaul'),
(321, 'RU', 'Asia/Tomsk'),
(322, 'RU', 'Asia/Novokuznetsk'),
(323, 'RU', 'Asia/Krasnoyarsk'),
(324, 'RU', 'Asia/Irkutsk'),
(325, 'RU', 'Asia/Chita'),
(326, 'RU', 'Asia/Yakutsk'),
(327, 'RU', 'Asia/Khandyga'),
(328, 'RU', 'Asia/Vladivostok'),
(329, 'RU', 'Asia/Ust-Nera'),
(330, 'RU', 'Asia/Magadan'),
(331, 'RU', 'Asia/Sakhalin'),
(332, 'RU', 'Asia/Srednekolymsk'),
(333, 'RU', 'Asia/Kamchatka'),
(334, 'RU', 'Asia/Anadyr'),
(335, 'RW', 'Africa/Kigali'),
(336, 'SA', 'Asia/Riyadh'),
(337, 'SB', 'Pacific/Guadalcanal'),
(338, 'SC', 'Indian/Mahe'),
(339, 'SD', 'Africa/Khartoum'),
(340, 'SE', 'Europe/Stockholm'),
(341, 'SG', 'Asia/Singapore'),
(342, 'SH', 'Atlantic/St_Helena'),
(343, 'SI', 'Europe/Ljubljana'),
(344, 'SJ', 'Arctic/Longyearbyen'),
(345, 'SK', 'Europe/Bratislava'),
(346, 'SL', 'Africa/Freetown'),
(347, 'SM', 'Europe/San_Marino'),
(348, 'SN', 'Africa/Dakar'),
(349, 'SO', 'Africa/Mogadishu'),
(350, 'SR', 'America/Paramaribo'),
(351, 'SS', 'Africa/Juba'),
(352, 'ST', 'Africa/Sao_Tome'),
(353, 'SV', 'America/El_Salvador'),
(354, 'SX', 'America/Lower_Princes'),
(355, 'SY', 'Asia/Damascus'),
(356, 'SZ', 'Africa/Mbabane'),
(357, 'TC', 'America/Grand_Turk'),
(358, 'TD', 'Africa/Ndjamena'),
(359, 'TF', 'Indian/Kerguelen'),
(360, 'TG', 'Africa/Lome'),
(361, 'TH', 'Asia/Bangkok'),
(362, 'TJ', 'Asia/Dushanbe'),
(363, 'TK', 'Pacific/Fakaofo'),
(364, 'TL', 'Asia/Dili'),
(365, 'TM', 'Asia/Ashgabat'),
(366, 'TN', 'Africa/Tunis'),
(367, 'TO', 'Pacific/Tongatapu'),
(368, 'TR', 'Europe/Istanbul'),
(369, 'TT', 'America/Port_of_Spain'),
(370, 'TV', 'Pacific/Funafuti'),
(371, 'TW', 'Asia/Taipei'),
(372, 'TZ', 'Africa/Dar_es_Salaam'),
(373, 'UA', 'Europe/Kiev'),
(374, 'UA', 'Europe/Uzhgorod'),
(375, 'UA', 'Europe/Zaporozhye'),
(376, 'UG', 'Africa/Kampala'),
(377, 'UM', 'Pacific/Midway'),
(378, 'UM', 'Pacific/Wake'),
(379, 'US', 'America/New_York'),
(380, 'US', 'America/Detroit'),
(381, 'US', 'America/Kentucky/Louisville'),
(382, 'US', 'America/Kentucky/Monticello'),
(383, 'US', 'America/Indiana/Indianapolis'),
(384, 'US', 'America/Indiana/Vincennes'),
(385, 'US', 'America/Indiana/Winamac'),
(386, 'US', 'America/Indiana/Marengo'),
(387, 'US', 'America/Indiana/Petersburg'),
(388, 'US', 'America/Indiana/Vevay'),
(389, 'US', 'America/Chicago'),
(390, 'US', 'America/Indiana/Tell_City'),
(391, 'US', 'America/Indiana/Knox'),
(392, 'US', 'America/Menominee'),
(393, 'US', 'America/North_Dakota/Center'),
(394, 'US', 'America/North_Dakota/New_Salem'),
(395, 'US', 'America/North_Dakota/Beulah'),
(396, 'US', 'America/Denver'),
(397, 'US', 'America/Boise'),
(398, 'US', 'America/Phoenix'),
(399, 'US', 'America/Los_Angeles'),
(400, 'US', 'America/Anchorage'),
(401, 'US', 'America/Juneau'),
(402, 'US', 'America/Sitka'),
(403, 'US', 'America/Metlakatla'),
(404, 'US', 'America/Yakutat'),
(405, 'US', 'America/Nome'),
(406, 'US', 'America/Adak'),
(407, 'US', 'Pacific/Honolulu'),
(408, 'UY', 'America/Montevideo'),
(409, 'UZ', 'Asia/Samarkand'),
(410, 'UZ', 'Asia/Tashkent'),
(411, 'VA', 'Europe/Vatican'),
(412, 'VC', 'America/St_Vincent'),
(413, 'VE', 'America/Caracas'),
(414, 'VG', 'America/Tortola'),
(415, 'VI', 'America/St_Thomas'),
(416, 'VN', 'Asia/Ho_Chi_Minh'),
(417, 'VU', 'Pacific/Efate'),
(418, 'WF', 'Pacific/Wallis'),
(419, 'WS', 'Pacific/Apia'),
(420, 'YE', 'Asia/Aden'),
(421, 'YT', 'Indian/Mayotte'),
(422, 'ZA', 'Africa/Johannesburg'),
(423, 'ZM', 'Africa/Lusaka'),
(424, 'ZW', 'Africa/Harare');

-- --------------------------------------------------------

--
-- Table structure for table `designations`
--

CREATE TABLE `designations` (
  `designation_id` int(11) NOT NULL,
  `designation` varchar(255) NOT NULL,
  `company_id` int(11) NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1 active 0 not active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `designations`
--

INSERT INTO `designations` (`designation_id`, `designation`, `company_id`, `is_active`) VALUES
(1, 'CEO/GM', 0, 1),
(2, 'Board Level Executive', 0, 1),
(3, 'Executive Assistant', 0, 1),
(4, 'Finance', 0, 1),
(5, 'HR', 0, 1),
(6, 'Operations', 0, 1),
(7, 'IT', 0, 1),
(8, 'Strategy', 0, 1),
(9, 'Legal Services', 0, 1),
(10, 'Sales/Marketing', 0, 1),
(11, 'R&D', 0, 1),
(12, 'Public Relations', 0, 1),
(13, 'Plant Manager', 0, 1),
(14, 'Software developer', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `my_cards`
--

CREATE TABLE `my_cards` (
  `card_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `template_id` int(11) NOT NULL,
  `master_card_id` int(11) NOT NULL COMMENT '0 itself master',
  `is_cardpic_deleted` tinyint(4) NOT NULL COMMENT '0 not deleted, 1 deleted',
  `name` varchar(100) NOT NULL,
  `middle_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `title` varchar(150) NOT NULL,
  `position` varchar(150) NOT NULL,
  `card_picture` varchar(500) NOT NULL,
  `profile_pic` varchar(255) NOT NULL,
  `company_name` varchar(255) NOT NULL,
  `company_logo` varchar(255) NOT NULL,
  `postal` varchar(20) NOT NULL,
  `building_name` varchar(255) NOT NULL,
  `street_address` varchar(255) NOT NULL,
  `house_number` varchar(20) NOT NULL,
  `state` varchar(100) NOT NULL,
  `city` varchar(100) NOT NULL,
  `country` varchar(100) NOT NULL,
  `fax_number` varchar(50) NOT NULL,
  `office_number` varchar(50) NOT NULL,
  `mobile_number` varchar(50) NOT NULL,
  `fax_country_code` varchar(20) NOT NULL,
  `office_country_code` varchar(20) NOT NULL,
  `mobile_country_code` varchar(20) NOT NULL,
  `email_address` varchar(150) NOT NULL,
  `website` varchar(150) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `is_active` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `my_cards`
--

INSERT INTO `my_cards` (`card_id`, `user_id`, `template_id`, `master_card_id`, `is_cardpic_deleted`, `name`, `middle_name`, `last_name`, `title`, `position`, `card_picture`, `profile_pic`, `company_name`, `company_logo`, `postal`, `building_name`, `street_address`, `house_number`, `state`, `city`, `country`, `fax_number`, `office_number`, `mobile_number`, `fax_country_code`, `office_country_code`, `mobile_country_code`, `email_address`, `website`, `created_at`, `updated_at`, `is_active`) VALUES
(1, 6, 0, 0, 1, 'Simranjeet', 'Kaur', 'Dhillon', 'Mr', 'HR Manager', '', 'media/cards/profile_pic/2018-03/20180320_PP-1945-1521531295.png', 'Company ', 'media/cards/company_logo/2018-03/20180320_BC-3072-1521531295.png', '2323232', 'Desmond ', 'Park Street123', '', 'DSDSD', 'NY', 'Aruba', '3232322322', '(312) 212-11', '842763728', '+93', '21', '+32', 'simranjeet.techwinlabs@gmail.com', 'www.mywebsite.com', '2018-01-30 01:03:38', '2018-03-20 07:34:55', 1),
(6, 46, 0, 0, 0, 'arbvg', '', '', 'ghfd', 'ertewte', '', 'media/cards/profile_pic/2018-02/20180201_PP-5236-1517482891.png', 'fertew', 'media/cards/company_logo/2018-02/20180201_BC-3432-1517482891.png', '765765', 'uyuy', 'uygyug', 'uygyug', 'ghfhg', 'ghfhg', 'ghfh', '378399', '3783833', '73837873', '91', '91', '91', 'ertet@gg.hgj', 'www.dfgdsg.gf', '2018-02-01 04:01:31', '2018-02-01 04:01:31', 1),
(7, 46, 0, 0, 0, 'arbvg', '', '', 'ghfd', 'ertewte', 'media/cards/card_picture/2018-02/20180201_PP-7856-1517482923.png', '', 'fertew', 'media/cards/company_logo/2018-02/20180201_BC-9658-1517482923.png', '765765', 'uyuy', 'uygyug', 'uygyug', 'ghfhg', 'ghfhg', 'ghfh', '378399', '3783833', '73837873', '91', '91', '91', 'arvindpoonia01@gmail.com', 'www.dfgdsg.gf', '2018-02-01 04:02:03', '2018-02-06 06:14:49', 1),
(8, 45, 0, 0, 0, 'Vivek', 'Liladhar', 'Sannabhadti', 'Mr', 'Executive', '', 'media/cards/profile_pic/2018-02/20180218_PP-1488-1518974352.png', 'Inr Circle', 'media/cards/company_logo/2018-02/20180218_BC-1162-1518974352.png', '81369', '', 'Fallstrasse', 'Fallstrasse', 'Bayern', 'Bayern', 'Germany', '', '7807 2356', '15146748527', '', '49', '49', 'vsannabhadti@gmail.com', 'www.', '2018-02-04 06:30:24', '2018-03-18 15:14:42', 1),
(10, 48, 0, 0, 1, 'Chloé Liu', '', '', 'Wonderwoman', 'Couch And Master ', '', 'media/cards/profile_pic/2018-03/20180304_PP-1097-1520179403.png', 'Inr Circle', '', '81543', '', 'Freibadstraße', '11', '', 'Munich', 'Germany', '', '', '015251971915', '', '', '+49', 'Chloe.xin1@gmail.com', 'www.inr-circle.com', '2018-02-04 08:08:55', '2018-03-19 17:04:45', 1),
(13, 47, 0, 0, 0, 'Vivek', '', '', 'mr', 'exe', '', '', 'ggh', '', '5666', 'ggh', 'ggh', 'ggh', 'gghh', 'gghh', 'germany', '5566', '5555', '01528563', '358', '355', '93', 'ghud@gmail.com', 'www.fghh.com', '2018-02-04 08:54:22', '2018-02-04 08:54:22', 1),
(14, 47, 0, 0, 0, 'Vivek', '', '', 'mr', 'exe', 'media/cards/card_picture/2018-02/20180204_PP-1917-1517759703.png', '', 'ggh', '', '5666', 'ggh', 'ggh', 'ggh', 'gghh', 'gghh', 'germany', '5566', '5555', '01528563', '358', '355', '93', 'ghud@gmail.com', 'www.fghh.com', '2018-02-04 08:55:04', '2018-02-04 08:55:04', 1),
(15, 47, 0, 0, 1, 'Vivek Sannabhadti', '', '', 'mr', 'Executive', '', 'media/cards/profile_pic/2018-02/20180218_PP-6259-1518974296.png', 'Inr circle gmbh', 'media/cards/company_logo/2018-02/20180218_BC-8160-1518974296.png', '81369', 'kirche bldg', 'Fallstr 16', 'Fallstr 16', 'Bayern', 'Bayern', 'Germany', '(0)7807 2356', '(0)151 4674 8527', '(0)15237391088', '49', '49', '49', 'ghud@gmail.com', 'www.', '2018-02-04 08:55:47', '2018-03-04 09:05:04', 1),
(17, 42, 0, 0, 1, 'Daniel ', 'Jan', 'Ouschek', 'CEO ', '', '', 'media/cards/profile_pic/2018-03/20180321_PP-8377-1521660609.png', 'Other AG', 'media/cards/company_logo/2018-03/20180321_BC-5291-1521660609.png', '81543', '', '', '', 'Bavaria', 'München Schwäbing Münchner Freheit', 'France', '(323) 641-8300', '6 25 85 23 69', '1706177395', '+1', '+33', '+49', 'djanouschek@gmail.com', '', '2018-02-04 09:05:37', '2018-03-21 19:30:09', 1),
(25, 49, 0, 0, 1, 'Arvind', '', 'Punia', 'Developer', 'Employee', '', 'media/cards/profile_pic/2018-02/20180208_PP-2192-1518085644.png', 'teachwinlabs', 'media/cards/company_logo/2018-02/20180208_BC-6996-1518085644.png', '746736', 'old building', '108 pb', '108 pb', 'Punjab', 'Punjab', 'India', '35643564', '98945846', '981274919', '91', '91', '91', 'arvindpoonia.techwinlabs@gmail.com', 'www.techwinlabs.com', '2018-02-07 00:12:55', '2018-03-13 08:42:14', 1),
(27, 44, 0, 0, 0, 'Arvind kumar', '', '', 'it', 'developer', 'media/cards/card_picture/2018-02/20180209_PP-5781-1518179713.png', 'media/cards/profile_pic/2018-02/20180209_PP-2520-1518174344.png', 'abc infotech', 'media/cards/company_logo/2018-02/20180209_BC-2689-1518174344.png', '16879', 'old building', '13 st', '13 st', 'punjab', 'punjab', 'india', '4664646467', '4646645464', '8053380461', '93', '355', '91', 'arvind.smugchucks@gmail.com', 'www.abc.com', '2018-02-07 04:17:26', '2018-02-09 05:35:13', 1),
(29, 51, 0, 0, 0, 'jaswinder', '', '', 'Developer', 'Employee', 'media/cards/card_picture/2018-02/20180215_PP-7380-1518701603.png', 'media/cards/profile_pic/2018-02/20180214_PP-7491-1518599122.png', 'hsjsj', 'media/cards/company_logo/2018-02/20180214_BC-6537-1518599122.png', '150065', 'f-195', 'ajit singh nagar, mohali phase-8b', 'ajit singh nagar, mo', 'punjab', 'punjab', 'india', '59648', '17189654', '9465346171', '', '', '91', 'jaswinder.techwin@gmail.com', 'www.twchwinlabs.com', '2018-02-07 05:34:44', '2018-02-15 06:33:23', 1),
(47, 50, 0, 0, 0, 'HItesh', '', '', '', '', 'media/cards/card_picture/2018-02/20180221_PP-6018-1519213542.png', 'media/cards/profile_pic/2018-02/20180221_PP-4063-1519213543.png', 'Tech win', 'media/cards/company_logo/2018-02/20180221_BC-2504-1519213543.png', '', 'Ddsfdsfsd. sffsd Did Co', '', '', '', '', '', '', '', '', '', '', '', 'hitesh.techwin@gmail.com', '', '2018-02-08 03:36:14', '2018-02-21 04:45:43', 1),
(79, 54, 0, 0, 0, 'Rahul', '', '', 'Android', 'employee', '', 'media/cards/profile_pic/2018-02/20180215_PP-5337-1518674537.png', 'Techwin', 'media/cards/company_logo/2018-02/20180215_BC-1008-1518674537.png', '141106', 'F-195', 'Mohali', 'Mohali', 'Punjab', 'Punjab', 'India', '17256948', '17125694', '8729012848', '91', '91', '91', 'rahul2.techwinlabs@gmail.com', 'www.techwinlabs.com', '2018-02-14 23:02:17', '2018-02-20 05:00:07', 1),
(107, 55, 0, 0, 0, 'Neelam', '', '', 'Developer', 'Employee', 'media/cards/card_picture/2018-03/20180301_PP-8956-1519885795.png', 'media/cards/profile_pic/2018-03/20180301_PP-4826-1519885795.png', 'Techwinlabs', 'media/cards/company_logo/2018-03/20180301_BC-4731-1519885795.png', '160008', 'F195', 'Mohali', '', 'Punjab', 'Mohali', 'India', '(784) 569-548845', '1(728) 448-484', '875467875454', '', '', '', 'neelam.techwinlabs@gmail.com', 'www.techwinlabs.com', '2018-02-19 04:40:39', '2018-02-28 23:29:55', 1),
(145, 53, 0, 0, 0, 'iPhone X', 'New', 'Silver', 'Master Repräsentation ', '', '', 'media/cards/profile_pic/2018-03/20180318_PP-2025-1521387441.png', 'Otter Ag', 'media/cards/company_logo/2018-03/20180318_BC-6431-1521387441.png', '', '', '', '', '', 'München', 'Germany', '', '', '01746599158', '', '', '+49', 'inrtestiphonex@gmail.com', '', '2018-03-04 09:25:33', '2018-03-18 15:37:21', 1),
(146, 66, 0, 0, 1, 'Rahul', 'kumar', 'singh', 'Developer', 'Android', '', 'media/cards/profile_pic/2018-03/20180319_PP-7992-1521436738.png', 'Techwin labs', 'media/cards/company_logo/2018-03/20180322_BC-2159-1521711029.png', '150068', 'f-195', 'Mohali', 'Mohali', 'Punjab', 'Punjab', 'India', '17725695', '17723695', '8729012848', '91', '91', '91', 'rahul2.techwinlabs@gmail.com', 'www.techwinlabs.com', '2018-03-04 23:45:50', '2018-03-22 09:30:29', 1),
(170, 6, 0, 1, 1, 'Simranjeet Kaur Dhillon', '', '', 'Mr', 'HR Manager', '', 'media/cards/profile_pic/2018-03/20180308_PP-4638-1520503214.png', 'Company ', 'media/cards/company_logo/2018-03/20180308_BC-4719-1520503214.png', '2323232', 'Desmond ', 'Park Street123', '', 'DSDSD', 'NY', 'Aruba', '3232322322', '(312) 212-11', '842763728', '+93', '21', '+32', 'simranjeet.techwinlabs@gmail.com', 'www.mywebsite.com', '2018-03-09 13:43:09', '2018-03-09 13:43:09', 1),
(171, 53, 0, 145, 0, 'iPhone X', '', '', '', '', '', 'media/cards/profile_pic/2018-03/20180304_PP-6832-1520180732.png', 'Otter Ag', 'media/cards/company_logo/2018-03/20180304_BC-9277-1520180733.png', '', '', '', '', '', '', '', '', '', '', '', '', '', 'inrtestiphonex@gmail.com', '', '2018-03-11 22:10:37', '2018-03-11 22:10:37', 1),
(173, 74, 0, 0, 0, 'Kawal', 'Kang ', '', 'iOS Developer ', '', '', 'media/cards/profile_pic/2018-03/20180320_PP-5966-1521525793.png', 'Techwin', 'media/cards/company_logo/2018-03/20180320_BC-4902-1521525793.png', '', '', '', '', 'Punjab', 'Amritsarhsisksnsbwjwjs', '', '', '', '', '', '', '', 'kawal.techwinlabs@gmail.com', '', '2018-03-16 05:55:42', '2018-03-20 06:03:13', 1),
(174, 76, 0, 0, 1, 'pradeep', 'kumar', 'sur', 'developer', 'hr', '', 'media/cards/profile_pic/2018-03/20180320_PP-3093-1521531673.png', 'test', '', '4667', 'old building', 'stres', 'stres', 'punjab', 'punjab', 'india', '979797', '466797979', '8872944278', '91', '91', '91', 'pradeep.techwinlabs@gmail.com', 'www.bzbz.com', '2018-03-16 10:35:17', '2018-03-20 07:41:13', 1),
(175, 76, 0, 174, 0, 'pradeep', '', '', '', '', '', '', 'test', '', '', '', '', '', '', '', '', '', '', '8872944278', '', '', '91', 'pradeep.techwinlabs@gmail.com', 'www.', '2018-03-16 10:37:56', '2018-03-16 10:37:56', 1),
(176, 76, 0, 174, 0, 'pradeep', '', '', '', '', '', '', 'test', '', '', '', '', '', '', '', '', '', '', '8872944278', '', '', '91', 'pradeep.techwinlabs@gmail.com', 'www.', '2018-03-16 10:39:42', '2018-03-16 10:39:42', 1),
(177, 76, 0, 174, 0, 'pradeep', '', '', '', '', '', '', 'test', '', '', '', '', '', '', '', '', '', '', '8872944278', '', '', '91', 'pradeep.techwinlabs@gmail.com', 'www.', '2018-03-16 10:47:41', '2018-03-16 10:47:41', 1),
(178, 76, 0, 174, 0, 'pradeep', '', '', '', '', '', '', 'test', '', '', '', '', '', '', '', '', '', '', '8872944278', '', '', '91', 'pradeep.techwinlabs@gmail.com', 'www.', '2018-03-16 10:48:35', '2018-03-16 10:48:35', 1),
(179, 76, 0, 174, 0, 'pradeep', '', '', '', '', '', '', 'test', '', '', '', '', '', '', '', '', '', '', '8872944278', '', '', '91', 'pradeep.techwinlabs@gmail.com', 'www.', '2018-03-16 10:55:24', '2018-03-16 10:55:24', 1),
(180, 76, 0, 174, 0, 'pradeep', '', '', '', '', '', '', 'test', '', '', '', '', '', '', '', '', '', '', '8872944278', '', '', '91', 'pradeep.techwinlabs@gmail.com', 'www.', '2018-03-16 11:01:48', '2018-03-16 11:01:48', 1),
(181, 76, 0, 174, 0, 'pradeep', '', '', '', '', '', '', 'test', '', '', '', '', '', '', '', '', '', '', '8872944278', '', '', '91', 'pradeep.techwinlabs@gmail.com', 'www.', '2018-03-16 11:03:07', '2018-03-16 11:03:07', 1),
(182, 76, 0, 174, 0, 'pradeep', '', '', '', '', '', '', 'test', '', '', '', '', '', '', '', '', '', '', '8872944278', '', '', '91', 'pradeep.techwinlabs@gmail.com', 'www.', '2018-03-16 11:13:45', '2018-03-16 11:13:45', 1),
(183, 76, 0, 174, 0, 'pradeep', '', '', '', '', '', '', 'test', '', '', '', '', '', '', '', '', '', '', '8872944278', '', '', '91', 'pradeep.techwinlabs@gmail.com', 'www.', '2018-03-16 11:15:20', '2018-03-16 11:15:20', 1),
(184, 76, 0, 174, 0, 'pradeep', '', '', '', '', '', '', 'test', '', '', '', '', '', '', '', '', '', '', '8872944278', '', '', '91', 'pradeep.techwinlabs@gmail.com', 'www.', '2018-03-16 11:16:01', '2018-03-16 11:16:01', 1),
(185, 76, 0, 174, 0, 'pradeep', '', '', '', '', '', '', 'test', '', '', '', '', '', '', '', '', '', '', '8872944278', '', '', '91', 'pradeep.techwinlabs@gmail.com', 'www.', '2018-03-16 11:16:11', '2018-03-16 11:16:11', 1),
(186, 76, 0, 174, 0, 'pradeep', '', '', '', '', '', '', 'test', '', '', '', '', '', '', '', '', '', '', '8872944278', '', '', '91', 'pradeep.techwinlabs@gmail.com', 'www.', '2018-03-16 12:03:17', '2018-03-16 12:03:17', 1),
(187, 76, 0, 174, 0, 'pradeep', '', '', '', '', '', '', 'test', '', '', '', '', '', '', '', '', '', '', '8872944278', '', '', '91', 'pradeep.techwinlabs@gmail.com', 'www.', '2018-03-16 12:40:33', '2018-03-16 12:40:33', 1),
(188, 76, 0, 174, 0, 'pradeep', '', '', '', '', '', '', 'test', '', '', '', '', '', '', '', '', '', '', '8872944278', '', '', '91', 'pradeep.techwinlabs@gmail.com', 'www.', '2018-03-16 12:40:55', '2018-03-16 12:40:55', 1),
(189, 76, 0, 174, 0, 'pradeep', '', '', '', '', '', '', 'test', '', '', '', '', '', '', '', '', '', '', '8872944278', '', '', '91', 'pradeep.techwinlabs@gmail.com', 'www.', '2018-03-16 12:41:24', '2018-03-16 12:41:24', 1),
(190, 66, 0, 146, 0, 'Rahul', 'kumar', 'singh', 'Developer', 'Android', '', '', 'Techwin', '', '150068', 'f-195', 'Mohali', 'Mohali', 'Punjab', 'Punjab', 'India', '17725695', '17723695', '8729012848', '91', '91', '91', 'rahul2.techwinlabs@gmail.com', 'www.techwinlabs.com', '2018-03-16 15:26:12', '2018-03-16 15:26:12', 1),
(191, 79, 0, 0, 0, 'Go', 'Ghjj', 'Flu', '', '', '', 'media/cards/profile_pic/2018-03/20180316_PP-9286-1521216747.png', 'The', 'media/cards/company_logo/2018-03/20180316_BC-2534-1521216747.png', '', 'Lufthansa', 'Geh', '244', '', '', '', '', '', '', '', '', '', 'inrtestiphonex3@gmail.com', '', '2018-03-16 16:11:00', '2018-03-16 16:12:27', 1),
(193, 45, 0, 8, 0, 'Vivek Sannabhadti', '', '', 'Mr', 'Executive ', '', 'media/cards/profile_pic/2018-02/20180218_PP-1488-1518974352.png', 'Inr Circle', 'media/cards/company_logo/2018-02/20180218_BC-1162-1518974352.png', '81369', '', 'Fallstrasse ', '16', 'Bayern', 'Munich', 'Germany', '', '7807 2356', '15146748527', '', '49', '49', 'vsannabhadti@gmail.com', '', '2018-03-18 15:14:42', '2018-03-18 15:14:42', 1),
(194, 53, 0, 145, 0, 'iPhone X', 'New', 'Silver', 'Master Repräsentation ', '', '', 'media/cards/profile_pic/2018-03/20180311_PP-4297-1520806237.png', 'Otter Ag', 'media/cards/company_logo/2018-03/20180311_BC-1637-1520806237.png', '', '', '', '', '', '', 'Germany', '', '', '01746599158', '', '', '+49', 'inrtestiphonex@gmail.com', '', '2018-03-18 15:37:21', '2018-03-18 15:37:21', 1),
(195, 66, 0, 146, 0, 'Rahul', 'kumar', 'singh', 'Developer', 'Android', '', '', 'Techwin', '', '150068', 'f-195', 'Mohali', 'Mohali', 'Punjab', 'Punjab', 'India', '17725695', '17723695', '8729012848', '91', '91', '91', 'rahul2.techwinlabs@gmail.com', 'www.techwinlabs.com', '2018-03-19 05:18:02', '2018-03-19 05:18:02', 1),
(196, 66, 0, 146, 0, 'Rahul', 'kumar', 'singh', 'Developer', 'Android', '', '', 'Techwin', '', '150068', 'f-195', 'Mohali', 'Mohali', 'Punjab', 'Punjab', 'India', '17725695', '17723695', '8729012848', '91', '91', '91', 'rahul2.techwinlabs@gmail.com', 'www.techwinlabs.com', '2018-03-19 05:18:27', '2018-03-19 05:18:27', 1),
(197, 66, 0, 146, 0, 'Rahul', 'kumar', 'singh', 'Developer', 'Android', '', '', 'Techwin labs', '', '150068', 'f-195', 'Mohali', 'Mohali', 'Punjab', 'Punjab', 'India', '17725695', '17723695', '8729012848', '91', '91', '91', 'rahul2.techwinlabs@gmail.com', 'www.techwinlabs.com', '2018-03-19 05:18:58', '2018-03-19 05:18:58', 1),
(198, 66, 0, 146, 0, 'Rahul', 'kumar', 'singh', 'Developer', 'Android', '', 'media/cards/profile_pic/2018-03/20180319_PP-7992-1521436738.png', 'Techwin labs', '', '150068', 'f-195', 'Mohali', 'Mohali', 'Punjab', 'Punjab', 'India', '17725695', '17723695', '8729012848', '91', '91', '91', 'rahul2.techwinlabs@gmail.com', 'www.techwinlabs.com', '2018-03-19 12:42:54', '2018-03-19 12:42:54', 1),
(199, 66, 0, 146, 0, 'Rahul', 'kumar', 'singh', 'Developer', 'Android', '', 'media/cards/profile_pic/2018-03/20180319_PP-7992-1521436738.png', 'Techwin labs', '', '150068', 'f-195', 'Mohali', 'Mohali', 'Punjab', 'Punjab', 'India', '17725695', '17723695', '8729012848', '91', '91', '91', 'rahul2.techwinlabs@gmail.com', 'www.techwinlabs.com', '2018-03-19 13:01:33', '2018-03-19 13:01:33', 1),
(200, 66, 0, 146, 1, 'Rahul', 'kumar', 'singh', 'Developer', 'Android', '', 'media/cards/profile_pic/2018-03/20180319_PP-7992-1521436738.png', 'Techwin labs', '', '150068', 'f-195', 'Mohali', 'Mohali', 'Punjab', 'Punjab', 'India', '17725695', '17723695', '8729012848', '91', '91', '91', 'rahul2.techwinlabs@gmail.com', 'www.techwinlabs.com', '2018-03-19 14:04:22', '2018-03-19 14:04:22', 1),
(201, 74, 0, 173, 0, 'Kawal', 'Kang ', '', 'iOS Developer ', '', '', 'media/cards/profile_pic/2018-03/20180316_PP-8624-1521179742.png', 'Techwin', 'media/cards/company_logo/2018-03/20180316_BC-8313-1521179742.png', '', '', '', '', '', '', '', '', '', '', '', '', '', 'kawal.techwinlabs@gmail.com', '', '2018-03-20 06:02:53', '2018-03-20 06:02:53', 1),
(202, 74, 0, 173, 0, 'Kawal', 'Kang ', '', 'iOS Developer ', '', '', 'media/cards/profile_pic/2018-03/20180320_PP-4921-1521525773.png', 'Techwin', 'media/cards/company_logo/2018-03/20180320_BC-1879-1521525773.png', '', '', '', '', 'Punjab', 'Amritsar', '', '', '', '', '', '', '', 'kawal.techwinlabs@gmail.com', '', '2018-03-20 06:02:55', '2018-03-20 06:02:55', 1),
(203, 74, 0, 173, 0, 'Kawal', 'Kang ', '', 'iOS Developer ', '', '', 'media/cards/profile_pic/2018-03/20180320_PP-6721-1521525775.png', 'Techwin', 'media/cards/company_logo/2018-03/20180320_BC-1308-1521525775.png', '', '', '', '', 'Punjab', 'Amritsar', '', '', '', '', '', '', '', 'kawal.techwinlabs@gmail.com', '', '2018-03-20 06:03:13', '2018-03-20 06:03:13', 1),
(204, 66, 0, 146, 1, 'Rahul', 'kumar', 'singh', 'Developer', 'Android', '', 'media/cards/profile_pic/2018-03/20180319_PP-7992-1521436738.png', 'Techwin labs', '', '150068', 'f-195', 'Mohali', 'Mohali', 'Punjab', 'Punjab', 'India', '17725695', '17723695', '8729012848', '91', '91', '91', 'rahul2.techwinlabs@gmail.com', 'www.techwinlabs.com', '2018-03-20 06:38:25', '2018-03-20 06:38:25', 1),
(205, 76, 0, 174, 0, 'pradeep', '', '', '', '', '', '', 'test', '', '', '', '', '', '', '', '', '', '', '8872944278', '', '', '91', 'pradeep.techwinlabs@gmail.com', 'www.', '2018-03-20 07:13:32', '2018-03-20 07:13:32', 1),
(206, 76, 0, 174, 0, 'pradeep', 'kumar', 'sur', 'developer', 'hr', '', '', 'test', '', '4667', 'old building', 'stres', 'stres', 'punjab', 'punjab', 'india', '979797', '466797979', '8872944278', '91', '91', '91', 'pradeep.techwinlabs@gmail.com', 'www.bzbz.com', '2018-03-20 07:14:18', '2018-03-20 07:14:18', 1),
(207, 76, 0, 174, 0, 'pradeep', 'kumar', 'sur', 'developer', 'hr', 'media/cards/card_picture/2018-03/20180320_PP-4178-1521530058.png', '', 'test', '', '4667', 'old building', 'stres', 'stres', 'punjab', 'punjab', 'india', '979797', '466797979', '8872944278', '91', '91', '91', 'pradeep.techwinlabs@gmail.com', 'www.bzbz.com', '2018-03-20 07:14:29', '2018-03-20 07:14:29', 1),
(208, 66, 0, 146, 1, 'Rahul', 'kumar', 'singh', 'Developer', 'Android', '', 'media/cards/profile_pic/2018-03/20180319_PP-7992-1521436738.png', 'Techwin labs', '', '150068', 'f-195', 'Mohali', 'Mohali', 'Punjab', 'Punjab', 'India', '17725695', '17723695', '8729012848', '91', '91', '91', 'rahul2.techwinlabs@gmail.com', 'www.techwinlabs.com', '2018-03-20 07:16:42', '2018-03-20 07:16:42', 1),
(209, 6, 0, 1, 1, 'Simranjeet', 'Kaur', 'Dhillon', 'Mr', 'HR Manager', '', 'media/cards/profile_pic/2018-03/20180309_PP-3663-1520602989.png', 'Company ', '', '2323232', 'Desmond ', 'Park Street123', '', 'DSDSD', 'NY', 'Aruba', '3232322322', '(312) 212-11', '842763728', '+93', '21', '+32', 'simranjeet.techwinlabs@gmail.com', 'www.mywebsite.com', '2018-03-20 07:34:53', '2018-03-20 07:34:53', 1),
(210, 6, 0, 1, 1, 'Simranjeet', 'Kaur', 'Dhillon', 'Mr', 'HR Manager', '', 'media/cards/profile_pic/2018-03/20180320_PP-6043-1521531293.png', 'Company ', 'media/cards/company_logo/2018-03/20180320_BC-5742-1521531293.png', '2323232', 'Desmond ', 'Park Street123', '', 'DSDSD', 'NY', 'Aruba', '3232322322', '(312) 212-11', '842763728', '+93', '21', '+32', 'simranjeet.techwinlabs@gmail.com', 'www.mywebsite.com', '2018-03-20 07:34:55', '2018-03-20 07:34:55', 1),
(211, 76, 0, 174, 1, 'pradeep', 'kumar', 'sur', 'developer', 'hr', '', '', 'test', '', '4667', 'old building', 'stres', 'stres', 'punjab', 'punjab', 'india', '979797', '466797979', '8872944278', '91', '91', '91', 'pradeep.techwinlabs@gmail.com', 'www.bzbz.com', '2018-03-20 07:41:13', '2018-03-20 07:41:13', 1),
(212, 77, 0, 0, 0, 'Amit', '', '', '', '', '', 'media/cards/profile_pic/2018-03/20180320_PP-8668-1521546579.png', 'TechWin', 'media/cards/company_logo/2018-03/20180320_BC-2469-1521546579.png', '', '', '', '', '', 'Gahwsiahavavagagwvwvs', '', '', '', '', '', '', '', 'amit.techwinlabs@gmail.com', '', '2018-03-20 11:45:42', '2018-03-20 11:49:39', 1),
(213, 66, 0, 146, 1, 'Rahul', 'kumar', 'singh', 'Developer', 'Android', '', 'media/cards/profile_pic/2018-03/20180319_PP-7992-1521436738.png', 'Techwin labs', '', '150068', 'f-195', 'Mohali', 'Mohali', 'Punjab', 'Punjab', 'India', '17725695', '17723695', '8729012848', '91', '91', '91', 'rahul2.techwinlabs@gmail.com', 'www.techwinlabs.com', '2018-03-20 11:49:39', '2018-03-20 11:49:39', 1),
(214, 77, 0, 212, 0, 'Amit', '', '', '', '', '', 'media/cards/profile_pic/2018-03/20180320_PP-2002-1521546342.png', 'TechWin', 'media/cards/company_logo/2018-03/20180320_BC-4960-1521546342.png', '', '', '', '', '', 'Gahwsiahavavagagwvwvs', '', '', '', '', '', '', '', 'amit.techwinlabs@gmail.com', '', '2018-03-20 11:49:39', '2018-03-20 11:49:39', 1),
(215, 66, 0, 146, 0, 'Rahul', 'kumar', 'singh', 'Developer', 'Android', 'media/cards/card_picture/2018-03/20180320_PP-6330-1521546579.png', 'media/cards/profile_pic/2018-03/20180319_PP-7992-1521436738.png', 'Techwin labs', '', '150068', 'f-195', 'Mohali', 'Mohali', 'Punjab', 'Punjab', 'India', '17725695', '17723695', '8729012848', '91', '91', '91', 'rahul2.techwinlabs@gmail.com', 'www.techwinlabs.com', '2018-03-20 11:49:42', '2018-03-20 11:49:42', 1),
(216, 66, 0, 146, 0, 'Rahul', 'kumar', 'singh', 'Developer', 'Android', 'media/cards/card_picture/2018-03/20180320_PP-4705-1521546582.png', 'media/cards/profile_pic/2018-03/20180319_PP-7992-1521436738.png', 'Techwin labs', '', '150068', 'f-195', 'Mohali', 'Mohali', 'Punjab', 'Punjab', 'India', '17725695', '17723695', '8729012848', '91', '91', '91', 'rahul2.techwinlabs@gmail.com', 'www.techwinlabs.com', '2018-03-20 11:50:18', '2018-03-20 11:50:18', 1),
(217, 81, 0, 0, 0, 'Inr', 'Test ', 'Iphonex3', '', '', '', 'media/cards/profile_pic/2018-03/20180321_PP-9335-1521649043.png', 'Other AG', 'media/cards/company_logo/2018-03/20180321_BC-6346-1521649043.png', '', '', '', '', '', '', '', '', '', '', '', '', '', 'inrtestiphonex3@gmail.com', '', '2018-03-21 16:15:35', '2018-03-21 16:17:23', 1),
(218, 81, 0, 217, 0, 'Inr', 'Test ', 'Iphonex3', '', '', '', 'media/cards/profile_pic/2018-03/20180321_PP-4310-1521648935.png', 'Other AG', 'media/cards/company_logo/2018-03/20180321_BC-2722-1521648935.png', '', '', '', '', '', '', '', '', '', '', '', '', '', 'inrtestiphonex3@gmail.com', '', '2018-03-21 16:16:00', '2018-03-21 16:16:00', 1),
(219, 81, 0, 217, 0, 'Inr', 'Test ', 'Iphonex3', '', '', '', 'media/cards/profile_pic/2018-03/20180321_PP-6671-1521648960.png', 'Other AG', 'media/cards/company_logo/2018-03/20180321_BC-3164-1521648960.png', '', '', '', '', '', '', '', '', '', '', '', '', '', 'inrtestiphonex3@gmail.com', '', '2018-03-21 16:17:23', '2018-03-21 16:17:23', 1),
(220, 42, 0, 17, 1, 'Daniel janouschek', '', '', 'CEO ', '', '', 'media/cards/profile_pic/2018-02/20180225_PP-3243-1519576413.png', 'Other AG', 'media/cards/company_logo/2018-02/20180225_BC-3022-1519576413.png', '81543', '', '', '', 'Bavaria', 'München Schwäbing Münchner Freheit', 'France', '(323) 641-8300', '6 25 85 23 69', '1706177395', '+1', '+33', '+49', 'djanouschek@gmail.com', '', '2018-03-21 16:52:21', '2018-03-21 16:52:21', 1),
(221, 42, 0, 17, 1, 'Daniel ', 'Jan', 'Ouschek', 'CEO ', '', '', 'media/cards/profile_pic/2018-03/20180321_PP-5488-1521651141.png', 'Other AG', 'media/cards/company_logo/2018-03/20180321_BC-1786-1521651141.png', '81543', '', '', '', 'Bavaria', 'München Schwäbing Münchner Freheit', 'France', '(323) 641-8300', '6 25 85 23 69', '1706177395', '+1', '+33', '+49', 'inrtestiphonex2@gmail.com', '', '2018-03-21 19:25:51', '2018-03-21 19:25:51', 1),
(222, 42, 0, 17, 1, 'Daniel ', 'Jan', 'Ouschek', 'CEO ', '', '', 'media/cards/profile_pic/2018-03/20180321_PP-5956-1521660351.png', 'Other AG', 'media/cards/company_logo/2018-03/20180321_BC-7626-1521660351.png', '81543', '', '', '', 'Bavaria', 'München Schwäbing Münchner Freheit', 'France', '(323) 641-8300', '6 25 85 23 69', '1706177395', '+1', '+33', '+49', 'djanouschek@gmail.com', '', '2018-03-21 19:28:45', '2018-03-21 19:28:45', 1),
(223, 42, 0, 17, 1, 'Daniel ', 'Jan', 'Ouschek', 'CEO ', '', '', 'media/cards/profile_pic/2018-03/20180321_PP-5880-1521660525.png', 'Other AG', 'media/cards/company_logo/2018-03/20180321_BC-1471-1521660525.png', '81543', '', '', '', 'Bavaria', 'München Schwäbing Münchner Freheit', 'France', '(323) 641-8300', '6 25 85 23 69', '1706177395', '+1', '+33', '+49', 'djanouschek@gmail.com', '', '2018-03-21 19:30:09', '2018-03-21 19:30:09', 1),
(224, 66, 0, 146, 0, 'Rahul', 'kumar', 'singh', 'Developer', 'Android', 'media/cards/card_picture/2018-03/20180320_PP-3661-1521546618.png', 'media/cards/profile_pic/2018-03/20180319_PP-7992-1521436738.png', 'Techwin labs', '', '150068', 'f-195', 'Mohali', 'Mohali', 'Punjab', 'Punjab', 'India', '17725695', '17723695', '8729012848', '91', '91', '91', 'rahul2.techwinlabs@gmail.com', 'www.techwinlabs.com', '2018-03-22 09:30:29', '2018-03-22 09:30:29', 1),
(225, 82, 0, 0, 0, 'inr test x2', 'test', 'x2', 'phone', 'fgh', '', '', 'xyz', '', '81369', 'ashakiran', '', '', '', '', '', '', '', '', '', '', '', 'inrtestiphonex2@gmail.com', 'www.', '2018-03-25 14:48:44', '2018-03-25 14:48:44', 1),
(226, 85, 0, 0, 0, 'jassy', 'kumar', 'sedik', '', '', 'media/cards/card_picture/2018-03/20180327_PP-2096-1522131416.png', 'media/cards/profile_pic/2018-03/20180327_PP-8665-1522129783.png', 'xyn', 'media/cards/company_logo/2018-03/20180327_BC-4672-1522129783.png', '', '', '', '', '', '', '', '', '', '9465346171', '', '', '91', 'jaswinder.techwin@gmail.com', 'www.fttt.ggy', '2018-03-27 05:49:43', '2018-03-27 06:16:56', 1),
(227, 85, 0, 226, 0, 'jassy', 'kumar', 'sedik', '', '', '', 'media/cards/profile_pic/2018-03/20180327_PP-8665-1522129783.png', 'xyn', 'media/cards/company_logo/2018-03/20180327_BC-4672-1522129783.png', '', '', '', '', '', '', '', '', '', '9465346171', '', '', '91', 'jaswinder.techwin@gmail.com', 'www.fttt.ggy', '2018-03-27 06:16:56', '2018-03-27 06:16:56', 1),
(228, 86, 0, 0, 0, 'Arvindu', 'kumar', 'punia', '', '', 'media/cards/card_picture/2018-03/20180327_PP-6385-1522149916.png', 'media/cards/profile_pic/2018-03/20180327_PP-4424-1522146140.png', 'gshhs', '', '', '', '', '', '', '', '', '', '', '8053380461', '', '', '91', 'arvindpoonia.techwinlabs@gmail.com', 'www.', '2018-03-27 08:01:22', '2018-03-27 11:25:16', 1),
(229, 86, 0, 228, 0, 'Arvind', 'kumar', 'punia', '', '', '', '', 'gshhs', '', '', '', '', '', '', '', '', '', '', '8053380461', '', '', '91', 'arvindpoonia.techwinlabs@gmail.com', 'www.', '2018-03-27 10:10:35', '2018-03-27 10:10:35', 1),
(230, 86, 0, 228, 0, 'Arvind', 'kumar', 'punia', '', '', '', '', 'gshhs', '', '', '', '', '', '', '', '', '', '', '8053380461', '', '', '91', 'arvindpoonia.techwinlabs@gmail.com', 'www.', '2018-03-27 10:10:46', '2018-03-27 10:10:46', 1),
(231, 86, 0, 228, 0, 'Arvind', 'kumar', 'punia', '', '', '', '', 'gshhs', '', '', '', '', '', '', '', '', '', '', '8053380461', '', '', '91', 'arvindpoonia.techwinlabs@gmail.com', 'www.', '2018-03-27 10:12:12', '2018-03-27 10:12:12', 1),
(232, 86, 0, 228, 0, 'Arvind', 'kumar', 'punia', '', '', '', '', 'gshhs', '', '', '', '', '', '', '', '', '', '', '8053380461', '', '', '91', 'arvindpoonia.techwinlabs@gmail.com', 'www.', '2018-03-27 10:21:22', '2018-03-27 10:21:22', 1),
(233, 86, 0, 228, 0, 'Arvind', 'kumar', 'punia', '', '', '', '', 'gshhs', '', '', '', '', '', '', '', '', '', '', '8053380461', '', '', '91', 'arvindpoonia.techwinlabs@gmail.com', 'www.', '2018-03-27 10:21:36', '2018-03-27 10:21:36', 1),
(234, 86, 0, 228, 0, 'Arvindu', 'kumar', 'punia', '', '', '', '', 'gshhs', '', '', '', '', '', '', '', '', '', '', '8053380461', '', '', '91', 'arvindpoonia.techwinlabs@gmail.com', 'www.', '2018-03-27 10:22:20', '2018-03-27 10:22:20', 1),
(235, 86, 0, 228, 0, 'Arvindu', 'kumar', 'punia', '', '', '', 'media/cards/profile_pic/2018-03/20180327_PP-4424-1522146140.png', 'gshhs', '', '', '', '', '', '', '', '', '', '', '8053380461', '', '', '91', 'arvindpoonia.techwinlabs@gmail.com', 'www.', '2018-03-27 10:43:14', '2018-03-27 10:43:14', 1),
(236, 86, 0, 228, 0, 'Arvindu', 'kumar', 'punia', '', '', '', 'media/cards/profile_pic/2018-03/20180327_PP-4424-1522146140.png', 'gshhs', '', '', '', '', '', '', '', '', '', '', '8053380461', '', '', '91', 'arvindpoonia.techwinlabs@gmail.com', 'www.', '2018-03-27 10:43:27', '2018-03-27 10:43:27', 1),
(237, 86, 0, 228, 0, 'Arvindu', 'kumar', 'punia', '', '', '', 'media/cards/profile_pic/2018-03/20180327_PP-4424-1522146140.png', 'gshhs', '', '', '', '', '', '', '', '', '', '', '8053380461', '', '', '91', 'arvindpoonia.techwinlabs@gmail.com', 'www.', '2018-03-27 10:43:57', '2018-03-27 10:43:57', 1),
(238, 86, 0, 228, 0, 'Arvindu', 'kumar', 'punia', '', '', '', 'media/cards/profile_pic/2018-03/20180327_PP-4424-1522146140.png', 'gshhs', '', '', '', '', '', '', '', '', '', '', '8053380461', '', '', '91', 'arvindpoonia.techwinlabs@gmail.com', 'www.', '2018-03-27 11:25:13', '2018-03-27 11:25:13', 1),
(239, 86, 0, 228, 0, 'Arvindu', 'kumar', 'punia', '', '', 'media/cards/card_picture/2018-03/20180327_PP-9163-1522149913.png', 'media/cards/profile_pic/2018-03/20180327_PP-4424-1522146140.png', 'gshhs', '', '', '', '', '', '', '', '', '', '', '8053380461', '', '', '91', 'arvindpoonia.techwinlabs@gmail.com', 'www.', '2018-03-27 11:25:16', '2018-03-27 11:25:16', 1),
(240, 84, 0, 0, 0, 'Arvind', 'kumar', 'poonia', 'Mr.', 'development', '', 'media/cards/profile_pic/2018-03/20180327_PP-9847-1522154204.png', 'comp1hack', 'media/cards/company_logo/2018-03/20180327_BC-3178-1522154204.png', '16079', 'old building', 'new street', 'new street', 'punjab', 'punjab', 'india', '981277919', '', '8053380461', '', '91', '91', 'arvindpoonia.techwinlabs@gmail.com', 'www.comphack.com', '2018-03-27 12:36:44', '2018-03-27 12:36:44', 1);

-- --------------------------------------------------------

--
-- Table structure for table `newschannel_subscribers`
--

CREATE TABLE `newschannel_subscribers` (
  `subscribe_id` int(11) NOT NULL,
  `channel_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `subscribed_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `newschannel_subscribers`
--

INSERT INTO `newschannel_subscribers` (`subscribe_id`, `channel_id`, `user_id`, `subscribed_date`) VALUES
(2, 2, 42, '2018-01-29 22:05:08'),
(3, 3, 42, '2018-01-29 22:09:25'),
(4, 4, 6, '2018-01-30 08:04:36'),
(6, 2, 45, '2018-01-30 20:41:05'),
(7, 3, 45, '2018-01-30 20:41:06'),
(8, 5, 45, '2018-01-30 20:42:51'),
(9, 2, 6, '2018-01-31 10:31:43'),
(10, 5, 42, '2018-01-31 14:52:28'),
(11, 4, 42, '2018-01-31 14:52:32'),
(21, 7, 47, '2018-02-04 14:36:28'),
(22, 7, 42, '2018-02-04 14:36:50'),
(23, 5, 47, '2018-02-04 16:51:02'),
(24, 2, 47, '2018-02-04 16:51:06'),
(25, 3, 47, '2018-02-04 16:51:12'),
(26, 8, 48, '2018-02-04 17:31:07'),
(27, 8, 45, '2018-02-04 17:32:15'),
(28, 8, 42, '2018-02-04 17:32:17'),
(29, 8, 47, '2018-02-04 17:32:20'),
(32, 9, 50, '2018-02-07 12:30:30'),
(33, 5, 50, '2018-02-07 12:30:42'),
(45, 2, 48, '2018-02-11 15:32:00'),
(46, 3, 48, '2018-02-11 15:32:12'),
(48, 9, 6, '2018-02-12 07:56:58'),
(50, 7, 54, '2018-02-14 12:18:50'),
(52, 12, 54, '2018-02-14 12:31:17'),
(53, 7, 41, '2018-02-14 12:39:31'),
(56, 2, 41, '2018-02-14 12:39:36'),
(57, 8, 41, '2018-02-14 12:39:37'),
(59, 4, 54, '2018-02-15 10:25:06'),
(64, 7, 45, '2018-02-18 15:35:48'),
(65, 14, 54, '2018-02-19 09:05:06'),
(66, 7, 50, '2018-02-19 10:59:40'),
(67, 7, 6, '2018-02-19 11:09:16'),
(69, 15, 55, '2018-02-21 06:33:06'),
(70, 7, 55, '2018-02-21 06:33:50'),
(71, 14, 55, '2018-02-21 06:33:54'),
(73, 15, 42, '2018-02-22 20:48:06'),
(74, 2, 61, '2018-02-25 17:09:25'),
(75, 2, 53, '2018-02-26 16:08:36'),
(76, 5, 53, '2018-02-26 16:08:41'),
(77, 3, 53, '2018-02-26 16:08:46'),
(78, 7, 53, '2018-02-26 16:08:55'),
(79, 16, 53, '2018-02-26 16:11:38'),
(80, 7, 48, '2018-03-04 14:37:37'),
(81, 17, 66, '2018-03-06 06:35:41'),
(82, 4, 66, '2018-03-07 06:12:53'),
(83, 12, 6, '2018-03-07 08:14:38'),
(84, 14, 6, '2018-03-07 08:14:54'),
(85, 17, 6, '2018-03-07 08:16:21'),
(86, 16, 42, '2018-03-11 16:17:03'),
(87, 4, 74, '2018-03-14 08:09:08'),
(88, 15, 74, '2018-03-14 08:09:28'),
(89, 17, 74, '2018-03-14 08:09:29'),
(90, 14, 74, '2018-03-14 08:09:37'),
(91, 18, 74, '2018-03-14 08:10:10'),
(92, 19, 75, '2018-03-15 13:55:15'),
(93, 7, 75, '2018-03-15 13:55:29'),
(94, 18, 77, '2018-03-16 05:54:33'),
(95, 17, 77, '2018-03-16 05:54:48'),
(96, 16, 66, '2018-03-19 06:03:46'),
(97, 9, 66, '2018-03-19 06:03:47'),
(98, 19, 66, '2018-03-19 06:03:50'),
(99, 8, 66, '2018-03-19 06:03:53'),
(100, 2, 66, '2018-03-19 06:03:54'),
(104, 15, 66, '2018-03-19 06:09:06'),
(105, 7, 76, '2018-03-19 06:43:48'),
(108, 20, 76, '2018-03-19 06:44:17'),
(109, 15, 76, '2018-03-20 09:14:27'),
(110, 4, 76, '2018-03-20 09:14:28'),
(111, 12, 76, '2018-03-20 09:14:31'),
(112, 5, 76, '2018-03-20 09:14:34'),
(113, 8, 76, '2018-03-20 09:14:37'),
(114, 2, 81, '2018-03-21 16:05:58'),
(115, 21, 82, '2018-03-21 18:45:06'),
(117, 3, 82, '2018-03-21 18:56:20'),
(118, 2, 82, '2018-03-21 18:56:29'),
(119, 7, 86, '2018-03-27 09:11:00'),
(120, 2, 86, '2018-03-27 09:11:02'),
(121, 8, 86, '2018-03-27 09:11:04'),
(122, 22, 86, '2018-03-27 11:29:46'),
(123, 14, 86, '2018-03-27 11:48:28'),
(124, 19, 86, '2018-03-27 11:48:29'),
(125, 9, 86, '2018-03-27 11:48:30'),
(126, 16, 86, '2018-03-27 11:48:31'),
(128, 15, 86, '2018-03-27 11:48:42');

-- --------------------------------------------------------

--
-- Table structure for table `news_brief`
--

CREATE TABLE `news_brief` (
  `newsbrief_id` int(11) NOT NULL,
  `channel_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `news` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `picture` varchar(255) NOT NULL,
  `published_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL,
  `is_active` tinyint(4) NOT NULL COMMENT '1 for active 2 for inactive'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `news_brief`
--

INSERT INTO `news_brief` (`newsbrief_id`, `channel_id`, `company_id`, `user_id`, `news`, `picture`, `published_at`, `modified_at`, `is_active`) VALUES
(1, 2, 1, 42, 'First try.', '', '2018-01-29 22:05:41', '0000-00-00 00:00:00', 2),
(2, 3, 1, 42, 'Second try', 'media/newschannel/newsbrief/2018-01/20180129_NBP-461354-1517263865.png', '2018-01-29 22:11:05', '0000-00-00 00:00:00', 2),
(3, 3, 1, 42, 'Third attempt.', '', '2018-01-29 22:13:57', '0000-00-00 00:00:00', 2),
(17, 4, 1, 6, 'One', '', '2018-01-30 12:57:23', '0000-00-00 00:00:00', 2),
(18, 4, 1, 6, 'Two', '', '2018-01-30 12:57:33', '0000-00-00 00:00:00', 2),
(19, 4, 1, 6, 'Hello', '', '2018-01-30 12:57:55', '0000-00-00 00:00:00', 2),
(20, 5, 1, 45, 'busy busy always busy', 'media/newschannel/newsbrief/2018-01/20180130_NBP-126614-1517345041.png', '2018-01-30 20:44:01', '0000-00-00 00:00:00', 2),
(21, 4, 1, 6, 'Fifth', '', '2018-01-31 08:55:36', '0000-00-00 00:00:00', 2),
(22, 4, 1, 6, 'Jddjdjjx', '', '2018-01-31 10:31:32', '0000-00-00 00:00:00', 2),
(24, 2, 1, 42, 'Mit meiner Ma beim Essen', 'media/newschannel/newsbrief/2018-02/20180203_NBP-178152-1517658319.png', '2018-02-03 11:45:19', '0000-00-00 00:00:00', 2),
(25, 7, 1, 47, 'the screen has to be similar to the iPhone layout', 'media/newschannel/newsbrief/2018-02/20180204_NBP-852295-1517755024.png', '2018-02-04 14:37:04', '0000-00-00 00:00:00', 2),
(26, 8, 1, 48, 'Hi there.', '', '2018-02-04 17:31:21', '0000-00-00 00:00:00', 2),
(27, 3, 1, 42, 'Is this skewed?', 'media/newschannel/newsbrief/2018-02/20180204_NBP-962024-1517765803.png', '2018-02-04 17:36:43', '0000-00-00 00:00:00', 2),
(28, 8, 1, 48, 'Quick message.', '', '2018-02-04 17:47:29', '0000-00-00 00:00:00', 2),
(33, 4, 1, 6, 'Publishing first here', '', '2018-02-07 07:26:41', '2018-02-07 07:26:55', 2),
(36, 9, 1, 50, 'Hello the inr app', '', '2018-02-07 12:53:38', '0000-00-00 00:00:00', 2),
(38, 2, 1, 42, 'Post some nonsense.', '', '2018-02-07 17:20:34', '0000-00-00 00:00:00', 2),
(45, 3, 1, 42, 'CNN broadcast 11 Feb text', '', '2018-02-11 15:28:43', '0000-00-00 00:00:00', 2),
(46, 2, 1, 42, '11 feb image. ', 'media/newschannel/newsbrief/2018-02/20180211_NBP-984806-1518362992.png', '2018-02-11 15:29:52', '0000-00-00 00:00:00', 2),
(47, 9, 1, 50, 'Hey there', '', '2018-02-12 07:50:41', '0000-00-00 00:00:00', 2),
(48, 9, 1, 50, 'Hey thereee adds fed sfbfdbdsbdsf dad dad did dnf daft den dsfndf add ', '', '2018-02-12 07:51:06', '0000-00-00 00:00:00', 2),
(49, 4, 1, 6, 'Jdhs dndnd jdbd dndn d', '', '2018-02-12 07:57:14', '0000-00-00 00:00:00', 2),
(53, 12, 1, 54, 'helloo..', '', '2018-02-14 13:07:33', '0000-00-00 00:00:00', 2),
(59, 4, 1, 6, 'Hey there', '', '2018-02-16 11:06:03', '0000-00-00 00:00:00', 2),
(62, 7, 1, 47, 'trumpy', 'media/newschannel/newsbrief/2018-02/20180218_NBP-682165-1518966293.png', '2018-02-18 15:04:53', '0000-00-00 00:00:00', 2),
(63, 2, 1, 42, 'I publish on news brief an attachment.', 'media/newschannel/newsbrief/2018-02/20180218_NBP-725873-1518966305.png', '2018-02-18 15:05:05', '0000-00-00 00:00:00', 2),
(64, 8, 1, 48, '???', '', '2018-02-18 15:34:56', '0000-00-00 00:00:00', 2),
(65, 8, 1, 48, 'hello', '', '2018-02-18 15:35:56', '0000-00-00 00:00:00', 2),
(72, 12, 1, 54, 'ऋऑऔझञ', '', '2018-02-19 07:31:16', '0000-00-00 00:00:00', 2),
(73, 14, 1, 54, 'hello', '', '2018-02-19 09:10:50', '0000-00-00 00:00:00', 2),
(76, 2, 1, 42, 'Hello Monday.', '', '2018-02-19 20:53:09', '0000-00-00 00:00:00', 2),
(78, 9, 1, 50, 'Hey there', '', '2018-02-21 11:48:39', '0000-00-00 00:00:00', 2),
(79, 2, 1, 42, 'Hello', '', '2018-02-23 11:23:37', '0000-00-00 00:00:00', 2),
(80, 2, 1, 42, 'Food ', 'media/newschannel/newsbrief/2018-02/20180223_NBP-896809-1519385166.png', '2018-02-23 11:26:06', '0000-00-00 00:00:00', 2),
(81, 2, 1, 42, '你好吗？ ????', '', '2018-02-25 13:29:25', '0000-00-00 00:00:00', 2),
(82, 8, 1, 48, '我很來.????', 'media/newschannel/newsbrief/2018-02/20180225_NBP-825096-1519565470.png', '2018-02-25 13:31:10', '0000-00-00 00:00:00', 2),
(83, 16, 1, 53, 'Say something', '', '2018-02-26 16:11:52', '0000-00-00 00:00:00', 2),
(84, 2, 1, 42, 'Hallo', '', '2018-03-04 12:25:28', '0000-00-00 00:00:00', 2),
(85, 7, 1, 47, 'hi hello', 'media/newschannel/newsbrief/2018-03/20180304_NBP-116200-1520174145.png', '2018-03-04 14:35:45', '0000-00-00 00:00:00', 2),
(86, 7, 1, 47, 'sare Jahan se accha', 'media/newschannel/newsbrief/2018-03/20180304_NBP-677216-1520175063.png', '2018-03-04 14:51:03', '0000-00-00 00:00:00', 2),
(87, 7, 1, 47, 'second time', 'media/newschannel/newsbrief/2018-03/20180304_NBP-170519-1520175113.png', '2018-03-04 14:51:53', '0000-00-00 00:00:00', 2),
(88, 8, 1, 48, 'Sent at 4.41pm cet.', '', '2018-03-04 15:41:53', '0000-00-00 00:00:00', 2),
(89, 7, 1, 47, 'so let us see', '', '2018-03-04 15:42:21', '0000-00-00 00:00:00', 2),
(93, 17, 1, 66, 'hello connections', 'media/newschannel/newsbrief/2018-03/20180307_NBP-455522-1520402403.png', '2018-03-07 06:00:03', '0000-00-00 00:00:00', 2),
(95, 4, 1, 6, 'Hiiii this is testing ', '', '2018-03-07 06:06:22', '0000-00-00 00:00:00', 2),
(96, 4, 1, 6, 'Hi this is again testing special $@^!*!(!(😔😏😉😭', '', '2018-03-07 06:07:43', '0000-00-00 00:00:00', 2),
(97, 4, 1, 6, 'Hi?????❤️❤️', '', '2018-03-07 06:21:47', '0000-00-00 00:00:00', 2),
(98, 4, 1, 6, 'Ok done', 'media/newschannel/newsbrief/2018-03/20180307_NBP-472788-1520403943.png', '2018-03-07 06:25:43', '0000-00-00 00:00:00', 2),
(99, 4, 1, 6, 'Okzzz', 'media/newschannel/newsbrief/2018-03/20180307_NBP-558042-1520405530.png', '2018-03-07 06:52:10', '0000-00-00 00:00:00', 2),
(100, 4, 1, 6, 'Hello ', 'media/newschannel/newsbrief/2018-03/20180307_NBP-521049-1520407089.png', '2018-03-07 07:18:09', '0000-00-00 00:00:00', 2),
(101, 4, 1, 6, 'Hi', 'media/newschannel/newsbrief/2018-03/20180307_NBP-742594-1520409735.png', '2018-03-07 08:02:15', '0000-00-00 00:00:00', 2),
(102, 4, 1, 6, 'Testing', 'media/newschannel/newsbrief/2018-03/20180307_NBP-743629-1520410161.png', '2018-03-07 08:09:21', '0000-00-00 00:00:00', 2),
(104, 17, 1, 66, 'hello team', 'media/newschannel/newsbrief/2018-03/20180307_NBP-200805-1520427994.png', '2018-03-07 13:06:34', '0000-00-00 00:00:00', 2),
(105, 2, 1, 42, 'Hallo', '', '2018-03-08 19:38:25', '0000-00-00 00:00:00', 2),
(106, 16, 1, 53, 'A text only msg plus 😘😘😘', '', '2018-03-11 16:17:54', '0000-00-00 00:00:00', 2),
(107, 7, 1, 47, 'gateway of india', 'media/newschannel/newsbrief/2018-03/20180311_NBP-797877-1520788913.png', '2018-03-11 17:21:53', '0000-00-00 00:00:00', 2),
(108, 2, 1, 42, 'Ich bin der cfo!', '', '2018-03-13 20:02:47', '0000-00-00 00:00:00', 2),
(109, 18, 1, 74, 'Testing', 'media/newschannel/newsbrief/2018-03/20180314_NBP-865941-1521015138.png', '2018-03-14 08:12:18', '0000-00-00 00:00:00', 2),
(110, 4, 1, 6, 'Testing', 'media/newschannel/newsbrief/2018-03/20180314_NBP-623523-1521028131.png', '2018-03-14 11:48:51', '0000-00-00 00:00:00', 2),
(111, 4, 1, 6, 'Hello testing ', '', '2018-03-14 11:59:34', '0000-00-00 00:00:00', 2),
(112, 4, 1, 6, '', 'media/newschannel/newsbrief/2018-03/20180314_NBP-390390-1521029348.png', '2018-03-14 12:09:08', '0000-00-00 00:00:00', 2),
(113, 17, 1, 66, 'hello team', 'media/newschannel/newsbrief/2018-03/20180315_NBP-283991-1521116169.png', '2018-03-15 12:16:09', '0000-00-00 00:00:00', 2),
(115, 17, 1, 66, 'hello inr connections', 'media/newschannel/newsbrief/2018-03/20180315_NBP-882024-1521116495.png', '2018-03-15 12:21:35', '0000-00-00 00:00:00', 2),
(116, 19, 1, 75, 'hi', 'media/newschannel/newsbrief/2018-03/20180315_NBP-532632-1521122255.png', '2018-03-15 13:57:35', '0000-00-00 00:00:00', 2),
(117, 19, 1, 75, 'hu', 'media/newschannel/newsbrief/2018-03/20180315_NBP-934252-1521122292.png', '2018-03-15 13:58:12', '0000-00-00 00:00:00', 2),
(118, 16, 1, 53, 'Desktop', 'media/newschannel/newsbrief/2018-03/20180318_NBP-998299-1521363725.png', '2018-03-18 09:02:05', '0000-00-00 00:00:00', 2),
(119, 16, 1, 53, '', 'media/newschannel/newsbrief/2018-03/20180318_NBP-705474-1521363879.png', '2018-03-18 09:04:39', '0000-00-00 00:00:00', 2),
(120, 16, 1, 53, '', 'media/newschannel/newsbrief/2018-03/20180318_NBP-678126-1521364031.png', '2018-03-18 09:07:11', '0000-00-00 00:00:00', 2),
(121, 16, 1, 53, '', 'media/newschannel/newsbrief/2018-03/20180318_NBP-718564-1521364069.png', '2018-03-18 09:07:49', '0000-00-00 00:00:00', 2),
(122, 5, 1, 45, 'hello', 'media/newschannel/newsbrief/2018-03/20180318_NBP-409935-1521379876.png', '2018-03-18 13:31:16', '0000-00-00 00:00:00', 2),
(123, 5, 1, 45, 't', 'media/newschannel/newsbrief/2018-03/20180318_NBP-573938-1521380036.png', '2018-03-18 13:33:19', '2018-03-18 13:33:56', 2),
(124, 5, 1, 45, 'hi', 'media/newschannel/newsbrief/2018-03/20180318_NBP-523772-1521380257.png', '2018-03-18 13:37:37', '0000-00-00 00:00:00', 2),
(125, 5, 1, 45, 'hi', 'media/newschannel/newsbrief/2018-03/20180318_NBP-309567-1521380310.png', '2018-03-18 13:38:30', '0000-00-00 00:00:00', 2),
(126, 5, 1, 45, 'o', 'media/newschannel/newsbrief/2018-03/20180318_NBP-917399-1521380823.png', '2018-03-18 13:47:03', '0000-00-00 00:00:00', 2),
(127, 5, 1, 45, 'kalnirnay', 'media/newschannel/newsbrief/2018-03/20180318_NBP-371792-1521381103.png', '2018-03-18 13:51:43', '0000-00-00 00:00:00', 2),
(128, 17, 1, 66, 'demo news feeds', 'media/newschannel/newsbrief/2018-03/20180319_NBP-742805-1521439818.png', '2018-03-19 06:10:18', '2018-03-19 06:12:53', 2),
(130, 17, 1, 66, 'new tech', 'media/newschannel/newsbrief/2018-03/20180319_NBP-474337-1521440967.png', '2018-03-19 06:29:27', '0000-00-00 00:00:00', 2),
(131, 17, 1, 66, 'camera sample  check for rotation', 'media/newschannel/newsbrief/2018-03/20180319_NBP-819436-1521441021.png', '2018-03-19 06:30:21', '0000-00-00 00:00:00', 2),
(132, 20, 1, 76, 'check for camera rotation', '', '2018-03-19 06:45:31', '0000-00-00 00:00:00', 2),
(133, 20, 1, 76, 'check 2', 'media/newschannel/newsbrief/2018-03/20180319_NBP-631101-1521441973.png', '2018-03-19 06:46:13', '0000-00-00 00:00:00', 2),
(134, 20, 1, 76, '', 'media/newschannel/newsbrief/2018-03/20180319_NBP-133646-1521442785.png', '2018-03-19 06:59:45', '0000-00-00 00:00:00', 2),
(135, 20, 1, 76, '', 'media/newschannel/newsbrief/2018-03/20180319_NBP-120395-1521445648.png', '2018-03-19 07:47:28', '0000-00-00 00:00:00', 2),
(136, 20, 1, 76, '', 'media/newschannel/newsbrief/2018-03/20180319_NBP-930509-1521445714.png', '2018-03-19 07:48:34', '0000-00-00 00:00:00', 2),
(137, 20, 1, 76, 'yjjhy', 'media/newschannel/newsbrief/2018-03/20180319_NBP-384996-1521445864.png', '2018-03-19 07:51:04', '0000-00-00 00:00:00', 2),
(138, 20, 1, 76, '', 'media/newschannel/newsbrief/2018-03/20180319_NBP-470633-1521445979.png', '2018-03-19 07:52:59', '0000-00-00 00:00:00', 2),
(139, 17, 1, 66, 'hfgg', '', '2018-03-19 09:31:22', '0000-00-00 00:00:00', 2),
(140, 17, 1, 66, '', 'media/newschannel/newsbrief/2018-03/20180319_NBP-874294-1521452236.png', '2018-03-19 09:37:16', '0000-00-00 00:00:00', 2),
(141, 20, 1, 76, '', 'media/newschannel/newsbrief/2018-03/20180320_NBP-195006-1521537318.png', '2018-03-20 09:15:18', '0000-00-00 00:00:00', 2),
(142, 20, 1, 76, 'diujjjj', '', '2018-03-20 09:37:37', '0000-00-00 00:00:00', 2),
(161, 16, 1, 53, 'Try that', 'media/newschannel/newsbrief/2018-03/20180321_NBP-451533-1521653272.png', '2018-03-21 17:27:52', '0000-00-00 00:00:00', 2),
(162, 16, 1, 53, 'How about a text message only?', '', '2018-03-21 18:19:05', '0000-00-00 00:00:00', 2),
(163, 21, 1, 82, 'Cannot set channel picture!', '', '2018-03-21 18:46:08', '0000-00-00 00:00:00', 2),
(164, 3, 1, 42, '', 'media/newschannel/newsbrief/2018-03/20180321_NBP-994471-1521658438.png', '2018-03-21 18:53:58', '0000-00-00 00:00:00', 2),
(165, 2, 1, 42, '', 'media/newschannel/newsbrief/2018-03/20180321_NBP-516325-1521658507.png', '2018-03-21 18:55:07', '0000-00-00 00:00:00', 2),
(177, 17, 1, 66, 'test', '', '2018-03-22 05:49:39', '0000-00-00 00:00:00', 2),
(178, 17, 1, 66, 'test2', 'media/newschannel/newsbrief/2018-03/20180322_NBP-118061-1521698092.jpg', '2018-03-22 05:54:52', '0000-00-00 00:00:00', 2),
(182, 20, 1, 76, 'test', 'media/newschannel/newsbrief/2018-03/20180322_NBP-871396-1521699929.jpg', '2018-03-22 06:25:29', '0000-00-00 00:00:00', 2),
(194, 17, 1, 66, 'erfwe', '', '2018-03-22 09:25:03', '0000-00-00 00:00:00', 2),
(195, 17, 1, 66, 'dfsaf', 'media/newschannel/newsbrief/2018-03/20180322_NBP-100720-1521710799.jpeg', '2018-03-22 09:26:39', '0000-00-00 00:00:00', 2),
(196, 20, 1, 76, '', 'media/newschannel/newsbrief/2018-03/20180322_NBP-414740-1521718877.mp4', '2018-03-22 11:41:17', '0000-00-00 00:00:00', 2),
(200, 5, 1, 45, '', 'media/newschannel/newsbrief/2018-03/20180325_NBP-142673-1521990164.jpg', '2018-03-25 15:02:44', '0000-00-00 00:00:00', 2),
(201, 5, 1, 45, '', 'media/newschannel/newsbrief/2018-03/20180325_NBP-682937-1521990240.jpg', '2018-03-25 15:04:00', '0000-00-00 00:00:00', 2),
(202, 5, 1, 45, 'hi', '', '2018-03-25 15:10:07', '0000-00-00 00:00:00', 2);

-- --------------------------------------------------------

--
-- Table structure for table `news_channel`
--

CREATE TABLE `news_channel` (
  `channel_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `channel_name` varchar(255) NOT NULL,
  `channel_description` varchar(500) NOT NULL,
  `channel_logo` varchar(255) NOT NULL,
  `channel_adminid` int(11) NOT NULL,
  `type` int(11) NOT NULL COMMENT '1 means user created or optional 2 means main channel mededtory',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `is_active` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `news_channel`
--

INSERT INTO `news_channel` (`channel_id`, `company_id`, `channel_name`, `channel_description`, `channel_logo`, `channel_adminid`, `type`, `created_at`, `updated_at`, `is_active`) VALUES
(2, 1, 'Chloe’s best friends', '', 'media/newschannel/logo/2018-02/20180207_NCL-940207-1518034641.png', 42, 1, '2018-01-29 22:05:08', '2018-02-11 15:56:11', 1),
(3, 1, 'Test Channel BBC', '', '', 42, 1, '2018-01-29 22:09:25', '2018-02-18 15:55:54', 1),
(4, 1, 'MyChannelSim', '', 'media/newschannel/logo/2018-02/20180222_NCL-351964-1519302798.png', 6, 1, '2018-01-30 08:04:36', '2018-02-22 12:33:18', 1),
(5, 1, 'marketing', '', '', 45, 1, '2018-01-30 20:42:51', '2018-02-11 15:56:41', 1),
(7, 1, 'android test', '', 'media/newschannel/logo/2018-02/20180218_NCL-716026-1518969393.png', 47, 1, '2018-02-04 14:36:28', '2018-02-18 15:56:33', 1),
(8, 1, 'Chloe’s Wonderland', '', 'media/newschannel/logo/2018-02/20180204_NCL-203360-1517765467.png', 48, 1, '2018-02-04 17:31:07', '2018-02-04 18:29:06', 1),
(9, 1, 'Hit Channel', '', '', 50, 1, '2018-02-07 12:30:30', '2018-02-07 12:54:05', 1),
(12, 1, 'myandroidchannel', '', '', 54, 1, '2018-02-14 12:31:17', '2018-02-14 12:31:17', 1),
(14, 1, 'helloandeoid', '', 'media/newschannel/logo/2018-02/20180219_NCL-391908-1519031106.png', 54, 1, '2018-02-19 09:05:06', '2018-02-19 09:05:06', 1),
(15, 1, 'MyIosTeam', '', '', 55, 1, '2018-02-21 06:33:06', '2018-02-21 06:33:06', 1),
(16, 1, 'iphone X Channel', '', '', 53, 1, '2018-02-26 16:11:38', '2018-02-26 16:11:38', 1),
(17, 1, 'rahul_test_channel', '', 'media/newschannel/logo/2018-03/20180315_NCL-530314-1521116404.png', 66, 1, '2018-03-06 06:35:41', '2018-03-15 12:20:04', 1),
(18, 1, 'testchannelios', '', '', 74, 1, '2018-03-14 08:10:10', '2018-03-14 08:10:10', 1),
(19, 1, 'hi', '', '', 75, 1, '2018-03-15 13:55:15', '2018-03-15 13:55:15', 1),
(20, 1, 'yuplay', '', '', 76, 1, '2018-03-19 06:44:17', '2018-03-19 06:44:17', 1),
(21, 1, 'iphonex2 channel', '', '', 82, 1, '2018-03-21 18:45:06', '2018-03-21 18:45:06', 1),
(22, 1, 'channel1', '', '', 86, 1, '2018-03-27 11:29:46', '2018-03-27 11:29:46', 1);

-- --------------------------------------------------------

--
-- Table structure for table `news_deleted`
--

CREATE TABLE `news_deleted` (
  `delete_id` int(11) NOT NULL,
  `channel_id` int(11) NOT NULL,
  `newsbrief_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `deleted_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `news_deleted`
--

INSERT INTO `news_deleted` (`delete_id`, `channel_id`, `newsbrief_id`, `user_id`, `deleted_at`) VALUES
(16, 11, 50, 54, '2018-02-14 12:21:49'),
(17, 11, 56, 54, '2018-02-16 07:56:31'),
(18, 7, 85, 42, '2018-03-04 14:37:20'),
(19, 7, 85, 42, '2018-03-04 14:37:54'),
(20, 7, 85, 48, '2018-03-04 14:38:04'),
(21, 16, 118, 66, '2018-03-19 06:12:27'),
(22, 2, 165, 66, '2018-03-22 08:05:48');

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `forward_cards` tinyint(4) NOT NULL,
  `todo_assigned` tinyint(4) NOT NULL,
  `new_ttalks` int(11) NOT NULL,
  `new_onetoone_msg` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `user_id`, `forward_cards`, `todo_assigned`, `new_ttalks`, `new_onetoone_msg`) VALUES
(1, 38, 0, 0, 0, 0),
(2, 6, 1, 0, 0, 0),
(3, 39, 0, 0, 0, 0),
(4, 40, 0, 0, 0, 0),
(5, 41, 0, 0, 0, 0),
(6, 42, 0, 0, 0, 0),
(7, 45, 0, 0, 0, 0),
(8, 48, 0, 0, 0, 0),
(9, 47, 0, 0, 0, 0),
(11, 50, 11, 0, 0, 0),
(13, 53, 0, 0, 0, 0),
(15, 54, 0, 1, 0, 0),
(16, 55, 0, 0, 0, 0),
(17, 57, 0, 0, 0, 0),
(18, 58, 0, 0, 0, 0),
(19, 59, 0, 0, 0, 0),
(20, 60, 0, 0, 0, 0),
(21, 61, 0, 0, 0, 0),
(22, 66, 0, 0, 0, 0),
(23, 68, 0, 0, 0, 0),
(24, 70, 0, 0, 0, 0),
(25, 71, 0, 0, 0, 0),
(26, 72, 0, 0, 0, 0),
(27, 74, 0, 0, 0, 0),
(29, 80, 0, 0, 0, 0),
(31, 77, 0, 0, 0, 0),
(32, 81, 0, 0, 0, 0),
(33, 82, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `reset_password`
--

CREATE TABLE `reset_password` (
  `reset_id` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `wrong_attempt` tinyint(4) NOT NULL,
  `request_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `blocked_untill` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reset_password`
--

INSERT INTO `reset_password` (`reset_id`, `email`, `wrong_attempt`, `request_time`, `blocked_untill`) VALUES
(1, 'jaswinder.techwin@gmail.com', 1, '2018-03-22 11:49:48', '2018-03-22 12:49:48'),
(2, 'arvind.smugchucks@gmail.com', 1, '2018-03-27 05:16:20', '2018-03-27 06:16:20');

-- --------------------------------------------------------

--
-- Table structure for table `rolodex`
--

CREATE TABLE `rolodex` (
  `rolodex_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `card_id` int(11) NOT NULL,
  `added_via` int(11) NOT NULL COMMENT '0 by normaly 1 by forworded',
  `note` varchar(500) NOT NULL,
  `latitude` varchar(20) NOT NULL,
  `longitude` varchar(20) NOT NULL,
  `received_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rolodex`
--

INSERT INTO `rolodex` (`rolodex_id`, `user_id`, `card_id`, `added_via`, `note`, `latitude`, `longitude`, `received_at`) VALUES
(7, 44, 29, 0, '', '', '', '2018-02-07 12:39:34'),
(9, 46, 1, 1, '', '', '', '0000-00-00 00:00:00'),
(15, 41, 10, 1, '', '', '', '0000-00-00 00:00:00'),
(16, 44, 25, 0, '', '', '', '2018-02-09 11:08:07'),
(23, 45, 10, 1, '', '', '', '0000-00-00 00:00:00'),
(26, 51, 25, 0, '', '', '', '2018-02-14 08:42:02'),
(30, 51, 79, 0, '', '', '', '2018-02-15 14:37:37'),
(31, 47, 17, 0, '', '', '', '2018-02-18 16:04:54'),
(34, 53, 8, 0, '', '48.118981', '11.573604', '2018-02-18 17:33:16'),
(35, 47, 10, 0, '', '', '', '2018-02-18 17:35:26'),
(36, 54, 25, 0, '', '', '', '2018-02-19 05:52:39'),
(37, 54, 107, 0, '', '', '', '2018-02-19 11:45:30'),
(40, 6, 47, 1, '', '', '', '0000-00-00 00:00:00'),
(41, 50, 1, 1, '', '', '', '0000-00-00 00:00:00'),
(42, 42, 10, 0, '', '48.118917', '11.573445', '2018-02-25 15:24:52'),
(47, 53, 17, 1, '', '', '', '0000-00-00 00:00:00'),
(48, 62, 25, 0, '', '', '', '2018-02-26 07:28:26'),
(59, 72, 47, 1, '', '', '', '2018-03-09 07:32:32'),
(60, 53, 10, 1, '', '', '', '2018-03-11 22:12:02'),
(62, 66, 1, 0, '', '', '', '2018-03-13 07:46:18'),
(63, 41, 47, 1, '', '', '', '2018-03-13 13:26:02'),
(64, 46, 47, 1, '', '', '', '2018-03-13 13:26:02'),
(65, 57, 47, 1, '', '', '', '2018-03-13 13:26:02'),
(77, 2147483647, 1, 1, '', '', '', '2018-03-16 10:28:23'),
(79, 74, 25, 1, '', '', '', '2018-03-16 10:38:50'),
(80, 45, 17, 0, '', '', '', '2018-03-18 15:06:12'),
(83, 76, 146, 0, '', '', '', '2018-03-20 07:18:11'),
(85, 77, 1, 0, '', '30.708670', '76.692162', '2018-03-20 10:57:52'),
(88, 76, 25, 0, '', '', '', '2018-03-22 12:23:45'),
(90, 71, 174, 1, '', '', '', '2018-03-23 06:45:55'),
(92, 44, 1, 1, '', '', '', '2018-03-23 08:03:18'),
(93, 49, 146, 0, '', '', '', '2018-03-23 08:04:27'),
(94, 49, 174, 0, '', '', '', '2018-03-23 09:36:34'),
(95, 86, 240, 0, '', '', '', '2018-03-27 12:42:33'),
(96, 84, 228, 0, '', '', '', '2018-03-27 12:43:40');

-- --------------------------------------------------------

--
-- Table structure for table `rolodexcard_notes`
--

CREATE TABLE `rolodexcard_notes` (
  `noteid` int(11) NOT NULL,
  `rolodex_id` int(11) NOT NULL,
  `note` varchar(500) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rolodexcard_notes`
--

INSERT INTO `rolodexcard_notes` (`noteid`, `rolodex_id`, `note`, `updated_at`) VALUES
(1, 25, 'Asa was as Adams as ', '2018-02-15 06:15:03'),
(7, 28, 'Hello android developers', '2018-02-15 10:19:42'),
(3, 28, 'Hello android team', '2018-02-15 07:23:44'),
(12, 36, 'hello', '2018-02-19 05:55:36'),
(6, 28, 'Hello team', '2018-02-15 10:15:04'),
(13, 40, 'Adamsdasdas', '2018-03-06 12:37:30'),
(11, 26, 'hello team', '2018-02-15 13:08:02'),
(14, 49, 'hello team', '2018-03-07 14:16:10'),
(15, 49, 'hi I team what\'s up', '2018-03-07 14:26:27'),
(16, 49, 'ydjddj', '2018-03-08 05:19:48'),
(17, 47, 'Handsome guy. But not Daniel. :)', '2018-03-11 21:52:49'),
(18, 61, 'hello', '2018-03-13 09:22:39'),
(21, 62, 'hiiiiii', '2018-03-13 11:07:54'),
(23, 61, 'cugnvkg', '2018-03-13 11:18:40');

-- --------------------------------------------------------

--
-- Table structure for table `rolodexcard_pictures`
--

CREATE TABLE `rolodexcard_pictures` (
  `picture_id` int(11) NOT NULL,
  `rolodex_id` int(11) NOT NULL,
  `picture` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rolodexcard_pictures`
--

INSERT INTO `rolodexcard_pictures` (`picture_id`, `rolodex_id`, `picture`) VALUES
(1, 25, 'media/cards/rolodex_picture/2018-02/20180215_ROLXP-1445-1518675709250.png'),
(2, 28, 'media/cards/rolodex_picture/2018-02/20180215_ROLXP-5735-1518679527280.png'),
(3, 49, 'media/cards/rolodex_picture/2018-03/20180307_ROLXP-8125-1520432188490.png'),
(4, 49, 'media/cards/rolodex_picture/2018-03/20180307_ROLXP-3059-1520432802490.png'),
(5, 49, 'media/cards/rolodex_picture/2018-03/20180307_ROLXP-3675-1520432895490.png'),
(6, 49, 'media/cards/rolodex_picture/2018-03/20180307_ROLXP-7455-1520432945490.png'),
(7, 49, 'media/cards/rolodex_picture/2018-03/20180308_ROLXP-7701-1520486404490.png'),
(8, 47, 'media/cards/rolodex_picture/2018-03/20180311_ROLXP-9045-1520805204470.png'),
(9, 47, 'media/cards/rolodex_picture/2018-03/20180311_ROLXP-3003-1520805520470.png'),
(14, 62, 'media/cards/rolodex_picture/2018-03/20180313_ROLXP-4986-1520938915620.png'),
(15, 62, 'media/cards/rolodex_picture/2018-03/20180313_ROLXP-2928-1520939259620.png'),
(16, 61, 'media/cards/rolodex_picture/2018-03/20180313_ROLXP-5008-1520939930610.png'),
(18, 42, 'media/cards/rolodex_picture/2018-03/20180318_ROLXP-2347-1521370929420.png'),
(19, 42, 'media/cards/rolodex_picture/2018-03/20180318_ROLXP-3416-1521371135420.png'),
(20, 40, 'media/cards/rolodex_picture/2018-03/20180319_ROLXP-3434-1521461098400.png'),
(21, 42, 'media/cards/rolodex_picture/2018-03/20180321_ROLXP-9487-1521660717420.png');

-- --------------------------------------------------------

--
-- Table structure for table `rolodex_forwarded_cards`
--

CREATE TABLE `rolodex_forwarded_cards` (
  `forward_id` int(11) NOT NULL,
  `phoneno` varchar(20) NOT NULL,
  `card_id` int(11) NOT NULL,
  `forward_by` int(11) NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rolodex_forwarded_cards`
--

INSERT INTO `rolodex_forwarded_cards` (`forward_id`, `phoneno`, `card_id`, `forward_by`, `date`) VALUES
(1, '14379925718', 1, 50, '2018-02-08 07:57:37'),
(2, '447462914341', 1, 50, '2018-02-08 07:57:37'),
(3, '9109910150225', 1, 50, '2018-02-08 07:57:37'),
(4, '919988853555', 1, 50, '2018-02-08 07:57:37'),
(5, '18885555512', 47, 50, '2018-02-10 04:48:53'),
(6, '919888875096', 1, 6, '2018-02-10 05:45:32'),
(7, '4901706177395', 17, 45, '2018-02-11 16:21:11'),
(8, '4915251971915', 10, 42, '2018-02-11 16:21:17'),
(9, '491736289745', 10, 42, '2018-02-11 16:21:17'),
(10, '779296969287', 10, 42, '2018-02-11 16:22:30'),
(11, '478458854', 10, 42, '2018-02-11 16:25:32'),
(12, '+91 90347 68570', 47, 6, '2018-02-12 13:12:13'),
(13, '+91 98 88 171184', 47, 6, '2018-02-12 13:12:13'),
(14, '+44 7462 914341', 47, 6, '2018-02-12 13:12:13'),
(15, '+1 888-555-5512', 47, 50, '2018-02-12 13:27:34'),
(17, '+91 99883 59350', 47, 6, '2018-02-15 08:20:44'),
(18, '+1 204-951-7772', 47, 6, '2018-02-15 08:20:44'),
(19, '+91 00 1 647-978-322', 47, 6, '2018-02-15 08:20:44'),
(20, '+91 9888875096', 1, 6, '2018-02-15 08:21:32'),
(21, '+91 94 17 495195', 47, 50, '2018-02-20 17:15:10'),
(22, '+91 70870 94104', 47, 50, '2018-02-20 17:15:10'),
(23, '+91 00 1 647-978-322', 47, 50, '2018-02-20 17:15:10'),
(24, '+91 00 1 647-978-322', 47, 50, '2018-02-20 17:15:56'),
(25, '+1 (842) 763-1369', 1, 6, '2018-02-20 17:18:20'),
(26, '+91 84255 55555', 1, 6, '2018-02-20 17:18:20'),
(27, '+1 555-522-8243', 1, 6, '2018-02-20 17:26:03'),
(28, '+91 95929 17501', 47, 50, '2018-02-20 17:41:12'),
(29, '+91 99-88-037554', 47, 50, '2018-02-20 17:43:19'),
(30, '015205914413', 10, 42, '2018-02-25 15:40:33'),
(31, '+49 173 6289745', 10, 42, '2018-03-11 22:12:02'),
(32, '015251971915', 10, 42, '2018-03-11 22:12:02'),
(33, '+91 3794 664 656', 1, 66, '2018-03-15 14:55:27'),
(34, '+91 85695 36959', 1, 66, '2018-03-16 07:03:02'),
(35, '+918569563256', 25, 66, '2018-03-16 10:41:34');

-- --------------------------------------------------------

--
-- Table structure for table `security_answers`
--

CREATE TABLE `security_answers` (
  `answer_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `answer` varchar(100) NOT NULL,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `security_answers`
--

INSERT INTO `security_answers` (`answer_id`, `question_id`, `user_id`, `answer`, `updated_at`) VALUES
(4, 1, 6, 'muffy', '2018-01-29 12:12:35'),
(5, 2, 6, 'dad', '2018-01-29 12:12:35'),
(6, 3, 6, 'morning', '2018-01-29 12:12:35'),
(10, 1, 40, 'muffy', '2018-01-29 13:29:47'),
(11, 2, 40, 'dad', '2018-01-29 13:29:47'),
(12, 3, 40, 'morning', '2018-01-29 13:29:47'),
(13, 1, 41, 'tommy', '2018-01-29 14:35:17'),
(14, 2, 41, 'batman', '2018-01-29 14:35:17'),
(15, 3, 41, 'monday', '2018-01-29 14:35:17'),
(16, 1, 42, 'jonas', '2018-01-29 21:19:21'),
(17, 2, 42, 'lothar matthäus', '2018-01-29 21:19:21'),
(18, 3, 42, '22.00', '2018-01-29 21:19:21'),
(19, 1, 43, 'om', '2018-01-30 07:11:36'),
(20, 2, 43, 'om', '2018-01-30 07:11:36'),
(21, 3, 43, 'om', '2018-01-30 07:11:36'),
(22, 1, 44, 'om', '2018-01-30 11:02:46'),
(23, 2, 44, 'om', '2018-01-30 11:02:46'),
(24, 3, 44, '26', '2018-01-30 11:02:46'),
(25, 1, 45, 'silky', '2018-01-30 20:21:50'),
(26, 2, 45, 'bahadur', '2018-01-30 20:21:50'),
(27, 3, 45, '2', '2018-01-30 20:21:50'),
(28, 1, 46, 'om', '2018-01-31 06:55:29'),
(29, 2, 46, 'om', '2018-01-31 06:55:29'),
(30, 3, 46, 'om', '2018-01-31 06:55:29'),
(31, 1, 47, 'silky', '2018-02-04 14:15:44'),
(32, 2, 47, 'bahadur', '2018-02-04 14:15:44'),
(33, 3, 47, '2', '2018-02-04 14:15:44'),
(34, 1, 48, 'maoxian', '2018-02-04 15:00:01'),
(35, 2, 48, 'me', '2018-02-04 15:00:01'),
(36, 3, 48, '22.00', '2018-02-04 15:00:01'),
(37, 1, 49, 'om', '2018-02-06 13:35:54'),
(38, 2, 49, 'om', '2018-02-06 13:35:54'),
(39, 3, 49, 'om', '2018-02-06 13:35:54'),
(40, 1, 50, 'muffy', '2018-02-07 10:58:26'),
(41, 2, 50, 'dad', '2018-02-07 10:58:26'),
(42, 3, 50, 'morning', '2018-02-07 10:58:26'),
(43, 1, 51, 'ok', '2018-02-07 12:23:00'),
(44, 2, 51, 'ok', '2018-02-07 12:23:00'),
(45, 3, 51, 'ok', '2018-02-07 12:23:00'),
(46, 1, 52, 'moti', '2018-02-09 11:58:40'),
(47, 2, 52, 'shaktiman', '2018-02-09 11:58:40'),
(48, 3, 52, '5', '2018-02-09 11:58:40'),
(49, 1, 53, 'jonas', '2018-02-10 15:45:51'),
(50, 2, 53, 'lothar  matthäus', '2018-02-10 15:45:51'),
(51, 3, 53, '22.00', '2018-02-10 15:45:51'),
(52, 1, 54, 'chhote', '2018-02-14 12:16:50'),
(53, 2, 54, 'sunny deol', '2018-02-14 12:16:50'),
(54, 3, 54, '1993', '2018-02-14 12:16:50'),
(55, 1, 55, 'newly', '2018-02-19 11:33:28'),
(56, 2, 55, 'salman khan', '2018-02-19 11:33:28'),
(57, 3, 55, '1994', '2018-02-19 11:33:28'),
(58, 1, 56, 'pradeep', '2018-02-20 11:13:49'),
(59, 2, 56, 'akshay', '2018-02-20 11:13:49'),
(60, 3, 56, '1994', '2018-02-20 11:13:49'),
(61, 1, 57, 'muffy', '2018-02-20 12:15:09'),
(62, 2, 57, 'dad', '2018-02-20 12:15:09'),
(63, 3, 57, 'morning', '2018-02-20 12:15:09'),
(64, 1, 58, 'muffy', '2018-02-20 12:35:09'),
(65, 2, 58, 'dad', '2018-02-20 12:35:09'),
(66, 3, 58, 'morning', '2018-02-20 12:35:09'),
(67, 1, 59, 'muffy', '2018-02-20 12:42:35'),
(68, 2, 59, 'dad', '2018-02-20 12:42:35'),
(69, 3, 59, 'morning', '2018-02-20 12:42:35'),
(70, 1, 60, 'muffy', '2018-02-22 13:52:11'),
(71, 2, 60, 'dad', '2018-02-22 13:52:11'),
(72, 3, 60, 'morning', '2018-02-22 13:52:11'),
(73, 1, 61, 'jonas', '2018-02-25 16:58:10'),
(74, 2, 61, 'lothar matthäus', '2018-02-25 16:58:10'),
(75, 3, 61, '22.00', '2018-02-25 16:58:10'),
(76, 1, 62, 'raju', '2018-02-26 06:09:36'),
(77, 2, 62, 'sunil', '2018-02-26 06:09:36'),
(78, 3, 62, '1993', '2018-02-26 06:09:36'),
(79, 1, 63, 'raju', '2018-02-27 14:14:33'),
(80, 2, 63, 'bholu', '2018-02-27 14:14:33'),
(81, 3, 63, '1993', '2018-02-27 14:14:33'),
(82, 1, 64, 'chjotechhote', '2018-02-28 07:38:15'),
(83, 2, 64, 'amit', '2018-02-28 07:38:15'),
(84, 3, 64, '1993', '2018-02-28 07:38:15'),
(85, 1, 65, 'chhote', '2018-02-28 08:16:47'),
(86, 2, 65, 'amit', '2018-02-28 08:16:47'),
(87, 3, 65, '1993', '2018-02-28 08:16:47'),
(88, 1, 66, 'chhote', '2018-02-28 08:36:00'),
(89, 2, 66, 'amit', '2018-02-28 08:36:00'),
(90, 3, 66, '1993', '2018-02-28 08:36:00'),
(91, 1, 68, 'dads', '2018-03-06 06:08:19'),
(92, 2, 68, 'sdfdsf', '2018-03-06 06:08:19'),
(93, 3, 68, 'sddd', '2018-03-06 06:08:19'),
(94, 1, 70, 'dog', '2018-03-06 08:03:21'),
(95, 2, 70, 'dog', '2018-03-06 08:03:21'),
(96, 3, 70, 'morning', '2018-03-06 08:03:21'),
(97, 1, 71, 'dog', '2018-03-06 13:03:17'),
(98, 2, 71, 'dad', '2018-03-06 13:03:17'),
(99, 3, 71, 'morning', '2018-03-06 13:03:17'),
(100, 1, 72, 'tommy', '2018-03-08 06:39:16'),
(101, 2, 72, 'dogra', '2018-03-08 06:39:16'),
(102, 3, 72, 'morning', '2018-03-08 06:39:16'),
(103, 1, 74, 'dog', '2018-03-14 08:08:38'),
(104, 2, 74, 'dad', '2018-03-14 08:08:38'),
(105, 3, 74, 'morning', '2018-03-14 08:08:38'),
(106, 1, 75, 'pradeep', '2018-03-14 11:49:57'),
(107, 2, 75, 'puneet', '2018-03-14 11:49:57'),
(108, 3, 75, 'morning', '2018-03-14 11:49:57'),
(109, 1, 76, 'test', '2018-03-16 05:19:36'),
(110, 2, 76, 'test', '2018-03-16 05:19:36'),
(111, 3, 76, 'test', '2018-03-16 05:19:36'),
(112, 1, 77, 'dog', '2018-03-16 05:51:20'),
(113, 2, 77, 'dad', '2018-03-16 05:51:20'),
(114, 3, 77, 'morning', '2018-03-16 05:51:20'),
(115, 1, 78, 'neelu', '2018-03-16 14:20:38'),
(116, 2, 78, 'none', '2018-03-16 14:20:38'),
(117, 3, 78, 'morning', '2018-03-16 14:20:38'),
(118, 1, 79, 'jonasx3', '2018-03-16 14:54:57'),
(119, 2, 79, 'lothar matthäusx3', '2018-03-16 14:54:57'),
(120, 3, 79, '22.00x3', '2018-03-16 14:54:57'),
(121, 1, 80, 'jonas', '2018-03-17 22:43:48'),
(122, 2, 80, 'lothar matthäus', '2018-03-17 22:43:48'),
(123, 3, 80, '22.00', '2018-03-17 22:43:48'),
(124, 1, 81, 'muzi', '2018-03-21 15:14:56'),
(125, 2, 81, 'batman', '2018-03-21 15:14:56'),
(126, 3, 81, '11.00', '2018-03-21 15:14:56'),
(127, 1, 82, 'jonas', '2018-03-21 18:43:03'),
(128, 2, 82, 'lothar matthäus', '2018-03-21 18:43:03'),
(129, 3, 82, '22.00', '2018-03-21 18:43:03'),
(130, 1, 84, 'om', '2018-03-26 09:58:59'),
(131, 2, 84, 'om', '2018-03-26 09:58:59'),
(132, 3, 84, 'om', '2018-03-26 09:58:59'),
(133, 1, 85, 'om', '2018-03-27 05:34:22'),
(134, 2, 85, 'om', '2018-03-27 05:34:22'),
(135, 3, 85, 'om', '2018-03-27 05:34:22'),
(136, 1, 86, 'om', '2018-03-27 07:51:28'),
(137, 2, 86, 'om', '2018-03-27 07:51:28'),
(138, 3, 86, 'om', '2018-03-27 07:51:28');

-- --------------------------------------------------------

--
-- Table structure for table `security_questions`
--

CREATE TABLE `security_questions` (
  `question_id` int(11) NOT NULL,
  `question` varchar(255) NOT NULL,
  `isactive` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `security_questions`
--

INSERT INTO `security_questions` (`question_id`, `question`, `isactive`) VALUES
(1, 'What is your pet\'s name?', 1),
(2, 'Who was your childhood hero?', 1),
(3, 'What time of the day were you born?', 1);

-- --------------------------------------------------------

--
-- Table structure for table `signup_allowed`
--

CREATE TABLE `signup_allowed` (
  `allowed_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `email` varchar(500) NOT NULL,
  `invited_by` int(11) NOT NULL,
  `invited_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_active` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `signup_allowed`
--

INSERT INTO `signup_allowed` (`allowed_id`, `company_id`, `email`, `invited_by`, `invited_at`, `is_active`) VALUES
(4, 1, 'vsannabhadti@gmail.com', 1, '2018-01-29 21:13:25', 1),
(7, 1, 'vivek.sannabhadti@alumni.insead.edu', 1, '2018-01-30 22:02:02', 1),
(9, 1, 'android01@gmail.com', 1, '2018-01-31 06:31:06', 1),
(11, 1, 'viveksannatest@gmail.com', 1, '2018-02-04 14:11:59', 1),
(12, 1, 'hitesh.techwin@gmail.com', 1, '2018-02-07 10:56:24', 1),
(14, 1, 'puneet.techwinlabs@gmail.com', 1, '2018-02-09 11:47:08', 1),
(16, 1, 'inrtestsamsunggalaxy3@gmail.com', 1, '2018-02-10 14:59:28', 1),
(18, 1, 'rahul2.techwinlabs@gmail.com', 1, '2018-02-14 12:13:11', 1),
(20, 1, 'pradeep.techwinlabs@gmail.com', 1, '2018-02-20 08:28:34', 1),
(21, 1, 'caftapnewu@gmail.com', 1, '2018-02-20 11:54:41', 1),
(23, 1, 'sandeep.techwinlabs@gmail.com', 1, '2018-03-01 09:53:17', 1),
(24, 1, 'inrtestsamsunggalaxys3@gmail.com', 1, '2018-03-04 14:20:39', 1),
(25, 1, 'anuj.techwinlabs@gmail.com', 1, '2018-03-08 06:34:31', 1),
(28, 1, 'kawal.techwinlabs@gmail.com', 1, '2018-03-13 10:14:14', 1),
(29, 1, 'neelam.techwinlabs@gmail.com', 1, '2018-03-16 14:13:23', 1),
(31, 1, 'amit.techwinlabs@gmail.com', 1, '2018-03-20 07:45:36', 1),
(32, 1, 'inrtestiphonex3@gmail.com', 1, '2018-03-21 15:08:59', 1),
(33, 1, 'djanouschek@hotmail.com', 1, '2018-03-21 15:09:17', 1),
(34, 1, 'inrtestiphonex2@gmail.com', 1, '2018-03-21 16:29:06', 1),
(35, 1, 'djanouschek@gmail.com', 1, '2018-03-21 17:25:30', 1),
(37, 1, 'arvindpoonia01@gmail.com', 1, '2018-03-26 06:54:30', 1),
(38, 1, 'arvindpoonia04@gmail.com', 1, '2018-03-27 05:23:56', 1),
(39, 1, 'jaswinder.techwin@gmail.com', 1, '2018-03-27 05:29:48', 1),
(40, 1, 'arvindpoonia.techwinlabs@gmail.com', 1, '2018-03-27 07:17:30', 1),
(41, 1, 'rosamunde.hofbauer@gmail.com', 1, '2018-03-30 11:22:32', 1);

-- --------------------------------------------------------

--
-- Table structure for table `teams`
--

CREATE TABLE `teams` (
  `team_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `team_name` varchar(255) NOT NULL,
  `team_coverphoto` varchar(255) NOT NULL,
  `briefing_validity` int(11) NOT NULL DEFAULT '7',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_active` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `teams`
--

INSERT INTO `teams` (`team_id`, `company_id`, `user_id`, `team_name`, `team_coverphoto`, `briefing_validity`, `created_at`, `updated_at`, `is_active`) VALUES
(1, 1, 42, 'Clean Sheet Team', 'media/teams/team_coverphoto/2018-01/20180129_TCP-7287-1517260978.png', 0, '2018-01-29 21:22:58', '2018-03-04 11:49:22', 1),
(2, 1, 42, 'Testing Team', 'media/teams/team_coverphoto/2018-01/20180129_TCP-1000-1517261212.png', 7, '2018-01-29 21:26:52', '2018-01-29 21:26:52', 1),
(3, 1, 42, 'Team Created via TTalk', 'media/teams/team_coverphoto/2018-01/20180129_TCP-4899-1517262142.png', 7, '2018-01-29 21:42:22', '2018-01-29 21:42:22', 1),
(4, 1, 6, 'NewTeam', 'media/teams/team_coverphoto/2018-01/20180130_TCP-1282-1517299221.png', 7, '2018-01-30 08:00:21', '2018-01-30 08:00:21', 1),
(5, 1, 6, 'New Team2', 'media/teams/team_coverphoto/2018-01/20180130_TCP-9678-1517299778.png', 7, '2018-01-30 08:09:38', '2018-01-30 08:09:38', 1),
(6, 1, 43, 'Teamone', '', 7, '2018-01-30 12:27:46', '2018-01-30 12:27:46', 1),
(7, 1, 45, 'have fun marketing', '', 7, '2018-01-30 20:42:00', '2018-01-30 20:42:00', 1),
(8, 1, 46, 'Newteamone', '', 7, '2018-02-01 06:32:12', '2018-02-01 06:32:12', 1),
(9, 1, 46, 'dhggf', '', 7, '2018-02-01 10:51:42', '2018-02-01 10:51:42', 1),
(10, 1, 47, 'android test', '', 0, '2018-02-04 14:33:02', '2018-03-04 15:15:29', 1),
(11, 1, 44, 'android team', '', 7, '2018-02-05 06:13:55', '2018-02-05 06:13:55', 1),
(12, 1, 46, 'Trainning', 'media/teams/team_coverphoto/2018-02/20180206_TCP-5212-1517920526.png', 7, '2018-02-06 12:35:26', '2018-02-06 12:35:26', 1),
(13, 1, 48, 'Chloe’s First Own team', 'media/teams/team_coverphoto/2018-02/20180206_TCP-6603-1517931956.png', 7, '2018-02-06 15:45:56', '2018-02-06 15:45:56', 1),
(16, 1, 48, 'Chloe & Daniel Fight Team', 'media/teams/team_coverphoto/2018-02/20180206_TCP-9496-1517934510.png', 7, '2018-02-06 16:28:30', '2018-02-06 16:28:30', 1),
(17, 1, 50, 'Hitnewteam', 'media/teams/team_coverphoto/2018-02/20180207_TCP-6705-1518002005.png', 7, '2018-02-07 11:13:25', '2018-02-07 11:13:25', 1),
(18, 1, 50, 'Hitnewteam2', 'media/teams/team_coverphoto/2018-02/20180207_TCP-9742-1518005350.png', 7, '2018-02-07 12:09:10', '2018-02-07 12:09:10', 1),
(19, 1, 51, 'moto', '', 7, '2018-02-07 12:45:30', '2018-02-07 12:45:30', 1),
(20, 1, 6, 'SimranTeam', 'media/teams/team_coverphoto/2018-02/20180207_TCP-9064-1518010491.png', 7, '2018-02-07 13:34:51', '2018-02-07 13:34:51', 1),
(21, 1, 6, 'SimranTeam2', 'media/teams/team_coverphoto/2018-02/20180207_TCP-9339-1518010893.png', 7, '2018-02-07 13:41:33', '2018-02-07 13:41:33', 1),
(22, 1, 44, 'teamno1', '', 7, '2018-02-08 10:32:24', '2018-02-08 10:32:24', 1),
(24, 1, 44, 'team3', '', 7, '2018-02-08 13:40:02', '2018-02-08 13:40:02', 1),
(25, 1, 49, 'Emulator', '', 7, '2018-02-08 13:40:33', '2018-02-08 13:40:33', 1),
(26, 1, 49, 'Emulator2', '', 7, '2018-02-08 13:41:05', '2018-02-08 13:41:05', 1),
(27, 1, 49, 'Emulater3', '', 7, '2018-02-08 13:41:22', '2018-02-08 13:41:22', 1),
(28, 1, 42, '11 Feb Team', 'media/teams/team_coverphoto/2018-02/20180211_TCP-4642-1518360617.png', 7, '2018-02-11 14:50:17', '2018-02-11 14:50:17', 1),
(29, 1, 42, '11 Feb Team b', 'media/teams/team_coverphoto/2018-02/20180211_TCP-7841-1518364450.png', 0, '2018-02-11 15:54:10', '2018-03-04 15:14:45', 1),
(30, 1, 49, 'Emulator4', '', 7, '2018-02-12 11:32:06', '2018-02-12 11:32:06', 1),
(32, 1, 53, 'IPhone X Team', 'media/teams/team_coverphoto/2018-02/20180215_TCP-6842-1518703093.png', 7, '2018-02-15 13:58:13', '2018-02-15 13:58:13', 1),
(33, 1, 47, 'timepass', '', 7, '2018-02-18 14:59:53', '2018-02-18 14:59:53', 1),
(34, 1, 42, 'Bloody Sunday', 'media/teams/team_coverphoto/2018-02/20180218_TCP-9457-1518966008.png', 7, '2018-02-18 15:00:08', '2018-02-18 15:00:08', 1),
(35, 1, 47, 'tp1', '', 7, '2018-02-18 15:02:22', '2018-02-18 15:02:22', 1),
(36, 1, 54, 'mynewgroup', '', 7, '2018-02-19 06:51:52', '2018-02-19 06:51:52', 1),
(37, 1, 55, 'NewTeamTech', 'media/teams/team_coverphoto/2018-02/20180221_TCP-7610-1519195073.png', 7, '2018-02-21 06:37:53', '2018-02-21 06:37:53', 1),
(38, 1, 62, 'Mytest_android_team', '', 7, '2018-02-26 06:58:57', '2018-02-26 06:58:57', 1),
(39, 1, 66, 'Android gamer', '', 7, '2018-03-06 04:38:35', '2018-03-06 04:38:35', 1),
(40, 1, 68, 'Hello', 'media/teams/team_coverphoto/2018-03/20180306_TCP-2106-1520316544.png', 7, '2018-03-06 06:09:04', '2018-03-06 06:09:04', 1),
(41, 1, 72, 'Pho', 'media/teams/team_coverphoto/2018-03/20180308_TCP-4622-1520492152.png', 7, '2018-03-08 06:55:52', '2018-03-08 06:55:52', 1),
(42, 1, 75, 'my team', '', 7, '2018-03-15 13:53:15', '2018-03-15 13:53:15', 1),
(43, 1, 77, 'my testing team ios', '', 7, '2018-03-16 05:52:32', '2018-03-16 05:52:32', 1),
(44, 1, 76, 'prsep', '', 7, '2018-03-19 07:57:50', '2018-03-19 07:57:50', 1),
(45, 1, 81, '21 March Team', 'media/teams/team_coverphoto/2018-03/20180321_TCP-7670-1521649419.png', 7, '2018-03-21 16:23:39', '2018-03-21 16:23:39', 1),
(46, 1, 71, 'Sandeep Testing Team', 'media/teams/team_coverphoto/2018-03/20180323_TCP-1695-1521787999.png', 7, '2018-03-23 06:53:19', '2018-03-23 06:53:19', 1),
(47, 1, 86, 'yuplay', 'media/teams/team_coverphoto/2018-03/20180327_TCP-9191-1522150149.png', 7, '2018-03-27 11:29:09', '2018-03-27 11:29:09', 1),
(48, 1, 84, 'gdhd', '', 7, '2018-03-28 05:12:38', '2018-03-28 05:12:38', 1);

-- --------------------------------------------------------

--
-- Table structure for table `team_invites`
--

CREATE TABLE `team_invites` (
  `invite_id` int(11) NOT NULL,
  `team_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `inviter_id` int(11) NOT NULL,
  `invited_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1 requested, 0 rejected'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `team_invites`
--

INSERT INTO `team_invites` (`invite_id`, `team_id`, `user_id`, `inviter_id`, `invited_on`, `status`) VALUES
(3, 2, 41, 42, '2018-01-29 21:26:52', 1),
(4, 2, 19, 42, '2018-01-29 21:26:52', 1),
(5, 2, 40, 42, '2018-01-29 21:26:52', 1),
(8, 3, 40, 42, '2018-01-29 21:42:22', 1),
(22, 8, 43, 46, '2018-02-01 06:32:12', 1),
(23, 8, 1, 46, '2018-02-01 06:32:12', 1),
(33, 12, 43, 46, '2018-02-06 12:35:26', 1),
(34, 12, 41, 46, '2018-02-06 12:35:26', 1),
(37, 13, 43, 48, '2018-02-06 15:45:56', 1),
(53, 5, 50, 6, '2018-02-12 07:02:00', 0),
(54, 22, 43, 44, '2018-02-08 10:32:24', 1),
(76, 29, 48, 42, '2018-03-11 21:21:56', 0),
(114, 38, 54, 62, '2018-02-26 06:58:57', 1),
(118, 34, 45, 42, '2018-03-04 14:33:29', 1),
(125, 40, 40, 68, '2018-03-06 06:09:04', 1),
(126, 40, 19, 68, '2018-03-06 06:09:04', 1),
(128, 21, 50, 6, '2018-03-07 07:48:16', 1),
(132, 41, 54, 72, '2018-03-08 06:55:52', 1),
(133, 41, 62, 72, '2018-03-08 06:55:52', 1),
(137, 20, 50, 6, '2018-03-08 08:02:48', 1),
(139, 20, 54, 6, '2018-03-08 08:02:48', 1),
(155, 39, 73, 66, '2018-03-14 08:59:25', 1),
(156, 39, 54, 66, '2018-03-14 08:59:25', 1),
(157, 39, 62, 66, '2018-03-14 08:59:25', 1),
(158, 39, 64, 66, '2018-03-14 08:59:25', 1),
(160, 42, 0, 75, '2018-03-15 13:53:15', 1),
(161, 43, 0, 77, '2018-03-16 05:52:32', 1),
(163, 32, 48, 53, '2018-03-18 09:00:23', 1),
(164, 32, 45, 53, '2018-03-18 09:00:23', 1),
(165, 44, 72, 76, '2018-03-19 07:57:50', 1),
(168, 45, 48, 81, '2018-03-21 16:23:39', 1),
(170, 45, 45, 81, '2018-03-21 16:23:39', 1),
(174, 46, 50, 71, '2018-03-23 06:53:19', 1),
(175, 46, 77, 71, '2018-03-23 06:53:19', 1),
(178, 47, 85, 86, '2018-03-27 11:29:09', 1),
(179, 48, 72, 84, '2018-03-28 05:12:38', 1);

-- --------------------------------------------------------

--
-- Table structure for table `team_members`
--

CREATE TABLE `team_members` (
  `member_id` int(11) NOT NULL,
  `team_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `team_owner` int(11) NOT NULL,
  `member_type` int(11) NOT NULL COMMENT '0 for member 1 for secretory 2 for admin',
  `ttalk_status` int(11) NOT NULL COMMENT '0 means allowed 1 means not allowed',
  `joined_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `unread_briefcount` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL COMMENT '0 active 1 inactive'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `team_members`
--

INSERT INTO `team_members` (`member_id`, `team_id`, `user_id`, `team_owner`, `member_type`, `ttalk_status`, `joined_at`, `unread_briefcount`, `status`) VALUES
(1, 1, 42, 42, 2, 0, '2018-01-29 21:22:58', 0, 0),
(2, 2, 42, 42, 2, 0, '2018-01-29 21:26:52', 0, 0),
(3, 3, 42, 42, 2, 0, '2018-01-29 21:42:22', 0, 0),
(4, 4, 6, 6, 2, 0, '2018-01-30 08:00:21', 0, 0),
(5, 5, 6, 6, 2, 0, '2018-01-30 08:09:38', 1, 0),
(6, 6, 43, 43, 2, 0, '2018-01-30 12:27:46', 1, 0),
(7, 7, 45, 45, 2, 0, '2018-01-30 20:42:00', 0, 0),
(8, 7, 42, 45, 0, 0, '2018-01-31 14:48:05', 0, 0),
(9, 6, 42, 43, 0, 0, '2018-01-31 14:48:07', 0, 0),
(10, 4, 42, 6, 0, 0, '2018-01-31 14:48:14', 3, 1),
(11, 8, 46, 0, 0, 0, '2018-02-01 06:32:12', 0, 1),
(12, 8, 44, 46, 0, 0, '2018-02-01 07:19:18', 3, 1),
(13, 9, 46, 0, 0, 0, '2018-02-01 10:51:42', 0, 1),
(14, 10, 47, 47, 2, 0, '2018-02-04 14:33:02', 0, 0),
(15, 8, 42, 46, 0, 0, '2018-02-04 14:33:21', 10, 1),
(16, 10, 42, 47, 0, 0, '2018-02-04 14:33:24', 0, 1),
(17, 8, 45, 46, 0, 0, '2018-02-04 14:41:40', 0, 0),
(18, 1, 45, 42, 0, 0, '2018-02-04 14:41:41', 0, 0),
(19, 2, 45, 42, 0, 0, '2018-02-04 14:41:42', 1, 1),
(20, 3, 45, 42, 0, 0, '2018-02-04 14:41:44', 1, 0),
(21, 2, 47, 42, 0, 0, '2018-02-04 17:39:52', 0, 0),
(22, 1, 48, 42, 0, 0, '2018-02-04 18:16:46', 14, 0),
(23, 7, 47, 45, 0, 0, '2018-02-04 18:47:00', 0, 0),
(24, 11, 44, 0, 0, 0, '2018-02-05 06:13:55', 0, 1),
(25, 11, 46, 44, 0, 0, '2018-02-05 06:14:14', 0, 1),
(26, 12, 46, 0, 0, 0, '2018-02-06 12:35:26', 2, 1),
(27, 13, 48, 48, 2, 0, '2018-02-06 15:45:56', 0, 0),
(29, 3, 48, 42, 0, 0, '2018-02-06 15:46:31', 0, 0),
(31, 16, 48, 48, 2, 0, '2018-02-06 16:28:30', 1, 0),
(32, 13, 42, 48, 0, 0, '2018-02-06 16:29:57', 1, 0),
(33, 16, 42, 48, 0, 0, '2018-02-06 16:29:58', 0, 0),
(34, 9, 6, 46, 0, 0, '2018-02-07 07:26:13', 0, 1),
(35, 8, 6, 46, 0, 0, '2018-02-07 07:26:15', 0, 0),
(36, 2, 6, 42, 0, 0, '2018-02-07 07:26:18', 0, 0),
(37, 1, 6, 42, 0, 0, '2018-02-07 07:26:19', 14, 0),
(38, 17, 50, 50, 2, 0, '2018-02-07 11:13:25', 0, 0),
(39, 17, 6, 50, 0, 0, '2018-02-07 12:01:20', 0, 1),
(40, 18, 50, 50, 2, 0, '2018-02-07 12:09:10', 0, 0),
(41, 18, 6, 50, 0, 0, '2018-02-07 12:21:28', 0, 1),
(42, 19, 51, 0, 0, 0, '2018-02-07 12:45:30', 0, 1),
(43, 12, 44, 46, 0, 0, '2018-02-07 12:56:38', 0, 1),
(44, 20, 6, 6, 2, 0, '2018-02-07 13:34:51', 0, 0),
(45, 21, 6, 6, 2, 0, '2018-02-07 13:41:33', 0, 0),
(46, 22, 44, 0, 0, 0, '2018-02-08 10:32:24', 5, 1),
(49, 24, 44, 0, 0, 0, '2018-02-08 13:40:02', 0, 1),
(50, 25, 49, 0, 0, 0, '2018-02-08 13:40:33', 0, 1),
(51, 25, 44, 49, 0, 0, '2018-02-08 13:40:40', 27, 1),
(52, 26, 49, 0, 0, 0, '2018-02-08 13:41:05', 0, 1),
(53, 27, 49, 0, 0, 0, '2018-02-08 13:41:22', 0, 1),
(54, 27, 44, 49, 0, 0, '2018-02-08 13:41:25', 2, 1),
(55, 26, 44, 49, 0, 0, '2018-02-08 13:41:27', 0, 1),
(56, 22, 42, 44, 0, 0, '2018-02-08 21:54:06', 5, 0),
(57, 28, 42, 42, 2, 0, '2018-02-11 14:50:17', 0, 0),
(58, 28, 47, 42, 0, 0, '2018-02-11 14:51:02', 0, 0),
(59, 28, 45, 42, 0, 0, '2018-02-11 14:53:45', 1, 0),
(60, 28, 48, 42, 0, 0, '2018-02-11 15:27:12', 0, 0),
(61, 1, 47, 42, 0, 0, '2018-02-11 15:43:23', 0, 1),
(62, 29, 42, 42, 2, 0, '2018-02-11 15:54:10', 0, 0),
(63, 29, 47, 42, 0, 0, '2018-02-11 15:54:30', 0, 1),
(64, 29, 45, 42, 0, 0, '2018-02-11 15:54:40', 4, 0),
(65, 20, 50, 6, 0, 0, '2018-02-12 07:02:08', 0, 1),
(66, 21, 50, 6, 0, 0, '2018-02-12 07:13:14', 0, 1),
(67, 25, 51, 49, 0, 0, '2018-02-12 07:39:35', 0, 1),
(68, 30, 49, 0, 0, 0, '2018-02-12 11:32:06', 0, 1),
(69, 30, 51, 49, 0, 0, '2018-02-14 07:53:06', 0, 1),
(70, 22, 51, 44, 0, 0, '2018-02-14 07:53:28', 5, 1),
(72, 22, 41, 44, 0, 0, '2018-02-14 12:39:43', 0, 0),
(73, 8, 41, 46, 0, 0, '2018-02-14 12:39:45', 0, 0),
(74, 3, 41, 42, 0, 0, '2018-02-14 12:39:47', 0, 0),
(75, 1, 41, 42, 0, 0, '2018-02-14 12:39:48', 15, 0),
(76, 25, 54, 49, 0, 0, '2018-02-14 13:50:29', 1, 0),
(77, 32, 53, 53, 2, 0, '2018-02-15 13:58:13', 0, 0),
(78, 32, 42, 53, 0, 0, '2018-02-18 14:52:10', 0, 0),
(79, 33, 47, 47, 2, 0, '2018-02-18 14:59:53', 0, 0),
(80, 34, 42, 42, 2, 0, '2018-02-18 15:00:08', 0, 0),
(81, 34, 47, 42, 0, 0, '2018-02-18 15:00:22', 0, 0),
(82, 33, 42, 47, 0, 0, '2018-02-18 15:00:36', 1, 0),
(83, 35, 47, 47, 2, 0, '2018-02-18 15:02:22', 0, 0),
(84, 10, 53, 47, 0, 0, '2018-02-18 15:13:40', 0, 0),
(85, 36, 54, 54, 2, 0, '2018-02-19 06:51:52', 0, 0),
(86, 36, 49, 54, 0, 0, '2018-02-19 06:52:04', 0, 1),
(87, 37, 55, 55, 2, 0, '2018-02-21 06:37:53', 0, 0),
(88, 37, 54, 55, 0, 0, '2018-02-21 07:28:32', 0, 0),
(89, 37, 49, 55, 0, 0, '2018-02-21 07:28:59', 3, 1),
(90, 34, 48, 42, 0, 0, '2018-02-25 13:15:25', 0, 0),
(91, 34, 53, 42, 0, 0, '2018-02-25 13:24:28', 0, 0),
(92, 38, 62, 62, 2, 0, '2018-02-26 06:58:57', 1, 0),
(93, 38, 49, 62, 0, 0, '2018-02-26 06:59:30', 0, 1),
(94, 30, 66, 49, 0, 0, '2018-02-28 11:49:22', 0, 0),
(95, 32, 47, 53, 0, 0, '2018-03-04 15:21:22', 0, 0),
(96, 39, 66, 66, 2, 0, '2018-03-06 04:38:35', 0, 0),
(97, 39, 51, 66, 0, 0, '2018-03-06 04:40:26', 10, 1),
(98, 39, 49, 66, 0, 0, '2018-03-06 04:43:31', 0, 1),
(99, 40, 68, 68, 2, 0, '2018-03-06 06:09:04', 0, 0),
(100, 40, 6, 68, 0, 0, '2018-03-07 05:10:48', 0, 0),
(101, 39, 72, 66, 0, 0, '2018-03-08 06:49:51', 0, 1),
(102, 41, 72, 72, 2, 0, '2018-03-08 06:55:52', 0, 0),
(103, 41, 66, 72, 0, 0, '2018-03-08 06:56:04', 0, 1),
(104, 5, 72, 6, 0, 0, '2018-03-08 07:07:51', 0, 1),
(105, 20, 72, 6, 0, 0, '2018-03-09 06:30:39', 0, 0),
(106, 20, 42, 6, 0, 0, '2018-03-11 16:29:47', 0, 0),
(107, 32, 48, 53, 0, 0, '2018-03-11 21:13:49', 0, 1),
(108, 20, 66, 6, 0, 0, '2018-03-13 05:49:05', 0, 0),
(109, 4, 66, 6, 0, 0, '2018-03-14 09:57:32', 0, 1),
(110, 42, 75, 75, 2, 0, '2018-03-15 13:53:15', 0, 0),
(111, 43, 77, 77, 2, 0, '2018-03-16 05:52:32', 0, 0),
(112, 43, 74, 77, 0, 0, '2018-03-16 05:53:12', 0, 0),
(113, 39, 74, 66, 0, 0, '2018-03-16 05:53:14', 10, 0),
(114, 44, 76, 76, 2, 0, '2018-03-19 07:57:50', 0, 0),
(115, 45, 81, 81, 2, 0, '2018-03-21 16:23:39', 5, 0),
(116, 45, 42, 81, 0, 0, '2018-03-21 16:36:07', 0, 0),
(117, 45, 53, 81, 0, 0, '2018-03-21 17:22:10', 0, 0),
(118, 1, 53, 42, 0, 0, '2018-03-21 19:15:10', 0, 0),
(119, 44, 66, 76, 0, 0, '2018-03-22 11:09:10', 11, 0),
(120, 46, 71, 71, 2, 0, '2018-03-23 06:53:19', 0, 0),
(121, 39, 71, 66, 0, 0, '2018-03-23 06:53:24', 0, 0),
(122, 46, 49, 71, 0, 0, '2018-03-23 06:53:28', 0, 1),
(123, 45, 47, 81, 0, 0, '2018-03-24 22:08:41', 0, 0),
(124, 47, 86, 86, 2, 0, '2018-03-27 11:29:09', 0, 0),
(125, 48, 84, 84, 2, 0, '2018-03-28 05:12:38', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `todolist`
--

CREATE TABLE `todolist` (
  `todoid` int(11) NOT NULL,
  `team_id` int(11) NOT NULL,
  `accountability` int(11) NOT NULL,
  `action_name` varchar(255) NOT NULL,
  `action_description` varchar(1000) NOT NULL,
  `attachment` varchar(500) NOT NULL,
  `link` varchar(255) NOT NULL,
  `deadline` varchar(20) NOT NULL,
  `status` int(11) NOT NULL COMMENT '0 means working, 1 means done',
  `assigned_by` int(11) NOT NULL,
  `assigned_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `complited_at` datetime NOT NULL,
  `is_active` tinyint(4) NOT NULL COMMENT '0 active 1 inactive'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `todolist`
--

INSERT INTO `todolist` (`todoid`, `team_id`, `accountability`, `action_name`, `action_description`, `attachment`, `link`, `deadline`, `status`, `assigned_by`, `assigned_at`, `complited_at`, `is_active`) VALUES
(1, 1, 42, 'Check Team Member Selection In Screen', 'I sent image with the next comments.', '', '', '2018-04-17', 0, 42, '2018-01-29 21:25:47', '0000-00-00 00:00:00', 0),
(2, 2, 42, 'Add Additional Additions', 'Aaa', '', '', '2018-02-28', 1, 42, '2018-01-29 21:59:10', '2018-02-13 16:10:53', 0),
(3, 5, 6, 'Dadas Dads dads  Day Sdfs', 'New task being assigned here it is', '', '', '2018-01-31', 0, 6, '2018-01-30 08:10:25', '2018-02-12 10:55:02', 0),
(4, 7, 45, 'use INR on Android phone', 'Login to INR from Android phone', '', '', '2018-3-21', 0, 45, '2018-01-30 20:45:29', '2018-02-04 17:58:53', 0),
(5, 7, 42, 'create team using android phone And Let Us See If This Works As Well', 'Vivek to create a team', 'media/todo//20180318_todo-7786-1521381945.pdf', '', '2018-5-9', 0, 45, '2018-01-30 20:46:24', '2018-03-18 14:08:38', 0),
(6, 10, 47, 'test INR on Android phones on Sony experia', 'test test test', '', '', '2018-2-5', 0, 47, '2018-02-04 17:12:56', '2018-02-04 17:20:21', 1),
(7, 1, 45, 'For Vivek', 'Edit the action text title please', '', '', '2018-05-04', 0, 42, '2018-02-04 17:14:23', '0000-00-00 00:00:00', 0),
(8, 7, 45, 'Have Fun At Work', 'Have fun', '', '', '2018-02-05', 1, 45, '2018-02-04 17:59:15', '2018-03-25 15:31:32', 0),
(9, 7, 45, 'Créate Terms And Conditions', 'Make Tcs for inr app', 'media/todo//20180318_todo-6418-1521381762.pdf', '', '2018-3-20', 0, 45, '2018-02-04 18:01:12', '0000-00-00 00:00:00', 0),
(11, 9, 46, 'bjhgjh dfdhg jhg dhjgf  hdjj fgjhg fhdsgjh gffdg', 'ghjd fghdsfdsf jd', '', '', '2018-2-23', 1, 46, '2018-02-05 05:54:20', '2018-02-05 06:40:25', 0),
(12, 11, 46, 'gshsjjd', 'gsgh', '', '', '2018-2-5', 0, 44, '2018-02-05 06:15:24', '0000-00-00 00:00:00', 1),
(13, 11, 46, 'gshssj', 'gshsh', '', '', '2018-2-23', 1, 44, '2018-02-05 06:17:32', '2018-02-05 06:40:28', 1),
(14, 11, 46, 'sh', 'ghhhz', '', '', '2018-2-21', 0, 44, '2018-02-05 06:20:52', '0000-00-00 00:00:00', 1),
(15, 11, 46, 'ttttk', 'gshsh', '', '', '2018-2-28', 1, 44, '2018-02-05 06:39:52', '2018-02-05 06:40:30', 1),
(17, 27, 44, 'rturur', 'ruetyeute', '', '', '2018-2-21', 0, 49, '2018-02-09 12:27:07', '0000-00-00 00:00:00', 1),
(18, 10, 42, 'have fun and play football and cricket. do not run away from the field', 'he he', '', '', '2018-2-12', 0, 47, '2018-02-11 14:42:07', '0000-00-00 00:00:00', 0),
(19, 2, 47, 'Ling title test Action. Long Title Test Action. Long Title Test Action', 'Geh', '', '', '2018-08-11', 1, 42, '2018-02-11 14:42:08', '2018-02-13 15:52:00', 0),
(20, 10, 47, 'test swipe action for leader', 'create action and swipe', '', '', '2018-2-12', 0, 47, '2018-02-11 15:08:34', '0000-00-00 00:00:00', 0),
(21, 5, 6, 'Dad adds Sunasdasd Dhillon', 'Sdfdsfdsfdf', '', '', '2019-02-12', 0, 6, '2018-02-12 10:54:17', '0000-00-00 00:00:00', 0),
(23, 25, 54, 'hggkjgkg', 'jgh,jbgg', '', '', '2018-2-28', 0, 49, '2018-02-19 07:40:23', '2018-02-22 10:39:03', 0),
(25, 17, 50, 'Sim New Task', 'Hdd djndjdnd', '', '', '2019-02-21', 0, 50, '2018-02-21 07:28:13', '0000-00-00 00:00:00', 0),
(28, 36, 49, 'hdnjsjs', 'bxjdkdjd', 'media/todo//20180222_todo-3235-1519310222.png', '', '2018-2-28', 0, 54, '2018-02-22 14:35:49', '0000-00-00 00:00:00', 1),
(29, 25, 54, 'sdsfsdf', 'fdsfdf', 'media/todo//20180223_todo-5285-1519362687.png', 'www.google.com', '2018-2-27', 0, 49, '2018-02-23 05:11:27', '2018-02-23 05:33:54', 0),
(30, 34, 53, 'The Djanouschek. Action', 'Finally', '', '', '2018-04-21', 0, 42, '2018-02-25 14:06:38', '0000-00-00 00:00:00', 0),
(31, 34, 48, 'The Chloe Action', 'Never', '', '', '2018-02-27', 1, 42, '2018-02-25 14:07:03', '2018-03-11 10:48:28', 0),
(32, 34, 53, 'The iPhone X Action', 'Done', '', '', '2018-05-20', 0, 42, '2018-02-25 14:07:29', '2018-03-11 21:24:32', 0),
(33, 37, 54, 'Ggg', 'Nana kal', '', '', '2018-03-01', 0, 55, '2018-03-01 06:41:15', '0000-00-00 00:00:00', 0),
(35, 30, 66, 'jashgdkjgsjf', 'zxdsxfdsfds', '', '', '2018-3-6', 1, 49, '2018-03-01 13:39:41', '2018-03-19 11:59:18', 0),
(36, 10, 53, 'test due date', 'how does the due date look when the date is later', '', '', '2018-3-6', 1, 47, '2018-03-04 15:02:26', '2018-03-11 21:23:21', 0),
(37, 33, 42, 'test due date for Daniel', 'how does the date appear?', '', '', '2018-4-18', 0, 47, '2018-03-04 15:05:23', '0000-00-00 00:00:00', 0),
(38, 10, 47, 'test due date vivek', 'check due date color', '', '', '2018-4-11', 0, 47, '2018-03-04 15:08:00', '0000-00-00 00:00:00', 0),
(39, 30, 66, 'complete assignment', 'please complete on time', 'media/todo//20180305_todo-9669-1520234880.png', 'www.google.com', '2018-3-21', 1, 49, '2018-03-05 07:28:00', '2018-03-19 11:59:19', 0),
(40, 40, 68, 'Dqwqw', 'Wqqwrqw', '', '', '2018-04-06', 0, 68, '2018-03-06 06:09:38', '0000-00-00 00:00:00', 0),
(41, 39, 72, 'do back-end changes', 'already  with you', '', '', '2018-3-22', 0, 66, '2018-03-08 06:52:23', '0000-00-00 00:00:00', 1),
(42, 41, 66, 'To Rahul', 'Already with Rahul', '', '', '2018-03-22', 0, 72, '2018-03-08 06:58:05', '0000-00-00 00:00:00', 1),
(43, 39, 72, 'figsdochchcoehc', 'Gkhslxhs', '', '', '2018-3-29', 0, 66, '2018-03-08 07:14:19', '0000-00-00 00:00:00', 1),
(44, 41, 66, 'Sssssddfff', 'Cccccc', '', '', '2018-03-11', 0, 72, '2018-03-08 07:18:36', '2018-03-08 07:23:05', 1),
(45, 32, 47, 'Action Before You Get Kicked Out.', 'Get ready!', '', '', '2018-05-11', 0, 53, '2018-03-11 16:26:03', '0000-00-00 00:00:00', 0),
(46, 32, 48, 'Chloe Please Kick Out', 'Please', '', '', '2018-06-11', 0, 53, '2018-03-11 21:15:02', '0000-00-00 00:00:00', 1),
(47, 5, 6, 'Xmas Tree', 'Have to create Xmas tree', '', '', '2018-04-19', 0, 6, '2018-03-14 06:52:42', '0000-00-00 00:00:00', 0),
(48, 4, 66, 'Xmas Tree', 'Xmas tree', '', '', '2018-11-14', 0, 6, '2018-03-14 09:58:05', '0000-00-00 00:00:00', 1),
(49, 39, 51, 'new trask', 'trhtrfhgfh  gfhgf', '', '', '2018-03-21', 0, 66, '2018-03-19 11:59:52', '0000-00-00 00:00:00', 1),
(50, 45, 81, 'Check Photo Orientation', 'Funny, isn’t it?', '', '', '2018-05-21', 0, 81, '2018-03-21 16:25:32', '0000-00-00 00:00:00', 0),
(51, 28, 42, 'Chrono Test', 'Check this list', '', '', '2018-04-18', 0, 42, '2018-03-21 19:05:50', '0000-00-00 00:00:00', 0),
(52, 34, 53, 'Chrono Test ', 'Also for you', '', '', '2019-11-21', 0, 42, '2018-03-21 19:09:51', '0000-00-00 00:00:00', 0),
(53, 34, 53, 'Chrono 2', 'Earlier', '', '', '2018-06-21', 0, 42, '2018-03-21 19:11:09', '0000-00-00 00:00:00', 0),
(54, 34, 53, 'Chrono 3', 'In the middle', '', '', '2018-10-21', 0, 42, '2018-03-21 19:11:38', '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `todo_reassign_action`
--

CREATE TABLE `todo_reassign_action` (
  `reassignaction_id` int(11) NOT NULL,
  `todoid` int(11) NOT NULL,
  `team_id` int(11) NOT NULL,
  `prev_assigned_by` int(11) NOT NULL,
  `prev_accountability` int(11) NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `todo_reassign_action`
--

INSERT INTO `todo_reassign_action` (`reassignaction_id`, `todoid`, `team_id`, `prev_assigned_by`, `prev_accountability`, `updated_at`) VALUES
(3, 41, 39, 66, 72, '2018-03-08 07:14:51'),
(4, 43, 39, 66, 72, '2018-03-08 07:14:51'),
(5, 42, 41, 72, 66, '2018-03-08 07:19:32'),
(6, 44, 41, 72, 66, '2018-03-08 07:19:32'),
(12, 46, 32, 53, 48, '2018-03-11 21:44:28'),
(13, 48, 4, 6, 66, '2018-03-14 09:58:47'),
(14, 12, 11, 44, 46, '2018-03-26 06:53:34'),
(15, 14, 11, 44, 46, '2018-03-26 06:53:34'),
(16, 17, 27, 49, 44, '2018-03-27 05:16:51'),
(17, 49, 39, 66, 51, '2018-03-27 05:26:00'),
(18, 28, 36, 54, 49, '2018-03-27 07:17:03');

-- --------------------------------------------------------

--
-- Table structure for table `ttalks`
--

CREATE TABLE `ttalks` (
  `ttalkid` int(11) NOT NULL,
  `thread_id` int(11) NOT NULL,
  `team_id` int(11) NOT NULL,
  `sender_id` int(11) NOT NULL,
  `privacy` int(11) NOT NULL COMMENT '1 public 2 private',
  `private_receiverid` varchar(1000) NOT NULL DEFAULT '0',
  `message_type` int(11) NOT NULL COMMENT '1 text, 2 picture, 3 video, 4 docs',
  `message` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `filelink` varchar(255) NOT NULL,
  `thumbnail` varchar(255) NOT NULL,
  `filesize` int(11) NOT NULL,
  `receiver_id` int(11) NOT NULL,
  `sender_trash` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0 deleted',
  `receiver_trash` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0 deleted',
  `is_read` tinyint(4) NOT NULL COMMENT '0 not read',
  `re_breaf` tinyint(4) NOT NULL,
  `main_sender_id` int(11) NOT NULL,
  `sent_date` datetime NOT NULL,
  `isactive` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ttalks`
--

INSERT INTO `ttalks` (`ttalkid`, `thread_id`, `team_id`, `sender_id`, `privacy`, `private_receiverid`, `message_type`, `message`, `filelink`, `thumbnail`, `filesize`, `receiver_id`, `sender_trash`, `receiver_trash`, `is_read`, `re_breaf`, `main_sender_id`, `sent_date`, `isactive`) VALUES
(1, 1, 4, 6, 1, '0', 1, 'Hello there', '', '', 0, 0, 1, 1, 0, 0, 6, '2018-01-30 08:04:18', 2),
(2, 1, 4, 6, 1, '0', 1, 'Hello everyone', '', '', 0, 0, 0, 1, 0, 0, 6, '2018-01-30 08:10:54', 2),
(3, 2, 8, 46, 2, '0', 1, 'hello', '', '', 0, 0, 1, 1, 0, 0, 46, '2018-02-01 07:54:26', 2),
(4, 2, 8, 46, 1, '0', 1, 'hello', '', '', 0, 0, 1, 1, 0, 0, 46, '2018-02-01 07:59:31', 2),
(5, 2, 8, 46, 1, '0', 2, '', 'media/ttalk/images/2018-02/20180201_TTK-392879-1517472000.png', '', 9850, 0, 1, 1, 0, 0, 46, '2018-02-01 08:00:00', 2),
(6, 2, 8, 46, 1, '0', 2, '', 'media/ttalk/images/2018-02/20180201_TTK-950142-1517478662.png', '', 7384, 0, 1, 1, 0, 0, 46, '2018-02-01 09:51:02', 2),
(7, 2, 8, 44, 1, '0', 2, '', 'media/ttalk/images/2018-02/20180201_TTK-674929-1517478884.png', '', 210062, 0, 1, 1, 0, 0, 44, '2018-02-01 09:54:44', 2),
(8, 3, 6, 42, 1, '0', 1, 'This is a private msg to open up conversation.', '', '', 0, 0, 1, 1, 0, 0, 42, '2018-02-01 17:45:12', 2),
(9, 4, 1, 42, 1, '0', 1, 'Vivek.', '', '', 0, 0, 1, 1, 0, 0, 42, '2018-02-04 14:42:59', 1),
(10, 4, 1, 42, 1, '0', 1, 'Vivek msg 2', '', '', 0, 0, 1, 1, 0, 0, 42, '2018-02-04 14:47:59', 1),
(11, 4, 1, 45, 1, '0', 1, 'Hi', '', '', 0, 0, 1, 1, 0, 0, 45, '2018-02-04 17:24:07', 1),
(12, 4, 1, 45, 1, '0', 2, '', 'media/ttalk/images/2018-02/20180204_TTK-356596-1517765066.png', '', 83133, 0, 1, 1, 0, 0, 45, '2018-02-04 17:24:26', 1),
(13, 4, 1, 42, 1, '0', 2, '', 'media/ttalk/images/2018-02/20180204_TTK-980853-1517765904.png', '', 1277595, 0, 1, 1, 0, 0, 42, '2018-02-04 17:38:24', 1),
(14, 4, 1, 42, 1, '0', 2, '', 'media/ttalk/images/2018-02/20180204_TTK-558622-1517765923.png', '', 952077, 0, 1, 1, 0, 0, 42, '2018-02-04 17:38:43', 1),
(15, 5, 2, 42, 1, '0', 1, 'Is this arriving on all accounts?', '', '', 0, 0, 1, 1, 0, 0, 42, '2018-02-04 17:41:04', 2),
(16, 5, 2, 42, 1, '0', 2, '', 'media/ttalk/images/2018-02/20180204_TTK-475312-1517766080.png', '', 952077, 0, 1, 1, 0, 0, 42, '2018-02-04 17:41:20', 2),
(17, 5, 2, 42, 1, '0', 2, '', 'media/ttalk/images/2018-02/20180204_TTK-169438-1517766085.png', '', 1055365, 0, 1, 1, 0, 0, 42, '2018-02-04 17:41:25', 2),
(18, 4, 1, 48, 2, '42', 1, 'Love you Daniel!????', '', '', 0, 0, 1, 1, 0, 0, 48, '2018-02-04 19:50:52', 1),
(19, 4, 1, 42, 1, '0', 1, 'Hello Vivek. How about we meet at your place next time?', '', '', 0, 0, 0, 1, 0, 0, 42, '2018-02-04 19:59:50', 2),
(20, 4, 1, 42, 1, '0', 1, 'Chloe could play with Pranav while we are working. Vidya is not  lonely. And later we have time to go for dinner somewhere near your place,...', '', '', 0, 0, 0, 1, 0, 0, 42, '2018-02-04 20:01:26', 2),
(21, 2, 8, 46, 1, '0', 1, 'hello', '', '', 0, 0, 1, 1, 0, 0, 46, '2018-02-05 05:11:42', 2),
(22, 2, 8, 46, 2, '44', 1, 'hello', '', '', 0, 0, 1, 1, 0, 0, 46, '2018-02-05 05:12:24', 2),
(23, 2, 8, 46, 2, '44', 1, 'this is  a private message', '', '', 0, 0, 1, 1, 0, 0, 46, '2018-02-05 05:13:02', 2),
(24, 2, 8, 46, 1, '0', 1, 'dummy', '', '', 0, 0, 1, 1, 0, 0, 46, '2018-02-05 05:13:36', 2),
(25, 2, 8, 46, 2, '45', 1, 'dummy private', '', '', 0, 0, 1, 1, 0, 0, 46, '2018-02-05 05:13:58', 2),
(26, 2, 8, 46, 2, '45', 2, '', 'media/ttalk/images/2018-02/20180205_TTK-562360-1517807667.png', '', 9850, 0, 1, 1, 0, 0, 46, '2018-02-05 05:14:27', 2),
(27, 2, 8, 46, 2, '44', 2, '', 'media/ttalk/images/2018-02/20180205_TTK-455059-1517807692.png', '', 9724, 0, 1, 1, 0, 0, 46, '2018-02-05 05:14:52', 2),
(28, 2, 8, 46, 1, '0', 1, 'helllo', '', '', 0, 0, 1, 1, 0, 0, 46, '2018-02-05 05:16:20', 2),
(29, 2, 8, 46, 1, '0', 1, 'ghddfhf', '', '', 0, 0, 1, 1, 0, 0, 46, '2018-02-05 05:16:57', 2),
(30, 2, 8, 44, 1, '0', 1, 'hello', '', '', 0, 0, 1, 1, 0, 0, 44, '2018-02-05 05:17:06', 2),
(31, 6, 0, 46, 1, '0', 2, '', 'media/ttalk/images/2018-02/20180205_frwd-152109-1517807868.png', '', 9724, 44, 1, 1, 1, 1, 46, '2018-02-05 05:17:48', 1),
(32, 6, 0, 44, 1, '0', 1, 'dcj', '', '', 0, 46, 1, 1, 1, 0, 44, '2018-02-05 05:18:03', 1),
(33, 4, 1, 42, 1, '0', 1, ' Who am I?', '', '', 0, 0, 0, 1, 0, 0, 42, '2018-02-05 15:44:45', 2),
(34, 4, 1, 42, 1, '0', 1, 'This is the guy you wanna meet.', '', '', 0, 0, 0, 1, 0, 0, 42, '2018-02-05 15:50:18', 2),
(35, 4, 1, 42, 1, '0', 1, 'This is the guy you want to address.', '', '', 0, 0, 0, 1, 0, 0, 42, '2018-02-05 15:51:30', 2),
(36, 4, 1, 42, 1, '0', 1, 'This is the guy you want to address.', '', '', 0, 0, 0, 1, 0, 0, 42, '2018-02-05 15:52:19', 2),
(37, 4, 1, 42, 1, '0', 1, 'This is the guy to talk to!', '', '', 0, 0, 1, 1, 0, 0, 42, '2018-02-05 15:53:55', 1),
(38, 4, 1, 42, 1, '0', 1, 'Can you open this link?: http://www.sueddeutsche.de/bayern/exklusiv-katholische-kirche-verliert-millionen-durch-immobilienprojekte-in-den-usa-1.3854334', '', '', 0, 0, 1, 1, 0, 0, 42, '2018-02-05 16:56:54', 1),
(39, 2, 8, 46, 1, '0', 3, '', 'media/ttalk/videos/2018-02/20180206_TTK-772545-1517919648.mp4', 'media/ttalk/videos/2018-02/20180206_TTK-772545-1517919648_thumb.png', 0, 0, 1, 1, 0, 0, 46, '2018-02-06 12:20:48', 2),
(40, 2, 8, 46, 1, '0', 3, '', 'media/ttalk/videos/2018-02/20180206_TTK-108177-1517919907.mp4', 'media/ttalk/videos/2018-02/20180206_TTK-108177-1517919907_thumb.png', 0, 0, 1, 1, 0, 0, 46, '2018-02-06 12:25:07', 2),
(41, 7, 16, 48, 1, '0', 1, 'Go watch your football!', '', '', 0, 0, 1, 1, 0, 0, 48, '2018-02-06 16:29:00', 2),
(42, 7, 16, 42, 1, '0', 1, 'For sure I do. ????', '', '', 0, 0, 1, 1, 0, 0, 42, '2018-02-06 16:30:58', 2),
(43, 7, 16, 48, 1, '0', 1, 'No. ', '', '', 0, 0, 1, 1, 0, 0, 48, '2018-02-06 16:31:27', 2),
(44, 7, 16, 48, 1, '0', 1, 'Stay here. ', '', '', 0, 0, 1, 1, 0, 0, 48, '2018-02-06 16:31:44', 2),
(45, 7, 16, 48, 1, '0', 1, 'Bug bug bug', '', '', 0, 0, 1, 1, 0, 0, 48, '2018-02-06 16:32:08', 2),
(46, 7, 16, 42, 1, '0', 1, 'Can I do only one message?', '', '', 0, 0, 1, 1, 0, 0, 42, '2018-02-06 16:32:12', 2),
(47, 7, 16, 48, 1, '0', 1, 'Massage massage ', '', '', 0, 0, 1, 1, 0, 0, 48, '2018-02-06 16:32:24', 2),
(48, 7, 16, 42, 1, '0', 1, 'Yes i can!', '', '', 0, 0, 1, 1, 0, 0, 42, '2018-02-06 16:32:24', 2),
(49, 7, 16, 48, 1, '0', 1, 'Alalalalalalal', '', '', 0, 0, 1, 1, 0, 0, 48, '2018-02-06 16:37:00', 2),
(50, 7, 16, 42, 1, '0', 2, '', 'media/ttalk/images/2018-02/20180206_TTK-993254-1517937811.png', '', 229632, 0, 1, 1, 0, 0, 42, '2018-02-06 17:23:31', 2),
(51, 7, 16, 42, 1, '0', 1, 'No one here. :(', '', '', 0, 0, 1, 1, 0, 0, 42, '2018-02-06 17:23:46', 2),
(52, 7, 16, 42, 1, '0', 2, '', 'media/ttalk/images/2018-02/20180206_TTK-405500-1517937884.png', '', 681980, 0, 0, 1, 0, 0, 42, '2018-02-06 17:24:44', 2),
(53, 7, 16, 42, 1, '0', 1, 'Tv screen.', '', '', 0, 0, 0, 1, 0, 0, 42, '2018-02-06 17:24:49', 2),
(54, 7, 16, 42, 1, '0', 1, 'Tv screen take 2', '', '', 0, 0, 0, 1, 0, 0, 42, '2018-02-06 17:25:26', 2),
(55, 7, 16, 42, 1, '0', 2, '', 'media/ttalk/images/2018-02/20180206_TTK-168454-1517937938.png', '', 809295, 0, 0, 1, 0, 0, 42, '2018-02-06 17:25:38', 2),
(56, 7, 16, 42, 1, '0', 1, 'Shall I pass by Käfer again?', '', '', 0, 0, 1, 1, 0, 0, 42, '2018-02-06 19:23:54', 2),
(57, 7, 16, 42, 1, '0', 1, 'For FOOOOD!', '', '', 0, 0, 1, 1, 0, 0, 42, '2018-02-06 19:24:28', 2),
(58, 7, 16, 48, 1, '0', 1, 'Käfer ist schon geschlossen. ', '', '', 0, 0, 1, 1, 0, 0, 48, '2018-02-06 19:58:24', 2),
(59, 7, 16, 42, 1, '0', 1, 'Schade. Dann geh ich jetzt heim. Nur noch kola-Weizen austrinken.', '', '', 0, 0, 1, 1, 0, 0, 42, '2018-02-06 20:23:41', 2),
(60, 7, 16, 42, 1, '0', 2, '', 'media/ttalk/images/2018-02/20180206_TTK-420637-1517948623.png', '', 778147, 0, 1, 1, 0, 0, 42, '2018-02-06 20:23:43', 2),
(61, 8, 17, 50, 1, '0', 1, 'Hello', '', '', 0, 0, 1, 1, 0, 0, 50, '2018-02-07 12:00:04', 2),
(62, 9, 18, 50, 1, '0', 1, 'Hellooooo', '', '', 0, 0, 1, 1, 0, 0, 50, '2018-02-07 12:21:14', 2),
(63, 10, 19, 51, 1, '0', 1, 'hi', '', '', 0, 0, 1, 1, 0, 0, 51, '2018-02-07 12:50:58', 1),
(64, 11, 12, 44, 1, '0', 2, '', 'media/ttalk/images/2018-02/20180207_TTK-917999-1518008383.png', '', 111067, 0, 1, 1, 0, 0, 44, '2018-02-07 12:59:43', 1),
(65, 7, 16, 48, 1, '0', 1, '????', '', '', 0, 0, 1, 1, 0, 0, 48, '2018-02-08 18:49:42', 2),
(66, 4, 1, 42, 1, '0', 2, '', 'media/ttalk/images/2018-02/20180208_frwd-162443-1518124987.png', '', 778147, 0, 1, 1, 0, 1, 42, '2018-02-08 21:23:07', 1),
(67, 7, 16, 42, 2, '48', 1, 'This is top Secret.', '', '', 0, 0, 1, 1, 0, 0, 42, '2018-02-08 21:23:44', 2),
(68, 11, 12, 44, 1, '0', 1, 'hell', '', '', 0, 0, 1, 1, 0, 0, 44, '2018-02-09 11:02:00', 1),
(69, 12, 25, 49, 1, '0', 1, 'gfhhhhhhhhhhh', '', '', 0, 0, 0, 1, 0, 0, 49, '2018-02-09 12:29:16', 2),
(70, 12, 25, 49, 1, '0', 1, 'rtheryu', '', '', 0, 0, 0, 1, 0, 0, 49, '2018-02-09 12:29:21', 2),
(71, 12, 25, 49, 1, '0', 2, '', 'media/ttalk/images/2018-02/20180209_TTK-586355-1518179375.png', '', 7678, 0, 1, 1, 0, 0, 49, '2018-02-09 12:29:35', 2),
(72, 7, 16, 48, 1, '0', 1, 'Wow wow tooooop Secret.  ', '', '', 0, 0, 1, 1, 0, 0, 48, '2018-02-10 18:48:39', 2),
(73, 7, 16, 48, 1, '0', 1, 'Ich bin da. ', '', '', 0, 0, 1, 1, 0, 0, 48, '2018-02-10 20:45:54', 2),
(74, 7, 16, 48, 1, '0', 1, 'Nochmal ', '', '', 0, 0, 1, 1, 0, 0, 48, '2018-02-10 20:46:06', 2),
(75, 13, 3, 48, 1, '0', 1, '：）', '', '', 0, 0, 1, 1, 0, 0, 48, '2018-02-10 20:53:30', 2),
(76, 14, 10, 42, 1, '0', 1, 'Hello', '', '', 0, 0, 1, 1, 0, 0, 42, '2018-02-11 14:59:00', 2),
(77, 5, 2, 47, 2, '42', 1, 'hi daniel put message', '', '', 0, 0, 1, 1, 0, 0, 47, '2018-02-11 15:19:56', 2),
(78, 15, 28, 48, 1, '0', 1, ';(', '', '', 0, 0, 1, 1, 0, 0, 48, '2018-02-11 15:27:46', 2),
(79, 7, 16, 48, 1, '0', 1, 'Lieber Daniel. ', '', '', 0, 0, 1, 1, 0, 0, 48, '2018-02-11 15:35:42', 2),
(80, 7, 16, 48, 1, '0', 1, 'Schnitzel ~ Schnitzel ~  Mein Schatz~', '', '', 0, 0, 1, 1, 0, 0, 48, '2018-02-11 15:36:36', 2),
(81, 16, 13, 42, 1, '0', 1, ' Msg before registering viveks', '', '', 0, 0, 1, 1, 0, 0, 42, '2018-02-11 15:50:19', 2),
(82, 16, 13, 42, 1, '0', 2, '', 'media/ttalk/images/2018-02/20180211_TTK-846278-1518364254.png', '', 156156, 0, 1, 1, 0, 0, 42, '2018-02-11 15:50:54', 2),
(83, 16, 13, 42, 1, '0', 1, 'Before', '', '', 0, 0, 1, 1, 0, 0, 42, '2018-02-11 15:50:57', 2),
(84, 17, 29, 42, 1, '0', 1, 'Hi', '', '', 0, 0, 1, 1, 0, 0, 42, '2018-02-11 15:54:29', 2),
(85, 12, 25, 49, 1, '0', 1, 'hello', '', '', 0, 0, 0, 1, 0, 0, 49, '2018-02-12 07:26:51', 2),
(86, 10, 19, 51, 1, '0', 1, 'hello', '', '', 0, 0, 1, 1, 0, 0, 51, '2018-02-12 07:37:22', 1),
(87, 10, 19, 51, 1, '0', 1, 'haaha', '', '', 0, 0, 1, 1, 0, 0, 51, '2018-02-12 07:37:33', 1),
(88, 12, 25, 49, 1, '0', 1, 'hello', '', '', 0, 0, 1, 1, 0, 0, 49, '2018-02-12 07:39:49', 2),
(89, 12, 25, 51, 1, '0', 1, 'hii', '', '', 0, 0, 1, 1, 0, 0, 51, '2018-02-12 07:40:05', 2),
(90, 12, 25, 51, 1, '0', 1, 'hello', '', '', 0, 0, 1, 1, 0, 0, 51, '2018-02-12 07:40:17', 2),
(91, 18, 27, 49, 2, '44', 1, 'hello', '', '', 0, 0, 1, 1, 0, 0, 49, '2018-02-12 11:25:11', 2),
(92, 12, 25, 51, 1, '0', 1, 'hello', '', '', 0, 0, 1, 1, 0, 0, 51, '2018-02-12 14:01:29', 2),
(93, 12, 25, 49, 1, '0', 1, 'hii', '', '', 0, 0, 1, 1, 0, 0, 49, '2018-02-12 14:01:40', 2),
(94, 5, 2, 42, 2, '47,45', 1, 'Here we go!', '', '', 0, 0, 1, 1, 0, 0, 42, '2018-02-13 20:15:05', 2),
(95, 18, 27, 49, 1, '0', 1, 'hii', '', '', 0, 0, 1, 1, 0, 0, 49, '2018-02-14 10:46:17', 2),
(96, 12, 25, 49, 2, '51', 2, '', 'media/ttalk/images/2018-02/20180214_TTK-949041-1518605418.png', '', 9720, 0, 1, 1, 0, 0, 49, '2018-02-14 10:50:18', 2),
(97, 12, 25, 54, 1, '0', 1, 'hello', '', '', 0, 0, 1, 1, 0, 0, 54, '2018-02-15 04:42:01', 2),
(98, 12, 25, 49, 2, '54', 2, '', 'media/ttalk/images/2018-02/20180215_TTK-353295-1518685041.png', '', 28569, 0, 1, 1, 0, 0, 49, '2018-02-15 08:57:21', 2),
(99, 7, 16, 48, 1, '0', 1, 'Never answer from here...', '', '', 0, 0, 1, 1, 0, 0, 48, '2018-02-17 15:04:17', 2),
(100, 17, 29, 47, 2, '42', 1, 'hello', '', '', 0, 0, 1, 1, 0, 0, 47, '2018-02-18 15:43:06', 2),
(101, 17, 29, 47, 1, '0', 2, '', 'media/ttalk/images/2018-02/20180218_TTK-899898-1518968680.png', '', 59074, 0, 1, 1, 0, 0, 47, '2018-02-18 15:44:40', 2),
(102, 12, 25, 49, 2, '54', 1, 'hello rahul', '', '', 0, 0, 1, 1, 0, 0, 49, '2018-02-19 07:55:50', 2),
(103, 12, 25, 49, 2, '54', 1, 'fdfgfdgfhfhgfhfgh fghgfhfgh fghfghgfh ghfghgf hfghgfhfgh', '', '', 0, 0, 1, 1, 0, 0, 49, '2018-02-19 07:58:01', 2),
(104, 8, 17, 50, 1, '0', 1, '英國應該要', '', '', 0, 0, 1, 1, 0, 0, 50, '2018-02-19 10:51:16', 1),
(105, 12, 25, 49, 1, '0', 3, '', 'media/ttalk/videos/2018-02/20180221_TTK-347065-1519189011.mp4', 'media/ttalk/videos/2018-02/20180221_TTK-347065-1519189011_thumb.png', 0, 0, 1, 1, 0, 0, 49, '2018-02-21 04:56:51', 2),
(106, 12, 25, 49, 1, '0', 3, '', 'media/ttalk/videos/2018-02/20180221_TTK-137913-1519189762.mp4', 'media/ttalk/videos/2018-02/20180221_TTK-137913-1519189762_thumb.png', 0, 0, 1, 1, 0, 0, 49, '2018-02-21 05:09:22', 2),
(107, 12, 25, 49, 1, '0', 3, '', 'media/ttalk/videos/2018-02/20180221_TTK-777902-1519192274.mp4', 'media/ttalk/videos/2018-02/20180221_TTK-777902-1519192274_thumb.png', 0, 0, 1, 1, 0, 0, 49, '2018-02-21 05:51:14', 2),
(108, 12, 25, 49, 1, '0', 1, 'hello', '', '', 0, 0, 1, 1, 0, 0, 49, '2018-02-21 05:51:48', 2),
(109, 12, 25, 54, 1, '0', 1, 'hiii', '', '', 0, 0, 1, 1, 0, 0, 54, '2018-02-21 05:51:54', 2),
(110, 12, 25, 49, 1, '0', 3, '', 'media/ttalk/videos/2018-02/20180221_TTK-975917-1519193131.mp4', 'media/ttalk/videos/2018-02/20180221_TTK-975917-1519193131_thumb.png', 0, 0, 1, 1, 0, 0, 49, '2018-02-21 06:05:31', 2),
(111, 19, 22, 41, 1, '0', 1, 'Hello Team ', '', '', 0, 0, 1, 1, 0, 0, 41, '2018-02-21 06:25:52', 1),
(112, 19, 22, 41, 1, '0', 1, 'What’s up ', '', '', 0, 0, 1, 1, 0, 0, 41, '2018-02-21 06:26:00', 1),
(113, 19, 22, 41, 1, '0', 1, 'I am getting this error ', '', '', 0, 0, 1, 1, 0, 0, 41, '2018-02-21 06:26:20', 1),
(114, 19, 22, 41, 1, '0', 2, '', 'media/ttalk/images/2018-02/20180221_TTK-356641-1519194386.png', '', 1083399, 0, 1, 1, 0, 0, 41, '2018-02-21 06:26:26', 1),
(115, 19, 22, 41, 1, '0', 2, '', 'media/ttalk/images/2018-02/20180221_TTK-246560-1519194391.png', '', 111387, 0, 1, 1, 0, 0, 41, '2018-02-21 06:26:31', 1),
(116, 12, 25, 49, 1, '0', 3, '', 'media/ttalk/videos/2018-02/20180221_TTK-207451-1519196073.mp4', 'media/ttalk/videos/2018-02/20180221_TTK-207451-1519196073_thumb.png', 0, 0, 1, 1, 0, 0, 49, '2018-02-21 06:54:33', 2),
(117, 12, 25, 49, 1, '0', 3, '', 'media/ttalk/videos/2018-02/20180221_TTK-228542-1519196109.mp4', 'media/ttalk/videos/2018-02/20180221_TTK-228542-1519196109_thumb.png', 0, 0, 1, 1, 0, 0, 49, '2018-02-21 06:55:09', 2),
(118, 20, 37, 49, 1, '0', 3, '', 'media/ttalk/videos/2018-02/20180221_TTK-939330-1519198167.mp4', 'media/ttalk/videos/2018-02/20180221_TTK-939330-1519198167_thumb.png', 0, 0, 1, 1, 0, 0, 49, '2018-02-21 07:29:27', 2),
(119, 20, 37, 55, 1, '0', 3, '', 'media/ttalk/videos/2018-02/20180221_TTK-504944-1519198274.mov', 'media/ttalk/videos/2018-02/20180221_TTK-504944-1519198274_thumb.png', 5266310, 0, 0, 1, 0, 0, 55, '2018-02-21 07:31:14', 2),
(120, 20, 37, 55, 1, '0', 2, '', 'media/ttalk/images/2018-02/20180221_TTK-269584-1519198279.png', '', 58909, 0, 0, 1, 0, 0, 55, '2018-02-21 07:31:19', 2),
(121, 20, 37, 55, 1, '0', 3, '', 'media/ttalk/videos/2018-02/20180221_TTK-973482-1519198327.mov', 'media/ttalk/videos/2018-02/20180221_TTK-973482-1519198327_thumb.png', 4900633, 0, 0, 1, 0, 0, 55, '2018-02-21 07:32:07', 2),
(122, 20, 37, 55, 1, '0', 3, '', 'media/ttalk/videos/2018-02/20180221_TTK-445437-1519199602.mov', 'media/ttalk/videos/2018-02/20180221_TTK-445437-1519199602_thumb.png', 1278737, 0, 0, 1, 0, 0, 55, '2018-02-21 07:53:22', 2),
(123, 20, 37, 55, 1, '0', 3, '', 'media/ttalk/videos/2018-02/20180221_TTK-162342-1519201028.mov', 'media/ttalk/videos/2018-02/20180221_TTK-162342-1519201028_thumb.png', 1548818, 0, 1, 1, 0, 0, 55, '2018-02-21 08:17:08', 2),
(124, 12, 25, 54, 1, '0', 2, '', 'media/ttalk/images/2018-02/20180221_TTK-159182-1519220254.png', '', 121869, 0, 1, 1, 0, 0, 54, '2018-02-21 13:37:34', 2),
(125, 12, 25, 54, 1, '0', 1, 'hello', '', '', 0, 0, 1, 1, 0, 0, 54, '2018-02-21 14:20:16', 2),
(126, 1, 4, 6, 1, '0', 1, 'Hey there', '', '', 0, 0, 1, 1, 0, 0, 6, '2018-02-21 17:34:48', 2),
(127, 1, 4, 6, 1, '0', 2, '', 'media/ttalk/images/2018-02/20180221_TTK-866018-1519234587.png', '', 1318156, 0, 1, 1, 0, 0, 6, '2018-02-21 17:36:27', 2),
(128, 1, 4, 6, 1, '0', 1, 'Hi its me', '', '', 0, 0, 1, 1, 0, 0, 6, '2018-02-22 11:52:24', 2),
(129, 12, 25, 54, 1, '0', 1, 'hi', '', '', 0, 0, 1, 1, 0, 0, 54, '2018-02-22 12:20:58', 2),
(130, 12, 25, 54, 1, '0', 1, 'this is last message', '', '', 0, 0, 1, 1, 0, 0, 54, '2018-02-22 12:21:12', 2),
(131, 21, 32, 53, 1, '0', 1, 'Hello 1', '', '', 0, 0, 1, 1, 0, 0, 53, '2018-02-25 13:27:13', 2),
(132, 21, 32, 42, 1, '0', 1, ' 你好吗？ ????', '', '', 0, 0, 1, 1, 0, 0, 42, '2018-02-25 13:28:14', 2),
(133, 21, 32, 53, 1, '0', 2, '', 'media/ttalk/images/2018-02/20180225_TTK-836973-1519569330.png', '', 350680, 0, 1, 1, 0, 0, 53, '2018-02-25 14:35:30', 2),
(134, 12, 25, 49, 2, '54', 1, 'hsgdfkshdkfsdfgjksd sjbdkjsfdfhkjsd jdbfskdjfjfskdfbhsjd bfdjbjf dbjfdfjd bfdjfbd bfjbsdj', '', '', 0, 0, 1, 1, 0, 0, 49, '2018-02-26 07:17:12', 2),
(135, 22, 38, 62, 2, '49', 1, 'Please share your excitement of my name subha jaldi serious kyu hi hai jo mera hai bhavya apne husband pe ho gya hai sonal I don\'t think it\'s', '', '', 0, 0, 1, 1, 0, 0, 62, '2018-02-26 07:18:16', 2),
(136, 22, 38, 49, 1, '0', 1, 'for day go good get John he get his John in did', '', '', 0, 0, 1, 1, 0, 0, 49, '2018-02-26 07:18:45', 2),
(137, 22, 38, 49, 2, '62', 1, 'jhjh gkjg jgkjg hkhjhj ugug jhkjgj hhjjk iyyoii jnjhjh njhn njhj nklk jjouy kmkjj', '', '', 0, 0, 1, 1, 0, 0, 49, '2018-02-26 07:19:41', 2),
(138, 22, 38, 62, 2, '49', 2, '', 'media/ttalk/images/2018-02/20180226_TTK-863959-1519629639.png', '', 147561, 0, 1, 1, 0, 0, 62, '2018-02-26 07:20:39', 2),
(139, 23, 30, 66, 1, '0', 1, '????', '', '', 0, 0, 1, 1, 0, 0, 66, '2018-02-28 11:49:46', 2),
(140, 22, 38, 49, 1, '0', 1, 'helo', '', '', 0, 0, 1, 1, 0, 0, 49, '2018-03-01 10:35:08', 2),
(141, 23, 30, 66, 1, '0', 1, '🤣😂😚😊☺', '', '', 0, 0, 1, 1, 0, 0, 66, '2018-03-01 12:18:31', 2),
(142, 23, 30, 66, 1, '0', 1, '😃🤣😂', '', '', 0, 0, 1, 1, 0, 0, 66, '2018-03-01 12:18:46', 2),
(143, 16, 13, 48, 1, '0', 1, 'Ich bin zurück!', '', '', 0, 0, 1, 1, 0, 0, 48, '2018-03-01 14:41:42', 2),
(144, 24, 33, 47, 2, '42', 1, 'hi test', '', '', 0, 0, 1, 1, 0, 0, 47, '2018-03-04 15:11:31', 2),
(145, 17, 29, 42, 1, '0', 1, ' Bye bye Vivek android', '', '', 0, 0, 1, 1, 0, 0, 42, '2018-03-04 15:16:45', 1),
(146, 17, 29, 42, 1, '0', 2, '', 'media/ttalk/images/2018-03/20180304_TTK-609636-1520176622.png', '', 292957, 0, 1, 1, 0, 0, 42, '2018-03-04 15:17:02', 1),
(147, 21, 32, 53, 1, '0', 1, 'Kicked out vivek', '', '', 0, 0, 1, 1, 0, 0, 53, '2018-03-04 15:20:30', 2),
(148, 21, 32, 53, 1, '0', 1, 'Reinbitten vivek', '', '', 0, 0, 1, 1, 0, 0, 53, '2018-03-04 15:20:39', 2),
(149, 21, 32, 47, 1, '0', 1, 'hello. thanks for invitation.', '', '', 0, 0, 1, 1, 0, 0, 47, '2018-03-04 15:21:51', 2),
(150, 21, 32, 47, 1, '0', 2, '', 'media/ttalk/images/2018-03/20180304_TTK-483142-1520176926.png', '', 162862, 0, 1, 1, 0, 0, 47, '2018-03-04 15:22:06', 2),
(151, 21, 32, 47, 1, '0', 2, '', 'media/ttalk/images/2018-03/20180304_TTK-449782-1520176947.png', '', 200356, 0, 1, 1, 0, 0, 47, '2018-03-04 15:22:27', 2),
(152, 4, 1, 6, 1, '0', 1, 'Hello', '', '', 0, 0, 1, 1, 0, 0, 6, '2018-03-07 07:47:33', 1),
(153, 25, 41, 72, 1, '0', 2, '', 'media/ttalk/images/2018-03/20180308_TTK-218609-1520494138.png', '', 1340838, 0, 1, 1, 0, 0, 72, '2018-03-08 07:28:58', 1),
(154, 25, 41, 72, 1, '0', 1, 'Zskvshapbabovsg', '', '', 0, 0, 1, 1, 0, 0, 72, '2018-03-08 07:29:03', 1),
(155, 25, 41, 72, 1, '0', 1, 'Shoaib n an ', '', '', 0, 0, 1, 1, 0, 0, 72, '2018-03-08 07:29:06', 1),
(156, 25, 41, 72, 1, '0', 2, '', 'media/ttalk/images/2018-03/20180308_TTK-364899-1520494156.png', '', 1220359, 0, 1, 1, 0, 0, 72, '2018-03-08 07:29:16', 1),
(157, 26, 5, 72, 1, '0', 1, 'Hi', '', '', 0, 0, 1, 1, 0, 0, 72, '2018-03-08 07:34:20', 1),
(158, 16, 13, 48, 1, '0', 1, '你好👋🏻', '', '', 0, 0, 1, 1, 0, 0, 48, '2018-03-11 10:46:14', 1),
(159, 7, 16, 48, 1, '0', 1, 'Why fright team?', '', '', 0, 0, 1, 1, 0, 0, 48, '2018-03-11 10:47:30', 2),
(160, 21, 32, 53, 1, '0', 1, 'Hello chloe', '', '', 0, 0, 1, 1, 0, 0, 53, '2018-03-11 21:32:40', 2),
(161, 21, 32, 53, 1, '0', 1, 'Is this you?', '', '', 0, 0, 1, 1, 0, 0, 53, '2018-03-11 21:32:47', 2),
(162, 21, 32, 53, 1, '0', 2, '', 'media/ttalk/images/2018-03/20180311_TTK-575062-1520803976.png', '', 256166, 0, 1, 1, 0, 0, 53, '2018-03-11 21:32:56', 2),
(163, 21, 32, 48, 1, '0', 1, '你好！', '', '', 0, 0, 1, 1, 0, 0, 48, '2018-03-11 21:34:24', 2),
(164, 21, 32, 42, 1, '0', 1, ' No. This is Daniel speaking!', '', '', 0, 0, 1, 1, 0, 0, 42, '2018-03-11 21:36:19', 2),
(165, 21, 32, 53, 1, '0', 1, 'Hello again😊', '', '', 0, 0, 1, 1, 0, 0, 53, '2018-03-11 21:45:27', 2),
(166, 21, 32, 53, 1, '0', 1, 'Chloe left!', '', '', 0, 0, 1, 1, 0, 0, 53, '2018-03-11 21:45:36', 2),
(167, 7, 16, 42, 1, '0', 1, '😘', '', '', 0, 0, 1, 1, 0, 0, 42, '2018-03-13 20:01:14', 1),
(168, 27, 43, 77, 1, '0', 1, 'hi', '', '', 0, 0, 1, 1, 0, 0, 77, '2018-03-16 05:53:26', 1),
(169, 27, 43, 77, 1, '0', 1, 'hi', '', '', 0, 0, 1, 1, 0, 0, 77, '2018-03-16 05:53:30', 1),
(170, 27, 43, 74, 1, '0', 1, '😞😒🧐😒☹️', '', '', 0, 0, 1, 1, 0, 0, 74, '2018-03-16 05:53:40', 1),
(171, 4, 1, 42, 1, '0', 1, 'This is me', '', '', 0, 0, 1, 1, 0, 0, 42, '2018-03-16 12:45:52', 1),
(172, 4, 1, 42, 1, '0', 1, 'From the iphone6', '', '', 0, 0, 1, 1, 0, 0, 42, '2018-03-16 12:46:01', 1),
(173, 4, 1, 42, 1, '0', 1, 'And this is me from the iPhoneX.', '', '', 0, 0, 1, 1, 0, 0, 42, '2018-03-16 13:00:06', 1),
(174, 4, 1, 42, 1, '0', 1, 'Cool it automatically logs out of the other device. Great 1-1-1 relationship.', '', '', 0, 0, 1, 1, 0, 0, 42, '2018-03-16 13:02:17', 1),
(175, 4, 1, 45, 1, '0', 3, '', 'media/ttalk/videos/2018-03/20180318_TTK-411456-1521382466.mp4', 'media/ttalk/videos/2018-03/20180318_TTK-411456-1521382466_thumb.png', 0, 0, 1, 1, 0, 0, 45, '2018-03-18 14:14:26', 1),
(176, 4, 1, 45, 1, '0', 4, '', 'media/ttalk/documents/2018-03/20180318_TTK-466795-1521382509.pdf', '', 365425, 0, 1, 1, 0, 0, 45, '2018-03-18 14:15:09', 1),
(177, 4, 1, 45, 1, '0', 3, '', 'media/ttalk/videos/2018-03/20180318_TTK-102335-1521382598.mp4', '', 12341780, 0, 1, 1, 0, 0, 45, '2018-03-18 14:16:38', 1),
(178, 28, 44, 76, 1, '0', 1, 'gjjh', '', '', 0, 0, 1, 1, 0, 0, 76, '2018-03-19 08:02:52', 1),
(179, 28, 44, 76, 1, '0', 3, '', 'media/ttalk/videos/2018-03/20180319_TTK-461894-1521446589.mp4', 'media/ttalk/videos/2018-03/20180319_TTK-461894-1521446589_thumb.png', 0, 0, 1, 1, 0, 0, 76, '2018-03-19 08:03:09', 1),
(180, 29, 39, 66, 1, '0', 1, 'hello', '', '', 0, 0, 1, 1, 0, 0, 66, '2018-03-19 12:10:15', 1),
(181, 29, 39, 66, 1, '0', 3, '', 'media/ttalk/videos/2018-03/20180320_TTK-823580-1521537247.mp4', 'media/ttalk/videos/2018-03/20180320_TTK-823580-1521537247_thumb.png', 0, 0, 1, 1, 0, 0, 66, '2018-03-20 09:14:07', 1),
(182, 28, 44, 76, 1, '0', 3, '', 'media/ttalk/videos/2018-03/20180320_TTK-312182-1521539159.mp4', 'media/ttalk/videos/2018-03/20180320_TTK-312182-1521539159_thumb.png', 0, 0, 1, 1, 0, 0, 76, '2018-03-20 09:45:59', 1),
(183, 28, 44, 76, 1, '0', 3, '', 'media/ttalk/videos/2018-03/20180320_TTK-563351-1521539439.mp4', 'media/ttalk/videos/2018-03/20180320_TTK-563351-1521539439_thumb.png', 0, 0, 1, 1, 0, 0, 76, '2018-03-20 09:50:39', 1),
(184, 28, 44, 76, 1, '0', 3, '', 'media/ttalk/videos/2018-03/20180320_TTK-804323-1521540678.mp4', 'media/ttalk/videos/2018-03/20180320_TTK-804323-1521540678_thumb.png', 0, 0, 1, 1, 0, 0, 76, '2018-03-20 10:11:19', 1),
(185, 29, 39, 66, 1, '0', 3, '', 'media/ttalk/videos/2018-03/20180320_TTK-595081-1521542399.3gpp', 'media/ttalk/videos/2018-03/20180320_TTK-595081-1521542399_thumb.png', 215799, 0, 1, 1, 0, 0, 66, '2018-03-20 10:39:59', 1),
(186, 29, 39, 66, 1, '0', 3, '', 'media/ttalk/videos/2018-03/20180320_TTK-494397-1521542508.mp4', 'media/ttalk/videos/2018-03/20180320_TTK-494397-1521542508_thumb.png', 383631, 0, 1, 1, 0, 0, 66, '2018-03-20 10:41:48', 1),
(187, 29, 39, 66, 1, '0', 3, '', 'media/ttalk/videos/2018-03/20180320_TTK-922572-1521543257.3gpp', 'media/ttalk/videos/2018-03/20180320_TTK-922572-1521543257_thumb.png', 215799, 0, 1, 1, 0, 0, 66, '2018-03-20 10:54:17', 1),
(188, 29, 39, 66, 1, '0', 3, '', 'media/ttalk/videos/2018-03/20180320_TTK-421065-1521543308.mp4', '', 560607, 0, 1, 1, 0, 0, 66, '2018-03-20 10:55:08', 1),
(189, 29, 39, 66, 1, '0', 2, '', 'media/ttalk/images/2018-03/20180320_TTK-139473-1521543707.png', '', 21255, 0, 1, 1, 0, 0, 66, '2018-03-20 11:01:47', 1),
(190, 28, 44, 76, 1, '0', 3, '', 'media/ttalk/videos/2018-03/20180320_TTK-603254-1521544430.mp4', 'media/ttalk/videos/2018-03/20180320_TTK-603254-1521544430_thumb.png', 12283675, 0, 1, 1, 0, 0, 76, '2018-03-20 11:13:50', 1),
(191, 30, 45, 81, 1, '0', 2, '', 'media/ttalk/images/2018-03/20180321_TTK-276419-1521649458.png', '', 70440, 0, 1, 1, 0, 0, 81, '2018-03-21 16:24:18', 1),
(192, 30, 45, 81, 1, '0', 1, 'It‘s me.', '', '', 0, 0, 1, 1, 0, 0, 81, '2018-03-21 16:24:25', 1),
(193, 4, 1, 42, 1, '0', 1, 'Dj from iphonex2', '', '', 0, 0, 1, 1, 0, 0, 42, '2018-03-21 16:42:29', 1),
(194, 4, 1, 42, 1, '0', 2, '', 'media/ttalk/images/2018-03/20180321_TTK-513707-1521650600.png', '', 1429410, 0, 1, 1, 0, 0, 42, '2018-03-21 16:43:21', 1),
(195, 4, 1, 42, 1, '0', 2, '', 'media/ttalk/images/2018-03/20180321_TTK-538761-1521650630.png', '', 2788216, 0, 1, 1, 0, 0, 42, '2018-03-21 16:43:50', 1),
(196, 4, 1, 42, 1, '0', 2, '', 'media/ttalk/images/2018-03/20180321_TTK-851609-1521650694.png', '', 2788216, 0, 1, 1, 0, 0, 42, '2018-03-21 16:44:54', 1),
(197, 4, 1, 42, 1, '0', 2, '', 'media/ttalk/images/2018-03/20180321_TTK-856630-1521650715.png', '', 2788216, 0, 1, 1, 0, 0, 42, '2018-03-21 16:45:15', 1),
(198, 4, 1, 42, 1, '0', 3, '', 'media/ttalk/videos/2018-03/20180321_TTK-180148-1521650761.mov', 'media/ttalk/videos/2018-03/20180321_TTK-180148-1521650761_thumb.png', 1230210, 0, 1, 1, 0, 0, 42, '2018-03-21 16:46:01', 1),
(199, 4, 1, 42, 1, '0', 3, '', 'media/ttalk/videos/2018-03/20180321_TTK-327010-1521650788.mov', 'media/ttalk/videos/2018-03/20180321_TTK-327010-1521650788_thumb.png', 1230210, 0, 1, 1, 0, 0, 42, '2018-03-21 16:46:28', 1),
(200, 30, 45, 42, 1, '0', 1, 'Now from iphonex2', '', '', 0, 0, 1, 1, 0, 0, 42, '2018-03-21 17:22:50', 1),
(201, 30, 45, 42, 1, '0', 2, '', 'media/ttalk/images/2018-03/20180321_TTK-370433-1521653037.png', '', 2788216, 0, 1, 1, 0, 0, 42, '2018-03-21 17:23:57', 1),
(202, 30, 45, 42, 1, '0', 1, 'Sent?', '', '', 0, 0, 1, 1, 0, 0, 42, '2018-03-21 17:24:15', 1),
(203, 30, 45, 42, 1, '0', 3, '', 'media/ttalk/videos/2018-03/20180321_TTK-858921-1521653057.mov', 'media/ttalk/videos/2018-03/20180321_TTK-858921-1521653057_thumb.png', 3356556, 0, 1, 1, 0, 0, 42, '2018-03-21 17:24:17', 1),
(204, 30, 45, 42, 1, '0', 1, 'And now this is me back from the iphonex2 address.', '', '', 0, 0, 1, 1, 0, 0, 42, '2018-03-21 17:38:45', 1),
(205, 29, 39, 66, 1, '0', 3, '', 'media/ttalk/videos/2018-03/20180322_TTK-686940-1521711814.mp4', 'media/ttalk/videos/2018-03/20180322_TTK-686940-1521711814_thumb.png', 383631, 0, 1, 1, 0, 0, 66, '2018-03-22 09:43:34', 1),
(206, 29, 39, 66, 1, '0', 3, '', 'media/ttalk/videos/2018-03/20180322_TTK-708578-1521712014.mp4', 'media/ttalk/videos/2018-03/20180322_TTK-708578-1521712014_thumb.png', 383631, 0, 1, 1, 0, 0, 66, '2018-03-22 09:46:54', 1),
(207, 29, 39, 66, 1, '0', 3, '', 'media/ttalk/videos/2018-03/20180322_TTK-931561-1521712054.mp4', 'media/ttalk/videos/2018-03/20180322_TTK-931561-1521712054_thumb.png', 383631, 0, 1, 1, 0, 0, 66, '2018-03-22 09:47:34', 1),
(208, 28, 44, 76, 1, '0', 3, '', 'media/ttalk/videos/2018-03/20180322_TTK-487506-1521713290.mp4', 'media/ttalk/videos/2018-03/20180322_TTK-487506-1521713290_thumb.png', 3473845, 0, 1, 1, 0, 0, 76, '2018-03-22 10:08:10', 1),
(209, 28, 44, 66, 1, '0', 3, '', 'media/ttalk/videos/2018-03/20180322_TTK-107303-1521717048.3gpp', 'media/ttalk/videos/2018-03/20180322_TTK-107303-1521717048_thumb.png', 215799, 0, 1, 1, 0, 0, 66, '2018-03-22 11:10:48', 1),
(210, 28, 44, 76, 1, '0', 1, 'hello', '', '', 0, 0, 1, 1, 0, 0, 76, '2018-03-22 11:11:24', 1),
(211, 28, 44, 76, 1, '0', 1, 'how are u', '', '', 0, 0, 1, 1, 0, 0, 76, '2018-03-22 11:11:31', 1),
(212, 28, 44, 76, 1, '0', 1, 'gahj', '', '', 0, 0, 1, 1, 0, 0, 76, '2018-03-22 11:11:52', 1),
(213, 28, 44, 76, 1, '0', 1, 'cgh', '', '', 0, 0, 1, 1, 0, 0, 76, '2018-03-22 11:13:18', 1),
(214, 28, 44, 76, 1, '0', 1, 'hello', '', '', 0, 0, 1, 1, 0, 0, 76, '2018-03-22 11:13:51', 1),
(215, 28, 44, 76, 2, '66', 1, 'helo', '', '', 0, 0, 1, 1, 0, 0, 76, '2018-03-22 11:27:37', 1),
(216, 28, 44, 76, 1, '0', 1, 'hgrgah', '', '', 0, 0, 1, 1, 0, 0, 76, '2018-03-22 11:27:51', 1),
(217, 28, 44, 76, 1, '0', 2, '', 'media/ttalk/images/2018-03/20180322_TTK-917315-1521718166.png', '', 126014, 0, 1, 1, 0, 0, 76, '2018-03-22 11:29:26', 1),
(218, 28, 44, 76, 1, '0', 2, '', 'media/ttalk/images/2018-03/20180322_TTK-808544-1521718473.png', '', 40875, 0, 1, 1, 0, 0, 76, '2018-03-22 11:34:33', 1),
(219, 31, 46, 49, 1, '0', 1, 'hello', '', '', 0, 0, 1, 1, 0, 0, 49, '2018-03-23 06:53:43', 1),
(220, 31, 46, 71, 1, '0', 1, 'Hi', '', '', 0, 0, 1, 1, 0, 0, 71, '2018-03-23 06:53:56', 1),
(221, 31, 46, 49, 1, '0', 2, '', 'media/ttalk/images/2018-03/20180323_TTK-128174-1521788057.png', '', 124126, 0, 1, 1, 0, 0, 49, '2018-03-23 06:54:17', 1),
(222, 31, 46, 71, 1, '0', 1, 'Ok', '', '', 0, 0, 1, 1, 0, 0, 71, '2018-03-23 06:54:47', 1),
(223, 31, 46, 49, 1, '0', 3, '', 'media/ttalk/videos/2018-03/20180323_TTK-690564-1521788159.mp4', 'media/ttalk/videos/2018-03/20180323_TTK-690564-1521788159_thumb.png', 5487369, 0, 1, 1, 0, 0, 49, '2018-03-23 06:56:00', 1),
(224, 31, 46, 49, 2, '71', 1, 'hell', '', '', 0, 0, 1, 1, 0, 0, 49, '2018-03-23 06:58:19', 1),
(225, 31, 46, 71, 2, '49,49', 1, 'Hi', '', '', 0, 0, 1, 1, 0, 0, 71, '2018-03-23 06:58:47', 1),
(226, 28, 44, 76, 1, '0', 3, '', 'media/ttalk/videos/2018-03/20180326_TTK-472187-1522039985.3gpp', 'media/ttalk/videos/2018-03/20180326_TTK-472187-1522039985_thumb.png', 215799, 0, 1, 1, 0, 0, 76, '2018-03-26 04:53:05', 1),
(227, 28, 44, 76, 1, '0', 2, '', 'media/ttalk/images/2018-03/20180326_TTK-556450-1522040019.png', '', 13586, 0, 1, 1, 0, 0, 76, '2018-03-26 04:53:39', 1),
(228, 2, 8, 1, 1, '0', 1, 'The team leader is no longer able to manage the team so all team members have 1 week to became the Team leader by pressing Assume leadership button.', '', '', 0, 0, 1, 1, 0, 0, 1, '2018-03-26 06:53:34', 1),
(229, 32, 9, 1, 1, '0', 1, 'The team leader is no longer able to manage the team so all team members have 1 week to became the Team leader by pressing Assume leadership button.', '', '', 0, 0, 1, 1, 0, 0, 1, '2018-03-26 06:53:34', 1),
(230, 11, 12, 1, 1, '0', 1, 'The team leader is no longer able to manage the team so all team members have 1 week to became the Team leader by pressing Assume leadership button.', '', '', 0, 0, 1, 1, 0, 0, 1, '2018-03-26 06:53:34', 1),
(231, 33, 11, 1, 1, '0', 1, 'The team leader is no longer able to manage the team so all team members have 1 week to became the Team leader by pressing Assume leadership button.', '', '', 0, 0, 1, 1, 0, 0, 1, '2018-03-27 05:16:51', 1),
(232, 19, 22, 1, 1, '0', 1, 'The team leader is no longer able to manage the team so all team members have 1 week to became the Team leader by pressing Assume leadership button.', '', '', 0, 0, 1, 1, 0, 0, 1, '2018-03-27 05:16:51', 1),
(233, 34, 24, 1, 1, '0', 1, 'The team leader is no longer able to manage the team so all team members have 1 week to became the Team leader by pressing Assume leadership button.', '', '', 0, 0, 1, 1, 0, 0, 1, '2018-03-27 05:16:51', 1),
(234, 10, 19, 1, 1, '0', 1, 'The team leader is no longer able to manage the team so all team members have 1 week to became the Team leader by pressing Assume leadership button.', '', '', 0, 0, 1, 1, 0, 0, 1, '2018-03-27 05:26:00', 1),
(235, 12, 25, 1, 1, '0', 1, 'The team leader is no longer able to manage the team so all team members have 1 week to became the Team leader by pressing Assume leadership button.', '', '', 0, 0, 1, 1, 0, 0, 1, '2018-03-27 07:17:03', 1),
(236, 35, 26, 1, 1, '0', 1, 'The team leader is no longer able to manage the team so all team members have 1 week to became the Team leader by pressing Assume leadership button.', '', '', 0, 0, 1, 1, 0, 0, 1, '2018-03-27 07:17:03', 1),
(237, 18, 27, 1, 1, '0', 1, 'The team leader is no longer able to manage the team so all team members have 1 week to became the Team leader by pressing Assume leadership button.', '', '', 0, 0, 1, 1, 0, 0, 1, '2018-03-27 07:17:03', 1),
(238, 23, 30, 1, 1, '0', 1, 'The team leader is no longer able to manage the team so all team members have 1 week to became the Team leader by pressing Assume leadership button.', '', '', 0, 0, 1, 1, 0, 0, 1, '2018-03-27 07:17:03', 1),
(239, 36, 47, 86, 1, '0', 1, 'hello', '', '', 0, 0, 1, 1, 0, 0, 86, '2018-03-27 11:29:18', 1),
(240, 37, 48, 84, 1, '0', 1, 'hello', '', '', 0, 0, 1, 1, 0, 0, 84, '2018-03-28 05:12:45', 1),
(241, 37, 48, 84, 1, '0', 1, 'gshsh', '', '', 0, 0, 1, 1, 0, 0, 84, '2018-03-28 05:12:48', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `account_type` int(11) NOT NULL DEFAULT '1' COMMENT '1 User, 2 Admin',
  `company_id` int(11) NOT NULL,
  `user_name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `designation` int(11) NOT NULL,
  `password` varchar(255) NOT NULL,
  `udid` varchar(255) NOT NULL COMMENT 'For IOS Finger tuch login process',
  `touch_login` tinyint(4) NOT NULL COMMENT '0 not configured 1 configured',
  `country_code` varchar(100) NOT NULL,
  `mobile_number` varchar(30) NOT NULL,
  `profile_picture` varchar(255) NOT NULL,
  `datetime` varchar(30) NOT NULL,
  `timezone` varchar(100) NOT NULL,
  `rolodex_collectinginfo` int(11) NOT NULL DEFAULT '1' COMMENT '1 means On 0 means Off',
  `new_email_to_change` varchar(255) NOT NULL,
  `device_change_verify_code` varchar(10) NOT NULL,
  `email_change_verify_code` varchar(10) NOT NULL,
  `email_varification` varchar(20) NOT NULL COMMENT 'blank verified, else verification code to verify',
  `last_email` varchar(255) DEFAULT NULL,
  `registered_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_login` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_active` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `account_type`, `company_id`, `user_name`, `email`, `designation`, `password`, `udid`, `touch_login`, `country_code`, `mobile_number`, `profile_picture`, `datetime`, `timezone`, `rolodex_collectinginfo`, `new_email_to_change`, `device_change_verify_code`, `email_change_verify_code`, `email_varification`, `last_email`, `registered_at`, `last_login`, `updated_at`, `is_active`) VALUES
(1, 2, 1, 'Admin', 'dani@gmail.com', 0, '$6$s8^3s;$S3HhiiOkyDXhF.zJkNXM2SsTlFsO5Irhlk.UF0D059QYUitI/RvXj/nOkcZtPiGR6DmLwOFLvraaa2Sfux.iF0', '', 0, '', '', '', '', '', 1, '', '', '', '', NULL, '2018-01-04 07:12:10', '2018-01-04 07:12:10', '2018-01-04 07:12:10', 1),
(6, 1, 1, 'Simran Dhillon', 'simranjeet.techwinlabs@gmail.com', 2, '$6$s8^3s;$HX3JluB20yJhFl.x/M8h.n8atJam/w03sMJt7tW8He6PVcrBYoFuMjm0eLEEIM3AA0y0gWU4EdOxy6pVhBxKf1', '', 0, '', '', 'media/profile/2018-02/20180216_PP-671879-1518779065.png', '2017-12-29 12:14:24', 'Africa/Asmara', 1, 'simranjeet.techwinlabs@gmail.com', '245262', '', '', NULL, '2017-10-23 07:03:41', '2018-03-07 08:06:21', '2018-03-06 09:42:20', 1),
(19, 1, 1, 'sim4', 'sim4@gmail.com', 2, '$6$s8^3s;$HX3JluB20yJhFl.x/M8h.n8atJam/w03sMJt7tW8He6PVcrBYoFuMjm0eLEEIM3AA0y0gWU4EdOxy6pVhBxKf1', '9bf9201124966722', 0, '', '8427631369', 'media/profile/2017-12/20171218_PP-355103-1513599739.png', '2017-12-19 18:27:01', 'Asia/Calcutta', 0, '', '', '', '', NULL, '2017-11-03 10:09:55', '2018-01-23 12:02:26', '2017-12-19 19:58:25', 1),
(40, 1, 1, 'simu', 'testing@gmail.com', 0, '$6$s8^3s;$xz52yN6Kc15WE4AsUGEbx.Jo4maw7cXkpPiLK3AYY1Q0JxJTiVQ8P1hnhII1wH3RnByOuy4l6uTI2UzEOLgUb.', '', 0, '', '', '', '', '', 1, '', '', '', '', NULL, '2018-01-29 13:29:35', '2018-01-29 13:29:35', '2018-01-29 13:32:46', 1),
(41, 1, 1, 'Baljinder', 'baljindersingh632@gmail.com', 0, '$6$s8^3s;$xz52yN6Kc15WE4AsUGEbx.Jo4maw7cXkpPiLK3AYY1Q0JxJTiVQ8P1hnhII1wH3RnByOuy4l6uTI2UzEOLgUb.', 'DFCE5988-4B23-45BC-83B8-3B2985A02AAD', 0, '91', '9988359350', '', '', '', 1, '', '', '', '', NULL, '2018-01-29 14:35:01', '2018-01-29 14:36:47', '2018-01-29 14:35:01', 1),
(42, 1, 1, 'Daniel Janouschek ', 'djanouschek@gmail.com', 12, '$6$s8^3s;$o9LFnvIlRyGO8SaXlBJqYd1dZE4wz30vJQD0MhF.USWhiTFkwI5I3iJ7.CPlYDIAQXqLdQGo7Q5zU2aahMBmb.', '852BACDF-8438-4B06-80C4-6D220901CAE2', 1, '49', '1706177395', 'media/profile/2018-02/20180204_PP-391842-1517761686.png', '2018-03-21 20:23:04', 'Africa/Conakry', 1, 'djanouschek@gmail.com', '', '', '', NULL, '2018-01-29 21:18:52', '2018-03-21 17:37:37', '2018-03-21 19:23:04', 1),
(43, 1, 1, 'AndroidEmulater', 'android@gmail.com', 2, '$6$s8^3s;$Y33WqvcEmPlDjdJ9/QB3Wd.EufD52l5kFfBC/juQ5hIJmam4ppPVDi/YKgr7A252OLUH0rYj2Q71iV/SHr7fn/', '', 0, '', '', 'media/profile/2018-01/20180130_PP-816435-1517296339.png', '2018-01-30 12:40:13', 'Asia/Calcutta', 1, '', '473251', '', '', NULL, '2018-01-30 07:11:14', '2018-02-05 04:56:50', '2018-01-31 06:53:23', 1),
(44, 1, 0, 'yuplay', '', 0, '', '', 0, '', '', '', '', '', 0, '', '', '', '', 'arvind.smugchucks@gmail.com', '2018-01-30 11:02:24', '2018-02-01 10:04:11', '2018-03-27 05:16:51', 2),
(45, 1, 1, 'Vivek Sannabhadti', 'vsannabhadti@gmail.com', 11, '$6$s8^3s;$HBbDYOkxUSaXGvfSN/RpzyFLypx.d6WPiUxLk7ACpAynaXVhsADlqhwPPMfHM/wrWUTWU/SGg.y2WHxRjTNRg.', '', 0, '49', '15237391088', 'media/profile/2018-02/20180204_PP-290991-1517756812.png', '2018-02-04 16:06:11', 'Europe/Berlin', 1, '', '', '', '', NULL, '2018-01-30 20:21:20', '2018-03-25 14:49:35', '2018-03-25 14:51:28', 1),
(46, 1, 0, 'Arvind Poonia', '', 0, '', '', 0, '', '', '', '', '', 0, '', '', '', '', 'arvindpoonia01@gmail.com', '2018-01-31 06:55:20', '2018-02-01 05:09:33', '2018-03-26 06:53:34', 2),
(47, 1, 1, 'Vivek S android', 'viveksannatest@gmail.com', 11, '$6$s8^3s;$HBbDYOkxUSaXGvfSN/RpzyFLypx.d6WPiUxLk7ACpAynaXVhsADlqhwPPMfHM/wrWUTWU/SGg.y2WHxRjTNRg.', '', 0, '49', '15146748527', 'media/profile/2018-02/20180218_PP-440931-1518971794.png', '2018-03-04 13:41:50', 'America/Detroit', 1, '', '', '', '', NULL, '2018-02-04 14:15:18', '2018-03-25 15:27:20', '2018-03-25 15:29:56', 1),
(48, 1, 1, 'Chloé Liu', 'chloe.xin1@gmail.com', 2, '$6$s8^3s;$o9LFnvIlRyGO8SaXlBJqYd1dZE4wz30vJQD0MhF.USWhiTFkwI5I3iJ7.CPlYDIAQXqLdQGo7Q5zU2aahMBmb.', '076526D3-068C-45AD-A807-96DD6F8DC771', 0, '49', '15251971915', 'media/profile/2018-02/20180204_PP-450537-1517767907.png', '2018-03-04 16:58:49', 'Africa/Kigali', 1, 'chloe.xin1@gmail.com', '', '', '', NULL, '2018-02-04 14:59:00', '2018-03-17 23:15:36', '2018-03-17 23:14:54', 1),
(49, 1, 0, 'Tony', '', 0, '', '', 0, '', '', '', '', '', 0, '', '', '', '', 'arvindpoonia.techwinlabs@gmail.com', '2018-02-06 13:35:44', '2018-03-27 07:10:33', '2018-03-27 07:17:03', 2),
(50, 1, 1, 'Hitesh', 'hitedfdfsh.techwin@gmail.com', 6, '$6$s8^3s;$xz52yN6Kc15WE4AsUGEbx.Jo4maw7cXkpPiLK3AYY1Q0JxJTiVQ8P1hnhII1wH3RnByOuy4l6uTI2UzEOLgUb.', '', 0, '91', '85560239338', 'media/profile/2018-02/20180221_PP-667987-1519213662.png', '2018-02-12 19:02:06', 'Asia/Kolkata', 0, '', '487529', '', '', NULL, '2018-02-07 10:58:14', '2018-02-20 17:10:35', '2018-02-21 11:47:55', 1),
(51, 1, 0, 'jaswinder', '', 0, '', '', 0, '', '', '', '', '', 0, '', '', '', '', 'jaswinder.techwin@gmail.com', '2018-02-07 12:22:50', '2018-02-07 12:25:58', '2018-03-27 05:26:00', 2),
(52, 1, 1, 'p Neet', 'pneet.techwinlabs@gmail.com', 0, '$6$s8^3s;$Y33WqvcEmPlDjdJ9/QB3Wd.EufD52l5kFfBC/juQ5hIJmam4ppPVDi/YKgr7A252OLUH0rYj2Q71iV/SHr7fn/', '2b5478bd88b1gfg', 0, '91', '81469123333', '', '', '', 1, '', '769055', '', '', NULL, '2018-02-09 11:56:56', '2018-02-26 05:52:35', '2018-02-19 13:45:38', 1),
(53, 1, 1, 'inr iPhoneX', 'inrtestiphonex@gmail.com', 14, '$6$s8^3s;$o9LFnvIlRyGO8SaXlBJqYd1dZE4wz30vJQD0MhF.USWhiTFkwI5I3iJ7.CPlYDIAQXqLdQGo7Q5zU2aahMBmb.', 'BD284683-BF0C-47FF-89DE-3B3F7685A450', 0, '49', '1746599158', '', '2018-03-20 01:06:06', 'Europe/Berlin', 1, '', '', '', '', NULL, '2018-02-10 15:43:42', '2018-03-21 19:08:17', '2018-03-21 19:13:44', 1),
(54, 1, 1, 'Rahul', 'rahdfdul5.techwinlabs@gmail.com', 6, '$6$s8^3s;$qBVg.Gzv6sSG2e//3gdqjjV4pRAudLMmPMfb9MDFGToyoBqm3zD.n8aIveO0cly4YU0e0P5OVRp0rpcw21FUF.', '2c17dda466sdfd46691edfdf', 0, '91', '4545455566', 'media/profile/2018-02/20180214_PP-775017-1518615786.png', '2018-02-16 11:41:21', 'Asia/Kolkata', 1, '', '', '', '', NULL, '2018-02-14 12:10:26', '2018-02-26 05:57:42', '2018-02-26 05:59:34', 1),
(55, 1, 1, 'Neelamdfd', 'nesdelam.techwinlabs@gmail.com', 0, '$6$s8^3s;$UAE16wH0j3OqNCFhO/D3um6Op7bojxBdy8sgn6/WVG.i7Hq9jrCVjedGvg5FMgyptIiOtKEbuLtkIXdQRDn1S/', '98D800D7-3E42-402E-B010-BBC79D3F2A7dfdsdf8', 0, '91', '88729434343', '', '', '', 1, '', '', '', '668447', NULL, '2018-02-19 11:32:47', '2018-02-26 06:53:36', '2018-02-26 06:51:29', 1),
(56, 1, 1, 'P deep', 'pra.techwinlabs@gmail.com', 0, '$6$s8^3s;$JjLcCQQJs2qLvkksShO.CJzF73zWNGgoMp62sffyZ.OyYBBmdvaw5WnlmtuT1oZrwRtXtVK2cJSOzF87R7Lii/', 'ddfdfd', 0, '91', '8988341354', '', '', '', 1, '', '677398', '', '', NULL, '2018-02-20 08:32:35', '2018-02-20 14:34:31', '2018-02-20 14:34:31', 1),
(57, 1, 1, 'Caf', 'caftd@gmail.com', 0, '$6$s8^3s;$xz52yN6Kc15WE4AsUGEbx.Jo4maw7cXkpPiLK3AYY1Q0JxJTiVQ8P1hnhII1wH3RnByOuy4l6uTI2UzEOLgUb.', '', 0, '91', '9834875096', '', '', '', 1, '', '', '', '', NULL, '2018-02-20 12:13:42', '2018-02-20 12:13:42', '2018-02-20 12:13:42', 1),
(58, 1, 1, 'Pneu', 'pnewu@gmail.com', 0, '$6$s8^3s;$xz52yN6Kc15WE4AsUGEbx.Jo4maw7cXkpPiLK3AYY1Q0JxJTiVQ8P1hnhII1wH3RnByOuy4l6uTI2UzEOLgUb.', '', 0, '91', '9548875096', '', '', '', 1, '', '', '', '', NULL, '2018-02-20 12:33:36', '2018-02-20 12:33:36', '2018-02-20 12:33:36', 1),
(59, 1, 1, 'Cat', 'apnewu@gmail.com', 0, '$6$s8^3s;$xz52yN6Kc15WE4AsUGEbx.Jo4maw7cXkpPiLK3AYY1Q0JxJTiVQ8P1hnhII1wH3RnByOuy4l6uTI2UzEOLgUb.', '', 0, '', '', '', '', '', 1, '', '', '', '', NULL, '2018-02-20 12:40:46', '2018-02-20 13:05:06', '2018-02-20 13:09:55', 1),
(60, 1, 1, 'cafy', 'caftapnewu@gmail.com', 0, '$6$s8^3s;$dq4KH6Qel4NUhoRQ0sr2XK9bWZbk2CBxKEPVLeyMg2CX3bYFrCEO3Jq8Cnre0MtWh1FpbQEtHH/xlm2aoLU5e0', '', 0, '91', '9888875096', '', '', '', 1, '', '494985', '', '', NULL, '2018-02-22 13:50:34', '2018-02-22 14:42:04', '2018-02-22 15:52:41', 1),
(61, 1, 1, 'tesing', 'tesing2@gmail.com', 0, '$6$s8^3s;$o9LFnvIlRyGO8SaXlBJqYd1dZE4wz30vJQD0MhF.USWhiTFkwI5I3iJ7.CPlYDIAQXqLdQGo7Q5zU2aahMBmb.', '', 1, '', '', '', '', '', 1, '', '492978', '', '', NULL, '2018-02-25 16:55:53', '2018-03-16 12:04:31', '2018-02-25 17:52:25', 1),
(62, 1, 1, 'Rahul dfd', 'rahulsdfd@gmail.com', 0, '$6$s8^3s;$AmwlrzelUl9mgbIFO3JIBfylYAqYFPWsRePKr6w79IpzqDhr1OZMY5Zl0E1/2yKeVOiua5OvDl9VUW1T7nVJA1', '2c17dda4dfdf6646691e', 0, '', '', '', '', '', 1, '', '', '', '', NULL, '2018-02-26 06:06:45', '2018-02-27 13:38:00', '2018-02-27 13:39:26', 1),
(63, 1, 1, 'kr singh', 'l2.techwinlabs@gmail.com', 0, '$6$s8^3s;$AmwlrzelUl9mgbIFO3JIBfylYAqYFPWsRePKr6w79IpzqDhr1OZMY5Zl0E1/2yKeVOiua5OvDl9VUW1T7nVJA1', '', 0, '', '', '', '', '', 1, '', '578806', '', '', NULL, '2018-02-27 14:10:42', '2018-02-27 14:17:16', '2018-02-28 06:19:25', 1),
(64, 1, 1, 'RKumar', 'rahudtechwinlabs@gmail.com', 0, '$6$s8^3s;$AmwlrzelUl9mgbIFO3JIBfylYAqYFPWsRePKr6w79IpzqDhr1OZMY5Zl0E1/2yKeVOiua5OvDl9VUW1T7nVJA1', 'dfdfdfd', 0, '91', '8723412848', '', '', '', 1, '', '832698', '', '', NULL, '2018-02-28 07:32:01', '2018-02-28 07:36:35', '2018-02-28 07:32:01', 1),
(65, 1, 1, 'Singh', 'rahuldfdchwinlabs@gmail.com', 0, '$6$s8^3s;$AmwlrzelUl9mgbIFO3JIBfylYAqYFPWsRePKr6w79IpzqDhr1OZMY5Zl0E1/2yKeVOiua5OvDl9VUW1T7nVJA1', '2c17dda46646dfdf691e', 0, '91', '2729012848', '', '', '', 1, '', '571267', '', '', NULL, '2018-02-28 08:08:13', '2018-02-28 08:26:46', '2018-02-28 08:26:49', 1),
(66, 1, 1, 'Rahul Singh', 'rahul2.techwinlabs@gmail.com', 7, '$6$s8^3s;$AmwlrzelUl9mgbIFO3JIBfylYAqYFPWsRePKr6w79IpzqDhr1OZMY5Zl0E1/2yKeVOiua5OvDl9VUW1T7nVJA1', '', 0, '', '', 'media/profile/2018-03/20180320_PP-294553-1521547407.png', '2018-03-20 17:31:04', 'Asia/Calcutta', 1, '', '417700', '', '', NULL, '2018-02-28 08:34:17', '2018-03-20 07:14:59', '2018-03-23 09:28:17', 1),
(68, 1, 1, 'Asd', 'sainlabs@gmail.com', 0, '$6$s8^3s;$Orh7qoSLv9cjKEGOhaNtbJfhLgjD8ed9.9/0MPYVgBM7RqTuYDpXtfwZ3YuEBJqN91c4kv9zoYJW8sIZHnzZ5/', '62BC9439-CE63-4377-BA9C-E28C38EsdfF8EC6', 0, '91', '9592634762', '', '', '', 1, '', '', '', '', NULL, '2018-03-01 11:30:59', '2018-03-06 06:07:52', '2018-03-01 11:30:59', 1),
(69, 1, 0, 'sdfd', '', 0, '', '', 0, '', '', '', '', '', 0, '', '', '', '', 'sandfgfgf eedfdfp.techwinlabs@gmail.com', '2018-03-06 06:38:20', '2018-03-06 07:11:07', '2018-03-19 09:33:58', 2),
(70, 1, 1, 'wwr rg', 'sechwinlabs@gmail.com', 0, '$6$s8^3s;$Orh7qoSLv9cjKEGOhaNtbJfhLgjD8ed9.9/0MPYVgBM7RqTuYDpXtfwZ3YuEBJqN91c4kv9zoYJW8sIZHnzZ5/', '62BC9439-CE63-4377-BA9C-E28C38EF8EC6fgf', 0, '', '', '', '', '', 1, '', '864005', '', '', NULL, '2018-03-06 07:58:11', '2018-03-06 09:50:25', '2018-03-06 09:49:33', 1),
(71, 1, 1, 'sandeep', 'sandeep.techwinlabs@gmail.com', 0, '$6$s8^3s;$Orh7qoSLv9cjKEGOhaNtbJfhLgjD8ed9.9/0MPYVgBM7RqTuYDpXtfwZ3YuEBJqN91c4kv9zoYJW8sIZHnzZ5/', '89F2907C-3473-4068-B783-22D25153C4F0', 0, '91', '9592687762', '', '', '', 1, '', '', '', '', NULL, '2018-03-06 13:01:12', '2018-03-21 06:37:28', '2018-03-21 06:46:19', 1),
(72, 1, 1, 'Anuj', 'anuj.techwinlabs@gmail.com', 0, '$6$s8^3s;$6I39TQB2DfQnehGVJV1LBLERjtmf.UZyiWUObr2cSY.7U2PRvUteccIlFBi0AUBWurCkxcWgmlUxq60qTBtsU/', '', 0, '91', '9988853555', '', '', '', 1, '', '', '', '', NULL, '2018-03-08 06:35:19', '2018-03-08 06:38:30', '2018-03-14 07:44:01', 1),
(73, 1, 1, 'kawaljddeet', 'kawalds.techwinlabs@gmail.com', 0, '$6$s8^3s;$jWBAIPMX6FYxUauKTWRO.e0m9w4xZ.pxLYYxITll5zKHPk4eprqTPRx5Epi2lqcVlHXi4s1cOZNRXMb/ekech/', '2DC80C08-1BC0-484A-ABC6-7DE67CE829a6', 0, '91', '9653468040', '', '', '', 1, '', '', '', '865876', NULL, '2018-03-13 10:50:40', '2018-03-13 10:50:40', '2018-03-13 10:50:40', 1),
(74, 1, 1, 'kawal', 'kawal.techwinlabs@gmail.com', 0, '$6$s8^3s;$jWBAIPMX6FYxUauKTWRO.e0m9w4xZ.pxLYYxITll5zKHPk4eprqTPRx5Epi2lqcVlHXi4s1cOZNRXMb/ekech/', 'F32DD7BE-1EFE-48CD-A880-BF48B817DD81', 0, '91', '9653868040', '', '', '', 1, '', '', '', '', NULL, '2018-03-14 08:06:04', '2018-03-21 06:31:35', '2018-03-21 06:30:09', 1),
(75, 1, 1, 'Pradep', 'pradsseep.techwinlabs@gmail.com', 0, '$6$s8^3s;$JjLcCQQJs2qLvkksShO.CJzF73zWNGgoMp62sffyZ.OyYBBmdvaw5WnlmtuT1oZrwRtXtVK2cJSOzF87R7Lii/', '', 0, '', '', '', '', '', 1, '', '305512', '', '', NULL, '2018-03-14 11:48:15', '2018-03-15 10:32:49', '2018-03-16 05:10:46', 1),
(76, 1, 1, 'pradeep', 'pradeep.techwinlabs@gmail.com', 0, '$6$s8^3s;$JjLcCQQJs2qLvkksShO.CJzF73zWNGgoMp62sffyZ.OyYBBmdvaw5WnlmtuT1oZrwRtXtVK2cJSOzF87R7Lii/', '', 0, '', '', 'media/profile/2018-03/20180322_PP-160996-1521718575.png', '2018-03-22 17:05:32', 'Asia/Calcutta', 1, '', '438363', '', '', NULL, '2018-03-16 05:17:25', '2018-03-26 04:49:38', '2018-03-26 06:12:16', 1),
(77, 1, 1, 'hitesh', 'amit.techwinlabs@gmail.com', 14, '$6$s8^3s;$P1oGmrzpoo/ZBRA8ZDBIJavA85HW.S.W79xZWj9m6vcn2nAjvU6VcTJj1X.LYnArUou5wgRgSH3u92sO0qwrd.', '761195BC-A99F-4BBA-A554-42C714EDDF42', 0, '91', '7837254343', '', '', '', 1, 'amit.techwinlabs@gmail.com', '867856', '', '', NULL, '2018-03-16 05:48:06', '2018-03-21 05:57:37', '2018-03-21 05:56:09', 1),
(78, 1, 1, 'Neelam', 'neelam.techwinlabs@gmail.com', 0, '$6$s8^3s;$U9bqJ5ZXF8tkywDjeXXyPJdvdBYqhI3rZ.WjN7XAwGRJLSZIjUS9rA.ks.aeM4DcU0I9Azmpsi8QOt..UiAiR1', '', 0, '91', '8988021354', '', '', '', 1, '', '', '', '', NULL, '2018-03-16 14:18:24', '2018-03-16 14:55:19', '2018-03-16 14:57:29', 1),
(79, 1, 0, 'iPhone X3', '', 0, '', '', 0, '', '', '', '', '', 0, '', '', '', '', 'inrtestiphonex3@gmail.com', '2018-03-16 14:51:32', '2018-03-21 14:55:44', '2018-03-21 15:01:26', 2),
(80, 1, 1, 'Chloe 2', 'Chloe.xin2test@gmail.com', 7, '$6$s8^3s;$o9LFnvIlRyGO8SaXlBJqYd1dZE4wz30vJQD0MhF.USWhiTFkwI5I3iJ7.CPlYDIAQXqLdQGo7Q5zU2aahMBmb.', '', 1, '93', '4567883', '', '', '', 1, '', '', '', '', NULL, '2018-03-17 22:35:12', '2018-03-17 22:35:12', '2018-03-17 23:09:39', 2),
(81, 1, 1, 'inrtestiphone x3', 'inrtestiphonex3@gmail.com', 0, '$6$s8^3s;$o9LFnvIlRyGO8SaXlBJqYd1dZE4wz30vJQD0MhF.USWhiTFkwI5I3iJ7.CPlYDIAQXqLdQGo7Q5zU2aahMBmb.', '', 1, '', '', '', '', '', 1, '', '', '', '', NULL, '2018-03-21 15:11:20', '2018-03-21 15:11:20', '2018-03-21 16:27:11', 1),
(82, 1, 1, 'inr test x2', 'inrtestiphonex2@gmail.com', 0, '$6$s8^3s;$o9LFnvIlRyGO8SaXlBJqYd1dZE4wz30vJQD0MhF.USWhiTFkwI5I3iJ7.CPlYDIAQXqLdQGo7Q5zU2aahMBmb.', '', 1, '49', '15237391088', '', '', '', 1, '', '', '', '', NULL, '2018-03-21 18:41:18', '2018-03-25 14:42:26', '2018-03-25 14:44:06', 1),
(83, 1, 0, 'trun', '', 0, '', '', 0, '', '', '', '', '', 0, '', '', '', '', 'trun.techwin@gmail.com', '2018-03-26 06:43:38', '2018-03-26 06:48:12', '2018-03-26 09:54:00', 2),
(84, 1, 1, 'Arvind', 'arvindpoonia01@gmail.com', 0, '$6$s8^3s;$4g/phj3q0dA1I19RA6vb5yLZ5cj39P/UZ3pjX3d.JQu5wznt3rQ0Atnz17gOq612Gems8PTzMNkKZwkw2AC6R1', '63b36cc83a520448', 0, '91', '9588366038', '', '', '', 1, '', '', '', '', NULL, '2018-03-26 09:56:59', '2018-03-27 12:30:31', '2018-03-27 12:32:45', 1),
(85, 1, 1, 'jassy', 'jaswinder.techwin@gmail.com', 0, '$6$s8^3s;$g5cYrlog98UlblWCXzF8uRbau0JjIB0YwWlleYhY9NDEHGTKLF3Ox.Ra72CAHiNqaCUUphZcrmWIflFFgdWeN1', '', 0, '', '', '', '', '', 1, '', '', '', '', NULL, '2018-03-27 05:30:32', '2018-03-27 05:30:32', '2018-03-27 06:51:51', 1),
(86, 1, 1, 'Arvind', 'arvindpoonia.techwinlabs@gmail.com', 1, '$6$s8^3s;$CqOekcGFRaaPh9hU1ff6l51VcOyXcYWsw/ZjdONBbnihRTZqHO2GV4X4jkSOA05pik62gYOdPyeKKCR6eodVb0', '846fb3f49aa380c5', 0, '91', '8053380461', '', '2018-03-27 16:58:13', 'Asia/Calcutta', 1, '', '', '', '', NULL, '2018-03-27 07:49:56', '2018-03-27 13:42:31', '2018-03-27 13:42:31', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users_authentication`
--

CREATE TABLE `users_authentication` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `token` varchar(255) NOT NULL,
  `device_type` int(11) NOT NULL COMMENT '1 for Apple, 2 for Android',
  `device_token` varchar(500) NOT NULL,
  `login_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users_authentication`
--

INSERT INTO `users_authentication` (`id`, `user_id`, `token`, `device_type`, `device_token`, `login_at`) VALUES
(9, 41, '$6$s8^3s;$NcNJYIzior6a5m8V4NJHe6HNUcDUGp.cWQB/VvxKWMDTtBTf1MP8ofEXpcbJbEPd3EZxeFU/BS7AITA3gMLmz0', 1, '5G8tb0T3', '2018-01-29 14:36:47'),
(19, 43, '$6$s8^3s;$fwVXeksHtAxfvQokExmZkIdr9l6Ula7yU1M59BbBlKL0q1uvYSsGVnB0po1cZwu5ib.QsZwsWO1LD81H5E2Ob.', 2, 'dR5f6oE_REg:APA91bHULtRHoW0hwQJbHxRIgmhENBJp9U2dhIZNLr0pKR1D9KUv4U-UXhw-c4gnFkptELoArXzUpUlzr2OoHX1RiN1rb6WUkUF6RTX7vM97ItH7Tj1s38XzzPawzhJ_rCec5NNTt4hQ', '2018-01-31 07:09:12'),
(36, 52, '$6$s8^3s;$RbviRgJC5.EEVPww2wfLbJvSANOAMHcR5OYnlx759iCuOv5fXXhCJsZf4fZq/lNbxE4pDk66tuQ.2ywo6N6vt/', 2, 'sdfsdfs', '2018-02-09 11:56:56'),
(84, 56, '$6$s8^3s;$AV1kns90rWfBOIYgB76xCVzN4cY7Ys2/S1uzewk.8.pCBaCBHXO01c2fSBZELUcnWx.680Uf/YdjFWXyZ7eS3/', 2, 'sdfsdfs', '2018-02-20 08:32:35'),
(105, 61, '$6$s8^3s;$fZw0WMk3cpXhZSBsTkMss53MKFyprj56oVI369k6Hd1IiJXvwtDAlATPjabbmbltfLeJhFqGIm4SPv.0C5SxB1', 2, 'Qs3VGcqA', '2018-02-25 18:49:43'),
(106, 54, '$6$s8^3s;$ltM.IIzxxAT75TfnPB.wZnJ80jwzuOtl03Uh7dA2f/N0cIqN9ABnbx.pUzlkQgkWTt3b3Cx0X6.3g08Qg1e2i.', 2, 'sdfsdfs', '2018-02-26 05:57:42'),
(108, 62, '$6$s8^3s;$giCao45DtTMML.fBd63.qNQkMp8Vhn7wWgeNsBwlPR3DPwMnoYFMlvp24eNUdX14C7W4sWBllJCoRhY5demD81', 2, 'sdfsdfs', '2018-02-26 06:47:40'),
(111, 64, '$6$s8^3s;$Naa8Cce.NJoL9uazr8gSWSC8.TgVLLKeEy5fIpKgWnkJJhiXku4Pi2Irs9iiUwU5XLVG5APtfrbXy6A9JiGSJ.', 2, 'sdfsdfs', '2018-02-28 07:32:01'),
(112, 65, '$6$s8^3s;$bW0hPcdR8cis883fGLFdMbWLNW92bDFKobVelp0aj40CQPEer9iLdf0q4UUqBQRDn3EWjaQ7Q3E1JpGwFYd.V1', 2, 'sdfsdfs', '2018-02-28 08:08:13'),
(114, 67, '$6$s8^3s;$RduK3s3IHAAUyQalSYyxahqjPiSyeHMu0AnhKz4Gio2Na/fD4IImyzLBDaOceBg/CXgYFPHV2odyscsVWoxeM1', 1, 'UEAoddyR', '2018-03-01 09:54:19'),
(121, 70, '$6$s8^3s;$vjNqJd9L4VGUA7EV7fi77N4W5aY.RJ67fEqQvQZ.eqHRHxFVRpwUqs6jOoz96jioDPzTozKX3iW/zLjek0ri2/', 1, 'oCm2rNjO', '2018-03-06 09:42:56'),
(122, 6, '$6$s8^3s;$30TeWvPA7uIGko.PPLEJATE3y6bQX4E8k.sH450lDqsLFxx/PsJlGASIzFPUS0Phc/EHWINS2cjKdFE1uI.KL1', 1, 'x4Ilww5J', '2018-03-06 11:02:59'),
(126, 73, '$6$s8^3s;$Yl74DOKUQKxy8DPW0yH4mMku63STHKZ6j9ptz/UGP.oeqq8fl7cvXbwG6QQt/YuqAlXf8qsCw/30lIkbB8ktK0', 1, 'dIiGlf69MzU:APA91bGfp6a2STK3preTehCZhCb4B5q4b2qQPr4lLLqMDc_Ruq-mgU90yAU4Yg3tI0OG5OSpuG_TEWLtt7oglW5cIh0BUSDc2yZS8ClwsL14lwBsJTXVJdJmSTK6nCTyWdaifNHzVkls', '2018-03-13 10:50:40'),
(146, 78, '$6$s8^3s;$GzYIavDJqI29LfyeX2uSmqfOct.iXu.yqYr.Ei8YHogdIH9/F4m8YNYmWGMis1jUOGEvnHqZdC9orYuwEmtr3.', 2, '2c17dda46646691e', '2018-03-16 14:55:19'),
(158, 48, '$6$s8^3s;$6EWWToLNpEBgpglvkCgaShY94hzlKimDbPnoIOeCX1ED/JcFc4CfbbMzj6ZBK2WpQsyF/trxYqCutmnj67VA8/', 1, 'UJ1Ipmjr', '2018-03-17 23:15:36'),
(165, 77, '$6$s8^3s;$5FFnb4Vztb2CcU8B39wuTR3VGv/Z6os1BsN7qzmg2bh2l7BYoWjO79SYMBMTwvPPkx4aL.zy1UR6B/SmhSxui0', 1, 'K10dsgOP', '2018-03-21 05:57:37'),
(168, 71, '$6$s8^3s;$qiFbt1Ytiiw83jO4S7OCNcjA0hu7fUAv2kk6IxraAdIbX40lAipT9klKbj1d4SNsK3D9yE9yYtSWnD0DB3s2K/', 1, 'f-uP4A_zFLU:APA91bG092CHh6_uq8bveZX6ZU82ZZmEPMxK8SauYbzik1c4fR40ZyKmL4JhrzY_JAnGBLujBntaEE6riSYV6JTh9TCARLjPP3liVSEu-re4B6sEE_0325ksLhXq1bmRTsUJV8nVngAW', '2018-03-21 06:37:28'),
(174, 42, '$6$s8^3s;$nzy1Uw.g427kXok4aR58w72/U9Dhwvgaqvsq9Ed8YlkfGx/CgF.r9deMgJBAPAesHP4RiEhy5zdlT7L2Q5Aw8.', 1, '9QTamfCv', '2018-03-21 17:37:37'),
(180, 47, '$6$s8^3s;$XH38oV9hUzP97b7pu5gu8GhVMRvCN1mOvAGprQrQAZ2icac5mXk62LJqBawzjB2a9ZOUQLslpgkFtclOHMQN/1', 2, 'b08ad2c3f4127742', '2018-03-24 22:04:54'),
(181, 82, '$6$s8^3s;$aEAlb4yJwPMFCthtTCpgThb5qgSyipNQ0OUpYjR3NqTtHpu7oss6rcNovSCCmA8fLgf.F7lka5GyO2yyAia0m/', 2, 'c8247cd8b7c2c4e8', '2018-03-25 14:42:26'),
(182, 45, '$6$s8^3s;$nJr1yLZdhYX4.pTHQZktrufOwb/IGZ6b2hweMQgUPAPMAJpBWFv2l0r8Wy5622GWvpbB8695eRyI6dYuYOgW7/', 2, 'c8247cd8b7c2c4e8', '2018-03-25 14:49:35'),
(184, 84, '$6$s8^3s;$g2Six8YzUv19WdAOQWQlgGAO1aYEeIOZpWhBXTRkx4yBaizZ9uV0lqPLlReBUwP85orbdSkIgENtqHS5yS.k01', 2, '173e85cedc166e12', '2018-03-26 09:56:59'),
(187, 86, '$6$s8^3s;$u1MiamYeu6sLuum.1rcx9WMYkGNZxh44TCbcyZsi1PHafq/lsSk4Ec.J5mDzaGLfIZ/V6RddlXH2ZAnCrVVLw/', 2, '846fb3f49aa380c5', '2018-03-27 12:37:56');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `card_templates`
--
ALTER TABLE `card_templates`
  ADD PRIMARY KEY (`template_id`);

--
-- Indexes for table `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`company_id`);

--
-- Indexes for table `company_account_requests`
--
ALTER TABLE `company_account_requests`
  ADD PRIMARY KEY (`request_id`);

--
-- Indexes for table `company_admin`
--
ALTER TABLE `company_admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`countries_id`);

--
-- Indexes for table `countries_timezone`
--
ALTER TABLE `countries_timezone`
  ADD PRIMARY KEY (`zone_id`),
  ADD KEY `idx_country_code` (`country_code`),
  ADD KEY `idx_zone_name` (`zone_name`);

--
-- Indexes for table `designations`
--
ALTER TABLE `designations`
  ADD PRIMARY KEY (`designation_id`);

--
-- Indexes for table `my_cards`
--
ALTER TABLE `my_cards`
  ADD PRIMARY KEY (`card_id`);

--
-- Indexes for table `newschannel_subscribers`
--
ALTER TABLE `newschannel_subscribers`
  ADD PRIMARY KEY (`subscribe_id`);

--
-- Indexes for table `news_brief`
--
ALTER TABLE `news_brief`
  ADD PRIMARY KEY (`newsbrief_id`);

--
-- Indexes for table `news_channel`
--
ALTER TABLE `news_channel`
  ADD PRIMARY KEY (`channel_id`);

--
-- Indexes for table `news_deleted`
--
ALTER TABLE `news_deleted`
  ADD PRIMARY KEY (`delete_id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reset_password`
--
ALTER TABLE `reset_password`
  ADD PRIMARY KEY (`reset_id`);

--
-- Indexes for table `rolodex`
--
ALTER TABLE `rolodex`
  ADD PRIMARY KEY (`rolodex_id`);

--
-- Indexes for table `rolodexcard_notes`
--
ALTER TABLE `rolodexcard_notes`
  ADD PRIMARY KEY (`noteid`);

--
-- Indexes for table `rolodexcard_pictures`
--
ALTER TABLE `rolodexcard_pictures`
  ADD PRIMARY KEY (`picture_id`);

--
-- Indexes for table `rolodex_forwarded_cards`
--
ALTER TABLE `rolodex_forwarded_cards`
  ADD PRIMARY KEY (`forward_id`);

--
-- Indexes for table `security_answers`
--
ALTER TABLE `security_answers`
  ADD PRIMARY KEY (`answer_id`);

--
-- Indexes for table `security_questions`
--
ALTER TABLE `security_questions`
  ADD PRIMARY KEY (`question_id`);

--
-- Indexes for table `signup_allowed`
--
ALTER TABLE `signup_allowed`
  ADD PRIMARY KEY (`allowed_id`);

--
-- Indexes for table `teams`
--
ALTER TABLE `teams`
  ADD PRIMARY KEY (`team_id`);

--
-- Indexes for table `team_invites`
--
ALTER TABLE `team_invites`
  ADD PRIMARY KEY (`invite_id`);

--
-- Indexes for table `team_members`
--
ALTER TABLE `team_members`
  ADD PRIMARY KEY (`member_id`);

--
-- Indexes for table `todolist`
--
ALTER TABLE `todolist`
  ADD PRIMARY KEY (`todoid`);

--
-- Indexes for table `todo_reassign_action`
--
ALTER TABLE `todo_reassign_action`
  ADD PRIMARY KEY (`reassignaction_id`);

--
-- Indexes for table `ttalks`
--
ALTER TABLE `ttalks`
  ADD PRIMARY KEY (`ttalkid`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `users_authentication`
--
ALTER TABLE `users_authentication`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `card_templates`
--
ALTER TABLE `card_templates`
  MODIFY `template_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `companies`
--
ALTER TABLE `companies`
  MODIFY `company_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `company_account_requests`
--
ALTER TABLE `company_account_requests`
  MODIFY `request_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `company_admin`
--
ALTER TABLE `company_admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `countries_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=250;

--
-- AUTO_INCREMENT for table `countries_timezone`
--
ALTER TABLE `countries_timezone`
  MODIFY `zone_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=425;

--
-- AUTO_INCREMENT for table `designations`
--
ALTER TABLE `designations`
  MODIFY `designation_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `my_cards`
--
ALTER TABLE `my_cards`
  MODIFY `card_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=241;

--
-- AUTO_INCREMENT for table `newschannel_subscribers`
--
ALTER TABLE `newschannel_subscribers`
  MODIFY `subscribe_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=129;

--
-- AUTO_INCREMENT for table `news_brief`
--
ALTER TABLE `news_brief`
  MODIFY `newsbrief_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=203;

--
-- AUTO_INCREMENT for table `news_channel`
--
ALTER TABLE `news_channel`
  MODIFY `channel_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `news_deleted`
--
ALTER TABLE `news_deleted`
  MODIFY `delete_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `reset_password`
--
ALTER TABLE `reset_password`
  MODIFY `reset_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `rolodex`
--
ALTER TABLE `rolodex`
  MODIFY `rolodex_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=97;

--
-- AUTO_INCREMENT for table `rolodexcard_notes`
--
ALTER TABLE `rolodexcard_notes`
  MODIFY `noteid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `rolodexcard_pictures`
--
ALTER TABLE `rolodexcard_pictures`
  MODIFY `picture_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `rolodex_forwarded_cards`
--
ALTER TABLE `rolodex_forwarded_cards`
  MODIFY `forward_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `security_answers`
--
ALTER TABLE `security_answers`
  MODIFY `answer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=139;

--
-- AUTO_INCREMENT for table `security_questions`
--
ALTER TABLE `security_questions`
  MODIFY `question_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `signup_allowed`
--
ALTER TABLE `signup_allowed`
  MODIFY `allowed_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `teams`
--
ALTER TABLE `teams`
  MODIFY `team_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT for table `team_invites`
--
ALTER TABLE `team_invites`
  MODIFY `invite_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=180;

--
-- AUTO_INCREMENT for table `team_members`
--
ALTER TABLE `team_members`
  MODIFY `member_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=126;

--
-- AUTO_INCREMENT for table `todolist`
--
ALTER TABLE `todolist`
  MODIFY `todoid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `todo_reassign_action`
--
ALTER TABLE `todo_reassign_action`
  MODIFY `reassignaction_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `ttalks`
--
ALTER TABLE `ttalks`
  MODIFY `ttalkid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=242;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=87;

--
-- AUTO_INCREMENT for table `users_authentication`
--
ALTER TABLE `users_authentication`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=188;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
